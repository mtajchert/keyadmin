<?php $positionOptions = [
    'top' => 'Góra',
    'center' => 'Środek',
    'bottom' => 'Dół'
]; ?>
<div class="customization-container well">
    <?php echo $this->Form->hidden("CustomizationItem.{$customization['CustomizationItem']['id']}.id", [
        'value' => $customization['CustomizationItem']['id']
    ]); ?>
    <?php echo $this->Form->hidden("CustomizationItem.{$customization['CustomizationItem']['id']}.sort_order", [
        'value' => $customization['CustomizationItem']['sort_order'], 'class' => 'sort-order'
    ]); ?>
    <?php if ($customization['CustomizationItem']['type'] == 'youtube'): ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="well">
                    <?php echo $this->Form->input("CustomizationItem.{$customization['CustomizationItem']['id']}.youtube_url", [
                        'label' => 'Link do YT', 'id' => 'product-customization-youtube-url-'.$customization['CustomizationItem']['id'],
                        'value' => $customization['CustomizationItem']['youtube_url'],
                        'class' => 'form-control'
                    ]); ?>
                </div>
            </div>
        </div>
        <?php echo $this->element('customized_options', ['id' => $customization['CustomizationItem']['id']]); ?>    
        <div class="clear">&nbsp;</div>       
    <?php elseif ($customization['CustomizationItem']['type'] == 'text'): ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="well">
                    <?php echo $this->Form->input("CustomizationItem.{$customization['CustomizationItem']['id']}.text1", [
                        'label' => false, 'id' => 'product-customization-text1-'.$customization['CustomizationItem']['id'],
                        'value' => $customization['CustomizationItem']['text1']
                    ]); ?>
                </div>
            </div>
        </div>
        <?php echo $this->element('customized_options', ['id' => $customization['CustomizationItem']['id']]); ?>    
        <div class="clear">&nbsp;</div>
        <script type="text/javascript">
            $(document).ready(function () {
                CKEDITOR.replace('product-customization-text1-<?php echo $customization['CustomizationItem']['id']; ?>', {
                    filebrowserBrowseUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'browse')); ?>',
                    filebrowserUploadUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'upload')); ?>',
                    height: 200
                });
            });
        </script>
    <?php elseif ($customization['CustomizationItem']['type'] == 'text-text'): ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="well">
                    <?php echo $this->Form->input("CustomizationItem.{$customization['CustomizationItem']['id']}.position", [
                        'label' => false, 'id' => 'product-customization-position-'.$customization['CustomizationItem']['id'],
                        'value' => (isset($customization['CustomizationItem']['position']))? $customization['CustomizationItem']['position']: 'top-top', 'options' => $positionOptions,
                        'class' => 'form-control'
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
                <div class="well">
                    <?php echo $this->Form->input("CustomizationItem.{$customization['CustomizationItem']['id']}.text1", [
                        'label' => false, 'id' => 'product-customization-text1-'.$customization['CustomizationItem']['id'],
                        'value' => $customization['CustomizationItem']['text1']
                    ]); ?>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="well">
                    <?php echo $this->Form->input("CustomizationItem.{$customization['CustomizationItem']['id']}.text2", [
                        'label' => false, 'id' => 'product-customization-text2-'.$customization['CustomizationItem']['id'],
                        'value' => $customization['CustomizationItem']['text2']
                    ]); ?>
                </div>
            </div>
        </div>
        <?php echo $this->element('customized_options', ['id' => $customization['CustomizationItem']['id']]); ?>    
        <div class="clear">&nbsp;</div>
        <script type="text/javascript">
            $(document).ready(function () {
                CKEDITOR.replace('product-customization-text1-<?php echo $customization['CustomizationItem']['id']; ?>', {
                    filebrowserBrowseUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'browse')); ?>',
                    filebrowserUploadUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'upload')); ?>',
                    height: 200
                });
                CKEDITOR.replace('product-customization-text2-<?php echo $customization['CustomizationItem']['id']; ?>', {
                    filebrowserBrowseUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'browse')); ?>',
                    filebrowserUploadUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'upload')); ?>',
                    height: 200
                });
            });
        </script>
    
    <?php elseif ($customization['CustomizationItem']['type'] == 'image'): ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="well text-center">
                    <?= $this->element('customized_image', [
                        'itemId' => $customization['CustomizationItem']['id'],
                        'item' => $customization['CustomizationItem'],
                        'type' => 'image1'
                    ]); ?>           
                </div>
            </div>
        </div>
        <?php echo $this->element('customized_options', ['id' => $customization['CustomizationItem']['id']]); ?>    
        <div class="clear">&nbsp;</div>
        <script type="text/javascript">
            $(document).ready(function () {
                App.Customization.setUpload('#CustomizationItem<?= $customization['CustomizationItem']['id']; ?>Image1');
            });
        </script>
        
    <?php elseif ($customization['CustomizationItem']['type'] == 'image-image'): ?>      
        <div class="row">
            <div class="col-xs-12">
                <div class="well">
                    <?php echo $this->Form->input("CustomizationItem.{$customization['CustomizationItem']['id']}.position", [
                        'label' => false, 'id' => 'product-customization-position-'.$customization['CustomizationItem']['id'],
                        'value' => (isset($customization['CustomizationItem']['position']))? $customization['CustomizationItem']['position']: 'top-top', 'options' => $positionOptions,
                        'class' => 'form-control'
                    ]); ?>
                </div>
            </div>
        </div>  
        <div class="row">
            <div class="col-xs-6">
                <div class="well text-center">
                    <?= $this->element('customized_image', [
                        'itemId' => $customization['CustomizationItem']['id'],
                        'item' => $customization['CustomizationItem'],
                        'type' => 'image1'
                    ]); ?>                     
                </div>
            </div>
            <div class="col-xs-6">
                <div class="well text-center">
                    <?= $this->element('customized_image', [
                        'itemId' => $customization['CustomizationItem']['id'],
                        'item' => $customization['CustomizationItem'],
                        'type' => 'image2'
                    ]); ?> 
                </div>
            </div>
        </div>
        <?php echo $this->element('customized_options', ['id' => $customization['CustomizationItem']['id']]); ?>    
        <div class="clear">&nbsp;</div>
        
    <?php elseif ($customization['CustomizationItem']['type'] == 'text-image'): ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="well">
                    <?php echo $this->Form->input("CustomizationItem.{$customization['CustomizationItem']['id']}.position", [
                        'label' => false, 'id' => 'product-customization-position-'.$customization['CustomizationItem']['id'],
                        'value' => (isset($customization['CustomizationItem']['position']))? $customization['CustomizationItem']['position']: 'top-top', 'options' => $positionOptions,
                        'class' => 'form-control'
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
                <div class="well">
                    <?php echo $this->Form->input("CustomizationItem.{$customization['CustomizationItem']['id']}.text1", [
                        'label' => false, 'id' => 'product-customization-text1-'.$customization['CustomizationItem']['id'],
                        'value' => $customization['CustomizationItem']['text1']
                    ]); ?>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="well text-center">
                    <?= $this->element('customized_image', [
                        'itemId' => $customization['CustomizationItem']['id'],
                        'item' => $customization['CustomizationItem'],
                        'type' => 'image1'
                    ]); ?> 
                </div>
            </div>
        </div>
        <?php echo $this->element('customized_options', ['id' => $customization['CustomizationItem']['id']]); ?>    
        <div class="clear">&nbsp;</div>
        <script type="text/javascript">
            $(document).ready(function () {
                CKEDITOR.replace('product-customization-text1-<?php echo $customization['CustomizationItem']['id']; ?>', {
                    filebrowserBrowseUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'browse')); ?>',
                    filebrowserUploadUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'upload')); ?>',
                    height: 200
                });
            });
        </script>
    <?php elseif ($customization['CustomizationItem']['type'] == 'image-text'): ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="well">
                    <?php echo $this->Form->input("CustomizationItem.{$customization['CustomizationItem']['id']}.position", [
                        'label' => false, 'id' => 'product-customization-position-'.$customization['CustomizationItem']['id'],
                        'value' => (isset($customization['CustomizationItem']['position']))? $customization['CustomizationItem']['position']: 'top-top', 'options' => $positionOptions,
                        'class' => 'form-control'
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
                <div class="well text-center">
                    <?= $this->element('customized_image', [
                        'itemId' => $customization['CustomizationItem']['id'],
                        'item' => $customization['CustomizationItem'],
                        'type' => 'image1'
                    ]); ?> 
                </div>
            </div>
            <div class="col-xs-6">
                <div class="well">
                    <?php echo $this->Form->input("CustomizationItem.{$customization['CustomizationItem']['id']}.text1", [
                        'label' => false, 'id' => 'product-customization-text1-'.$customization['CustomizationItem']['id'],
                        'value' => $customization['CustomizationItem']['text1']
                    ]); ?>
                </div>
            </div>
        </div>
        <?php echo $this->element('customized_options', ['id' => $customization['CustomizationItem']['id']]); ?>    
        <div class="clear">&nbsp;</div>
        <script type="text/javascript">
            $(document).ready(function () {
                CKEDITOR.replace('product-customization-text1-<?php echo $customization['CustomizationItem']['id']; ?>', {
                    filebrowserBrowseUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'browse')); ?>',
                    filebrowserUploadUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'upload')); ?>',
                    height: 200
                });
            });
        </script>
    <?php endif; ?>
</div>
