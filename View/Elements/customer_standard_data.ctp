<?php if ($user['guest_account']): ?>
<span class="icon-ghost" style="color: red;font-size: 18px;"></span>
<?php endif; ?>
<?php if (!empty($user['company'])): ?>
  <strong><?php echo h($user['company']); ?></strong>
<?php else: ?>
	<strong><?php echo h($user['first_name'].' '.$user['last_name']); ?>&nbsp;</strong>
<?php endif; ?>
<?php if (!empty($user['email'])): ?>
  <br/><strong>Email:</strong> <?php echo h($user['email']); ?>
<?php endif; ?>
<?php if (!empty($user['contact_phone'])): ?>
  <br/><strong>Tel:</strong> <?php echo h($user['contact_phone']); ?>
<?php endif; ?>