<div class="btn-group">
  <?php echo $this->Html->link('<span class="btn-label"><em class="icon-plus"></em></span>'.__('Dodaj promocję'), array('controller' => 'KeyPromotions', 'action' => 'create'), array('escape' => false, 'title' => __('Dodaj promocję'), 'class' => 'btn btn-labeled btn-default')); ?>
</div>
<div class="btn-group">
  <?php echo $this->Html->link('<span class="btn-label"><em class="icon-calculator"></em></span>'.__('Zarządzaj promocjami'), array('controller' => 'KeyPromotions', 'action' => 'manage'), array('escape' => false, 'title' => __('Zarządzaj promocjami'), 'class' => 'btn btn-labeled btn-default')); ?>
</div>