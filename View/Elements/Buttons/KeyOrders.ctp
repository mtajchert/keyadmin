<div id="archive-btn" class="btn-group" style="display:none;">
  <?php echo $this->Html->link('<span class="btn-label"><em class="glyphicon glyphicon-folder-close"></em></span>'.__('Archiwizuj'), 'javascript:void(0);', array('escape' => false, 'title' => __('Archiwizuj'), 'class' => 'btn btn-labeled btn-default', 'onclick' => 'App.Orders.setArchive();')); ?>
</div>

<?php if (isset($this->data['Order']) && !empty($this->data['Order'])): ?>
  <div class="btn-group">
    <?php echo $this->Html->link('<span class="btn-label"><em class="icon-reload"></em></span>'.__('Wyczyść filtry'), '/admin/orders/index/', array('escape' => false, 'title' => __('Generuj raport'), 'class' => 'btn btn-labeled btn-warning')); ?>
  </div>
<?php endif; ?>

<div class="btn-group">
  <?php echo $this->Html->link('<span class="btn-label"><em class="icon-magnifier"></em></span>'.__('Filtruj listę'), 
      'javascript:void(0);', 
      array('escape' => false, 'title' => __('Generuj raport'), 
        'class' => 'btn btn-labeled btn-default', 'onclick' => 'App.Orders.openFilterBox();'        
  )); ?>
</div>
<div class="btn-group">
  <?php echo $this->Html->link('<span class="btn-label"><em class="icon-graph"></em></span>'.__('Generuj raport'), 'javascript:void(0);', array('escape' => false, 'title' => __('Generuj raport'), 'class' => 'btn btn-labeled btn-default', 'onclick' => 'App.Orders.openOrdersReportBox();')); ?>
</div>
<div class="btn-group">
  <?php echo $this->Html->link('<span class="btn-label"><em class="icon-plus"></em></span>'.__('Dodaj zamówienie'), array('controller' => 'KeyOrders', 'action' => 'create'), array('escape' => false, 'title' => __('Dodaj zamówienie'), 'class' => 'btn btn-labeled btn-default')); ?>
</div>
