<?php /*
<div class="btn-group">
  <?php echo $this->Html->link('<span class="btn-label"><em class="icon-plus"></em></span>'.__('Dodaj fakturę VAT'), array('controller' => 'KeyInvoices', 'action' => 'create_invoice'), array('escape' => false, 'title' => __('Dodaj fakturę VAT'), 'class' => 'btn btn-labeled btn-default')); ?>
</div>
<div class="btn-group">
  <?php echo $this->Html->link('<span class="btn-label"><em class="icon-plus"></em></span>'.__('Dodaj fakturę Proforma'), array('controller' => 'KeyInvoices', 'action' => 'create_quote'), array('escape' => false, 'title' => __('Dodaj fakturę Proforma'), 'class' => 'btn btn-labeled btn-default')); ?>
</div> */ ?>
<div class="btn-group">
  <?php echo $this->Html->link('<span class="btn-label"><em class="icon-graph"></em></span>'.__('Generuj zestawienie'), 'javascript:void(0);', 
      array('escape' => false, 'title' => __('Generuj zestawienie'), 'class' => 'btn btn-labeled btn-default', 'onclick' => 'App.Invoices.openReportBox();')); ?>
</div>