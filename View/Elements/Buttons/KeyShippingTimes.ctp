<div class="btn-group">
  <?php echo $this->Html->link('<span class="btn-label"><em class="icon-plus"></em></span>'.__('Dodaj termin wysyłki'), array('controller' => 'KeyShippingTimes', 'action' => 'create'), array('escape' => false, 'title' => __('Dodaj termin wysyłki'), 'class' => 'btn btn-labeled btn-default')); ?>
</div>