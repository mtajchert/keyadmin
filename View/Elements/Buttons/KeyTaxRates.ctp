<div class="btn-group">
  <?php echo $this->Html->link('<span class="btn-label"><em class="icon-plus"></em></span>'.__('Dodaj stawkę VAT'), array('controller' => 'KeyTaxRates', 'action' => 'create'), array('escape' => false, 'title' => __('Dodaj stawkę VAT'), 'class' => 'btn btn-labeled btn-default')); ?>
</div>