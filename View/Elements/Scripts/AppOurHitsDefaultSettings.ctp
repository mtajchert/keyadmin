App.OurHits.lang = {
  confirmDeleteMessage: '<?php echo __('Czy na pewno chcesz usunąć nasz hit "<%- name %>"?'); ?>',
  noChoosedProduct: '<?php echo __('Wybierz produkt do naszego hitu'); ?>',
  saveOurHitStatusErrorMsg: '<?php echo __('Wystąpił błąd podczas zapisu statusu'); ?>',
  saveProductStatusErrorMsg: '<?php echo __('Wystąpił błąd podczas zapisu statusu'); ?>',
  confirmDeleteOurHits: '<?php echo __('Czy na pewno chcesz usunąć nasz hit z zaznaczonych produktów?'); ?>',
  confirmDeleteProducts: '<?php echo __('Czy na pewno chcesz usunąć wybrane produkty?'); ?>',
  confirmActionTitle: '<?php echo __('Potwierdź wykonanie akcji'); ?>',
  confirmDeactivateOurHits: '<?php echo __('Czy na pewno chcesz zmienić status wybranych hitów na nieaktywny?'); ?>',
  confirmActivateOurHits: '<?php echo __('Czy na pewno chcesz zmienić status wybranych hitów na aktywny?'); ?>',
  confirmDeactivateProducts: '<?php echo __('Czy na pewno chcesz zmienić status wybranych produktów na nieaktywny?'); ?>',
  confirmActivateProducts: '<?php echo __('Czy na pewno chcesz zmienić status wybranych produktów na aktywny?'); ?>',
  confirmClearOurHitsDate: '<?php echo __('Czy na pewno wyzerować datę rozpoczęcia wybranych hitów?'); ?>',
  confirmClearOurHitsDateEnd: '<?php echo __('Czy na pewno wyzerować datę zakończenia wybranych hitów?'); ?>',
  confirmManipulateOurHitsDate: '<?php echo __('Wprowadź ilość dni o jaką zmniejszyć/zwiększyć datę rozpoczęcia:'); ?>',
  confirmManipulateOurHitsDateEnd: '<?php echo __('Wprowadź ilość dni o jaką zmniejszyć/zwiększyć datę zakończenia:'); ?>',
  manipulateDateInputPlaceholder: '<?php echo __('Wpisz ilość dni'); ?>'
};

App.OurHits.getProductsTableUrl = '<?php echo Router::url(array('controller' => 'KeyOurHits', 'action' => 'get_products_table')); ?>';
App.OurHits.getProductDataUrl = '<?php echo Router::url(array('controller' => 'KeyOurHits', 'action' => 'get_product_data')); ?>';
App.OurHits.saveOurHitStatusUrl = '<?php echo Router::url(array('controller' => 'KeyOurHits', 'action' => 'save_our_hit_status')); ?>';
App.OurHits.saveProductStatusUrl = '<?php echo Router::url(array('controller' => 'KeyOurHits', 'action' => 'save_product_status')); ?>';