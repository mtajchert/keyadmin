App.Units.lang = {
  confirmDeleteMessage: '<?php echo __('Czy na pewno chcesz usunąć jednostkę miary "<%- name %>"?'); ?>',
  saveSortOrderErrorMsg: '<?php echo __('Wystąpił błąd podczas zapisu kolejności sortowania.'); ?>',
  sortOrderInvalidValue: '<?php echo __('Wpisana wartość jest nieprawidłowa'); ?>'
};

App.Units.saveSortOrderUrl = '<?php echo Router::url(array('controller' => 'KeyUnits', 'action' => 'saveUnitSortOrder')); ?>';