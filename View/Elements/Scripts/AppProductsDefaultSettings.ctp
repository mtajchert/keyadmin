App.Products.lang = {
  confirmDeleteMessage: '<?php echo __('Czy na pewno chcesz usunąć produkt "<%- name %>"?'); ?>',
  saveSortOrderErrorMsg: '<?php echo __('Wystąpił błąd podczas zapisu kolejności sortowania.'); ?>',
  sortOrderInvalidValue: '<?php echo __('Wpisana wartość jest nieprawidłowa'); ?>',
  saveStatusErrorMsg: '<?php echo __('Wystąpił błąd podczas zapisu statusu.'); ?>',
  savePriceErrorMsg: '<?php echo __('Wystąpił błąd podczas zapisu ceny.'); ?>',
  priceInvalidValue: '<?php echo __('Wpisana cena jest nieprawidłowa'); ?>',
  saveFeatureErrorMsg: '<?php echo __('Wystąpił błąd podczas zapisu.'); ?>'
};


App.Products.saveSortOrderUrl = '<?php echo Router::url(array('controller' => 'KeyProducts', 'action' => 'saveProductSortOrder')); ?>';
App.Products.saveStatusUrl = '<?php echo Router::url(array('controller' => 'KeyProducts', 'action' => 'saveProductStatus')); ?>';
App.Products.savePriceUrl = '<?php echo Router::url(array('controller' => 'KeyProducts', 'action' => 'saveProductPrice')); ?>';
App.Products.saveFeatureStatusUrl = '<?php echo Router::url(array('controller' => 'KeyProducts', 'action' => 'saveProductFeatureStatus')); ?>';
App.Products.getCategoryNestedNameUrl = '<?php echo Router::url(array('controller' => 'KeyProducts', 'action' => 'getCategoryNestedName')); ?>';
App.Products.getProductOptionRow = '<?php echo Router::url(array('controller' => 'KeyProducts', 'action' => 'get_product_option_row')); ?>';
App.Products.getProductOptionValues = '<?php echo Router::url(array('controller' => 'KeyProducts', 'action' => 'get_product_option_values')); ?>';
App.Products.getProductOptions = '<?php echo Router::url(array('controller' => 'KeyProducts', 'action' => 'get_product_options')); ?>';