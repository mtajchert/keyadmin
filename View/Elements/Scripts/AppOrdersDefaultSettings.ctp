<script>
  $(document).ready(function () {
  App.Orders.lang = {
    confirmDeleteMessage: '<?php echo __('Czy na pewno chcesz usunąć zamówienie "<%- number %>"?'); ?>',
    noChoosedCustomer: '<?php echo __('Wybierz klienta'); ?>',
    noChoosedProduct: '<?php echo __('Wybierz produkt do promocji'); ?>',
    chooseAddress: '<?php echo __('Wybierz adres'); ?>',
    chooseShipping: '<?php echo __('Wybierz formę dostawy'); ?>',
    choosePayment: '<?php echo __('Wybierz formę płatności'); ?>',
    currency: '<?php echo __('zł'); ?>',
    noChoosedProductsProductsOption: '<?php echo __('Wybierz cechy produktu'); ?>',
  };

  App.Orders.getCustomersTableUrl = '<?php echo Router::url(array('controller' => 'KeyOrders', 'action' => 'get_customers_table')); ?>';
  App.Orders.getCustomerInfoUrl = '<?php echo Router::url(array('controller' => 'KeyOrders', 'action' => 'get_customer_info')); ?>';
  
  App.Orders.getProductsTableUrl = '<?php echo Router::url(array('controller' => 'KeyProducts', 'action' => 'get_products_table')); ?>';
  App.Orders.getProductRowUrl = '<?php echo Router::url(array('controller' => 'KeyOrders', 'action' => 'get_product_row')); ?>';
  App.Orders.getProductPreviewUrl = '<?php echo Router::url(array('controller' => 'KeyOrders', 'action' => 'get_order_product_preview')); ?>';
  App.Orders.productEditUrl = '<?php echo Router::url(array('controller' => 'KeyProducts', 'action' => 'edit')); ?>';
  App.Orders.getOrderUserAddressDataUrl = '<?php echo Router::url(array('controller' => 'KeyOrders', 'action' => 'get_order_user_address_data')); ?>';
  App.Orders.getOrderUserAddressesListUrl = '<?php echo Router::url(array('controller' => 'KeyOrders', 'action' => 'get_order_user_addresses_list')); ?>';
  App.Orders.addUserAddressesUrl = '<?php echo Router::url(array('controller' => 'KeyCustomers', 'action' => 'edit')); ?>';
  App.Orders.getShippingsListUrl = '<?php echo Router::url(array('controller' => 'KeyOrders', 'action' => 'get_shippings_list')); ?>';
  App.Orders.getPaymentsListUrl = '<?php echo Router::url(array('controller' => 'KeyOrders', 'action' => 'get_payments_list')); ?>';
  App.Orders.getProductsProductsOptionFormUrl = '<?php echo Router::url(array('controller' => 'KeyOrders', 'action' => 'get_products_products_option_form')); ?>';
  App.Orders.generateOrdersReportUrl = '<?php echo Router::url(array('controller' => 'KeyOrders', 'action' => 'generate_orders_report')); ?>';
  
  /*App.Promotions.getProductDataUrl = '<?php echo Router::url(array('controller' => 'KeyOrders', 'action' => 'get_product_data')); ?>';
  App.Promotions.savePromotionStatusUrl = '<?php echo Router::url(array('controller' => 'KeyOrders', 'action' => 'save_promotion_status')); ?>';
  App.Promotions.saveProductStatusUrl = '<?php echo Router::url(array('controller' => 'KeyOrders', 'action' => 'save_product_status')); ?>';*/
  });
</script>