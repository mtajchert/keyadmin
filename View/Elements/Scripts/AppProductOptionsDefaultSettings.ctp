<script>
  $(document).ready(function () {
    App.ProductOptions.lang = {
      confirmDeleteMessage: '<?php echo __('Czy na pewno chcesz usunąć cechę produktów "<%- name %>" wraz z wartościami?'); ?>',
      confirmValueDeleteMessage: '<?php echo __('Czy na pewno chcesz usunąć wartość "<%- name %>"?'); ?>',
      saveSortOrderErrorMsg: '<?php echo __('Wystąpił błąd podczas zapisu kolejności sortowania.'); ?>',
      sortOrderInvalidValue: '<?php echo __('Wpisana wartość jest nieprawidłowa'); ?>'
    };

    App.ProductOptions.saveSortOrderUrl = '<?php echo Router::url(array('controller' => 'KeyProductOptions', 'action' => 'saveProductOptionSortOrder')); ?>';
    App.ProductOptions.getValuesUrl = '<?php echo Router::url(array('controller' => 'KeyProductOptions', 'action' => 'getProductOptionValuesList')); ?>';
  });
</script>
