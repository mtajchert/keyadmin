App.Payments.lang = {
  saveSortOrderErrorMsg: '<?php echo __('Wystąpił błąd podczas zapisu kolejności sortowania.'); ?>',
  sortOrderInvalidValue: '<?php echo __('Wpisana wartość jest nieprawidłowa'); ?>',
  saveStatusErrorMsg: '<?php echo __('Wystąpił błąd podczas zapisu statusu.'); ?>'
};

App.Payments.saveSortOrderUrl = '<?php echo Router::url(array('controller' => 'KeyPayments', 'action' => 'savePaymentSortOrder')); ?>';
App.Payments.saveStatusUrl = '<?php echo Router::url(array('controller' => 'KeyPayments', 'action' => 'savePaymentStatus')); ?>';