App.Categories.lang = {
  confirmDeleteMsg: '<?php echo __('Czy na pewno chcesz usunąć kategorię "<%- name %>"?'); ?>',
  saveSortOrderErrorMsg: '<?php echo __('Wystąpił błąd podczas zapisu kolejności sortowania.'); ?>',
  sortOrderInvalidValue: '<?php echo __('Wpisana wartość jest nieprawidłowa'); ?>',
  saveStatusErrorMsg: '<?php echo __('Wystąpił błąd podczas zapisu statusu.'); ?>',
  getCategoryPathErrorMsg: '<?php echo __('Wystąpił błąd podczas pobierania ścieżki kategorii'); ?>',
  loadingCategoriesMsg: '<?php echo __('Pobieranie kategorii...'); ?>',
  noChoosedProduct: '<?php echo __('Wybierz produkt do przekierowania banera'); ?>',
};

App.Categories.getCategoryBreadcrumbPathUrl = '<?php echo Router::url(array('controller' => 'KeyCategories', 'action' => 'getCategoryBreadcrumbPath')); ?>';
App.Categories.saveSortOrderUrl = '<?php echo Router::url(array('controller' => 'KeyCategories', 'action' => 'saveCategorySortOrder')); ?>';
App.Categories.saveStatusUrl = '<?php echo Router::url(array('controller' => 'KeyCategories', 'action' => 'saveCategoryStatus')); ?>';
App.Categories.getProductsTableUrl = '<?php echo Router::url(array('controller' => 'KeyProducts', 'action' => 'get_products_table')); ?>';
App.Categories.getProductDataUrl = '<?php echo Router::url(array('controller' => 'KeyProducts', 'action' => 'get_product_data')); ?>';