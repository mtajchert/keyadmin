App.CategoriesTree.lang = {
  noCategoriesMsg: '<?php echo __('Brak kategorii'); ?>',
  getCategoriesErrorMsg: '<?php echo __('Wystąpił błąd podczas pobierania kategorii'); ?>',
  loadingCategoriesMsg: '<?php echo __('Pobieranie kategorii...'); ?>'
};

App.CategoriesTree.chooseMode = false;
App.CategoriesTree.withRoot = false;
App.CategoriesTree.dataLoadUrl = '<?php echo Router::url(array('controller' => 'KeyCategories', 'action' => 'getCategoriesTreeData')); ?>';