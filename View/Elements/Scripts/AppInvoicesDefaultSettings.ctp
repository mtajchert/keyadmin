<script>
  $(document).ready(function () {
    App.Invoices.lang = {
      confirmDeleteInvoiceMessage: '<?php echo __('Czy na pewno chcesz usunąć fakturę VAT "<%- number %>"?'); ?>',
      confirmDeleteQuoteMessage: '<?php echo __('Czy na pewno chcesz usunąć fakturę Proforma "<%- number %>"?'); ?>',
      noChoosedCustomer: '<?php echo __('Wybierz klienta'); ?>',
      noChoosedProduct: '<?php echo __('Wybierz produkt do promocji'); ?>',
      chooseAddress: '<?php echo __('Wybierz adres'); ?>',
      chooseShipping: '<?php echo __('Wybierz formę dostawy'); ?>',
      choosePayment: '<?php echo __('Wybierz formę płatności'); ?>',
      currency: '<?php echo __('zł'); ?>',
      noChoosedProductsProductsOption: '<?php echo __('Wybierz cechy produktu'); ?>',
    };

    App.Invoices.getCustomersTableUrl = '<?php echo Router::url(array('controller' => 'KeyInvoices', 'action' => 'get_customers_table')); ?>';
    App.Invoices.getCustomerInfoUrl = '<?php echo Router::url(array('controller' => 'KeyInvoices', 'action' => 'get_customer_info')); ?>';

    App.Invoices.getProductsTableUrl = '<?php echo Router::url(array('controller' => 'KeyProducts', 'action' => 'get_products_table')); ?>';
    App.Invoices.getProductRowUrl = '<?php echo Router::url(array('controller' => 'KeyInvoices', 'action' => 'get_product_row')); ?>';
    App.Invoices.getProductPreviewUrl = '<?php echo Router::url(array('controller' => 'KeyInvoices', 'action' => 'get_order_product_preview')); ?>';
    App.Invoices.productEditUrl = '<?php echo Router::url(array('controller' => 'KeyProducts', 'action' => 'edit', 1)); ?>';
    App.Invoices.getOrderUserAddressDataUrl = '<?php echo Router::url(array('controller' => 'KeyInvoices', 'action' => 'get_order_user_address_data')); ?>';
    App.Invoices.getOrderUserAddressesListUrl = '<?php echo Router::url(array('controller' => 'KeyInvoices', 'action' => 'get_order_user_addresses_list')); ?>';
    App.Invoices.addUserAddressesUrl = '<?php echo Router::url(array('controller' => 'KeyCustomers', 'action' => 'edit')); ?>';
    App.Invoices.getShippingsListUrl = '<?php echo Router::url(array('controller' => 'KeyInvoices', 'action' => 'get_shippings_list')); ?>';
    App.Invoices.getPaymentsListUrl = '<?php echo Router::url(array('controller' => 'KeyInvoices', 'action' => 'get_payments_list')); ?>';
    App.Invoices.getProductsProductsOptionFormUrl = '<?php echo Router::url(array('controller' => 'KeyInvoices', 'action' => 'get_products_products_option_form')); ?>';
  });
</script>