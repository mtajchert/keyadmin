<script>
  $(document).ready(function () {
    App.Carts.lang = {
      confirmDeleteMessage: '<?php echo __('Czy na pewno chcesz usunąć koszyk "<%- id %>"?'); ?>'
    };
    App.Carts.getProductPreviewUrl = '<?php echo Router::url(array('controller' => 'KeyCarts', 'action' => 'get_cart_product_preview')); ?>';
    App.Carts.productEditUrl = '<?php echo Router::url(array('controller' => 'KeyProducts', 'action' => 'edit')); ?>';
  });
</script>
