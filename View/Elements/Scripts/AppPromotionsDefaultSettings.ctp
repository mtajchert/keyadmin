App.Promotions.lang = {
  confirmDeleteTitle: '<?php echo __('Potwierdź usunięcie'); ?>',
  confirmDeleteMessage: '<?php echo __('Czy po usunięciu promocji dla produktu "<%= name %>" przywrócić starą cenę produktu?'); ?>',
  noChoosedProduct: '<?php echo __('Wybierz produkt do promocji'); ?>',
  savePromotionStatusErrorMsg: '<?php echo __('Wystąpił błąd podczas zapisu statusu'); ?>',
  saveProductStatusErrorMsg: '<?php echo __('Wystąpił błąd podczas zapisu statusu'); ?>',
  confirmDeletePromotionsNotRestorePrice: '<?php echo __('Czy na pewno chcesz usunąć zaznaczone promocje pozostawiając obecne ceny produktów?'); ?>',
  confirmDeletePromotionsAndRestorePrice: '<?php echo __('Czy na pewno chcesz usunąć zaznaczone promocje oraz przywrócić poprzednie ceny produktów?'); ?>',
  confirmActionTitle: '<?php echo __('Potwierdź wykonanie akcji'); ?>',
  confirmDeactivatePromotions: '<?php echo __('Czy na pewno chcesz zmienić status wybranych promocji na nieaktywne?'); ?>',
  confirmActivatePromotions: '<?php echo __('Czy na pewno chcesz zmienić status wybranych promocji na aktywne?'); ?>',
  confirmDeactivateProducts: '<?php echo __('Czy na pewno chcesz zmienić status wybranych produktów na nieaktywne?'); ?>',
  confirmActivateProducts: '<?php echo __('Czy na pewno chcesz zmienić status wybranych produktów na aktywne?'); ?>',
  confirmDeleteProducts: '<?php echo __('Czy na pewno chcesz usunąć wybrane produkty?'); ?>',
  confirmClearPromotionsDate: '<?php echo __('Czy na pewno wyzerować datę rozpoczęcia wybranych promocji?'); ?>',
  confirmClearPromotionsDateEnd: '<?php echo __('Czy na pewno wyzerować datę zakończenia wybranych promocji?'); ?>',
  confirmManipulatePromotionsDate: '<?php echo __('Wprowadź ilość dni o jaką zmniejszyć/zwiększyć datę rozpoczęcia promocji:'); ?>',
  confirmManipulatePromotionsDateEnd: '<?php echo __('Wprowadź ilość dni o jaką zmniejszyć/zwiększyć datę zakończenia promocji:'); ?>',
  manipulateDateInputPlaceholder: '<?php echo __('Wpisz ilość dni'); ?>',
  confirmManipulateOldPriceAmount: '<?php echo __('Wprowadź wartość o jaką zmniejszyć/zwiększyć poprzednią cenę produktu:'); ?>',
  manipulatePriceAmountInputPlaceholder: '<?php echo __('Wpisz wartość'); ?>',
  confirmManipulateOldPricePercent: '<?php echo __('Wprowadź procent o jaki zmniejszyć/zwiększyć poprzednią cenę produktu:'); ?>',
  manipulatePricePercentInputPlaceholder: '<?php echo __('Wpisz procent'); ?>',
};

App.Promotions.getProductsTableUrl = '<?php echo Router::url(array('controller' => 'KeyProducts', 'action' => 'get_products_table')); ?>';
App.Promotions.getProductDataUrl = '<?php echo Router::url(array('controller' => 'KeyProducts', 'action' => 'get_product_data')); ?>';
App.Promotions.savePromotionStatusUrl = '<?php echo Router::url(array('controller' => 'KeyPromotions', 'action' => 'save_promotion_status')); ?>';
App.Promotions.saveProductStatusUrl = '<?php echo Router::url(array('controller' => 'KeyPromotions', 'action' => 'save_product_status')); ?>';