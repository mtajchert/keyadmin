App.Customers.lang = {
  confirmDeleteMessage: '<?php echo __('Czy na pewno chcesz usunąć klienta "<%- name %>"?'); ?>',
  saveStatusErrorMsg: '<?php echo __('Wystąpił błąd podczas zapisu statusu.'); ?>',
  loadZonesErrorMsg: '<?php echo __('Wystąpił błąd podczas pobierania województw.'); ?>',
  confirmDeleteTitle: '<?php echo __('Potwierdź usunięcie'); ?>',
  confirmDeleteAddressMsg: '<?php echo __('Czy na pewno chcesz usunąć adres "<%- alias %>"?'); ?>',
  delete: '<?php echo __('Usuń'); ?>',
  cancel: '<?php echo __('Anuluj'); ?>',
  defaultBillingAddressLabel: '<?php echo __('Domyślny adres rozliczeniowy'); ?>',
  defaultShippingAddressLabel: '<?php echo __('Domyślny adres dostawy'); ?>'
};

App.Customers.saveStatusUrl = '<?php echo Router::url(array('controller' => 'KeyCustomers', 'action' => 'save_customer_status')); ?>';
App.Customers.getZonesUrl = '<?php echo Router::url(array('controller' => 'KeyCustomers', 'action' => 'get_zones')); ?>';
App.Customers.getUserAddressRowUrl = '<?php echo Router::url(array('controller' => 'KeyCustomers', 'action' => 'get_user_address_row')); ?>';