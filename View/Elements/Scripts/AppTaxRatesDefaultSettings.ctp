App.TaxRates.lang = {
  confirmDeleteMessage: '<?php echo __('Czy na pewno chcesz usunąć stawkę VAT "<%- name %>"?'); ?>',
  saveSortOrderErrorMsg: '<?php echo __('Wystąpił błąd podczas zapisu kolejności sortowania.'); ?>',
  sortOrderInvalidValue: '<?php echo __('Wpisana wartość jest nieprawidłowa'); ?>'
};

App.TaxRates.saveSortOrderUrl = '<?php echo Router::url(array('controller' => 'KeyTaxRates', 'action' => 'saveTaxRateSortOrder')); ?>';