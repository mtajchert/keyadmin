<div class="form-group">
    <?php echo $this->Form->create('CustomizationItem', array('type' => 'file')); ?>
    <div class="form-image <?php echo (!empty($item[$type]))? 'hide': ''; ?>">
        <i class="fa fa-picture-o" aria-hidden="true" style="font-size: 40px;padding: 20px 0px;" onclick="$('#CustomizationItem<?= $itemId; ?><?= ucfirst($type); ?>').click();"></i>
        <?php echo $this->Form->input("CustomizationItem.{$itemId}.{$type}", array(
            'label' => 'Wybierz plik', 'type' => 'file', 
            'class' => 'hide', 'label' => false, 'name' => $type,
            'data-id' => $itemId                                
        )); ?>
        <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">0%</div>
        </div>
    </div>    
    <?php echo $this->Form->end(); ; ?>
    
    <div class="view-image <?php echo (empty($item[$type]))? 'hide': ''; ?>">                        
        <img src="/img/description/<?php echo $item[$type]; ?>" class="img-responsive center-block" />
        <?php echo $this->Html->link('Usuń', array(
            'admin' => true,
            'controller' => 'KeyCustomizations',
            'action' => 'delete_image',
            $type, 
            $itemId
        ), array('class' => 'center-block btn btn-danger btn-delate-template-image')); ?>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        App.Customization.setUpload('#CustomizationItem<?= $itemId; ?><?= ucfirst($type); ?>');
    });
</script>