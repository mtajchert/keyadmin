<form id="categories-image-form" accept-charset="utf-8" method="post" enctype="multipart/form-data" action="/admin/categories/upload_image">
  <input type="hidden" name="data[Category][id]" value="<%= categoryId %>" />
  <input type="file" name="data[Category][image]" class="image-choosen" data-show-upload="false" data-show-caption="true"/>
<?php echo $this->Form->end(); ?>