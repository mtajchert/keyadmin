<div class="col-xs-12">
  <div role="alert" class="alert alert-success" id="flash_success">
    <?php echo $message; ?>
  </div>
</div>
<script>
  setTimeout(function () {
    $('#flash_success').fadeOut(600);
  }, 3000);
</script>