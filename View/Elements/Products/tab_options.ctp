<div role="tabpanel" class="tab-pane" id="tab-options">
	<div class="row">      
      <div class="col-sm-3">
        <?php echo $this->Form->input('new_product_option', array('label' => __('Cecha produktu:'), 'empty' => __('Wybierz cechę'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'options' => $productOptions, 'default' => $defaultProductOptionId, 'onchange' => 'App.Products.changeProductOption(this);')); ?>
      </div>
		<div class="col-sm-3">
        <?php echo $this->Form->input('new_product_option_value', array('label' => __('Wartość cechy:'), 'empty' => __('Wybierz wartość'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'options' => $productOptionValues)); ?>
      </div>
		<div class="col-sm-3" style="padding-top: 35px;">
			<a href="javascript:void(0);" class="option-link"
				onclick="App.Products.addProductOption();"> <em
				class="icon-plus"></em>
          <?php echo __('Dodaj pozycję'); ?>
        </a>
		</div>
	</div>
	<div class="row">&nbsp;</div>
	<div class="row">
		<table class="table products-options-table table-bordered">
			<thead>
				<tr>
					<th><?php echo __('Wart. cechy'); ?></th>
					<th><?php echo __('Cena netto'); ?></th>
					<th><?php echo __('Cena brutto'); ?></th>
					<th><?php echo __('Ilość w mag.'); ?></th>
					<th><?php echo __('Dostępność.'); ?></th>
					<th><?php echo __('Kolejność.'); ?></th>
					<th><?php echo __('Nr kat.'); ?></th>
					<th><?php echo __('Opcje'); ?></th>
				</tr>
			</thead>
			<tbody>
          <?php if(isset($this->data['ProductsProductsOption'])): ?>
            <?php foreach ($this->data['ProductsProductsOption'] as $key => $productOption): ?>
              <?php echo $this->element('../KeyProducts/product_option_row', array('key' => $key, 'productOption' => $productOption)); ?>
            <?php endforeach; ?>
          <?php endif; ?>
          <tr class="no-products-options"
					<?php echo empty($this->data['ProductsProductsOption']) ? '' : 'style="display:none;"'; ?>>
					<td colspan="5" class="text-center">Brak pozycji do
						wyświetlenia, kliknij przycisk powyżej, aby dodać</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>