<div role="tabpanel" class="tab-pane" id="tab-seo">
	<div
		class="form-group <?php echo $this->Form->isFieldError('meta_title_tag') ? 'has-error' : ''; ?>">
      <?php echo $this->Form->input('meta_title_tag', array('label' => __('Meta tagi - tytuł:'), 'placeholder' => __('Wpisz meta tag - tytuł'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
    </div>
	<div
		class="form-group <?php echo $this->Form->isFieldError('meta_desc_tag') ? 'has-error' : ''; ?>">
      <?php echo $this->Form->input('meta_desc_tag', array('label' => __('Meta tagi - opis:'), 'placeholder' => __('Wpisz meta tag - opis'), 'class' => 'form-control', 'rows' => 3, 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
    </div>
	<div
		class="form-group <?php echo $this->Form->isFieldError('meta_keywords_tag') ? 'has-error' : ''; ?>">
      <?php echo $this->Form->input('meta_keywords_tag', array('label' => __('Meta tagi - słowa kluczowe:'), 'placeholder' => __('Wpisz meta tag - słowa kluczowe'), 'class' => 'form-control', 'rows' => 3, 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
    </div>
	<div
		class="form-group <?php echo $this->Form->isFieldError('seo_url') ? 'has-error' : ''; ?>">
      <?php echo $this->Form->input('seo_url', array('label' => __('Przyjazny adres URL:'), 'placeholder' => __('Wpisz przyjazny adres URL'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
    </div>
</div>