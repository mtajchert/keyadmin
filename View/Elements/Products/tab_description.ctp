<div role="tabpanel" class="tab-pane" id="tab-description">
	<div class="col-md-12">
		<div
			class="form-group <?php echo $this->Form->isFieldError('youtube_url') ? 'has-error' : ''; ?>">
        <?php echo $this->Form->input ( 'youtube_url', array (
          'label' => __ ( 'Youtube url:' ),
          'placeholder' => __ ( 'Podaj adres filmiku Youtube:' ),
          'class' => 'form-control',
          'error' => array (
            'attributes' => array (
              'wrap' => 'span',
              'class' => 'help-block m-b-none' 
            ) 
          ) 
        ) );
        ?>
      </div>
	</div>
	<div class="col-md-12">
		<div
			class="form-group <?php echo $this->Form->isFieldError('short_description') ? 'has-error' : ''; ?>">
        <?php echo $this->Form->input('short_description', array('label' => __('Krótki opis produktu:'), 'placeholder' => __('Wpisz krótki opis produktu:'), 'class' => 'form-control', 'rows' => 10, 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
      </div>
	</div>
	<div class="col-md-12">
		<div
			class="form-group <?php echo $this->Form->isFieldError('description') ? 'has-error' : ''; ?>">
        <?php echo $this->Form->input('description', array('label' => __('Opis produktu:'), 'placeholder' => __('Wpisz opis produktu:'), 'class' => 'form-control', 'rows' => 15, 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
      </div>
	</div>
	<div class="col-md-12">
		<div
			class="form-group <?php echo $this->Form->isFieldError('technical_description') ? 'has-error' : ''; ?>">
        <?php echo $this->Form->input('technical_description', array('label' => __('Dane techniczne:'), 'placeholder' => __('Wpisz dane techniczne:'), 'class' => 'form-control', 'rows' => 15, 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
      </div>
	</div>
</div>