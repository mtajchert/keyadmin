<div role="tabpanel" class="tab-pane" id="tab-shippings">
	<div
		class="col-sm-12 <?php echo $this->Form->isFieldError('all_shippings') || $this->Form->isFieldError('shippings') ? 'has-error' : ''; ?>">
		<label class="control-label"><?php echo __('Formy dostawy: *'); ?></label>
		<div
			class="checkbox c-checkbox <?php echo $this->Form->isFieldError('shipping_id') || $this->Form->isFieldError('all_shippings') ? 'has-error' : ''; ?>">
			<label>
          <?php echo $this->Form->input('all_shippings', array('type' => 'checkbox', 'label' => false, 'div' => false, 'class' => 'form-control', 'onchange' => 'App.Products.toggleAllShippings(this.checked);')); ?>
          <span class="fa fa-check"></span><?php echo __('wszystkie formy'); ?>
        </label>
		</div>
		<br />

      <?php echo $this->Form->isFieldError('shippings') ? $this->Form->error('shippings', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>

      <input name="data[Shipping][Shipping]" type="checkbox"
			value="" checked="checked" class="empty" style="display: none;">
		<div class="shippings_list">
        <?php
        $shipping_ids = [ ];
        if (isset ( $this->data ['Shipping'] ) && isset ( $this->data ['Shipping'] ['Shipping'] )) {
          $shipping_ids = ( array ) $this->data ['Shipping'] ['Shipping'];
        } elseif (isset ( $this->data ['Shipping'] )) {
          foreach ( $this->data ['Shipping'] as $shipping ) {
            $shipping_ids [] = $shipping ['id'];
          }
        }
        ?>
        <?php foreach ($shippings as $shipping_id => $shipping_name): ?>
          <div class="checkbox c-checkbox">
				<label> <input name="data[Shipping][Shipping][]"
					value="<?php echo h($shipping_id); ?>" type="checkbox"
					<?php echo in_array($shipping_id, $shipping_ids) ? 'checked' : ''; ?>>
					<span class="fa fa-check"></span><?php echo h($shipping_name); ?>
            </label>
			</div>
        <?php endforeach; ?>
      </div>
	</div>
</div>