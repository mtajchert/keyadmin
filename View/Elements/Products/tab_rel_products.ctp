<div role="tabpanel" class="tab-pane" id="tab-rel-products">
	<?php for ($i = 0; $i < 4; $i++): ?>
	  <div class="row">      
  		<div class="well">
  			<div class="col-sm-6">
  				<div class="form-group">
  					<?php echo $this->Form->input('RelProduct.RelProduct.'.$i.'.rel_product_id', array('label' => __('Wybierz produkt'), 'options' => $productsList, 'class' => 'rel-product-id form-control')); ?>
  				</div>
  			</div>
  			<div class="col-sm-6">
  				<div class="form-group">
  					<?php $options = array(); ?>
  					<?php if (!empty($this->data['RelProduct']['RelProduct'][$i]['rel_product_id'])): ?>
  						<?php $options = $this->Product->getProductOptions($this->data['RelProduct']['RelProduct'][$i]['rel_product_id']); ?>
  					<?php endif; ?>
  					<?php echo $this->Form->input('RelProduct.RelProduct.'.$i.'.option_id', array('label' => __('Wybierz ceche'), 'options' => $options, 'disabled' => empty($options), 'class' => 'rel-option-id form-control')); ?>
  				</div>
  			</div>
  			<div style="clear: both;"></div>
  		</div>		
  	</div>
  	<div style="clear: both;">&nbsp;</div>
	<?php endfor; ?>
</div>