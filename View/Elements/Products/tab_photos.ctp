<div role="tabpanel" class="tab-pane" id="tab-photos">
	<input type="file" name="data[Product][image][]"
		class="image-choosen" data-show-upload="false"
		data-show-caption="true" multiple="multiple" />
	<table class="table table-stripe">
		<thead>
			<tr>
				<th stye="width: 100px;">Kolejność</th>
				<th>Opis</th>
				<th>Zdjęcie</th>
				<th>Opcje</th>
			</tr>
		</thead>
      <?php if (!empty($this->data['ProductsImage'])): ?>
        <tbody>
          <?php $count = 0; ?>
          <?php foreach($this->data['ProductsImage'] as $image): ?>
            <tr>
				<td>
              <?php echo $this->Form->input("ProductsImage.{$count}.id", array('value' => $image['id'], 'div' => false, 'label' =>false, 'type' => 'hidden')); ?>
              <?php echo $this->Form->input("ProductsImage.{$count}.image", array('value' => $image['image'], 'div' => false, 'label' =>false, 'type' => 'hidden')); ?>
              <?php echo $this->Form->input("ProductsImage.{$count}.sort_order", array('value' => $image['sort_order'], 'label' =>false, 'class' => 'form-control')); ?>
              </td>
				<td>

                <?php echo $this->Form->input("ProductsImage.{$count}.description", array('label' =>false, 'placeholder' => __('Opis zdjęcia'), 'class' => 'form-control')); ?>
              </td>
				<td><?php echo $this->App->showProductsImage($image['image'], 'LIST'); ?></td>
				<td>
            <?php echo $this->Html->link ( '<em class="icon-trash"></em>', 'javascript:void(0);', array (
              'onclick' => "$(this).parent().parent().remove();",
              'escape' => false,
              'title' => __ ( 'Usuń' ) 
            ) );
            ?>
        </td>
			</tr>
            <?php $count++; ?>
          <?php endforeach; ?>
        </tbody>
      <?php endif; ?>
    </table>
</div>