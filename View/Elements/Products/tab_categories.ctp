<div role="tabpanel" class="tab-pane" id="tab-categories">
	<div class="col-md-12">
      <?php
      $categories_ids = [ ];
      if (isset ( $this->data ['Category'] ) && isset ( $this->data ['Category'] ['Category'] )) {
        $categories_ids = ( array ) $this->data ['Category'] ['Category'];
      } elseif (isset ( $this->data ['Category'] )) {
        foreach ( $this->data ['Category'] as $category ) {
          $categories_ids [] = $category ['id'];
        }
      }
      
      $categoriesOption = [ ];
      foreach ( $categories_ids as $category_id ) {
        $categoriesOption [] = [ 
          'value' => $category_id,
          'name' => h ( $this->Category->getNestedName ( $category_id ) ) 
        ];
      }
      ?>
      <div
			class="form-group <?php echo $this->Form->isFieldError('default_category_id') ? 'has-error' : ''; ?>">
        <?php echo $this->Form->input('default_category_id', array('label' => __('Główna kategoria: *'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'options' => $categoriesOption, 'empty' => __('Wybierz kategorię główną'))); ?>
      </div>
	</div>
	<div class="col-md-12">
		<div
			class="form-group <?php echo $this->Form->isFieldError('categories') ? 'has-error' : ''; ?>">
			<label class="control-label"><?php echo __('Wybierz kategorie: *'); ?></label>
        <?php echo $this->Form->isFieldError('categories') ? $this->Form->error('categories', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
        <div class="categories_list">
				<p id="categories_tree_loading"><?php echo __('Pobieranie kategorii...'); ?></p>
				<ul id="categories_tree"></ul>

          <?php foreach ($categories_ids as $category_id): ?>
            <input name="data[Category][Category][]"
					value="<?php echo h($category_id); ?>" type="hidden"> <input
					name="data[Category][CategoryNid][]"
					value="<?php echo h($this->Category->getNestedId($category_id)); ?>"
					type="hidden">
          <?php endforeach; ?>
        </div>

		</div>
	</div>
</div>