<div role="tabpanel" class="tab-pane active" id="tab-basic-data">
	<div class="row">
		<div class="well">
			<div class="col-md-6">
			
				<div
					class="form-group <?php echo $this->Form->isFieldError('name') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('name', array('label' => __('Nazwa: *'), 'placeholder' => __('Wpisz nazwę produktu'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
          </div>

				<div
					class="form-group <?php echo $this->Form->isFieldError('name_info') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('name_info', array('label' => __('Nazwa dodatkowa:'), 'placeholder' => __('Wpisz dodatkową nazwę produktu'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
          </div>
				<div
					class="form-group <?php echo $this->Form->isFieldError('catalog_no') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('catalog_no', array('label' => __('Nr katalogowy:'), 'placeholder' => __('Wpisz numer katalogowy'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
          </div>

				<div
					class="form-group form-group-margin-left-0 <?php echo $this->Form->isFieldError('status') ? 'has-error' : ''; ?>">
					<label style="display: block;" for="ProductStatus"><?php echo __('Status: *'); ?></label>
					<div class="">
						<label class="checkbox-inline c-checkbox">
                <?php echo $this->Form->input('status', array('type' => 'checkbox', 'label' => false, 'div' => false)); ?>
                <span class="fa fa-check"></span><?php echo __('aktywny'); ?>
              </label>

					</div>
				</div>
				<div
					class="form-group<?php echo $this->Form->isFieldError('new_status') ? 'has-error' : ''; ?>">
					<div class="checkbox c-checkbox">
						<label>
                <?php echo $this->Form->input('new_status', array('type' => 'checkbox', 'label' => false, 'div' => false, 'error' => false)); ?>
                <span class="fa fa-check"></span><?php echo __('Oznacz produkt jako &bdquo;Nowość&rdquo;'); ?></label>
					</div>
				</div>
				<div
					class="form-group<?php echo $this->Form->isFieldError('free_shipping_status') ? 'has-error' : ''; ?>">
					<div class="checkbox c-checkbox">
						<label>
                <?php echo $this->Form->input('free_shipping_status', array('type' => 'checkbox', 'label' => false, 'div' => false, 'error' => false)); ?>
                <span class="fa fa-check"></span><?php echo __('Produkt objęty darmową dostawą'); ?></label>
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div
					class="form-group <?php echo $this->Form->isFieldError('ext_code') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('ext_code', array('label' => __('Kod producenta:'), 'placeholder' => __('Wpisz kod producenta'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
          </div>
				<div
					class="form-group <?php echo $this->Form->isFieldError('pkwiu') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('pkwiu', array('label' => __('PKWiU:'), 'placeholder' => __('Wpisz kod PKWiU'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
          </div>
				<div
					class="form-group <?php echo $this->Form->isFieldError('ean') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('ean', array('label' => __('Kod EAN:'), 'placeholder' => __('Wpisz kod EAN'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
          </div>
				<div
					class="form-group form-group-margin-left-0 <?php echo $this->Form->isFieldError('sort_order') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('sort_order', array('min' => '0', 'step' => 1, 'label' => __('Kolejność sort.:'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
          </div>
			</div>
			<div style="clear: both;"></div>
		</div>
	</div>
	<hr />
	<div class="row">
		<div class="well">
			<div class="col-md-4">
				<?php
				  $taxRatesOptions = [ ];
				  foreach ( $taxRatesFull as $taxRate ) {
				    $taxRatesOptions [] = [
				      'value' => $taxRate ['TaxRate'] ['id'],
				      'name' => $taxRate ['TaxRate'] ['description'],
				      'data-rate' => $taxRate ['TaxRate'] ['rate']
			      ];
			    }
		    ?>
		    <div class="form-group <?php echo $this->Form->isFieldError('tax_rate_id') ? 'has-error' : ''; ?>">
		    	<?php echo $this->Form->input('tax_rate_id', array('label' => __('Stawka VAT: *'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'options' => $taxRatesOptions, 'default' => $defaultTaxRateId, 'onchange' => "App.Products.changePrice('tax')")); ?>
	    	</div>
      </div>
			<div class="col-md-4">
				<div
					class="form-group <?php echo $this->Form->isFieldError('price') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->label('price', __('Cena netto: *')); ?>
            <?php echo $this->Form->input('price', array('label' => false, 'placeholder' => __('Wpisz cenę netto'), 'min' => 0, 'class' => 'form-control', 'error' => false, 'div' => array('class' => 'input-group m-b'), 'after' => '<span class="input-group-addon">'.__('zł').'</span>', 'onchange' => "App.Products.changePrice('net')")); ?>
            <?php echo $this->Form->isFieldError('price') ? $this->Form->error('price', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
          </div>
        </div>
      <div class="col-md-4">
          <?php echo $this->Form->input('tax', array('type' => 'hidden', 'label' => false, 'error' => false, 'div' => false)); ?>
          <div
					class="form-group <?php echo $this->Form->isFieldError('price_tax') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->label('price_tax', __('Cena brutto: *')); ?>
            <?php echo $this->Form->input('price_tax', array('label' => false, 'placeholder' => __('Wpisz cenę brutto'), 'min' => 0, 'class' => 'form-control', 'error' => false, 'div' => array('class' => 'input-group m-b'), 'after' => '<span class="input-group-addon">'.__('zł').'</span>', 'onchange' => "App.Products.changePrice('gross')")); ?>
            <?php echo $this->Form->isFieldError('price_tax') ? $this->Form->error('price_tax', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
          </div>
			</div>
			<div style="clear: both;"></div>
			<div class="col-md-4">
				<div class="form-group <?php echo $this->Form->isFieldError('retail_price') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->label('retail_price', __('Cena katalogowa:')); ?>
          <?php echo $this->Form->input('retail_price', array('label' => false, 'placeholder' => __('Wpisz cenę katalogową'), 'min' => 0, 'class' => 'form-control', 'error' => false, 'div' => array('class' => 'input-group m-b'), 'after' => '<span class="input-group-addon">'.__('zł').'</span>')); ?>
          <?php echo $this->Form->isFieldError('retail_price') ? $this->Form->error('retail_price', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
        </div>
			</div>
			<div class="col-md-4">
				<div class="form-group <?php echo $this->Form->isFieldError('purchase_price') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->label('purchase_price', __('Cena zakupu netto:')); ?>
          <?php echo $this->Form->input('purchase_price', array(
            'label' => false, 'placeholder' => __('Wpisz cenę zakupu netto'), 'min' => 0, 'class' => 'form-control',
            'onchange' => "App.Products.changePurchasePrice('net')",
            'error' => false, 'div' => array('class' => 'input-group m-b'), 'after' => '<span class="input-group-addon">'.__('zł').'</span>'            
          )); ?>
          <?php echo $this->Form->isFieldError('purchase_price') ? $this->Form->error('purchase_price', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
        </div>
			</div>
			<div class="col-md-4">
				<div class="form-group <?php echo $this->Form->isFieldError('purchase_price_tax') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->label('purchase_price_tax', __('Cena zakupu brutto:')); ?>
          <?php echo $this->Form->input('purchase_price_tax', array(
            'label' => false, 'placeholder' => __('Wpisz cenę katalogowązakupu brutto'), 'min' => 0, 'class' => 'form-control', 
            'onchange' => "App.Products.changePurchasePrice('gross')",
            'error' => false, 'div' => array('class' => 'input-group m-b'), 'after' => '<span class="input-group-addon">'.__('zł').'</span>')); ?>
          <?php echo $this->Form->isFieldError('purchase_price_tax') ? $this->Form->error('purchase_price_tax', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
        </div>
			</div>
		</div>
	</div>
	<hr />
	<div class="row">
		<div class="well">
			<div class="col-md-6">
				<div
					class="form-group <?php echo $this->Form->isFieldError('unit_id') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('unit_id', array('label' => __('Jednostka miary: *'), 'class' => 'form-control', 'empty' => 'Wybierz jednostkę miary', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'onchange' => "$('.current_unit').text($(this).find('option:selected').text())")); ?>
          </div>
			</div>
			<div class="col-md-6">
				<div
					class="form-group <?php echo $this->Form->isFieldError('store_quantity') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->label('store_quantity', __('Ilość w magazynie:')); ?>
            <?php echo $this->Form->input('store_quantity', array('label' => false, 'placeholder' => __('Wpisz ilość w magazynie'), 'min' => 0, 'class' => 'form-control', 'error' => false, 'div' => array('class' => 'input-group m-b'), 'after' => '<span class="input-group-addon current_unit"></span>')); ?>
            <?php echo $this->Form->isFieldError('store_quantity') ? $this->Form->error('store_quantity', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
          </div>
			</div>
			<div style="clear: both;"></div>
			<div class="col-md-4">
				<div
					class="form-group <?php echo $this->Form->isFieldError('order_min_quantity') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->label('order_min_quantity', __('Minimalna ilość zakupu:')); ?>
            <?php echo $this->Form->input('order_min_quantity', array('label' => false, 'placeholder' => __('Wpisz minimalną ilość zakupu'), 'min' => 0, 'class' => 'form-control', 'error' => false, 'div' => array('class' => 'input-group m-b'), 'after' => '<span class="input-group-addon current_unit"></span>')); ?>
            <?php echo $this->Form->isFieldError('order_min_quantity') ? $this->Form->error('order_min_quantity', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
          </div>
			</div>
			<div class="col-md-4">
				<div
					class="form-group <?php echo $this->Form->isFieldError('order_max_quantity') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->label('order_max_quantity', __('Maksymalna ilość zakupu:')); ?>
            <?php echo $this->Form->input('order_max_quantity', array('label' => false, 'placeholder' => __('Wpisz maksymalną ilość zakupu'), 'min' => 0, 'class' => 'form-control', 'error' => false, 'div' => array('class' => 'input-group m-b'), 'after' => '<span class="input-group-addon current_unit"></span>')); ?>
            <?php echo $this->Form->isFieldError('order_max_quantity') ? $this->Form->error('order_max_quantity', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
          </div>
			</div>
			<div class="col-md-4">
				<div
					class="form-group <?php echo $this->Form->isFieldError('order_step_quantity') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->label('order_step_quantity', __('Przyrost ilość zakupu:')); ?>
            <?php echo $this->Form->input('order_step_quantity', array('label' => false, 'placeholder' => __('Wpisz przyrost ilość zakupu'), 'min' => 0, 'class' => 'form-control', 'error' => false, 'div' => array('class' => 'input-group m-b'), 'after' => '<span class="input-group-addon current_unit"></span>')); ?>
            <?php echo $this->Form->isFieldError('order_step_quantity') ? $this->Form->error('order_step_quantity', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
          </div>
			</div>
			<div style="clear: both;"></div>
		</div>
	</div>
	<hr />

	<div class="row">
		<div class="well">
			<div class="col-md-4">
				<div
					class="form-group <?php echo $this->Form->isFieldError('type') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('type', array('label' => __('Rodzaj produktu: *'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'options' => ['standard' => __('standardowy'), 'custom' => __('niestandardowy, indywidualny'), 'service' => __('usługa'), 'digital' => __('treść cyfrowa')], 'empty' => 'Wybierz rodzaj produktu')); ?>
          </div>
				<div
					class="form-group <?php echo $this->Form->isFieldError('product_availability_id') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('product_availability_id', array('label' => __('Dostępność:'), 'class' => 'form-control', 'empty' => 'Wybierz dostępność', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
          </div>
			</div>
			<div class="col-md-4">
				<div
					class="form-group <?php echo $this->Form->isFieldError('product_condition_id') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('product_condition_id', array('label' => __('Stan produktu:'), 'class' => 'form-control', 'empty' => 'Wybierz stan produktu', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
          </div>
				<div
					class="form-group <?php echo $this->Form->isFieldError('available_date') ? 'has-error' : ''; ?>">
          	<?php echo $this->Form->label('available_date', __('Dostępne od:')); ?>
            <?php
                      
echo $this->Form->input ( 'available_date', array (
                        'type' => 'text',
                        'label' => false,
                        'placeholder' => __ ( 'Wybierz datę dostępności' ),
                        'class' => 'form-control',
                        'div' => array (
                          'class' => 'input-group m-b',
                          'id' => 'ProductAvailableDateBox' 
                        ),
                        'error' => false,
                        'after' => '<span class="input-group-addon"><span class="fa fa-calendar"></span></span>' 
                      ) );
                      ?>                        
          </div>
			</div>
			<div class="col-md-4">
				<div
					class="form-group <?php echo $this->Form->isFieldError('product_warranty_id') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('product_warranty_id', array('label' => __('Gwarancja:'), 'class' => 'form-control', 'empty' => 'Wybierz gwarancję', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
          </div>
				<div
					class="form-group <?php echo $this->Form->isFieldError('shipping_time_id') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('shipping_time_id', array('label' => __('Termin wysyłki:'), 'class' => 'form-control', 'empty' => 'Wybierz termin wysyłki', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
          </div>
			</div>
			<div style="clear: both;"></div>
		</div>
	</div>
	<hr />

	<div class="row">
		<div class="well">
			<div class="col-md-3">
				<div
					class="form-group <?php echo $this->Form->isFieldError('manufacturer_id') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('manufacturer_id', array('label' => __('Producent:'), 'class' => 'form-control', 'empty' => 'Wybierz producenta', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
          </div>
			</div>
			<div class="col-md-3">
				<div
					class="form-group <?php echo $this->Form->isFieldError('ind_shipping_price_tax') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->label('ind_shipping_price_tax', __('Ind. cena brutto wysyłki:')); ?>
            <?php echo $this->Form->input('ind_shipping_price_tax', array('label' => false, 'placeholder' => __('Wpisz indywidualną cenę brutto wysyłki'), 'min' => 0, 'class' => 'form-control', 'error' => false, 'div' => array('class' => 'input-group m-b'), 'after' => '<span class="input-group-addon">'.__('zł').'</span>')); ?>
            <?php echo $this->Form->isFieldError('ind_shipping_price_tax') ? $this->Form->error('ind_shipping_price_tax', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
          </div>
			</div>
			<div class="col-md-3">
				<div
					class="form-group <?php echo $this->Form->isFieldError('weight') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('weight', array('label' => __('Waga:'), 'placeholder' => __('Wpisz wagę produktu'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
          </div>
			</div>
			<div class="col-md-3">
				<div
					class="form-group <?php echo $this->Form->isFieldError('ext_store_id') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('ext_store_id', array('type' => 'text', 'label' => __('Id produktu w prog. mag.:'), 'placeholder' => __('Wpisz id produktu w programie magazynowym'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
			</div>
			<div style="clear: both;"></div>
			<div class="col-md-12">
				<div
					class="form-group <?php echo $this->Form->isFieldError('admin_notes') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('admin_notes', array('label' => __('Notatki do produktu'), 'placeholder' => __('Wpisz notatki do produktu'), 'class' => 'form-control', 'rows' => 3, 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
          </div>
			</div>
			<div style="clear: both;"></div>
		</div>
	</div>
	<hr />

	<div class="row">
		<div class="well">
			<div class="col-md-4">
				<div
					class="form-group<?php echo $this->Form->isFieldError('our_hit_status') ? 'has-error' : ''; ?>">
					<div class="checkbox c-checkbox">
						<label>
                <?php echo $this->Form->input('our_hit_status', array('type' => 'checkbox', 'label' => false, 'div' => false, 'error' => false)); ?>
                <span class="fa fa-check"></span><?php echo __('Oznacz produkt jako &bdquo;Nasz hit&rdquo;'); ?></label>
					</div>
            <?php echo $this->Form->isFieldError('our_hit_status') ? $this->Form->error('our_hit_status', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
          </div>
				<div
					class="form-group <?php echo $this->Form->isFieldError('our_hit_date') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->label('our_hit_date', 'Data rozpoczęcia:', array('class' => '')); ?>
            <?php echo $this->Form->input('our_hit_date', array('type' => 'text', 'label' => false, 'placeholder' => __('Wybierz datę rozpoczęcia'), 'class' => 'form-control', 'div' => array('class' => 'input-group date', 'id' => 'ProductOurHitDateBox'), 'error' => false, 'after' => '<span class="input-group-addon"><span class="fa fa-calendar"></span></span>')); ?>
            <?php echo $this->Form->isFieldError('our_hit_date') ? $this->Form->error('our_hit_date', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
          </div>
				<div
					class="form-group<?php echo $this->Form->isFieldError('our_hit_date_end') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->label('our_hit_date_end', 'Data zakończenia:', array('class' => '')); ?>
            <?php echo $this->Form->input('our_hit_date_end', array('type' => 'text', 'label' => false, 'placeholder' => __('Wybierz datę zakończenia'), 'class' => 'form-control', 'div' => array('class' => 'input-group date', 'id' => 'ProductOurHitDateBox'), 'error' => false, 'after' => '<span class="input-group-addon"><span class="fa fa-calendar"></span></span>')); ?>
            <?php echo $this->Form->isFieldError('our_hit_date_end') ? $this->Form->error('our_hit_date_end', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
          </div>
			</div>
			<div class="col-md-4">
				<div
					class="form-group<?php echo $this->Form->isFieldError('featured_status') ? 'has-error' : ''; ?>">
					<div class="checkbox c-checkbox">
						<label>
                <?php echo $this->Form->input('featured_status', array('type' => 'checkbox', 'label' => false, 'div' => false, 'error' => false)); ?>
                <span class="fa fa-check"></span><?php echo __('Oznacz produkt jako &bdquo;Polecany&rdquo;'); ?></label>
					</div>
            <?php echo $this->Form->isFieldError('featured_status') ? $this->Form->error('featured_status', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
          </div>
				<div
					class="form-group <?php echo $this->Form->isFieldError('featured_date') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->label('featured_date', 'Data rozpoczęcia:', array('class' => '')); ?>
            <?php echo $this->Form->input('featured_date', array('type' => 'text', 'label' => false, 'placeholder' => __('Wybierz datę rozpoczęcia'), 'class' => 'form-control', 'div' => array('class' => 'input-group date', 'id' => 'ProductOurHitDateBox'), 'error' => false, 'after' => '<span class="input-group-addon"><span class="fa fa-calendar"></span></span>')); ?>
            <?php echo $this->Form->isFieldError('featured_date') ? $this->Form->error('featured_date', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
          </div>
				<div
					class="form-group <?php echo $this->Form->isFieldError('featured_date_end') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->label('featured_date_end', 'Data zakończenia:', array('class' => '')); ?>
            <?php echo $this->Form->input('featured_date_end', array('type' => 'text', 'label' => false, 'placeholder' => __('Wybierz datę zakończenia'), 'class' => 'form-control', 'div' => array('class' => 'input-group date', 'id' => 'ProductOurHitDateBox'), 'error' => false, 'after' => '<span class="input-group-addon"><span class="fa fa-calendar"></span></span>')); ?>
            <?php echo $this->Form->isFieldError('featured_date_end') ? $this->Form->error('featured_date_end', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
          </div>
			</div>
			<div class="col-md-4">
				<div
					class="form-group <?php echo $this->Form->isFieldError('promotion_status') ? 'has-error' : ''; ?>">
					<div class="checkbox c-checkbox">
						<label>
                <?php echo $this->Form->input('promotion_status', array('type' => 'checkbox', 'label' => false, 'div' => false, 'error' => false)); ?>
                <span class="fa fa-check"></span><?php echo __('Oznacz produkt jako &bdquo;Promocja&rdquo;'); ?></label>
					</div>
            <?php echo $this->Form->isFieldError('promotion_status') ? $this->Form->error('promotion_status', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
          </div>

				<div
					class="form-group<?php echo $this->Form->isFieldError('promotion_info') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->label('promotion_info', __('Wysokość rabatu (informacyjnie):'), array('class' => '')); ?>
            <?php echo $this->Form->input('promotion_info', array('label' => false, 'placeholder' => __('Wpisz wysokość rabat'), 'class' => 'form-control', 'error' => false, 'div' => array('class' => 'input-group m-b'))); ?>
            <?php echo $this->Form->isFieldError('old_price') ? $this->Form->error('old_price', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
          </div>

				<div
					class="form-group <?php echo $this->Form->isFieldError('old_price') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->label('old_price', __('Poprzednia cena:'), array('class' => '')); ?>
            <?php echo $this->Form->input('old_price', array('label' => false, 'placeholder' => __('Wpisz poprzednią cenę'), 'min' => 0, 'class' => 'form-control', 'error' => false, 'div' => array('class' => 'input-group m-b'), 'after' => '<span class="input-group-addon">'.__('zł').'</span>')); ?>
            <?php echo $this->Form->isFieldError('old_price') ? $this->Form->error('old_price', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
          </div>

				<div
					class="form-group form-group-margin-left-0 <?php echo $this->Form->isFieldError('promotion_date') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->label('promotion_date', 'Data rozpoczęcia:', array('class' => '')); ?>
            <?php echo $this->Form->input('promotion_date', array('type' => 'text', 'label' => false, 'placeholder' => __('Wybierz datę rozpoczęcia'), 'class' => 'form-control', 'div' => array('class' => 'input-group date', 'id' => 'ProductOurHitDateBox'), 'error' => false, 'after' => '<span class="input-group-addon"><span class="fa fa-calendar"></span></span>')); ?>
            <?php echo $this->Form->isFieldError('promotion_date') ? $this->Form->error('promotion_date', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
          </div>

				<div
					class="form-group form-group-margin-left-0 <?php echo $this->Form->isFieldError('promotion_date_end') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->label('promotion_date_end', 'Data zakończenia:', array('class' => '')); ?>
            <?php echo $this->Form->input('promotion_date_end', array('type' => 'text', 'label' => false, 'placeholder' => __('Wybierz datę zakończenia'), 'class' => 'form-control', 'div' => array('class' => 'input-group date', 'id' => 'ProductOurHitDateBox'), 'error' => false, 'after' => '<span class="input-group-addon"><span class="fa fa-calendar"></span></span>')); ?>
            <?php echo $this->Form->isFieldError('promotion_date_end') ? $this->Form->error('promotion_date_end', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
          </div>
			</div>
			<div style="clear: both;"></div>
		</div>
	</div>
</div>