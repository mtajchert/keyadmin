<script type="text/template" id="modal-choose-category-template">
  <?php echo $this->element('Modals/chooseCategory'); ?>
</script>

<script type="text/template" id="modal-choose-category-product-template">
  <?php echo $this->element('Modals/chooseCategoryProduct'); ?>
</script>

<script type="text/template" id="modal-choose-customer-template">
  <?php echo $this->element('Modals/chooseCustomer'); ?>
</script>

<script type="text/template" id="modal-choose-products-products-option-template">
  <?php echo $this->element('Modals/ChooseProductsProductsOption'); ?>
</script>

<script type="text/template" id="modal-confirm-delete-template">
  <?php echo $this->element('Modals/confirmDelete'); ?>
</script>

<script type="text/template" id="modal-confirm-yes-no-cancel-template">
  <?php echo $this->element('Modals/confirmYesNoCancel'); ?>
</script>

<script type="text/template" id="modal-confirm-yes-cancel-template">
  <?php echo $this->element('Modals/confirmYesCancel'); ?>
</script>

<script type="text/template" id="modal-confirm-ok-cancel-template">
  <?php echo $this->element('Modals/confirmOkCancel'); ?>
</script>

<script type="text/template" id="modal-orders-report-template">
  <?php echo $this->element('Modals/ordersReport'); ?>
</script>

<script type="text/template" id="modal-invoices-report-template">
  <?php echo $this->element('Modals/invoicesReport'); ?>
</script>

<script type="text/template" id="modal-filter-order-template">
  <?php echo $this->element('Modals/filterOrderList'); ?>
</script>