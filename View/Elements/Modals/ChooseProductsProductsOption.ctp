<!-- Modal -->
<div class="modal fade" id="ChooseProductsProductsOptionBox" tabindex="-1" role="dialog" aria-labelledby="ChooseProductsProductsOptionBoxLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo __('Zamknij'); ?>"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="ChooseProductsProductsOptionBoxLabel"><?php echo __('Wybierz cechy produktu'); ?></h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            <br/>
            <input type="hidden" id="ChooseProductsProductsOptionBoxProductId" value="<%= productId %>" />
            <table class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th></th>
                  <th><?php echo __('Wartość cechy'); ?></th>
                  <th><?php echo __('Zmiana ceny netto'); ?></th>
                  <th><?php echo __('Zmiana ceny brutto'); ?></th>
                  <th><?php echo __('Nr katalogowy'); ?></th>
                </tr>
              </thead>
              <tbody class="options-content"><%= optionsContent %></tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Anuluj'); ?></button>
        <button type="button" class="btn btn-primary" onclick="<%= saveScript %>"><?php echo __('Zapisz'); ?></button>
      </div>
    </div>
  </div>
</div>