<?php echo $this->Form->create('Order', array('id' => 'OrderFilterForm')); ?>

<div class="row">
	<div class="col-xs-4">
		<?php echo $this->Form->input('phraze', array('class' => 'form-control', 'label' => 'Fraza (Klient lub id zamówienia)', '')); ?>
	</div>
	
	<div class="col-xs-4">
		<?php echo $this->Form->input('phraze_product', array('class' => 'form-control', 'label' => 'Product (Nazwa lub kod)', '')); ?>
	</div>
	
	<div class="col-xs-4">
		<?php echo $this->Form->input('status_id', array('class' => 'form-control', 'label' => 'Status')); ?>
	</div>
</div>

<div class="clearfix">&nbsp;</div>

<div class="row">
	<div class="col-xs-3">
		<?php echo $this->Form->input('date_from', array('class' => 'form-control order-filter-datepicker', 'label' => 'Data od')); ?>
	</div>
	<div class="col-xs-3">
		<?php echo $this->Form->input('date_to', array('class' => 'form-control order-filter-datepicker', 'label' => 'Data do')); ?>
	</div>
	
	<div class="col-xs-3">
		<?php echo $this->Form->input('payment_id', array('class' => 'form-control', 'label' => 'Rodzaj płatności')); ?>
	</div>
	<div class="col-xs-3">
		<?php echo $this->Form->input('shipping_id', array('class' => 'form-control', 'label' => 'Rodzaj dostawy')); ?>
	</div>
	
	
</div>

<div class="clearfix">&nbsp;</div>

<div class="row">
	<div class="col-xs-3">
		<?php echo $this->Form->input('price_from', array('class' => 'form-control', 'label' => 'Cena od', 'type' => 'number')); ?>
	</div>
	<div class="col-xs-3">
		<?php echo $this->Form->input('price_to', array('class' => 'form-control', 'label' => 'Cena do', 'type' => 'number')); ?>
	</div>
	<div class="col-xs-6">
		<?php echo $this->Form->input('shipment_no', array('class' => 'form-control', 'label' => 'Numer listu przewozowego')); ?>
	</div>
</div>

<div class="clearfix">&nbsp;</div>

<div class="row">
	<div class="col-xs-2">
		<div class="form-group">
        <label><?php echo __('Zapłacono'); ?></label><br/>
        <label class="checkbox-inline c-checkbox">
          <?php echo $this->Form->input('paid', array('type' => 'checkbox', 'label' => false, 'div' => false)); ?><span class="fa fa-check"></span>
        </label>
      </div>
	</div>
</div>
<?php echo $this->Form->end(); ?>