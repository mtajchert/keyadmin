<!-- Modal -->
<div class="modal fade" id="OrdersReport" tabindex="-1" role="dialog" aria-labelledby="OrdersReportLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo __('Zamknij'); ?>"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="ParentCategoryBoxLabel"><?php echo __('Generuj raport'); ?></h4>
      </div>
      <div class="modal-body">
        <p><?php echo __('Wybierz zakres dat (włącznie):'); ?></p>
        <div class="row">
          <div class="form-group col-xs-12">
            <div class="col-xs-6">
              <?php echo $this->Form->input('orders_report_date_from', array('type' => 'text', 'label' => false, 'placeholder' => __('Wybierz datę od'), 'class' => 'form-control', 'div' => array('class' => 'input-group date', 'id' => 'OrdersReportDateFrom'), 'error' => false, 'after' => '<span class="input-group-addon"><span class="fa fa-calendar"></span></span>')); ?>              
            </div>
            <div class="col-xs-6">
              <?php echo $this->Form->input('orders_report_date_to', array('type' => 'text', 'label' => false, 'placeholder' => __('Wybierz datę do'), 'class' => 'form-control', 'div' => array('class' => 'input-group date', 'id' => 'OrdersReportDateTo'), 'error' => false, 'after' => '<span class="input-group-addon"><span class="fa fa-calendar"></span></span>')); ?>
            </div>
          </div>
        </div>
        
        <div class="row">
        	<div class="form-group col-xs-12 col-sm-6">
        		<div class="col-xs-12">
          		<?php echo $this->Form->input('report_type', array(
          		  'options' => array('PDF' => 'Wydruk PDF', 'CSV' => 'Arkusz CSV'),  
          		  'label' => 'Typ raportu', 'placeholder' => __('Wybierz datę do'), 'class' => 'form-control', 		  
          		)); ?>
        		</div>
        	</div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Anuluj'); ?></button>
        <button type="button" class="btn btn-primary" onclick="<%= saveScript %>"><?php echo __('Generuj'); ?></button>
      </div>
    </div>
  </div>
</div>