<!-- Modal -->
<div class="modal fade" id="InvoicesReport" tabindex="-1" role="dialog" aria-labelledby="OrdersReportLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo __('Zamknij'); ?>"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="ParentCategoryBoxLabel"><?php echo __('Generuj raport'); ?></h4>
      </div>
    	<?php echo $this->Form->create('Invoice', array('url' => array('plugin' => 'KeyAdmin', 'admin' => true, 'controller' => 'KeyInvoices', 'action' => 'generate_pdf'))); ?>
      <div class="modal-body">
        <p><?php echo __('Wybierz zakres dat (włącznie):'); ?></p>
        <div class="row">
          <div class=" col-xs-6">
            <div class="form-group">
              <?php echo $this->Form->input('report_date_from', array('type' => 'text', 'label' => 'Data od:', 'placeholder' => __('Wybierz datę od'), 'class' => 'form-control', 'error' => false)); ?>
            </div>
          </div>
          <div class="col-xs-6">
          	<div class="form-group">
            	<?php echo $this->Form->input('report_date_to', array('type' => 'text', 'label' => 'Data do:', 'placeholder' => __('Wybierz datę do'), 'class' => 'form-control', 'error' => false)); ?>
            </div>            
          </div>
        </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Anuluj'); ?></button>
        <button type="submit" class="btn btn-primary"><?php echo __('Generuj'); ?></button>
      </div>
      <?php echo $this->Form->end(); ?>
    </div>
  </div>
</div>