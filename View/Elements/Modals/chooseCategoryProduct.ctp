<!-- Modal -->
<div class="modal fade" id="ChooseCategoryProductBox" tabindex="-1" role="dialog" aria-labelledby="ChooseCategoryProductBoxLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo __('Zamknij'); ?>"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="ChooseCategoryProductBoxLabel"><?php echo __('Wybierz produkt'); ?></h4>
      </div>
      <div class="modal-body">
        <div class="col-sm-4">
          <p id="categories_tree_loading"><?php echo __('Pobieranie kategorii...'); ?></p>
          <ul id="categories_tree"></ul>
        </div>
        <div class="col-sm-8">
          <div class="row">
            <div class="input-group">
              <input class="form-control" value="" type="text" id="ChooseCategoryProductBoxQuery" placeholder="Szukaj produktów" onkeyup="this.setAttribute('value', this.value);">
              <span class="input-group-btn">
                <span class="inputclear fa fa-times-circle-o" onclick="App.Form.clearInput(this); <%= searchScript %>"></span>
                <button type="button" class="btn btn-default" onclick="<%= searchScript %>"><?php echo __('Szukaj'); ?></button>
              </span>
            </div>
          </div>
          <div class="row">
            <br/>
            <table class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th></th>
                  <th>Id</th>
                  <th>Zdjęcie</th>
                  <th>Nazwa</th>
                  <th>Nr kat.</th>
                  <th style="min-width:80px;">Cena</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody class="products-content">
                <tr>
                  <td colspan="6" class="text-center">
                    <?php echo __('Wybierz kategorię i/lub wpisz frazę do wyszukiwania'); ?>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Anuluj'); ?></button>
        <button type="button" class="btn btn-primary" onclick="<%= saveScript %>"><?php echo __('Zapisz'); ?></button>
      </div>
    </div>
  </div>
</div>