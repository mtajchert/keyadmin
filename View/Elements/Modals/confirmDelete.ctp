<!-- Modal -->
<div class="modal fade" id="ConfirmDeleteBox" tabindex="-1" role="dialog" aria-labelledby="ConfirmDeleteBoxLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo __('Zamknij'); ?>"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="ConfirmDeleteBoxLabel"><?php echo __('Potwierdź usunięcie'); ?></h4>
      </div>
      <div class="modal-body">
        <%= confirmMessage %>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Anuluj'); ?></button>
        <button type="button" class="btn btn-primary" onclick="<%= confirmScript %>"><?php echo __('Usuń'); ?></button>
      </div>
    </div>
  </div>
</div>