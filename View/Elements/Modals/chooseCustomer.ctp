<!-- Modal -->
<div class="modal fade" id="ChooseCustomerBox" tabindex="-1" role="dialog" aria-labelledby="ChooseCustomerBoxLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo __('Zamknij'); ?>"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="ChooseCustomerBoxLabel"><?php echo __('Wybierz klienta'); ?></h4>
      </div>
      <div class="modal-body">
        <div class="col-sm-12">
          <div class="row">
            <div class="input-group">
              <input class="form-control" value="" type="text" id="ChooseCustomerBoxQuery" placeholder="Szukaj klientów" onkeyup="this.setAttribute('value', this.value);">
              <span class="input-group-btn">
                <span class="inputclear fa fa-times-circle-o" onclick="App.Form.clearInput(this); <%= searchScript %>"></span>
                <button type="button" class="btn btn-default" onclick="<%= searchScript %>"><?php echo __('Szukaj'); ?></button>
              </span>
            </div>
          </div>
          <div class="row">
            <br/>
            <table class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th></th>
                  <th><?php echo h('Id'); ?></th>
                  <th><?php echo h('Klient'); ?></th>
                  <th><?php echo h('Kontakt'); ?></th>
                  <th><?php echo h('Data utworzenia'); ?></th>
                  <th><?php echo h('Zamówienia'); ?></th>
                  <th><?php echo h('Status'); ?></th>
                </tr>
              </thead>
              <tbody class="customers-content">
                <tr>
                  <td colspan="7" class="text-center">
                    <?php echo __('Wpisz frazę do wyszukiwania'); ?>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Anuluj'); ?></button>
        <button type="button" class="btn btn-primary" onclick="<%= saveScript %>"><?php echo __('Zapisz'); ?></button>
      </div>
    </div>
  </div>
</div>