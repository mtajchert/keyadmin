<!-- Modal -->
<div class="modal fade" id="ParentCategoryBox" tabindex="-1" role="dialog" aria-labelledby="ParentCategoryBoxLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo __('Zamknij'); ?>"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="ParentCategoryBoxLabel"><?php echo __('Wybierz kategorię'); ?></h4>
      </div>
      <div class="modal-body">
        <p id="categories_tree_loading"><?php echo __('Pobieranie kategorii...'); ?></p>
        <ul id="categories_tree"></ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Anuluj'); ?></button>
        <button type="button" class="btn btn-primary" onclick="<%= saveScript %>"><?php echo __('Zapisz'); ?></button>
      </div>
    </div>
  </div>
</div>