<?php
$tmp = [];
if (!empty($user['email'])) {
  $tmp[] = $user['email'];
}
if (!empty($user['contact_phone'])) {
  $tmp[] = __('tel.').' '.$user['contact_phone'];
}
echo h(implode(', ', $tmp));
?>&nbsp;