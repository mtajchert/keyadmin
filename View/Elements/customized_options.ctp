<div class="row">     
    <div class="col-xs-4">
        <?php echo $this->Html->link('<i class="fa fa-arrow-up" aria-hidden="true"></i>&nbsp;&nbsp;Wyżej', '#', [
            'class' => 'btn btn-default btn-block btn-sort-up',
            'escape' => false,
        ]); ?>
    </div>
    <div class="col-xs-4">
        <?php echo $this->Html->link('<i class="fa fa-arrow-down" aria-hidden="true"></i>&nbsp;&nbsp;Nizej', '#', [
            'class' => 'btn btn-default btn-block btn-sort-down',
            'escape' => false,
        ]); ?>
    </div>
    <div class="col-xs-4">
        <?php echo $this->Html->link('<i class="fa fa-trash-o" aria-hidden="true"></i>&nbsp;&nbsp;Usuń linię', [
            'action' => 'admin_delete_customization',
            $id
        ], [
            'class' => 'btn btn-danger btn-block btn-delete-line',
            'escape' => false,
        ]); ?>
    </div>
</div>