<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th><?php echo $this->Paginator->sort('id', __('ID')); ?></th>
              <th><?php echo $this->Paginator->sort('rate', __('Stawka %')); ?></th>
              <th><?php echo $this->Paginator->sort('description', __('Opis')); ?></th>
              <th><?php echo $this->Paginator->sort('short_description', __('Opis skrócony')); ?></th>
              <th><?php echo $this->Paginator->sort('sort_order', __('Kolejność sort.')); ?></th>
              <th><?php echo $this->Paginator->sort('default', __('Domyślna')); ?></th>
              <th><?php echo __('Opcje'); ?></th>
            </tr>
          </thead>
          <tbody>
            <?php if (isset($tax_rates) && !empty($tax_rates)): ?>
              <?php foreach ($tax_rates as $tax_rate): ?>
                <tr>
                  <td><?php echo h($tax_rate['TaxRate']['id']); ?>&nbsp;</td>
                  <td><?php echo h($tax_rate['TaxRate']['rate']); ?>&nbsp;</td>
                  <td><?php echo h($tax_rate['TaxRate']['description']); ?>&nbsp;</td>
                  <td><?php echo h($tax_rate['TaxRate']['short_description']); ?>&nbsp;</td>
                  <td>
                    <input class="form-control sort-order-input input-sm" type="text" value="<?php echo h($tax_rate['TaxRate']['sort_order']); ?>" onchange="App.TaxRates.saveSortOrder(<?php echo h($tax_rate['TaxRate']['id']); ?>, this.value);">
                  </td>
                  <td>
                    <span class="checkbox c-checkbox c-checkbox-rounded c-checkbox-no-label">
                      <input <?php echo $tax_rate['TaxRate']['default'] ? 'checked' : ''; ?> type="checkbox" disabled>
                      <span class="fa fa-check"></span>
                    </span>
                  </td>
                  <td class="options-buttons text-center">
                    <?php echo $this->Html->link('<em class="icon-note"></em>', array('controller' => 'KeyTaxRates', 'action' => 'edit', $tax_rate['TaxRate']['id']), array('escape' => false, 'title' => __('Edytuj'))); ?>
                    &nbsp;
                    <?php echo $this->Html->link('<em class="icon-trash"></em>', 'javascript:void(0);', array('escape' => false, 'title' => __('Usuń'), 'onclick' => 'App.TaxRates.deleteTaxRate(\''.h($tax_rate['TaxRate']['description']).'\', \''.$this->Html->url(array('controller' => 'KeyTaxRates', 'action' => 'delete', $tax_rate['TaxRate']['id'])).'\')')); ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <td colspan="7"><?php echo __('Brak stawek VAT do wyświetlenia'); ?></td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="panel-footer text-center">
      <div class="col-sm-6">
        <div class="pull-left">
          <?php echo $this->Paginator->counter(array('format' => __('Strona {:page} z {:pages}'))); ?>
        </div>
      </div>
      <div class="col-sm-6 pagination pagination-large">
        <ul class="pagination pull-right">
          <?php
          $this->Paginator->options['url'] = array('controller' => 'KeyTaxRates', 'action' => 'index');
          echo $this->Paginator->prev(__('poprzednia'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1, 'modulus' => 5, 'ellipsis' => '<li class="disabled"><a>...</a></li>'));
          echo $this->Paginator->next(__('następna'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          ?>
        </ul>
      </div>
      <span class="clearfix"></span>
    </div>
  </div>
</div>

<script>
  $(document).ready(function () {
    <?php echo $this->element('Scripts/AppTaxRatesDefaultSettings'); ?>
    App.TaxRates.onDocReady();
  });
</script>