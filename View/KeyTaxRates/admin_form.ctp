<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <?php echo $this->Form->create('TaxRate', array('class' => 'form-horizontal')); ?>
      <div class="col-md-8">
        <div class="form-group <?php echo $this->Form->isFieldError('description') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('description', array('label' => __('Opis: *'), 'placeholder' => __('Wpisz opis'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
        <div class="form-group <?php echo $this->Form->isFieldError('short_description') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('short_description', array('label' => __('Opis skrócony: *'), 'placeholder' => __('Wpisz opis skrócony'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
        <div class="form-group <?php echo $this->Form->isFieldError('rate') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->label('rate', __('Stawka: *')); ?>
          <?php echo $this->Form->input('rate', array('label' => false, 'placeholder' => __('Wpisz stawkę'), 'class' => 'form-control', 'error' => false, 'div' => array('class' => 'input-group m-b'), 'after' => '<span class="input-group-addon">%</span>')); ?>
          <?php echo $this->Form->isFieldError('rate') ? $this->Form->error('rate', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group form-group-margin-left-0 <?php echo $this->Form->isFieldError('default') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->label('default', __('Domyślna: *'), array('type' => 'radio', 'class' => 'col-xs-5 control-label text-left-force')); ?>
          <div class="col-xs-7">
            <label class="radio-inline c-radio">
              <input id="TaxRateDefault1" name="data[TaxRate][default]" value="1" <?php echo $tax_rate['TaxRate']['default'] ? 'checked' : ''; ?> type="radio">
              <span class="fa fa-circle"></span><?php echo(__('tak')); ?>
            </label>
            <label class="radio-inline c-radio">
              <input id="TaxRateDefault0" name="data[TaxRate][default]" value="0" <?php echo !$tax_rate['TaxRate']['default'] ? 'checked' : ''; ?> type="radio">
              <span class="fa fa-circle"></span><?php echo(__('nie')); ?>
            </label>
          </div>
        </div>
        <div class="form-group form-group-margin-left-0 <?php echo $this->Form->isFieldError('sort_order') ? 'has-error' : ''; ?>">
          <label class="col-xs-5 control-label text-left-force" for="exampleInputEmail1"><?php echo __('Kolejność sort.:'); ?></label>
          <div class="col-xs-7">
            <?php echo $this->Form->input('sort_order', array('min' => '0', 'step' => 1, 'label' => false, 'div' => false, 'class' => 'form-control')); ?>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="pull-right">
          <?php echo $this->Html->link(__('Anuluj'), array('controller' => 'KeyTaxRates', 'action' => 'index', 'admin' => true), array('title' => __('Anuluj'), 'class' => 'btn btn-default')); ?>
          <button type="submit" class="btn btn-primary"><?php echo __('Zapisz'); ?></button>
        </div>
      </div>
      <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
    </div>
  </div>
</div>

<script>
  $(document).ready(function () {
    <?php echo $this->element('Scripts/AppTaxRatesDefaultSettings'); ?>
    App.TaxRates.onDocReady();
  });
</script>