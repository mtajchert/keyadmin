<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <?php echo $this->Form->create('Shipping', array('class' => 'form-horizontal', 'type' => 'file')); ?>
      <div class="col-md-8">
        <fieldset class="fgmb15">
          <div class="form-group <?php echo $this->Form->isFieldError('name') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('name', array('label' => __('Nazwa: *'), 'placeholder' => __('Wpisz nazwę'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
          </div>
          <div class="form-group <?php echo $this->Form->isFieldError('shop_name') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('shop_name', array('label' => __('Nazwa w sklepie: *'), 'placeholder' => __('Wpisz nazwę wyświetlaną w sklepie'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
          </div>
          <div class="form-group <?php echo $this->Form->isFieldError('description') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('description', array('label' => __('Opis w sklepie:'), 'placeholder' => __('Wpisz opis wyświetlany w sklepie'), 'class' => 'form-control', 'rows' => 3, 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
          </div>
          <div class="form-group <?php echo $this->Form->isFieldError('info') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('info', array('label' => __('Informacje w potwierdzeniu zamówienia:'), 'placeholder' => __('Wpisz informacje wyświetlane w sklepie w podsumowaniu zamówienia'), 'class' => 'form-control', 'rows' => 3, 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
          </div>
          <div class="form-group <?php echo $this->Form->isFieldError('pkwiu') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('pkwiu', array('label' => __('PKWIU:'), 'placeholder' => __('Wpisz numer PKWIU'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
          </div>
        </fieldset>
        <fieldset class="fgmb15">
          <div class="form-group <?php echo $this->Form->isFieldError('max_weight') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->label('max_weight', __('Maks. waga przesyłki:')); ?>
            <?php echo $this->Form->input('max_weight', array('label' => false, 'placeholder' => __('Wpisz maksymalną wagę produktów'), 'class' => 'form-control', 'error' => false, 'div' => array('class' => 'input-group m-b'), 'after' => '<span class="input-group-addon">'.__('kg').'</span>')); ?>
            <?php echo $this->Form->isFieldError('max_weight') ? $this->Form->error('max_weight', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
          </div>
          <div class="form-group <?php echo $this->Form->isFieldError('max_price_tax') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->label('max_price_tax', __('Maks. wartość zamówienia:')); ?>
            <?php echo $this->Form->input('max_price_tax', array('label' => false, 'placeholder' => __('Wpisz maksymalną wartość zamówienia'), 'class' => 'form-control', 'error' => false, 'div' => array('class' => 'input-group m-b'), 'after' => '<span class="input-group-addon">'.__('zł').'</span>')); ?>
            <?php echo $this->Form->isFieldError('max_price_tax') ? $this->Form->error('max_price_tax', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
          </div>
          <div class="form-group <?php echo $this->Form->isFieldError('free_from') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->label('free_from', __('Darmowa dostawa od:')); ?>
            <?php echo $this->Form->input('free_from', array('label' => false, 'placeholder' => __('Wpisz od jakiej wartości dostawa jest darmowa'), 'class' => 'form-control', 'error' => false, 'div' => array('class' => 'input-group m-b'), 'after' => '<span class="input-group-addon">'.__('zł').'</span>')); ?>
            <?php echo $this->Form->isFieldError('free_from') ? $this->Form->error('free_from', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
          </div>
        </fieldset>
        <fieldset class="fgmb15">
          <div class="form-group <?php echo $this->Form->isFieldError('tax_rate_id') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('tax_rate_id', array('label' => __('Stawka VAT: *'), 'placeholder' => __('Wybierz stawkę VAT'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
          </div>
          <div class="form-group <?php echo $this->Form->isFieldError('price_type') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('price_type', array('label' => __('Rodzaj opłaty: *'), 'placeholder' => __('Wybierz rodzaj opłaty'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'options' => ['const' => __('Opłata stała'), 'depend_weight' => __('Opłata zależna od wagi produktów'), 'depend_price' => __('Opłata zależna od wartości zamówienia'), 'depend_amount' => __('Opłata zależna od ilości produktów')], 'empty' => 'Wybierz rodzaj opłaty', 'onchange' => 'App.Shippings.changePriceType($(this).val())')); ?>
          </div>
          
          <div class="form-group <?php echo $this->Form->isFieldError('price_tax') ? 'has-error' : ''; ?>" id="ShippingPriceConst" <?php if ($shipping['Shipping']['price_type'] != 'const') { echo 'style="display:none;"'; } ?>>
            <?php echo $this->Form->label('price_tax', __('Koszt wysyłki: *')); ?>
            <?php echo $this->Form->input('price_tax', array('label' => false, 'placeholder' => __('Wpisz koszt wysyłki'), 'class' => 'form-control', 'error' => false, 'div' => array('class' => 'input-group m-b'), 'after' => '<span class="input-group-addon">'.__('zł').'</span>')); ?>
            <?php echo $this->Form->isFieldError('price_tax') ? $this->Form->error('price_tax', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
          </div>
          
          <div class="form-group" id="ShippingPricesItems" <?php if ($shipping['Shipping']['price_type'] == 'const' || empty($shipping['Shipping']['price_type'])) { echo 'style="display:none;"'; } ?>>
            <?php echo $this->Form->label('price_tax', __('Koszt wysyłki: *')); ?>
            <div class="items">
              <?php if (isset($shipping['ShippingPrice']) && !empty($shipping['ShippingPrice']) && $shipping['Shipping']['price_type'] != 'const' && !empty($shipping['Shipping']['price_type'])): ?>
                <?php $first = true; ?>
                <?php foreach ($shipping['ShippingPrice'] as $key => $shippingPrice): ?>
                  <div class="item form-inline">
                    <div class="clearfix">
                      <?php echo $this->Form->input("ShippingPrice.{$key}.id", array('label' => false, 'class' => 'hide', 'error' => false, 'div' => false)); ?>
                      <div class="form-group col-xs-2X <?php echo $this->Form->isFieldError("ShippingPrice.{$key}.to_weight") || $this->Form->isFieldError("ShippingPrice.{$key}.to_price_tax") || $this->Form->isFieldError("ShippingPrice.{$key}.to_product_amount") ? 'has-error' : ''; ?>">
                        <?php if ($shipping['Shipping']['price_type'] == 'depend_weight'): ?>
                          <?php echo $this->Form->label("ShippingPrice.{$key}.to_weight", __('waga do: *'), array('class' => 'col-xs-12 control-label')); ?>
                        <?php elseif ($shipping['Shipping']['price_type'] == 'depend_price'): ?>
                          <?php echo $this->Form->label("ShippingPrice.{$key}.to_price_tax", __('wartość do: *'), array('class' => 'col-xs-12 control-label')); ?>
                        <?php elseif ($shipping['Shipping']['price_type'] == 'depend_amount'): ?>
                          <?php echo $this->Form->label("ShippingPrice.{$key}.to_product_amount", __('ilość do: *'), array('class' => 'col-xs-12 control-label')); ?>
                        <?php endif; ?>
                      </div>
                      <div class="form-group col-sm-3X <?php echo $this->Form->isFieldError("ShippingPrice.{$key}.to_weight") || $this->Form->isFieldError("ShippingPrice.{$key}.to_price_tax") || $this->Form->isFieldError("ShippingPrice.{$key}.to_product_amount") ? 'has-error' : ''; ?>">
                        <?php if ($shipping['Shipping']['price_type'] == 'depend_weight'): ?>
                          <?php echo $this->Form->input("ShippingPrice.{$key}.to_weight", array('label' => false, 'placeholder' => __('wpisz wagę'), 'class' => 'form-control', 'error' => false, 'div' => array('class' => 'input-group m-b'), 'after' => '<span class="input-group-addon">'.__('kg').'</span>')); ?>
                        <?php elseif ($shipping['Shipping']['price_type'] == 'depend_price'): ?>
                          <?php echo $this->Form->input("ShippingPrice.{$key}.to_price_tax", array('label' => false, 'placeholder' => __('wpisz wartość'), 'class' => 'form-control', 'error' => false, 'div' => array('class' => 'input-group m-b'), 'after' => '<span class="input-group-addon">'.__('zł').'</span>')); ?>
                        <?php elseif ($shipping['Shipping']['price_type'] == 'depend_amount'): ?>
                          <?php echo $this->Form->input("ShippingPrice.{$key}.to_product_amount", array('label' => false, 'placeholder' => __('wpisz ilość'), 'class' => 'form-control', 'error' => false, 'div' => array('class' => 'input-group m-b'), 'after' => '<span class="input-group-addon">'.__('j.m.').'</span>')); ?>
                        <?php endif; ?>
                      </div>
                      <div class="form-group col-xs-1X <?php echo $this->Form->isFieldError("ShippingPrice.{$key}.price_tax") ? 'has-error' : ''; ?>">
                        <?php echo $this->Form->label("ShippingPrice.{$key}.price_tax", __('cena: *'), array('class' => 'col-xs-12 control-label')); ?>
                      </div>
                      <div class="form-group col-sm-3X <?php echo $this->Form->isFieldError("ShippingPrice.{$key}.price_tax") ? 'has-error' : ''; ?>">
                        <?php echo $this->Form->input("ShippingPrice.{$key}.price_tax", array('label' => false, 'placeholder' => __('wpisz cenę'), 'class' => 'form-control', 'error' => false, 'div' => array('class' => 'input-group m-b'), 'after' => '<span class="input-group-addon">'.__('zł').'</span>')); ?>
                      </div>
                      <div class="form-group col-sm-3X rel-icons">
                        <?php if ($first): ?>
                          <?php $first = false; ?>
                        <?php else: ?>
                          <a title="<?php echo __('Usuń'); ?>" data-original-title="" href="javascript:void(0);" onclick="App.Shippings.deleteShippingPrice(this);"><em class="icon-close"></em></a>
                         <?php endif; ?>
                      </div>
                    </div>
                    <?php if ($this->Form->isFieldError("ShippingPrice.{$key}.to_weight") || $this->Form->isFieldError("ShippingPrice.{$key}.to_price_tax") || $this->Form->isFieldError("ShippingPrice.{$key}.to_product_amount") || $this->Form->isFieldError("ShippingPrice.{$key}.price_tax")): ?>
                      <div class="has-error has-error-box">
                        <?php echo $this->Form->isFieldError("ShippingPrice.{$key}.to_weight") ? $this->Form->error("ShippingPrice.{$key}.to_weight", null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
                        <?php echo $this->Form->isFieldError("ShippingPrice.{$key}.to_price_tax") ? $this->Form->error("ShippingPrice.{$key}.to_price_tax", null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
                        <?php echo $this->Form->isFieldError("ShippingPrice.{$key}.to_product_amount") ? $this->Form->error("ShippingPrice.{$key}.to_product_amount", null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
                        <?php echo $this->Form->isFieldError("ShippingPrice.{$key}.price_tax") ? $this->Form->error("ShippingPrice.{$key}.price_tax", null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
                      </div>
                    <?php endif; ?>
                  </div>
                <?php endforeach; ?>
              <?php endif; ?>
            </div>
            <div>
              <a title="<?php echo __('Dodaj zakres'); ?>" data-original-title="" href="javascript:void(0);" onclick="App.Shippings.addShippingPrice();"><em class="icon-plus"></em>&nbsp;Dodaj zakres</a>
            </div>
          </div>
        </fieldset>
        <fieldset class="fgmb15">
          <div class="row">
            <div class="col-sm-6 <?php echo $this->Form->isFieldError('payment_id') ? 'has-error' : ''; ?>">
              <label class="control-label"><?php echo __('Dostępne płatności:'); ?></label>
              <div class="checkbox c-checkbox <?php echo $this->Form->isFieldError('payment_id') || $this->Form->isFieldError('all_payments') ? 'has-error' : ''; ?>">
                <label>
                  <?php echo $this->Form->input('all_payments', array('type' => 'checkbox', 'label' => false, 'div' => false, 'class' => 'form-control', 'onchange' => 'App.Shippings.toggleAllCategories(this.checked);')); ?>
                  <span class="fa fa-check"></span><?php echo __('wszystkie płatności'); ?>
                </label>
              </div>
              <br/>
              
              <input name="data[Payment][Payment]" type="checkbox" value="" checked="checked" class="empty" style="display:none;">
              <div class="form-max-height-400 payments_list">
                <?php
                $payment_ids = [];
                if (isset($shipping['Payment'])) {
                  if (isset($shipping['Payment']['Payment'])) {
                    $payment_ids = (array)$shipping['Payment']['Payment'];
                  } else {
                    foreach ($shipping['Payment'] as $payment) {
                      $payment_ids[] = $payment['id'];
                    }
                  }
                }
                ?>
                <?php foreach ($payments as $payment_id => $payment_name): ?>
                  <div class="checkbox c-checkbox">
                    <label>
                      <input name="data[Payment][Payment][]" value="<?php echo h($payment_id); ?>" type="checkbox" <?php echo in_array($payment_id, $payment_ids) ? 'checked' : ''; ?>>
                      <span class="fa fa-check"></span><?php echo h($payment_name); ?>
                    </label>
                  </div>
                <?php endforeach; ?>
              </div>
              <?php echo $this->Form->isFieldError('payment_id') ? '<div class="has-error"><br/>'.$this->Form->error('payment_id', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')).'</div>' : ''; ?>
            </div>
            <div class="col-sm-6 <?php echo $this->Form->isFieldError('country_id') ? 'has-error' : ''; ?>">
              <label class="control-label"><?php echo __('Kraje dostawy:'); ?></label>
              <div class="checkbox c-checkbox <?php echo $this->Form->isFieldError('country_id') || $this->Form->isFieldError('all_countries') ? 'has-error' : ''; ?>">
                <label>
                  <?php echo $this->Form->input('all_countries', array('type' => 'checkbox', 'label' => false, 'div' => false, 'class' => 'form-control', 'onchange' => 'App.Shippings.toggleAllCountries(this.checked);')); ?>
                  <span class="fa fa-check"></span><?php echo __('wszystkie kraje'); ?>
                </label>
              </div>
              <br/>
              
              <input name="data[Country][Country]" type="checkbox" value="" checked="checked" class="empty" style="display:none;">
              <div class="form-max-height-400 countries_list">
                <?php
                $country_ids = [];
                if (isset($shipping['Country'])) {
                  if (isset($shipping['Country']['Country'])) {
                    $country_ids = (array)$shipping['Country']['Country'];
                  } else {
                    foreach ($shipping['Country'] as $country) {
                      $country_ids[] = $country['id'];
                    }
                  }
                }
                ?>
                <?php foreach ($countries as $country_id => $country_name): ?>
                  <div class="checkbox c-checkbox">
                    <label>
                      <input name="data[Country][Country][]" value="<?php echo h($country_id); ?>" type="checkbox" <?php echo in_array($country_id, $country_ids) ? 'checked' : ''; ?>>
                      <span class="fa fa-check"></span><?php echo h($country_name); ?>
                    </label>
                  </div>
                <?php endforeach; ?>
              </div>
              <?php echo $this->Form->isFieldError('country_id') ? '<div class="has-error"><br/>'.$this->Form->error('country_id', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')).'</div>' : ''; ?>
            </div>
          </div>
        </fieldset>
      </div>
      <div class="col-md-4">
        <div class="form-group form-group-margin-left-0 <?php echo $this->Form->isFieldError('status') ? 'has-error' : ''; ?>">
          <label class="col-xs-5 control-label text-left-force" for="exampleInputEmail1"><?php echo __('Status: *'); ?></label>
          <div class="col-xs-7">
            <label class="checkbox-inline c-checkbox">
              <?php echo $this->Form->input('status', array('type' => 'checkbox', 'label' => false, 'div' => false)); ?>
              <span class="fa fa-check"></span><?php echo __('aktywny'); ?>
            </label>
          </div>
        </div>
        <div class="form-group form-group-margin-left-0 <?php echo $this->Form->isFieldError('large_scale') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->label('large_scale', __('Gabaryt: *'), array('class' => 'col-xs-5 control-label text-left-force')); ?>
          <div class="col-xs-7">
            <label class="radio-inline c-radio">
              <input id="ShippingLargeScale1" name="data[Shipping][large_scale]" value="1" <?php echo $shipping['Shipping']['large_scale'] ? 'checked' : ''; ?> type="radio">
              <span class="fa fa-circle"></span><?php echo(__('tak')); ?>
            </label>
            <label class="radio-inline c-radio">
              <input id="ShippingLargeScale0" name="data[Shipping][large_scale]" value="0" <?php echo!$shipping['Shipping']['large_scale'] ? 'checked' : ''; ?> type="radio">
              <span class="fa fa-circle"></span><?php echo(__('nie')); ?>
            </label>
          </div>
        </div>
        <div class="form-group form-group-margin-left-0 <?php echo $this->Form->isFieldError('sort_order') ? 'has-error' : ''; ?>">
          <label class="col-xs-5 control-label text-left-force" for="exampleInputEmail1"><?php echo __('Kolejność sort.:'); ?></label>
          <div class="col-xs-7">
            <?php echo $this->Form->input('sort_order', array('min' => '0', 'step' => 1, 'label' => false, 'div' => false, 'class' => 'form-control')); ?>
          </div>
        </div>
        <div class="form-group form-group-margin-left-0">
          <div class="col-xs-12 img-box">
            <label><?php echo __('Logo:'); ?></label>
            <?php if (isset($this->data['Shipping']['image']) && !empty($this->data['Shipping']['image'])): ?>
              <?php echo $this->App->showShippingImage($this->data['Shipping']['image'], 'THUMB'); ?>
            <?php endif; ?>
            <input type="file" name="data[Shipping][image]" class="image-choosen" data-show-upload="false" data-show-caption="true"/>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="pull-right">
          <?php echo $this->Html->link(__('Anuluj'), array('controller' => 'KeyShippings', 'action' => 'index', 'admin' => true), array('title' => __('Anuluj'), 'class' => 'btn btn-default')); ?>
          <button type="submit" class="btn btn-primary"><?php echo __('Zapisz'); ?></button>
        </div>
      </div>
      <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
    </div>
  </div>
</div>

<script>
  $(document).ready(function () {
    App.Shippings.toggleAllCategories($('#ShippingAllPayments').is(':checked'));
    App.Shippings.toggleAllCountries($('#ShippingAllCountries').is(':checked'));
    
<?php echo $this->element('Scripts/AppShippingsDefaultSettings'); ?>
    App.Shippings.onDocReady();
  });
</script>