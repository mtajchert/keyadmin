<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th><?php echo $this->Paginator->sort('id', __('ID')); ?></th>
              <th><?php echo $this->Paginator->sort('name', __('Nazwa')); ?></th>
              <th><?php echo $this->Paginator->sort('price_type', __('Koszt dostawy')); ?></th>
              <th><?php echo $this->Paginator->sort('max_weight', __('Maks. waga')); ?></th>
              <th><?php echo $this->Paginator->sort('max_price_tax', __('Maks. cena')); ?></th>
              <th><?php echo $this->Paginator->sort('free_from', __('Darmowa od')); ?></th>
              <th><?php echo $this->Paginator->sort('large_scale', __('Gabaryt')); ?></th>
              <th><?php echo $this->Paginator->sort('sort_order', __('Kolejność sort.')); ?></th>
              <th><?php echo $this->Paginator->sort('status', __('Status')); ?></th>
              <th><?php echo __('Opcje'); ?></th>
            </tr>
          </thead>
          <tbody>
            <?php if (isset($shippings) && !empty($shippings)): ?>
              <?php foreach ($shippings as $shipping): ?>
                <tr>
                  <td><?php echo h($shipping['Shipping']['id']); ?>&nbsp;</td>
                  <td><?php echo h($shipping['Shipping']['name']); ?>&nbsp;</td>
                  <td><?php
                    $min = 10000000;
                    $max = 0;
                    foreach ($shipping['ShippingPrice'] as $shippingPrice) {
                      $min = $shippingPrice['price_tax'] < $min ? $shippingPrice['price_tax'] : $min;
                      $max = $shippingPrice['price_tax'] > $max ? $shippingPrice['price_tax'] : $max;
                    }
                    switch ($shipping['Shipping']['price_type']) {
                      case 'const':
                        echo __('Opłata stała').'<br/>'.$this->Number->precision($shipping['Shipping']['price_tax'], 2).'&nbsp;'.__('zł');
                        break;
                      case 'depend_weight':
                        echo __('Zależna od wagi zamówienia').'<br/>'.__('od').'&nbsp;'.$this->Number->precision($min, 2).'&nbsp;'.__('zł').'&nbsp;'.__('do').'&nbsp;'.$this->Number->precision($max, 2);
                        break;
                      case 'depend_price':
                        echo __('Zależna od wartości zamówienia').'<br/>'.__('od').'&nbsp;'.$this->Number->precision($min, 2).'&nbsp;'.__('zł').'&nbsp;'.__('do').'&nbsp;'.$this->Number->precision($max, 2);
                        break;
                      case 'depend_amount':
                        echo __('Zależna od ilości produktów').'<br/>'.__('od').'&nbsp;'.$this->Number->precision($min, 2).'&nbsp;'.__('zł').'&nbsp;'.__('do').'&nbsp;'.$this->Number->precision($max, 2);
                        break;
                    }
                  ?>&nbsp;</td>
                  <td><?php echo is_null($shipping['Shipping']['max_weight']) ? '-' : $this->Number->precision($shipping['Shipping']['max_weight'], 2).'&nbsp;'.__('kg'); ?>&nbsp;</td>
                  <td><?php echo is_null($shipping['Shipping']['max_price_tax']) ? '-' : $this->Number->precision($shipping['Shipping']['max_price_tax'], 2).'&nbsp;'.__('zł'); ?>&nbsp;</td>
                  <td><?php echo is_null($shipping['Shipping']['free_from']) ? '-' : $this->Number->precision($shipping['Shipping']['free_from'], 2).'&nbsp;'.__('zł'); ?>&nbsp;</td>
                  <td><?php echo __($shipping['Shipping']['large_scale'] ? 'tak' : 'nie'); ?>&nbsp;</td>
                  <td>
                    <input class="form-control sort-order-input input-sm" type="text" value="<?php echo h($shipping['Shipping']['sort_order']); ?>" onchange="App.Shippings.saveSortOrder(<?php echo h($shipping['Shipping']['id']); ?>, this.value);">
                  </td>
                  <td>
                    <span class="checkbox c-checkbox c-checkbox-rounded c-checkbox-no-label">
                      <input <?php echo $shipping['Shipping']['status'] ? 'checked' : ''; ?> type="checkbox" onchange="App.Shippings.saveStatus(<?php echo h($shipping['Shipping']['id']); ?>, this.checked ? 1 : 0);">
                      <span class="fa fa-check" onclick="$(this).parent().find('input').click();"></span>
                    </span>
                  </td>
                  <td class="options-buttons text-center">
                    <?php echo $this->Html->link('<em class="icon-note"></em>', array('controller' => 'KeyShippings', 'action' => 'edit', $shipping['Shipping']['id']), array('escape' => false, 'title' => __('Edytuj'))); ?>
                    &nbsp;
                    <?php echo $this->Html->link('<em class="icon-trash"></em>', 'javascript:void(0);', array('escape' => false, 'title' => __('Usuń'), 'onclick' => 'App.Shippings.deleteShipping(\''.h($shipping['Shipping']['name']).'\', \''.$this->Html->url(array('controller' => 'KeyShippings', 'action' => 'delete', $shipping['Shipping']['id'])).'\')')); ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <td colspan="10"><?php echo __('Brak form dostawy do wyświetlenia'); ?></td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="panel-footer text-center">
      <div class="col-sm-6">
        <div class="pull-left">
          <?php echo $this->Paginator->counter(array('format' => __('Strona {:page} z {:pages}'))); ?>
        </div>
      </div>
      <div class="col-sm-6 pagination pagination-large">
        <ul class="pagination pull-right">
          <?php
          $this->Paginator->options['url'] = array('controller' => 'KeyShippings', 'action' => 'index');
          echo $this->Paginator->prev(__('poprzednia'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1, 'modulus' => 5, 'ellipsis' => '<li class="disabled"><a>...</a></li>'));
          echo $this->Paginator->next(__('następna'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          ?>
        </ul>
      </div>
      <span class="clearfix"></span>
    </div>
  </div>
</div>

<script>
  $(document).ready(function () {
    <?php echo $this->element('Scripts/AppShippingsDefaultSettings'); ?>
    App.Shippings.onDocReady();
  });
</script>