<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <?php echo $this->Form->create('Product', array('class' => 'form-horizontal')); ?>
      <div class="col-md-6">
        <?php echo $this->Form->input('id', array('label' => false, 'div' => false, 'type' => 'hidden')); ?>
        <div class="form-group <?php echo $this->Form->isFieldError('id') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->label('name', __('Produkt: *')); ?>
          <?php echo $this->Form->input('name', array('label' => false, 'placeholder' => __('Wybierz produkt'), 'readonly' => 'true', 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'div' => array('class' => (isset($create) && $create) ? 'input-group m-b' : ''), 'after' => (isset($create) && $create) ? '<span class="input-group-btn"><button type="button" class="btn btn-default" onclick="App.Promotions.openChooseProductBox();">'.__('wybierz').'</button></span>' : '')); ?>
          <?php echo $this->Form->isFieldError('id') ? $this->Form->error('id', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
        </div>
        <div class="form-group <?php echo $this->Form->isFieldError('old_price') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->label('old_price', __('Poprzednia cena: *')); ?>
          <?php echo $this->Form->input('old_price', array('label' => false, 'placeholder' => __('Wpisz poprzednią cenę'), 'class' => 'form-control', 'error' => false, 'div' => array('class' => 'input-group m-b'), 'after' => '<span class="input-group-addon">'.__('zł').'</span>')); ?>
          <?php echo $this->Form->isFieldError('old_price') ? $this->Form->error('old_price', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
        </div>
        <div class="form-group <?php echo $this->Form->isFieldError('price_tax') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->label('price_tax', __('Nowa cena brutto: *')); ?>
          <?php echo $this->Form->input('price_tax', array('label' => false, 'placeholder' => __('Wpisz nową cenę brutto'), 'class' => 'form-control', 'error' => false, 'div' => array('class' => 'input-group m-b'), 'after' => '<span class="input-group-addon">'.__('zł').'</span>')); ?>
          <?php echo $this->Form->isFieldError('price_tax') ? $this->Form->error('price_tax', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group form-group-margin-left-0 <?php echo $this->Form->isFieldError('promotion_status') ? 'has-error' : ''; ?>">
          <label class="col-xs-5 control-label text-left-force" for="ProductPromotionStatus"><?php echo __('Status:'); ?></label>
          <div class="col-xs-7">
            <label class="checkbox-inline c-checkbox">
              <?php echo $this->Form->input('promotion_status', array('type' => 'checkbox', 'label' => false, 'div' => false)); ?>
              <span class="fa fa-check"></span><?php echo __('aktywna'); ?>
            </label>
          </div>
        </div>
        <div class="form-group form-group-margin-left-0">
          <label class="col-xs-5 control-label text-left-force" for="ProductPromotionDate"><?php echo __('Data rozpoczęcia:'); ?></label>
          <div class="col-xs-7 <?php echo $this->Form->isFieldError('promotion_date') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('promotion_date', array('type' => 'text', 'label' => false, 'placeholder' => __('Wybierz datę rozpoczęcia promocji'), 'class' => 'form-control', 'div' => array('class' => 'input-group date', 'id' => 'ProductPromotionDateBox'), 'error' => false, 'after' => '<span class="input-group-addon"><span class="fa fa-calendar"></span></span>')); ?>
            <?php echo $this->Form->isFieldError('promotion_date') ? $this->Form->error('promotion_date', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
            </label>
          </div>
        </div>
        <div class="form-group form-group-margin-left-0">
          <label class="col-xs-5 control-label text-left-force" for="ProductPromotionDateEnd"><?php echo __('Data zakończenia:'); ?></label>
          <div class="col-xs-7 <?php echo $this->Form->isFieldError('promotion_date_end') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('promotion_date_end', array('type' => 'text', 'label' => false, 'placeholder' => __('Wybierz datę zakończenia promocji'), 'class' => 'form-control', 'div' => array('class' => 'input-group date', 'id' => 'ProductPromotionDateEndBox'), 'error' => false, 'after' => '<span class="input-group-addon"><span class="fa fa-calendar"></span></span>')); ?>
            <?php echo $this->Form->isFieldError('promotion_date_end') ? $this->Form->error('promotion_date_end', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
            </label>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="pull-right">
          <?php echo $this->Html->link(__('Anuluj'), array('controller' => 'KeyPromotions', 'action' => 'index', 'admin' => true), array('title' => __('Anuluj'), 'class' => 'btn btn-default')); ?>
          <button type="submit" class="btn btn-primary"><?php echo __('Zapisz'); ?></button>
        </div>
      </div>
      <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
    </div>
  </div>
</div>

<script>
  $(document).ready(function () {
    $('#ProductPromotionDateBox, #ProductPromotionDateEndBox').datetimepicker({
      icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-arrow-up",
        down: "fa fa-arrow-down"
      },
      locale: 'pl',
      format: 'YYYY-MM-DD HH:mm'
    });

    <?php echo $this->element('Scripts/AppPromotionsDefaultSettings'); ?>
    
    <?php echo $this->element('Scripts/AppCategoriesTreeDefaultSettings'); ?>
    App.CategoriesTree.chooseMode = true;
    
    App.Promotions.onDocReady();
    App.CategoriesTree.onDocReady();
  });
</script>