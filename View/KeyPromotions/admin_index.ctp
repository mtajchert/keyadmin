<div class="col-sm-12">
  <?php echo $this->Form->create('Promotions', array('class' => 'panel panel-default')); ?>
    <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th data-check-all="">
                <div title="" data-original-title="" data-toggle="tooltip" data-title="<?php echo __('Zaznacz wszystkie'); ?>" class="checkbox c-checkbox">
                  <label>
                    <input type="checkbox">
                    <span class="fa fa-check"></span>
                  </label>
                </div>
              </th>
              <th><?php echo $this->Paginator->sort('id', __('ID')); ?></th>
              <th><?php echo __('Zdjęcie'); ?></th>
              <th><?php echo $this->Paginator->sort('name', __('Produkt')); ?></th>
              <th><?php echo $this->Paginator->sort('promotion_date', __('Data rozpoczęcia')); ?></th>
              <th><?php echo $this->Paginator->sort('promotion_date_end', __('Data zakończenia')); ?></th>
              <th><?php echo $this->Paginator->sort('price_tax', __('Cena')); ?></th>
              <th><?php echo $this->Paginator->sort('promotion_status', __('Status promocji')); ?></th>
              <th><?php echo $this->Paginator->sort('status', __('Status produktu')); ?></th>
              <th><?php echo __('Opcje'); ?></th>
            </tr>
          </thead>
          <tbody>
            <?php if (isset($products) && !empty($products)): ?>
              <?php foreach ($products as $product): ?>
                <tr>
                  <td>
                    <div class="checkbox c-checkbox">
                      <label>
                         <input type="checkbox" name="check_product_id[]" value="<?php echo h($product['Product']['id']); ?>">
                         <span class="fa fa-check"></span>
                      </label>
                    </div>
                  </td>
                  <td><?php echo h($product['Product']['id']); ?></td>
                  <td>
                    <?php if (!empty($product['ProductsImage'])): ?>
                      <?php echo $this->App->showProductsImage($product['ProductsImage'][0]['image'], 'LIST'); ?>
                    <?php endif; ?>
                  </td>
                  <td>
                    <strong><?php echo h($product['Product']['name']); ?></strong><br/>

                    <?php
                    $categories = array();
                    foreach ($product['Category'] as $category) {
                      $categories[] = $category['name'];
                    }
                    ?>

                    <?php echo __('Kategoria:').'&nbsp;'.h(implode(', ', $categories)); ?>
                    <?php if (!empty($product['Product']['catalog_no'])): ?>
                      <br/>
                      <?php echo __('Nr kat.:').'&nbsp;'.h($product['Product']['catalog_no']); ?>
                    <?php endif; ?>
                    <?php if (!empty($product['Product']['ext_code'])): ?>
                      <br/>
                      <?php echo __('Kod prod.:').'&nbsp;'.h($product['Product']['ext_code']); ?>
                    <?php endif; ?>
                    <?php if (isset($product['Manufacturer']) && !empty($product['Manufacturer'])): ?>
                      <br/>
                      <?php echo __('Producent:').'&nbsp;'.h($product['Manufacturer']['name']); ?>
                    <?php endif; ?>
                  </td>
                  <td><?php echo strtotime($product['Product']['promotion_date']) > 0 ? h($this->Time->format($product['Product']['promotion_date'], '%Y-%m-%d %H:%M')) : '-'; ?>&nbsp;</td>
                  <td><?php echo strtotime($product['Product']['promotion_date_end']) > 0 ? h($this->Time->format($product['Product']['promotion_date_end'], '%Y-%m-%d %H:%M')) : '-'; ?>&nbsp;</td>
                  <td>
                    <?php if ($product['Product']['old_price'] > 0) : ?>
                      <span class="no-matter"><?php echo h($product['Product']['old_price']); ?></span>
                    <?php endif; ?>
                    <?php echo h($product['Product']['price_tax']); ?>
                  </td>
                  <td>
                    <span class="checkbox c-checkbox c-checkbox-rounded c-checkbox-no-label">
                      <input <?php echo $product['Product']['promotion_status'] ? 'checked' : ''; ?> type="checkbox" onchange="App.Promotions.savePromotionStatus(<?php echo h($product['Product']['id']); ?>, this.checked ? 1 : 0);">
                      <span class="fa fa-check" data-toggle="tooltip" data-title="<?php echo __('Zmień status promocji'); ?>" onclick="$(this).parent().find('input').click();"></span>
                    </span>
                  </td>
                  <td>
                    <span class="checkbox c-checkbox c-checkbox-rounded c-checkbox-no-label">
                      <input <?php echo $product['Product']['status'] ? 'checked' : ''; ?> type="checkbox" onchange="App.Promotions.saveProductStatus(<?php echo h($product['Product']['id']); ?>, this.checked ? 1 : 0);">
                      <span class="fa fa-check" data-toggle="tooltip" data-title="<?php echo __('Zmień status produktu'); ?>" onclick="$(this).parent().find('input').click();"></span>
                    </span>
                  </td>
                  <td class="options-buttons text-center">
                    <?php echo $this->Html->link('<em class="icon-note"></em>', array('controller' => 'KeyPromotions', 'action' => 'edit', $product['Product']['id']), array('escape' => false, 'data-title' => __('Edytuj promocję'), 'data-toggle' => 'tooltip')); ?>
                    &nbsp;
                    <?php echo $this->Html->link('<em class="icon-trash"></em>', 'javascript:void(0);', array('escape' => false, 'data-title' => __('Usuń promocję'), 'data-toggle' => 'tooltip', 'onclick' => 'App.Promotions.deletePromotion(\''.h($product['Product']['name']).'\', \''.$this->Html->url(array('controller' => 'KeyPromotions', 'action' => 'delete', $product['Product']['id'])).'\')')); ?>
                    &nbsp;
                    <?php echo $this->Html->link('<em class="icon-note"></em>', array('controller' => 'KeyProducts', 'action' => 'edit', $product['Product']['id']), array('escape' => false, 'data-title' => __('Edytuj produkt'), 'data-toggle' => 'tooltip')); ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <td colspan="10" class="text-center"><?php echo __('Brak promocji do wyświetlenia'); ?></td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="panel-footer text-center">
      <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-8">
            <div class="input-group pull-right">
              <select class="input-sm form-control" name="bulk_action" id="bulk_action">
                  <option value="" selected disabled><?php echo __('Wybierz akcję'); ?></option>
                  <option value="delete_promotions_not_restore_price"><?php echo __('usuń promocje bez zmian cen produktów'); ?></option>
                  <option value="delete_promotions_and_restore_price"><?php echo __('usuń promocje i przywróć poprzednie ceny produktów'); ?></option>
                  <option value="deactivate_promotions"><?php echo __('zmień status promocji na nieaktywne'); ?></option>
                  <option value="activate_promotions"><?php echo __('zmień status promocji na aktywne'); ?></option>
                  <option value="deactivate_products"><?php echo __('zmień status produktów na nieaktywne'); ?></option>
                  <option value="activate_products"><?php echo __('zmień status produktów na aktywne'); ?></option>
                  <option value="delete_products"><?php echo __('usuń produkty'); ?></option>
                  <option value="clear_promotions_date"><?php echo __('wyzeruj datę rozpoczęcia promocji'); ?></option>
                  <option value="clear_promotions_date_end"><?php echo __('wyzeruj datę zakończenia promocji'); ?></option>
                  <option value="manipulate_promotions_date"><?php echo __('dodaj/odejmij ilość dni do daty rozpoczęcia promocji'); ?></option>
                  <option value="manipulate_promotions_date_end"><?php echo __('dodaj/odejmij ilość dni do daty zakończenia promocji'); ?></option>
                  <option value="manipulate_promotions_old_price_amount"><?php echo __('zwiększ/zmniejsz poprzednie ceny o x zł'); ?></option>
                  <option value="manipulate_promotions_old_price_percent"><?php echo __('zwiększ/zmniejsz poprzednie ceny o x %'); ?></option>
               </select>
               <span class="input-group-btn">
                  <a class="btn btn-sm btn-default" href="javascript:void(0);" onclick="App.Promotions.runBulkAction();"><?php echo __('Wykonaj'); ?></a>
               </span>
            </div>
         </div>
      </div>
      &nbsp;
      <div class="row">
        <div class="col-sm-6">
          <div class="pull-left">
            <?php echo $this->Paginator->counter(array('format' => __('Strona {:page} z {:pages}'))); ?>
          </div>
        </div>
        <div class="col-sm-6 pagination pagination-large">
          <ul class="pagination pull-right">
            <?php
            $this->Paginator->options['url'] = array('controller' => 'KeyPromotions', 'action' => 'index');
            echo $this->Paginator->prev(__('poprzednia'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
            echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1, 'modulus' => 5, 'ellipsis' => '<li class="disabled"><a>...</a></li>'));
            echo $this->Paginator->next(__('następna'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
            ?>
          </ul>
        </div>
      </div>
      <span class="clearfix"></span>
    </div>
  <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
</div>

<script>
  $(document).ready(function () {
    <?php echo $this->element('Scripts/AppPromotionsDefaultSettings'); ?>
    
    App.Promotions.onDocReady();
  });
</script>