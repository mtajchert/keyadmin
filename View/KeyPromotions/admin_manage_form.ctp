<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <?php echo $this->Form->create('PromotionManage', array('class' => 'form-horizontal')); ?>
      <div class="col-md-12">
        <fieldset>
          <legend><?php echo __('Podstawowe parametry'); ?></legend>
          <div class="form-group <?php echo $this->Form->isFieldError('action') ? 'has-error' : ''; ?>">
            <label class="col-sm-2 control-label"><?php echo __('Akcja: *'); ?></label>
            <div class="col-sm-10">
              <label class="radio-inline c-radio">
                <input name="data[PromotionManage][action]" value="create" type="radio" <?php echo isset($data['PromotionManage']['action']) && $data['PromotionManage']['action'] == 'create' ? 'checked' : ''; ?> onchange="App.Form.toggleShow(this.checked, '.ts_action_create', '.ts_action_delete:not(.ts_action_create)'); App.Form.toggleShow($('input[name=\'data[PromotionManage][price_change_kind]\']:checked').val() !== undefined && $('input[name=\'data[PromotionManage][price_change_kind]\']:checked').val() !== 'no_change', '.ts_price_modify', 'xxx');">
                <span class="fa fa-circle"></span><?php echo __('dodaj/zmień promocje'); ?>
              </label>
              <label class="radio-inline c-radio">
                <input name="data[PromotionManage][action]" value="delete" type="radio" <?php echo isset($data['PromotionManage']['action']) && $data['PromotionManage']['action'] == 'delete' ? 'checked' : ''; ?> onchange="App.Form.toggleShow(this.checked, '.ts_action_delete', '.ts_action_create:not(.ts_action_delete), .ts_price_modify:not(.ts_action_delete)');">
                <span class="fa fa-circle"></span><?php echo __('usuń promocje'); ?>
              </label>
              <?php echo $this->Form->isFieldError('action') ? $this->Form->error('action', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
            </div>
          </div>
        </fieldset>
        <fieldset class="ts_action_delete" style="display:none;">
          <div class="form-group <?php echo $this->Form->isFieldError('restore_price') ? 'has-error' : ''; ?>">
            <label class="col-sm-2 control-label"><?php echo __('Zmiana cen: *'); ?></label>
            <div class="col-sm-10">
              <div class="checkbox c-checkbox">
                <label>
                  <?php echo $this->Form->input('restore_price', array('label' => false, 'div' => false, 'class' => 'form-control', 'error' => false)); ?>
                  <span class="fa fa-check"></span><?php echo __('Przywróć poprzednie ceny produktów'); ?>
                </label>
              </div>
              <?php echo $this->Form->isFieldError('restore_price') ? '<div class="has-error">'.$this->Form->error('restore_price', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')).'</div>' : ''; ?>
            </div>
          </div>
        </fieldset>
        <fieldset class="ts_action_create" style="display:none;">
          <div class="form-group <?php echo $this->Form->isFieldError('price_change_kind') ? 'has-error' : ''; ?>">
            <label class="col-sm-2 control-label"><?php echo __('Zmiana cen: *'); ?></label>
            <div class="col-sm-10">
              <div class="radio c-radio">
                <label>
                  <input name="data[PromotionManage][price_change_kind]" value="no_change" type="radio" <?php echo isset($data['PromotionManage']['price_change_kind']) && $data['PromotionManage']['price_change_kind'] == 'no_change' ? 'checked' : ''; ?> onchange="App.Form.toggleShow(this.checked, 'xxx', '.ts_price_change_kind_price_tax, .ts_price_change_kind_old_price');">
                  <span class="fa fa-circle"></span><?php echo __('bez zmian'); ?>
                </label>
              </div>
              <div class="radio c-radio">
                <label>
                  <input name="data[PromotionManage][price_change_kind]" value="price_tax" type="radio" <?php echo isset($data['PromotionManage']['price_change_kind']) && $data['PromotionManage']['price_change_kind'] == 'price_tax' ? 'checked' : ''; ?> onchange="App.Form.toggleShow(this.checked, '.ts_price_change_kind_price_tax', '.ts_price_change_kind_old_price:not(.ts_price_change_kind_price_tax)');">
                  <span class="fa fa-circle"></span><?php echo __('ustaw aktualną cenę (aktualna cena zostanie przeniesiona do ceny poprzedniej)'); ?>
                </label>
              </div>
              <div class="radio c-radio">
                <label>
                  <input name="data[PromotionManage][price_change_kind]" value="old_price" type="radio" <?php echo isset($data['PromotionManage']['price_change_kind']) && $data['PromotionManage']['price_change_kind'] == 'old_price' ? 'checked' : ''; ?> onchange="App.Form.toggleShow(this.checked, '.ts_price_change_kind_old_price', '.ts_price_change_kind_price_tax:not(.ts_price_change_kind_old_price)');">
                  <span class="fa fa-circle"></span><?php echo __('ustaw poprzednią cenę (zostanie jedynie ustawiona cena poprzednia na podstawie aktualnej ceny)'); ?>
                </label>
              </div>
              <?php echo $this->Form->isFieldError('price_change_kind') ? $this->Form->error('price_change_kind', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
            </div>
          </div>
        </fieldset>
        <fieldset class="ts_price_modify ts_price_change_kind_price_tax ts_price_change_kind_old_price" style="display:none;">
          <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo __('Modyfikacja ceny:'); ?></label>
            <div class="col-sm-10">
              <p class="ts_price_change_kind_price_tax" style="display:none;"><?php echo __('Aktualna cena zostanie zmodyfikowana wg poniższych kryteriów'); ?></p>
              <p class="ts_price_change_kind_old_price" style="display:none;"><?php echo __('Poprzednia cena zostanie zmodyfikowana względem aktualnej ceny wg poniższych kryteriów'); ?></p>
              <div class="form-inline">
                <div class="inline <?php echo $this->Form->isFieldError('price_change_value') ? 'has-error' : ''; ?>">
                  <span class="text-control">Zmień cenę o&nbsp;</span>
                  <?php echo $this->Form->input('price_change_value', array('label' => false, 'div' => false, 'placeholder' => __('Wpisz wartość'), 'class' => 'form-control', 'step' => 0.01, 'error' => false, 'onkeyup' => '$(this).attr(\'value\', this.value)')); ?>
                </div>
                <div class="inline <?php echo $this->Form->isFieldError('price_change_type') ? 'has-error' : ''; ?>">
                  <div class="radio c-radio">
                    <label>
                      <input name="data[PromotionManage][price_change_type]" value="percent" type="radio" onchange="this.checked ? $('input[name=\'data[PromotionManage][price_change_value]\']').attr('step', '0.00001') : void(0)" <?php echo isset($data['PromotionManage']['price_change_type']) && $data['PromotionManage']['price_change_type'] == 'percent' ? 'checked' : ''; ?>>
                      <span class="fa fa-circle"></span><?php echo __('%'); ?>
                    </label>
                  </div>
                  <div class="radio c-radio">
                    <label>
                      <input name="data[PromotionManage][price_change_type]" value="value" type="radio" onchange="this.checked ? $('input[name=\'data[PromotionManage][price_change_value]\']').attr('step', '0.01') : void(0)" <?php echo isset($data['PromotionManage']['price_change_type']) && $data['PromotionManage']['price_change_type'] == 'value' ? 'checked' : ''; ?>>
                      <span class="fa fa-circle"></span><?php echo __('zł'); ?>
                    </label>
                  </div>
                </div>
              </div>
              <?php echo $this->Form->isFieldError('price_change_value') ? '<div class="has-error">'.$this->Form->error('price_change_value', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')).'</div>' : ''; ?>
              <?php echo $this->Form->isFieldError('price_change_type') ? '<div class="has-error">'.$this->Form->error('price_change_type', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')).'</div>' : ''; ?>
              <div class="checkbox c-checkbox">
                <label>
                  <?php echo $this->Form->input('round_price', array('label' => false, 'div' => false, 'class' => 'form-control', 'error' => false)); ?>
                  <span class="fa fa-check"></span><?php echo __('Zaokrąglij wyliczone ceny do pełnej kwoty'); ?>
                </label>
              </div>
              <?php echo $this->Form->isFieldError('round_price') ? '<div class="has-error">'.$this->Form->error('round_price', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')).'</div>' : ''; ?>
            </div>
          </div>
        </fieldset>
        <fieldset class="ts_action_create" style="display:none;">
          <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo __('Data rozpoczęcia:'); ?></label>
            <div class="col-sm-10">
              <div class="form-inline <?php echo $this->Form->isFieldError('promotion_date') ? 'has-error' : ''; ?>">
                <?php echo $this->Form->input('promotion_date', array('type' => 'text', 'label' => false, 'placeholder' => __('Wybierz datę rozpoczęcia promocji'), 'class' => 'form-control', 'div' => array('class' => 'input-group date', 'id' => 'PromotionManagePromotionDateBox'), 'error' => false, 'after' => '<span class="input-group-addon"><span class="fa fa-calendar"></span></span>')); ?>
                <div class="checkbox-inline c-checkbox">
                  <label class="no-bold">
                    <?php echo $this->Form->input('no_change_promotion_date', array('label' => false, 'div' => false, 'class' => 'form-control', 'error' => false, 'onchange' => '$(this).closest(\'.form-group\').find(\'input[type=text]\').prop(\'disabled\', this.checked)')); ?>
                    <span class="fa fa-check"></span>Nie ustawiaj/nie zmieniaj daty rozpoczęcia promocji
                  </label>
                </div>
                <?php echo $this->Form->isFieldError('promotion_date') ? $this->Form->error('promotion_date', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
              </div>
            </div>
          </div>
          <br/>
          <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo __('Data zakończenia:'); ?></label>
            <div class="col-sm-10">
              <div class="form-inline <?php echo $this->Form->isFieldError('promotion_date_end') ? 'has-error' : ''; ?>">
                <?php echo $this->Form->input('promotion_date_end', array('type' => 'text', 'label' => false, 'placeholder' => __('Wybierz datę zakończenia promocji'), 'class' => 'form-control', 'div' => array('class' => 'input-group date', 'id' => 'PromotionManagePromotionDateEndBox'), 'error' => false, 'after' => '<span class="input-group-addon"><span class="fa fa-calendar"></span></span>')); ?>
                <div class="checkbox-inline c-checkbox">
                  <label class="no-bold">
                    <?php echo $this->Form->input('no_change_promotion_date_end', array('label' => false, 'div' => false, 'class' => 'form-control', 'error' => false, 'onchange' => '$(this).closest(\'.form-group\').find(\'input[type=text]\').prop(\'disabled\', this.checked)')); ?>
                    <span class="fa fa-check"></span>Nie ustawiaj/nie zmieniaj daty zakończenia promocji
                  </label>
                </div>
                <?php echo $this->Form->isFieldError('promotion_date_end') ? $this->Form->error('promotion_date_end', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
              </div>
            </div>
          </div>
        </fieldset>
        
        <fieldset class="ts_action_create ts_action_delete" style="display:none;">
          <legend><?php echo __('Wybór produktów'); ?></legend>
          <div class="radio c-radio <?php echo $this->Form->isFieldError('product_status') ? 'has-error' : ''; ?>">
            <label>
              <input name="data[PromotionManage][product_status]" value="only_active" type="radio" <?php echo isset($data['PromotionManage']['product_status']) && $data['PromotionManage']['product_status'] == 'only_active' ? 'checked' : ''; ?>>
              <span class="fa fa-circle"></span><?php echo __('tylko aktywne produkty'); ?>
            </label>
          </div>
          <div class="radio c-radio <?php echo $this->Form->isFieldError('product_status') ? 'has-error' : ''; ?>">
            <label>
              <input name="data[PromotionManage][product_status]" value="only_unactive" type="radio" <?php echo isset($data['PromotionManage']['product_status']) && $data['PromotionManage']['product_status'] == 'only_unactive' ? 'checked' : ''; ?>>
              <span class="fa fa-circle"></span><?php echo __('tylko nieaktywne produkty'); ?>
            </label>
          </div>
          <?php echo $this->Form->isFieldError('product_status') ? '<div class="has-error">'.$this->Form->error('product_status', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')).'</div>' : ''; ?>
          
          <div class="checkbox c-checkbox <?php echo $this->Form->isFieldError('without_promotion') ? 'has-error' : ''; ?>">
            <label>
              <?php echo $this->Form->input('without_promotion', array('type' => 'checkbox', 'label' => false, 'div' => false, 'class' => 'form-control')); ?>
              <span class="fa fa-check"></span><?php echo __('tylko produkty aktualnie bez promocji'); ?>
            </label>
          </div>
          <?php echo $this->Form->isFieldError('without_promotion') ? '<div class="has-error">'.$this->Form->error('without_promotion', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')).'</div>' : ''; ?>
        </fieldset>
        <fieldset class="ts_action_create ts_action_delete" style="display:none;">
          <div class="form-inline <?php echo $this->Form->isFieldError('price_tax_from') || $this->Form->isFieldError('price_tax_to') ? 'has-error' : ''; ?>">
            <span><?php echo __('Tylko produkty z ceną brutto od'); ?></span>
            &nbsp;
            <?php echo $this->Form->input('price_tax_from', array('label' => false, 'div' => false, 'error' => false, 'class' => 'form-control')); ?>
            &nbsp;<?php echo __('do'); ?>&nbsp;
            <?php echo $this->Form->input('price_tax_to', array('label' => false, 'div' => false, 'error' => false, 'class' => 'form-control')); ?>
            <?php echo $this->Form->isFieldError('price_tax_from') ? '<div class="has-error">'.$this->Form->error('price_tax_from', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')).'</div>' : ''; ?>
            <?php echo $this->Form->isFieldError('price_tax_to') ? '<div class="has-error">'.$this->Form->error('price_tax_to', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')).'</div>' : ''; ?>
          </div>
          <br/>
          <div class="form-inline">
            <span><?php echo __('Tylko produkty z tekstem'); ?></span>
            &nbsp;
            <?php echo $this->Form->input('name_contain', array('label' => false, 'div' => false, 'error' => false, 'class' => 'form-control')); ?>
            &nbsp;<?php echo __('w nazwie'); ?>
            <?php echo $this->Form->isFieldError('name_contain') ? '<div class="has-error">'.$this->Form->error('name_contain', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')).'</div>' : ''; ?>
          </div>
        </fieldset>
        <fieldset class="ts_action_create ts_action_delete" style="display:none;">
          <div class="row">
            <div class="col-sm-6 <?php echo $this->Form->isFieldError('categories') ? 'has-error' : ''; ?>">
              <label class="control-label"><?php echo __('Wybierz kategorie:'); ?></label>
              <div class="checkbox c-checkbox <?php echo $this->Form->isFieldError('all_categories') ? 'has-error' : ''; ?>">
                <label>
                  <?php echo $this->Form->input('all_categories', array('type' => 'checkbox', 'label' => false, 'div' => false, 'class' => 'form-control', 'onchange' => 'App.Form.toggleShow(this.checked, \'xxx\', \'.categories_list\'); App.Form.toggleShow(!this.checked, \'.categories_list\', \'xxx\');')); ?>
                  <span class="fa fa-check"></span><?php echo __('wszystkie kategorie'); ?>
                </label>
              </div>
              <br/>
              <div class="form-max-height-400 categories_list" style="display:none;">
                <p id="categories_tree_loading"><?php echo __('Pobieranie kategorii...'); ?></p>
                <ul id="categories_tree"></ul>
                <?php if (isset($data['PromotionManage']['categories']) && !empty($data['PromotionManage']['categories'])): ?>
                  <?php foreach ($data['PromotionManage']['categories'] as $category): ?>
                    <input name="data[PromotionManage][categories][]" value="<?php echo h($category); ?>" type="hidden">
                  <?php endforeach; ?>
                <?php endif; ?>
                <?php if (isset($data['PromotionManage']['categories_nid']) && !empty($data['PromotionManage']['categories_nid'])): ?>
                  <?php foreach ($data['PromotionManage']['categories_nid'] as $category): ?>
                    <input name="data[PromotionManage][categories_nid][]" value="<?php echo h($category); ?>" type="hidden">
                  <?php endforeach; ?>
                <?php endif; ?>
              </div>
              <?php echo $this->Form->isFieldError('categories') ? '<div class="has-error"><br/>'.$this->Form->error('categories', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')).'</div>' : ''; ?>
            </div>
            <div class="col-sm-6 <?php echo $this->Form->isFieldError('manufacturers') ? 'has-error' : ''; ?>">
              <?php if (isset($manufactureres) && count($manufactureres)): ?>
                <label class="control-label"><?php echo __('Wybierz producenta/ów:'); ?></label>
                <div class="checkbox c-checkbox <?php echo $this->Form->isFieldError('all_manufacturers') ? 'has-error' : ''; ?>">
                <label>
                  <?php echo $this->Form->input('all_manufacturers', array('type' => 'checkbox', 'label' => false, 'div' => false, 'class' => 'form-control', 'onchange' => 'App.Form.toggleShow(this.checked, \'xxx\', \'.manufacturers_list\'); App.Form.toggleShow(!this.checked, \'.manufacturers_list\', \'xxx\');')); ?>
                  <span class="fa fa-check"></span><?php echo __('wszyscy producenci'); ?>
                </label>
              </div>
              <br/>
                <div class="form-max-height-400 manufacturers_list" style="display:none;">
                  <?php foreach ($manufactureres as $manufacturer): ?>
                    <div class="checkbox c-checkbox">
                      <label>
                        <input name="data[PromotionManage][manufacturers][]" value="<?php echo h($manufacturer['Manufacturer']['id']); ?>" type="checkbox" <?php echo isset($data['PromotionManage']['manufacturers']) && in_array($manufacturer['Manufacturer']['id'], $data['PromotionManage']['manufacturers']) ? 'checked' : ''; ?>>
                        <span class="fa fa-check"></span><?php echo h($manufacturer['Manufacturer']['name']); ?>
                      </label>
                    </div>
                  <?php endforeach; ?>
                </div>
              <?php echo $this->Form->isFieldError('manufacturers') ? '<div class="has-error"><br/>'.$this->Form->error('manufacturers', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')).'</div>' : ''; ?>
              <?php endif; ?>
            </div>
          </div>
        </fieldset>
      </div>
      
      <div class="col-md-12">
        <div class="pull-right">
          <?php echo $this->Html->link(__('Anuluj'), array('controller' => 'KeyPromotions', 'action' => 'index', 'admin' => true), array('title' => __('Anuluj'), 'class' => 'btn btn-default')); ?>
          <button type="submit" class="btn btn-primary"><?php echo __('Zapisz'); ?></button>
        </div>
      </div>
      <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
    </div>
  </div>
</div>

<script>
  $(document).ready(function () {
    $('#PromotionManagePromotionDateBox, #PromotionManagePromotionDateEndBox').datetimepicker({
      icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-arrow-up",
        down: "fa fa-arrow-down"
      },
      locale: 'pl',
      format: 'YYYY-MM-DD HH:mm'
    });

    <?php echo $this->element('Scripts/AppPromotionsDefaultSettings'); ?>

    <?php echo $this->element('Scripts/AppCategoriesTreeDefaultSettings'); ?>
    App.CategoriesTree.chooseMode = true;
    App.CategoriesTree.multiSelect = true;
    
    if ($('input[name="data[PromotionManage][action]"]:checked').val() == 'create') {
      $('.ts_action_create').show();
    } else if ($('input[name="data[PromotionManage][action]"]:checked').val() == 'delete') {
      $('.ts_action_delete').show();
    }
    if ($('input[name="data[PromotionManage][price_change_kind]"]:checked').val() == 'price_tax') {
      $('.ts_price_change_kind_price_tax').show();
    } else if ($('input[name="data[PromotionManage][price_change_kind]"]:checked').val() == 'old_price') {
      $('.ts_price_change_kind_old_price').show();
    }
    if ($('input[name="data[PromotionManage][price_change_type]"]:checked').val() == 'percent') {
      $('input[name="data[PromotionManage][price_change_value]"]').attr({
        step:'0.00001',
        value:$('input[name="data[PromotionManage][price_change_value]"]').val(),
        type:'text'
      });
      $('input[name="data[PromotionManage][price_change_value]"]').attr('type','number');
    }
    if ($('input[name="data[PromotionManage][no_change_promotion_date]"]').is(':checked')) {
      $('input[name="data[PromotionManage][promotion_date]"]').prop('disabled', true);
    }
    if ($('input[name="data[PromotionManage][no_change_promotion_date_end]"]').is(':checked')) {
      $('input[name="data[PromotionManage][promotion_date_end]"]').prop('disabled', true);
    }
    App.Form.toggleShow(!$('input[name="data[PromotionManage][all_manufacturers]"]').is(':checked'), '.manufacturers_list', 'xxx');
    App.Form.toggleShow(!$('input[name="data[PromotionManage][all_categories]"]').is(':checked'), '.categories_list', 'xxx');
    
    App.Promotions.onDocReady();
    App.CategoriesTree.onDocReady();
    
    App.Promotions.loadPromotionManageForm();
  });
</script>