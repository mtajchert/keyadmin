<?php if ($empty_query): ?>
  <tr>
    <td colspan="7" class="text-center">
      <?php echo __('Wpisz frazę do wyszukiwania'); ?>
    </td>
  </tr>
<?php elseif (empty($customers)): ?>
  <tr>
    <td colspan="7" class="text-center">
      <?php echo __('Brak klientów pasujących do podanej frazy'); ?>
    </td>
  </tr>
<?php else: ?>
  <?php foreach ($customers as $customer): ?>
    <tr>
      <td>
        <div class="radio c-radio">
          <label>
            <input type="radio" name="ChooseCustomerBoxRadio" value="<?php echo h($customer['Customer']['id']); ?>">
            <span class="fa fa-check"></span>
          </label>
        </div>
      </td>
      <td><?php echo h($customer['Customer']['id']); ?></td>
      <td><?php echo $this->element('customer_standard_data', array('user' => $customer['Customer'], 'userData' => $customer['UserData'], 'userAddress' => isset($customer['UserAddress'][0]) ? $customer['UserAddress'][0] : null)); ?></td>
      <td><?php echo $this->element('customer_standard_contact', array('user' => $customer['Customer'], 'userData' => $customer['UserData'], 'userAddress' => isset($customer['UserAddress'][0]) ? $customer['UserAddress'][0] : null)); ?></td>
      <td>
         <?php echo strtotime($customer['Customer']['created']) > 0 ? h($this->Time->format($customer['Customer']['created'], '%Y-%m-%d %H:%M')) : '-'; ?>&nbsp;
      </td>
      <td>0</td>
      <td>
        <div class="checkbox c-checkbox">
          <label>
            <input type="checkbox" disabled <?php echo $customer['UserData']['status'] ? 'checked' : ''; ?>>
            <span class="fa fa-check"></span>
          </label>
        </div>
      </td>
    </tr>
  <?php endforeach; ?>
<?php endif; ?>
