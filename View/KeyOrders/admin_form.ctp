<div class="col-sm-12">
  <?php echo $this->Form->create('Order'); ?>
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="row">
        <div class="col-md-6">
          <fieldset>
            <?php echo $this->Form->input('id'); ?>
            <div class="form-group">
              <label for="ProductPaid"><?php echo __('Zapłacone:'); ?></label>
              <label class="checkbox-inline c-checkbox">
                <?php echo $this->Form->input('paid', array('type' => 'checkbox', 'label' => false, 'div' => false)); ?><span class="fa fa-check"></span>
              </label>
            </div>
            
            <?php if ($create): ?>
              <div class="form-group <?php echo $this->Form->isFieldError('user_id') ? 'has-error' : ''; ?>">
                <?php echo $this->Form->label('choose_customer', __('Klient: *')); ?>
                <?php echo $this->Form->input('choose_customer', array('label' => false, 'placeholder' => __('Wybierz klienta'), 'readonly' => 'true', 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'div' => array('class' => (isset($create) && $create) ? 'input-group m-b' : ''), 'after' => '<span class="input-group-btn"><button type="button" class="btn btn-default" onclick="App.Orders.openChooseCustomerBox();">'.__('wybierz').'</button></span>')); ?>

                <?php echo $this->Form->isFieldError('id') ? $this->Form->error('id', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
              </div>
            <?php endif; ?>
            <div class="form-group">
              <?php if (!$create): ?>
                <?php echo $this->Form->label('choose_customer', __('Klient: *')); ?>
              <?php endif; ?>
              <?php echo $this->Form->input('user_id', array('type' => 'hidden')); ?>
              <?php if ($create): ?>
                <div class="col-md-12">
                  <br>
              <?php endif; ?>
              <p id="customer_info">
                <?php if (isset($this->data['Customer']) && !empty($this->data['Customer'])): ?>
                  <?php echo $this->element('../KeyOrders/customer_info', array('user' => $order['Customer'], 'userData' => $order['Customer']['UserData'], 'userAddress' => isset($order['Customer']['UserAddress'][0]) ? $order['Customer']['UserAddress'][0] : null)); ?>
                <?php endif; ?>
              </p>
              <?php if ($create): ?>
                </div>
              <?php endif; ?>
            </div>
          </fieldset>
          <fieldset class="hide"></fieldset>
        </div>
        <div class="col-md-6">
          <fieldset>
            <div class="form-group <?php echo $this->Form->isFieldError('order_status_id') ? 'has-error' : ''; ?>">
              <div class="row">
                <?php
                $orderStatuses = [];
                $defaultOrderStatusId = 0;
                foreach ($orderStatusesFull as $orderStatus) {
                  $orderStatuses[] = [
                    'value' => $orderStatus['OrderStatus']['id'],
                    'name' => $orderStatus['OrderStatus']['name'],
                    'data-code' => $orderStatus['OrderStatus']['code']
                  ];
                  if ($orderStatus['OrderStatus']['default']) {
                    $defaultOrderStatusId = $orderStatus['OrderStatus']['id'];
                  }
                }
                ?>
                <div class="col-xs-9">
                  <?php echo $this->Form->input('order_status_id', array('label' => __('Status zamówienia: *'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'options' => $orderStatuses, 'default' => $defaultOrderStatusId)); ?>
                </div>
              </div>
              <?php if (!$create): ?>
                <div class="row">
                  <div class="col-xs-12">
                    <a href="javascript:void(0);" class="option-link clearfix" id="orderStatusHistoryShow" title="<?php echo __('Pokaż historię zamówienia'); ?>" onclick="App.Orders.showOrderStatusHistory()">
                      <em class="icon-arrow-down"></em>
                      <?php echo __('Pokaż historię zamówienia'); ?>
                    </a>
                    <a href="javascript:void(0);" class="option-link clearfix" id="orderStatusHistoryHide" title="<?php echo __('Ukryj historię zamówienia'); ?>" onclick="App.Orders.hideOrderStatusHistory()" style="display:none;">
                      <em class="icon-arrow-up"></em>
                      <?php echo __('Ukryj historię zamówienia'); ?>
                    </a>
                  </div>
                </div>
                <div class="row" id="orderStatusHistoryContent" style="display:none;">
                  <div class="col-xs-12">
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th>Data</th>
                          <th>Status</th>
                          <th>Użytkownik</th>
                        </tr>
                      </thead>
                      <?php if (isset($this->data['OrderStatusHistory']) && !empty($this->data['OrderStatusHistory'])): ?>
                        <tbody>
                          <?php foreach ($this->data['OrderStatusHistory'] as $orderHistory): ?>
                          <tr>
                            <td><?php echo h($this->Time->format($orderHistory['created'], '%Y-%m-%d %H:%M')); ?>&nbsp;</td>
                            <td><?php echo h($orderHistory['OrderStatus']['name']); ?></td>
                            <td><?php echo h(trim($orderHistory['User']['first_name']. ' '.$orderHistory['User']['last_name'])); ?></td>
                          </tr>
                          <?php endforeach; ?>
                        </tbody>
                      <?php else: ?>
                        <tfoot>
                          <tr>
                            <td colspan="3"><?php echo __('Brak historii do wyświetlenia'); ?></td>
                        </tfoot>
                      <?php endif; ?>
                    </table>
                  </div>
                </div>
              <?php endif; ?>
            </div>
          </fieldset>
          <fieldset class="hide"></fieldset>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <fieldset>
            <div class="form-group">
              <?php echo $this->Form->label('products', __('Produkty: *')); ?>
              <table class="table products-table table-bordered">
                <thead>
                  <tr>
                    <th rowspan="2" class="text-v-center"><?php echo __('Id'); ?></th>
                    <th colspan="3"><?php echo __('Nazwa produktu'); ?></th>
                    <th colspan="3"><?php echo __('Dod. informacje'); ?></th>
                    <th rowspan="2" class="text-v-center"><?php echo __('Opcje'); ?></th>
                  </tr>
                  <tr>
                    <th><?php echo __('Cena netto'); ?></th>
                    <th><?php echo __('VAT'); ?></th>
                    <th><?php echo __('Cena brutto'); ?></th>
                    <th><?php echo __('Ilość'); ?></th>
                    <th><?php echo __('Rabat'); ?></th>
                    <th><?php echo __('Wartość brutto'); ?></th>
                  </tr>
                </thead>
                <tbody>
                  <?php if(isset($this->data['OrderProduct'])): ?>
                    <?php foreach ($this->data['OrderProduct'] as $key => $orderProduct): ?>
                      <?php echo $this->element('../KeyOrders/product_row', array('key' => $key, 'orderProduct' => $orderProduct)); ?>
                    <?php endforeach; ?>
                  <?php endif; ?>
                  <tr class="no-products" <?php echo empty($this->data['OrderProduct']) ? '' : 'style="display:none;"'; ?>>
                    <td colspan="7" class="text-center">Brak pozycji do wyświetlenia, kliknij przycisk poniżej, aby dodać</td>
                  </tr>
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="7">
                      <a href="javascript:void(0);" class="option-link" onclick="App.Orders.openChooseProductBox();">
                        <em class="icon-plus"></em>
                        <?php echo __('Dodaj pozycję'); ?>
                      </a>
                    </td>
                  </tr>
                </tfoot>
              </table>
              <?php echo $this->Form->isFieldError('products') ? '<div class="has-error">'.$this->Form->error('products', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')).'</div>' : ''; ?>
            </div>
          </fieldset>
          <fieldset class="hide"></fieldset>
        </div>
      </div>
      <div class="row">
        <div class="address_update">
          <?php echo $this->Form->input('address_update', array('type' => 'hidden')); ?>
        </div>
        <div class="col-md-6">
          <fieldset>
            <div class="form-group address <?php echo $this->Form->isFieldError('billing_order_address_id') ? 'has-error' : ''; ?>">
              <div class="row">
                <div class="col-xs-9">
                  <h5><?php //var_dump($this->data['Order']['billing_order_address_id']); ?></h5>
                  <?php echo $this->Form->input('billing_order_address_id', array('label' => __('Dane do rozliczenia: *'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'options' => $userAddresses, 'default' => $defaultBillingUserAddressId, 'empty' => __('Wybierz adres'), 'onchange' => "App.Orders.changeAddress(this)")); ?>
                </div>
                <div class="col-xs-3 no-padding address-options" <?php echo isset($this->data['Order']['user_id']) && $this->data['Order']['user_id'] > 0 ? '' : 'style="display:none;"'; ?>>
                  <a href="javascript:void(0);" class="option-link" title="<?php echo __('Odśwież listę'); ?>" data-toggle="tooltip" data-title="<?php echo __('Odśwież listę'); ?>" onclick="App.Orders.reloadAddressesList()"><em class="icon-reload"></em></a>
                  <?php if (!$create): ?>
                    <a href="javascript:void(0);" class="option-link" title="<?php echo __('Aktualizuj wybrany adres'); ?>" data-toggle="tooltip" data-title="<?php echo __('Aktualizuj wybrany adres'); ?>" onclick="App.Orders.updateAddress(this)"><em class="icon-refresh"></em></a>
                  <?php endif; ?>
                  <a href="javascript:void(0);" class="option-link" title="<?php echo __('Dodaj nowy adres'); ?>" data-toggle="tooltip" data-title="<?php echo __('Dodaj nowy adres'); ?>" onclick="App.Orders.addAddress(this)"><em class="icon-plus"></em></a>
                </div>
              </div>
              <div class="col-xs-12" id="billing_address_no_user" <?php echo isset($this->data['Order']['user_id']) && $this->data['Order']['user_id'] > 0 ? 'style="display:none;"' : ''; ?>>
                <br>Wybierz klienta, aby zobaczyć dostępne adresy
              </div>
              <div class="col-xs-12">
                <br>
                <p class="address-content">
                  <?php if (isset($this->data['BillingOrderUserAddress']['id']) && $this->data['BillingOrderUserAddress']['id'] > 0): ?>
                    <?php echo $this->User->getFormattedAddress($this->data['BillingOrderUserAddress']); ?>
                  <?php endif; ?>
                </p>
              </div>
            </div>
          </fieldset>
          <fieldset class="hide"></fieldset>
        </div>
        <div class="col-md-6">
          <fieldset>
            <div class="form-group address <?php echo $this->Form->isFieldError('shipping_order_address_id') ? 'has-error' : ''; ?>">
              <div class="row">
                <div class="col-xs-9">
                  <h5><?php //var_dump($this->data['Order']['shipping_order_address_id']); ?></h5>
                  <?php echo $this->Form->input('shipping_order_address_id', array('label' => __('Dane do dostawy: *'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'options' => $userAddresses, 'default' => $defaultShippingUserAddressId, 'empty' => __('Wybierz adres'), 'onchange' => "App.Orders.changeAddress(this)")); ?>
                </div>
                <div class="col-xs-3 no-padding address-options" <?php echo isset($this->data['Order']['user_id']) && $this->data['Order']['user_id'] > 0 ? '' : 'style="display:none;"'; ?>>
                  <a href="javascript:void(0);" class="option-link" title="<?php echo __('Odśwież listę'); ?>" data-toggle="tooltip" data-title="<?php echo __('Odśwież listę'); ?>" onclick="App.Orders.reloadAddressesList()"><em class="icon-reload"></em></a>
                  <a href="javascript:void(0);" class="option-link" title="<?php echo __('Aktualizuj wybrany adres'); ?>" data-toggle="tooltip" data-title="<?php echo __('Aktualizuj wybrany adres'); ?>" onclick="App.Orders.updateAddress(this)"><em class="icon-refresh"></em></a>
                  <a href="javascript:void(0);" class="option-link" title="<?php echo __('Dodaj nowy adres'); ?>" data-toggle="tooltip" data-title="<?php echo __('Dodaj nowy adres'); ?>" onclick="App.Orders.addAddress(this)"><em class="icon-plus"></em></a>
                </div>
              </div>
              <div class="col-xs-12" id="shipping_address_no_user" <?php echo isset($this->data['Order']['user_id']) && $this->data['Order']['user_id'] > 0 ? 'style="display:none;"' : ''; ?>>
                <br>Wybierz klienta, aby zobaczyć dostępne adresy
              </div>
              <div class="col-xs-12">
                <?php
                $shipping_country_id = 0;
                if (isset($this->data['ShippingOrderUserAddress']['country_id']) && $this->data['ShippingOrderUserAddress']['country_id'] > 0) {
                  $shipping_country_id = $this->data['ShippingOrderUserAddress']['country_id'];
                }
                ?>
                <input type="hidden" id="shipping_country_id" value="<?php echo h($shipping_country_id); ?>" />
                <br>
                <p class="address-content">
                  <?php if (isset($this->data['ShippingOrderUserAddress']['id']) && $this->data['ShippingOrderUserAddress']['id'] > 0): ?>
                    <?php echo $this->User->getFormattedAddress($this->data['ShippingOrderUserAddress']); ?>
                  <?php endif; ?>
                </p>
              </div>
            </div>
          </fieldset>
          <fieldset class="hide"></fieldset>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <fieldset>
            <?php
            $shippings = [];
            foreach ($shippingsFull as $shipping) {
              $shippings[] = [
                'value' => $shipping['Shipping']['id'],
                'name' => $shipping['Shipping']['name'].' - '.$shipping['Shipping']['for_order_price_tax'].' '.__('zł'),
                'data-price_tax' => $shipping['Shipping']['for_order_price_tax']
              ];
            }
            ?>
            <div class="form-group <?php echo $this->Form->isFieldError('shipping_id') ? 'has-error' : ''; ?>">
              <div class="row">
                <div class="col-xs-9">
                  <?php echo $this->Form->input('shipping_id', array('label' => __('Forma dostawy: *'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'options' => $shippings, 'empty' => __('Wybierz formę dostawy'), 'onchange' => "App.Orders.changeShipping()")); ?>
                </div>
              </div>
              <div class="col-xs-12" id="shipping_no_country" <?php echo $shipping_country_id > 0 ? 'style="display:none;"' : ''; ?>>
                <br>Wybierz adres dostawy, aby zobaczyć dostępne formy dostawy
              </div>
            </div>
          </fieldset>
          <fieldset class="hide"></fieldset>
        </div>
        <div class="col-md-6">
          <fieldset>
            <div class="form-group <?php echo $this->Form->isFieldError('payment_id') ? 'has-error' : ''; ?>">
              <div class="row">
                <div class="col-xs-9">
                  <?php echo $this->Form->input('payment_id', array('label' => __('Forma płatności: *'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'empty' => __('Wybierz formę płatności'), 'onchange' => "App.Orders.changePayment()")); ?>
                </div>
              </div>
              <div class="col-xs-12" id="payment_no_shipping" <?php echo $this->data['Order']['shipping_id'] > 0 ? 'style="display:none;"' : ''; ?>>
                <br>Wybierz formę dostawy, aby zobaczyć dostępne formy płatności
              </div>
            </div>
          </fieldset>
          <fieldset class="hide"></fieldset>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <fieldset>
            <div class="form-group">
              <div class="row">
                <div class="col-xs-11 <?php echo $this->Form->isFieldError('add_info') ? 'has-error' : ''; ?>">
                  <?php echo $this->Form->input('add_info', array('label' => __('Uwagi dotyczące zamówienia: *'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'placeholder' => __('Wpisz uwagi dotyczące zamówienia'), 'rows' => 3)); ?>
                  <br>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-11">
                  <?php echo $this->Form->input('Customer.UserData.add_info', array('label' => __('Uwagi dotyczące klienta: *'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'rows' => 3, 'disabled' => true)); ?>
                </div>
            </div>
          </fieldset>
        </div>
        <div class="col-md-6">
          <fieldset>
            <?php echo $this->Form->label('choose_customer', __('Podsumowanie zamówienia:')); ?>
            <div class="col-xs-11">
              <table class="table table-bordered table-striped">
                <tr>
                  <th width="65%" class="text-right">
                    <?php echo $this->Form->input('products_price_tax', array('type' => 'hidden')); ?>
                    <?php echo __('Produkty brutto'); ?>
                  </th>
                  <td width="35%" class="text-right products_price_tax_display">
                    <?php echo h($this->data['Order']['products_price_tax']).' '.__('zł'); ?>
                  </td>
                </tr>
                <tr>
                  <th width="65%" class="text-right">
                    <?php echo $this->Form->input('shipping_price_tax', array('type' => 'hidden')); ?>
                    <?php echo __('Dostawa brutto'); ?>
                  </th>
                  <td width="35%" class="text-right shipping_price_tax_display">
                    <?php echo h($this->data['Order']['shipping_price_tax']).' '.__('zł'); ?>
                  </td>
                </tr>
                <tr>
                  <th width="65%" class="text-right">
                    <?php echo $this->Form->input('order_price_tax', array('type' => 'hidden')); ?>
                    <?php echo __('Razem brutto'); ?>
                  </th>
                  <td width="35%" class="text-right order_price_tax_display">
                    <?php echo h($this->data['Order']['order_price_tax']).' '.__('zł'); ?>
                  </td>
                </tr>
              </table>
            </div>
          </fieldset>
        </div>
      </div>
    </div>
    <div class="panel-footer">
      <div class="clearfix">
        <div class="pull-right">
          <?php echo $this->Html->link(__('Anuluj'), array('controller' => 'KeyOrders', 'action' => 'index', 'admin' => true), array('title' => __('Anuluj'), 'class' => 'btn btn-default')); ?>
          <button type="submit" class="btn btn-primary"><?php echo __('Zapisz'); ?></button>
        </div>
      </div>
    </div>
  </div>
  <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
</div>

<?php echo $this->element('Scripts/AppOrdersDefaultSettings'); ?>
<script>
  $(document).ready(function () {
    <?php echo $this->element('Scripts/AppCategoriesTreeDefaultSettings'); ?>
    App.CategoriesTree.chooseMode = true;
    
    App.Orders.onDocReady();
    App.CategoriesTree.onDocReady();
  });
</script>