<style>
  *, td, th, h1, h2, p, span, div, label {
    font-family: sans-serif;
    font-size: 12px;
    font-weight: normal;
    margin: 0;
    padding: 0;
  }
  h1 {
    font-size: 24px;
  }
  h2 {
    font-size: 18px;
  }
  h2 small {
    font-size:12px;
    color: #888;
  }
  label {
    color: #888;
  }
  hr {
    margin:20px 0;
  }
  table {
    width: 100%;
  }
  table, tr, th, td {
    border-collapse: collapse;
  }
  table.bordered {
    margin-bottom:20px;
  }
  table.bordered th, table.bordered td {
    border: 1px solid #ccc;
    text-align: center;
    vertical-align: middle;
    padding: 2px 6px;
  }
  table.bordered thead th {
    background-color: #ccc;
  }
  table.summary {
    width:50%;
    float:right;
  }
</style>

<h1>Raport sprzedaży</h1>
<p>Zamówienia od <?php echo h($date_from); ?> do <?php echo h($date_to); ?></p>
<?php
  $all_products_total_net = 0;
  $all_products_total_gross = 0;
  $all_shipping_net = 0;
  $all_shipping_gross = 0;
  $all_order_margin_gross = 0;
  $products_stats = array();
?>
<hr/>

<?php foreach ($orders as $order): ?>
  <br/>
  <h2>Zamówienie <?php echo h($order['Order']['number']); ?> <small>/ <?php echo h($order['Order']['created']); ?></small></h2>
  <p>
    <div style="float:left;width:50%;">
      <label>Forma płatności:</label> <?php echo h($order['Payment']['name']); ?>
    </div>
    <div style="float:left;width:50%;">
      <label>Forma dostawy:</label> <?php echo h($order['Shipping']['name']); ?>
    </div>
  </p>
  
  <table class="bordered" style="margin-top:10px;">
    <thead>
      <tr>
        <th>Id</th>
        <th>Nazwa produktu / Dod. informacje</th>
        <th>Cena netto</th>
        <th>VAT</th>
        <th>Cena brutto</th>
        <th>Ilość</th>
        <th>Wartość brutto</th>
      </tr>
    </thead>
    <tbody>
      <?php $products_total_net = 0; ?>
      <?php foreach ($order['OrderProduct'] as $orderProduct): ?>
        <?php 
          $products_total_net += $orderProduct['price'] * $orderProduct['amount'];
          $stats_key = $orderProduct['product_id'].'-'.$orderProduct['products_products_option_id'];
          if (array_key_exists($stats_key, $products_stats)) {
            $products_stats[$stats_key]['amount'] += $orderProduct['amount'];
            $products_stats[$stats_key]['total_price_net'] += $orderProduct['price'] * $orderProduct['amount'];
            $products_stats[$stats_key]['total_price_tax'] += $orderProduct['total_price_tax'];
          } else {
            $products_stats[$stats_key] = array(
              'product_id' => $orderProduct['product_id'],
              'name' => $orderProduct['name'],
              'add_info' => $orderProduct['add_info'],
              'amount' => $orderProduct['amount'],
              'total_price_net' => $orderProduct['price'] * $orderProduct['amount'],
              'total_price_tax' => $orderProduct['total_price_tax'],
            );
          }
        ?>
        <tr>
          <td><?php echo h($orderProduct['product_id']); ?></td>
          <td><?php echo h($orderProduct['name']).'<br/>'.h($orderProduct['add_info']); ?></td>
          <td><?php echo h($orderProduct['price']).' '.__('zł'); ?></td>
          <td><?php echo h($orderProduct['TaxRate']['description']); ?></td>
          <td><?php echo h($orderProduct['price_tax']).' '.__('zł'); ?></td>
          <td><?php echo h($orderProduct['amount']); ?></td>
          <td><?php echo h($orderProduct['total_price_tax']).' '.__('zł'); ?></td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
  
  <table class="bordered summary">
    <thead>
      <tr>
        <td></td>
        <th>Netto</th>
        <th>Brutto</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th>Produkty razem</th>
        <td><?php echo h($products_total_net.' '.__('zł')); ?></td>
        <td><?php echo h($order['Order']['products_price_tax'].' '.__('zł')); ?></td>
        <?php
          $all_products_total_net += $products_total_net;
          $all_products_total_gross += $order['Order']['products_price_tax'];
        ?>
      </tr>
      <tr>
        <?php
          $shipping_net = 0;
          if ($order['Order']['shipping_price_tax'] > 0) {
            $shipping_net = round($order['Order']['shipping_price_tax'] / ((100 + $order['Shipping']['TaxRate']['rate']) / 100), 2);
          }
        ?>
        <th>Koszt dostawy</th>
        <td><?php echo h($shipping_net.' '.__('zł')); ?></td>
        <td><?php echo h($order['Order']['shipping_price_tax'].' '.__('zł')); ?></td>
        <?php
          $all_shipping_net += $shipping_net;
          $all_shipping_gross += $order['Order']['shipping_price_tax'];
        ?>
      </tr>
      <tr>
        <th>Razem</th>
        <td><?php echo h((number_format($products_total_net + $shipping_net, 2)).' '.__('zł')); ?></td>
        <td><?php echo h($order['Order']['order_price_tax'].' '.__('zł')); ?></td>
      </tr>
      <tr>
        <?php
          $order_margin = $order['Order']['order_price_tax'] * $order['Payment']['margin'] / 100;
        ?>
        <th>Marża</th>
        <td></td>
        <td><?php echo h((number_format($order_margin, 2)).' '.__('zł')); ?></td>
        <?php
          $all_order_margin_gross += $order_margin;
        ?>
      </tr>
    </tbody>
  </table>
  <hr/>
<?php endforeach; ?>

<h2>Podsumowanie zamówień</h2>
<br/>
<table class="bordered summary summary-left">
  <thead>
    <tr>
      <td></td>
      <th>Netto</th>
      <th>Brutto</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Produkty razem</th>
      <td><?php echo h((number_format($all_products_total_net, 2)).' '.__('zł')); ?></td>
      <td><?php echo h((number_format($all_products_total_gross, 2)).' '.__('zł')); ?></td>
    </tr>
    <tr>
      <th>Koszt dostawy</th>
      <td><?php echo h((number_format($all_shipping_net, 2)).' '.__('zł')); ?></td>
      <td><?php echo h((number_format($all_shipping_gross, 2)).' '.__('zł')); ?></td>
    </tr>
    <tr>
      <th>Razem</th>
      <td><?php echo h((number_format($all_products_total_net + $all_shipping_net, 2)).' '.__('zł')); ?></td>
      <td><?php echo h((number_format($all_products_total_gross + $all_shipping_gross, 2)).' '.__('zł')); ?></td>
    </tr>
    <tr>
      <th>Marża</th>
      <td></td>
      <td><?php echo h((number_format($all_order_margin_gross, 2)).' '.__('zł')); ?></td>
    </tr>
  </tbody>
</table>

<pagebreak />

<h2>Statystyki produktów</h2>
<br/>
<table class="bordered">
  <thead>
    <tr>
      <th>Id</th>
      <th>Nazwa produktu / Dod. informacje</th>
      <th>Ilość</th>
      <th>Wartość netto</th>
      <th>Wartość brutto</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($products_stats as $product_stats): ?>
      <tr>
        <td><?php echo h($product_stats['product_id']); ?></td>
        <td><?php echo h($product_stats['name']).'<br/>'.h($product_stats['add_info']); ?></td>
        <td><?php echo h(number_format($product_stats['amount'], 2)); ?></td>
        <td><?php echo h(number_format($product_stats['total_price_net'], 2)); ?></td>
        <td><?php echo h(number_format($product_stats['total_price_tax'], 2)); ?></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
