<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th data-check-all="">
                <div title="" data-original-title="" data-toggle="tooltip" data-title="<?php echo __('Zaznacz wszystkie'); ?>" class="checkbox c-checkbox">
                  <label>
                    <input type="checkbox">
                    <span class="fa fa-check"></span>
                  </label>
                </div>
              </th>
              <th><?php echo $this->Paginator->sort('id', __('ID')); ?></th>
              <th><span class="glyphicon glyphicon-usd"></span></th>
              <th><?php echo __('Klient'); ?></th>
              <th><?php echo $this->Paginator->sort('created', __('Data zamówienia')); ?></th>
              <th><?php echo $this->Paginator->sort('order_price_tax', __('Wartość')); ?></th>
              <th><?php echo $this->Paginator->sort('Payment.name', __('Forma płatności')); ?></th>
              <th><?php echo $this->Paginator->sort('Shipping.name', __('Forma dostawy')); ?></th>
              <th><?php echo $this->Paginator->sort('OrderStatus.name', __('Status')); ?></th>
              <?php if (Configure::read('Invoice.quote_enabled')): ?>
              	<th><?php echo __('Faktura'); ?></th>
            	<?php endif; ?>
              <th><?php echo __('Opcje'); ?></th>
            </tr>
          </thead>
          <tbody>
            <?php if (isset($orders) && !empty($orders)): ?>
              <?php foreach ($orders as $order): ?>
                <tr class="<?php echo $order['OrderStatus']['css_label_class']; ?>">
                  <td>
                    <div class="checkbox c-checkbox">
                      <label>
                         <input type="checkbox" name="check_order_id[]" value="<?php echo h($order['Order']['id']); ?>">
                         <span class="fa fa-check"></span>
                      </label>
                    </div>
                  </td>
                  <td><?php echo h($order['Order']['id']); ?>&nbsp;</td>
                  <td>
                  	<div class="checkbox c-checkbox">
                      <label>
                      	<input type="checkbox" <?php echo (!empty($order['Order']['paid']))? 'checked="checked"': '';?>
                      	data-order-id="<?php echo $order['Order']['id']; ?>" value="1" onchange="App.Orders.updatePaidStatus(this);">
                      	<span class="fa fa-check"></span>
                      </label>
                    </div>
									</td>
                  <td>
                    <?php if (!empty($order['Order']['is_allegro'])): ?>
                        <span class="icon-handbag" style="color: red;font-size: 18px;"></span>
                    <?php endif; ?>
                    <?php echo $this->element('customer_standard_data', array('user' => $order['Customer'], 'userData' => $order['Customer']['UserData'], 'userAddress' => isset($order['Customer']['UserAddress'][0]) ? $order['Customer']['UserAddress'][0] : null)); ?>
                  </td>
                  <td><?php echo h($this->Time->format($order['Order']['created'], '%Y-%m-%d %H:%M')); ?>&nbsp;</td>
                  <td><?php echo h($order['Order']['order_price_tax']); ?>&nbsp;</td>
                  <td><?php echo h($order['Payment']['name']); ?>&nbsp;</td>
                  <td><?php echo h($order['Shipping']['name']); ?>&nbsp;</td>
                  <td><?php echo h($order['OrderStatus']['name']); ?>&nbsp;</td>
                  <?php if (Configure::read('Invoice.quote_enabled')): ?>
                  	<td><?php echo h($order['Invoice']['number_all']); ?>&nbsp;</td>
                	<?php endif; ?>
                  <td class="options-buttons text-center">
                    <?php if (Configure::read('Invoice.quote_enabled')): ?>
                      <?php echo $this->Html->link('<em class="icon-printer"></em>', 
                          //array('controller' => 'KeyOrders', 'action' => 'generate_invoice', $order['Order']['id']),
                          'javascript:void(0);',
                          array('escape' => false, 'title' => __('Pobierz FV'), 'class' => 'option-link',
                            'onclick' => 'App.Orders.invoiceOrderTemplate('.$order['Order']['id'].');'
                      )); ?>
                    &nbsp;
                    <?php endif; ?>
                    <?php echo $this->Html->link('<em class="icon-list"></em>', 'javascript:void(0);', 
                        array('escape' => false, 'title' => __('Statusy'), 'onclick' => 'App.Orders.changeOrderStatus('.$order['Order']['id'].');', 'class' => 'option-link')); ?>
                    &nbsp;
                    <?php echo $this->Html->link('<em class="icon-magnifier"></em>', 'javascript:void(0);', 
                        array('onclick' => 'App.Orders.showPreview('.$order['Order']['id'].');', 'escape' => false, 'title' => __('Podgląd'), 'class' => 'option-link')); ?>
                    &nbsp;
                    <?php echo $this->Html->link('<em class="icon-note"></em>', array('controller' => 'KeyOrders', 'action' => 'edit', $order['Order']['id']), array('escape' => false, 'title' => __('Edytuj'), 'class' => 'option-link')); ?>
                    &nbsp;
                    <?php echo $this->Html->link('<em class="icon-trash"></em>', 'javascript:void(0);', array('escape' => false, 'title' => __('Usuń'), 'onclick' => 'App.Orders.deleteOrder(\''.h($order['Order']['number']).'\', \''.$this->Html->url(array('controller' => 'KeyOrders', 'action' => 'delete', $order['Order']['id'])).'\')', 'class' => 'option-link')); ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <td colspan="9"><?php echo __('Brak zamówień do wyświetlenia'); ?></td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="panel-footer text-center">
      <div class="col-sm-6">
        <div class="pull-left">
          <?php echo $this->Paginator->counter(array('format' => __('Strona {:page} z {:pages}'))); ?>
        </div>
      </div>
      <div class="col-sm-6 pagination pagination-large">
        <ul class="pagination pull-right">
          <?php
          $this->Paginator->options['url'] = array('controller' => 'KeyOrders', 'action' => 'index');
          if (isset($this->data['Order']) && !empty($this->data['Order'])){
            foreach ($this->data['Order'] as $key2 => $value2) {
              $this->Paginator->options['url']['Order['.$key2.']'] = $value2;
            }
          }
          echo $this->Paginator->prev(__('poprzednia'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1, 'modulus' => 5, 'ellipsis' => '<li class="disabled"><a>...</a></li>'));
          echo $this->Paginator->next(__('następna'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          ?>
        </ul>
      </div>
      <span class="clearfix"></span>
    </div>
  </div>
</div>

<?php echo $this->element('Scripts/AppOrdersDefaultSettings'); ?>
<script>
  $(document).ready(function () {
    
    App.Orders.onDocReady();
  });
</script>