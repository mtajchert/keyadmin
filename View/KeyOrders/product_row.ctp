<?php //print_r($orderProduct); ?>
<tr>
  <td rowspan="2">
    <?php echo $this->Form->input("OrderProduct.{$key}.id"); ?>
    <?php echo $this->Form->input("OrderProduct.{$key}.product_id", array('type' => 'hidden')); ?>
    <?php echo $this->Form->input("OrderProduct.{$key}.catalog_no", array('type' => 'hidden')); ?>
    <?php echo $this->Form->input("OrderProduct.{$key}.ext_code", array('type' => 'hidden')); ?>
    <?php echo $this->Form->input("OrderProduct.{$key}.ean", array('type' => 'hidden')); ?>
    <?php echo $this->Form->input("OrderProduct.{$key}.pkwiu", array('type' => 'hidden')); ?>
    <?php echo $this->Form->input("OrderProduct.{$key}.weight", array('type' => 'hidden')); ?>
    <?php echo $this->Form->input("OrderProduct.{$key}.manufacturer_id", array('type' => 'hidden')); ?>
    <?php echo $this->Form->input("OrderProduct.{$key}.shipping_time_id", array('type' => 'hidden')); ?>
    <?php echo $this->Form->input("OrderProduct.{$key}.product_warranty_id", array('type' => 'hidden')); ?>
    <?php echo $this->Form->input("OrderProduct.{$key}.product_condition_id", array('type' => 'hidden')); ?>
    <?php echo $this->Form->input("OrderProduct.{$key}.unit_id", array('type' => 'hidden')); ?>
    <?php echo $this->Form->input("OrderProduct.{$key}.products_products_option_id", array('type' => 'hidden')); ?>
    
    <?php echo h($orderProduct['Product']['id']); ?>
  </td>
  <td colspan="3" class="<?php echo $this->Form->isFieldError("OrderProduct.{$key}.product_id") || $this->Form->isFieldError("OrderProduct.{$key}.name") ? 'has-error' : ''; ?>">
    
    <?php echo $this->Form->input("OrderProduct.{$key}.name", array('label' => false, 'placeholder' => __('Nazwa produktu'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'div' => false, 'data-toggle' => 'tooltip', 'data-title' => __('Nazwa produktu'))); ?>
  </td>
  <td colspan="3" class="<?php echo $this->Form->isFieldError("OrderProduct.{$key}.add_info") ? 'has-error' : ''; ?>">
    <?php echo $this->Form->input("OrderProduct.{$key}.add_info", array('label' => false, 'placeholder' => __('Informacje dodatkowe'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'div' => false, 'data-toggle' => 'tooltip', 'data-title' => __('Informacje dodatkowe'), 'type' => 'text')); ?>
  </td>
  <td rowspan="2">    
    <?php if (isset($orderProduct['id']) && $orderProduct['id'] > 0): ?>      
      <a href="javascript:void(0);" class="option-link" onclick="App.Orders.previewOrderProduct(<?php echo h($orderProduct['id']); ?>);" title="<?php echo __('Podgląd'); ?>"><em class="icon-magnifier"></em></a>
      &nbsp;
    <?php endif; ?>
    <a href="javascript:void(0);" class="option-link" title="<?php echo __('Usuń'); ?>" onclick="App.Orders.deleteProduct($([$(this).closest('tr').get()[0], $(this).closest('tr').next().get()[0]]))"><em class="icon-trash"></em></a>
  </td>
</tr>
<tr>
  <td class="<?php echo $this->Form->isFieldError("OrderProduct.{$key}.price") ? 'has-error' : ''; ?>">
    <?php echo $this->Form->input("OrderProduct.{$key}.price", array('label' => false, 'placeholder' => __('Cena netto'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'div' => false, 'data-toggle' => 'tooltip', 'data-title' => __('Cena netto'), 'min' => 0, 'onchange' => "App.Orders.changeProductPrice($(this).closest('tr'), 'price')")); ?>
    <?php echo $this->Form->input("OrderProduct.{$key}.tax", array('type' => 'hidden')); ?>
  </td>
  <td class="<?php echo $this->Form->isFieldError("OrderProduct.{$key}.tax_rate_id") ? 'has-error' : ''; ?>">
    <?php
    $taxRatesOptions = [];
    foreach ($taxRatesFull as $taxRate) {
      $taxRatesOptions[] = [
        'value' => $taxRate['TaxRate']['id'],
        'name' => $taxRate['TaxRate']['description'],
        'data-rate' => $taxRate['TaxRate']['rate'],
      ];
    }
    ?>
      <?php echo $this->Form->input("OrderProduct.{$key}.tax_rate_id", array('label' => false, 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'data-toggle' => 'tooltip', 'data-title' => __('Stawka VAT'), 'options' => $taxRatesOptions, 'onchange' => "App.Orders.changeProductPrice($(this).closest('tr'), 'tax_rate')")); ?>
  </td>
  <td class="<?php echo $this->Form->isFieldError("OrderProduct.{$key}.price_tax") ? 'has-error' : ''; ?>">
    <?php echo $this->Form->input("OrderProduct.{$key}.price_tax", array('label' => false, 'placeholder' => __('Cena brutto'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'div' => false, 'data-toggle' => 'tooltip', 'data-title' => __('Cena brutto'), 'min' => 0, 'onchange' => "App.Orders.changeProductPrice($(this).closest('tr'), 'price_tax')")); ?>
  </td>
  <td class="<?php echo $this->Form->isFieldError("OrderProduct.{$key}.amount") ? 'has-error' : ''; ?>">
    <?php
    $amountParams = array('label' => false, 'placeholder' => __('Ilość'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'div' => array('class' => 'input-group m-b unit-div'), 'data-toggle' => 'tooltip', 'data-title' => __('Ilość'), 'after' => '<span class="input-group-addon">'.h($orderProduct['Unit']['name']).'</span>');
    if ($orderProduct['Product']['order_min_quantity'] > 0) {
      $amountParams['min'] = $orderProduct['Product']['order_min_quantity'];
    } else {
      $amountParams['min'] = 1;
    }
    if ($orderProduct['Product']['order_max_quantity'] > 0) {
      $amountParams['max'] = $orderProduct['Product']['order_max_quantity'];
    }
    if ($orderProduct['Product']['order_step_quantity'] > 0) {
      if ($orderProduct['Unit']['quantity_type'] == 'int') {
        $amountParams['step'] = (int) $orderProduct['Product']['order_step_quantity'];
      } else {
        $amountParams['step'] = (float) $orderProduct['Product']['order_step_quantity'];
      }
    } else {
      $amountParams['step'] = $orderProduct['Unit']['quantity_type'] == 'int' ? 1 : 0.01;
    }
    $amountParams['onchange'] = "App.Orders.changeProductAmount($(this).closest('tr'))";
    ?>
    <?php echo $this->Form->input("OrderProduct.{$key}.amount", $amountParams); ?>
  </td>
  <td>
  	<?php echo $this->Form->input("OrderProduct.{$key}.discount", array('type' => 'number', 'label' => false, 'placeholder' => __('Rabat (%)'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'div' => false, 'data-toggle' => 'tooltip', 'data-title' => __('Rabat'), 'min' => 0, 'onchange' => "App.Orders.changeProductPrice($(this).closest('tr'), 'price_discount')")); ?>
  </td>
  <td class="<?php echo $this->Form->isFieldError("OrderProduct.{$key}.total_price_tax") ? 'has-error' : ''; ?>">
    <?php echo $this->Form->input("OrderProduct.{$key}.total_price_tax", array('label' => false, 'placeholder' => __('Wartość brutto'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'div' => false, 'data-toggle' => 'tooltip', 'data-title' => __('Wartość brutto'), 'min' => 0, 'readonly' => true, 'type' => 'text')); ?>
  </td>
</tr>