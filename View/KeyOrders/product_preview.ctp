<div class="row">
	<div class="col-xs-4">
		<?php echo $this->App->showProductsImage($orderProduct['Product']['ProductsImage'][0]['image'], 'ORIGIN', array('class' => 'img-responsive center-block')); ?>
	</div>
	<div class="col-xs-8">
		<table class="table table-bordered table-striped">
      <tr>
        <th width="35%"><?php echo __('Id produktu'); ?></th>
        <td width="65%"><?php echo h($orderProduct['Product']['id']); ?></td>
      </tr>
      <tr>
        <th width="35%"><?php echo __('Nazwa produktu'); ?></th>
        <td width="65%"><?php echo h($orderProduct['OrderProduct']['name']); ?></td>
      </tr>
      <tr>
        <th width="35%"><?php echo __('Dodatkowe informacje'); ?></th>
        <td width="65%"><?php echo h($orderProduct['OrderProduct']['add_info']); ?></td>
      </tr>
      <tr>
        <th width="35%"><?php echo __('Cena zakupu'); ?></th>
        <td width="65%"><?php echo h($orderProduct['OrderProduct']['purchase_price']); ?></td>
      </tr>
    </table>
	</div>
</div>