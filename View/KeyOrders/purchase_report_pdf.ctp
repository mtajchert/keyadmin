<style>
  *, td, th, h1, h2, p, span, div, label {
    font-family: sans-serif;
    font-size: 12px;
    font-weight: normal;
    margin: 0;
    padding: 0;
  }
  h1 {
    font-size: 24px;
  }
  h2 {
    font-size: 18px;
  }
  h2 small {
    font-size:12px;
    color: #888;
  }
  label {
    color: #888;
  }
  hr {
    margin:20px 0;
  }
  table {
    width: 100%;
  }
  table, tr, th, td {
    border-collapse: collapse;
  }
  table.bordered {
    margin-bottom:20px;
  }
  table.bordered th, table.bordered td {
    border: 1px solid #ccc;
    text-align: center;
    vertical-align: middle;
    padding: 2px 6px;
  }
  table.bordered thead th {
    background-color: #ccc;
  }
  table.summary {
    width:50%;
    float:right;
  }
</style>

<h1 style="text-align:center;margin-bottom: 10px;">Raport sprzedażowy</h1>
<div>&nbsp;</div>
<table class="bordered" style="width:100%;">
  <thead>
    <tr>
    	<th>Id zam</th>
    	<th>Nazwa produktu</th>
    	<th>Zakup netto</th>
    	<th>Sprzedaż netto</th>
    	<th>Ilość</th>
    	<th>Suma zakupu netto</th>
    	<th>Suma sprzedaży netto</th>
    </tr>    
  </thead>
  <?php $sumPurchase = $sumSell = 0; ?>
  <?php foreach ($products as $product): ?>
    <tr>
    	<td><?php echo $product['order_id']; ?></td>
    	<td><?php echo $product['name']; ?></td>
    	<td><?php echo $product['Product']['purchase_price']; ?></td>
    	<td><?php echo $product['price']; ?></td>
    	<td><?php echo $product['amount']; ?></td>
    	<?php $sumPurchase += round($product['amount']*$product['Product']['purchase_price'], 2); ?>
    	<td><?php echo round($product['amount']*$product['Product']['purchase_price'], 2); ?></td>
    	<?php $sumSell += round($product['amount']*$product['price'], 2); ?>
    	<td><?php echo round($product['amount']*$product['price'], 2); ?></td>
    </tr>
  <?php endforeach; ?>
  <tr>
  	<td COLSPAN="5" style="text-align: right;">Sumy</td>
  	<td><?php echo $sumPurchase; ?></td>
  	<td><?php echo $sumSell; ?></td>
  </tr>
  <tr>
  	<td COLSPAN="5" style="text-align: right;">Zysk</td>
  	<td COLSPAN="2"><?php echo $sumSell-$sumPurchase; ?></td>
  </tr>    
</table>