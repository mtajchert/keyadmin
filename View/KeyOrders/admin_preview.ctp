<div class="row">
	<div class="col-xs-6">
		<div class="panel panel-primary">
      <div class="panel-heading">Adres dostawy</div>
      <div class="panel-body">
      	<?php $order['ShippingOrderUserAddress']['is_company'] = 0; ?>
      	<?php echo $this->User->getFormattedAddress($order['ShippingOrderUserAddress'], '<br/>'); ?>
      </div>
    </div>
	</div>
	<div class="col-xs-6">
		<div class="panel panel-primary">
      <div class="panel-heading">Dane do faktury</div>
      <div class="panel-body">
        <?php if (!empty($order['InvoiceOrderUserAddress']['id'])): ?>
            <?php echo $this->User->getFormattedAddress($order['InvoiceOrderUserAddress'], '<br/>'); ?>
        <?php else: ?>
            <?php echo $this->User->getFormattedAddress($order['BillingOrderUserAddress'], '<br/>'); ?>
        <?php endif; ?>
      	
      </div>
    </div>
	</div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Dostawa</div>
            <div class="panel-body">
                <p>Dostawa: <?php echo $order['Shipping']['name']; ?></p>
                <p>Info: <?php echo $order['Order']['add_info']; ?></p>
            </div>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-xs-12">
    <div class="panel panel-primary">
      <div class="panel-heading">Lista produktów</div>
      <div class="panel-body">
    		<table class="table cartTable table-striped">
      		<tr>
      			<th>Lp</th>
      			<th>Nazwa produktu</th>
      			<th>Opcja</th>
      			<th>Kod produktu</th>
      			<th>Ilość</th>
      			<th>Cena jedn.</th>
      		</tr>
      		<?php foreach ($order['OrderProduct'] as $key => $cartProduct): ?>
      			<tr>	
      				<td><?php echo ++$key; ?></td>
              <td>              	
              	<?php if ($cartProduct['Product']['status'] == '1'): ?>
              		<span style="font-size: 18px;color: #27c24c" class="glyphicon glyphicon-ok" aria-hidden="true"></span>
              	<?php else: ?>
              		<span style="font-size: 18px;color: #f05050" class="glyphicon glyphicon-remove" aria-hidden="true"></span>
              	<?php endif; ?>
                <?php $url = $this->App->getProductViewUrl(['Product' => $cartProduct]); ?>
              	&nbsp;<?php echo $this->Html->link($cartProduct['name'], [
              	    'controller' => 'KeyProducts',
              	    'admin' => true,
              	    'action' => 'edit',
              	    $cartProduct['product_id']
              	], [
              	    'target' => '_blank'
              	]); ?>
            	</td>
              <td>
              <?php echo (empty($cartProduct['add_info']))? '---': $cartProduct['add_info']; ?></td>
              <td>
                  <?php if (isset($cartProduct['ProductsProductsOption']['id']) && !empty($cartProduct['ProductsProductsOption']['id'])): ?>
                      <?php echo $cartProduct['ProductsProductsOption']['catalog_no'];  ?>
                  <?php else: ?>
                      <?php echo (!empty($cartProduct['catalog_no']))? $cartProduct['catalog_no']: '---';  ?>
                  <?php endif; ?>
              </td>
              <td><?php echo round($cartProduct['amount']); ?></td>
              <td><div class="price"><span> <?php echo h($cartProduct['price_tax']).' '.__('zł'); ?> </span></div></td>
      			</tr>
      		<?php endforeach; ?>
      	</table>
      	
      </div>
    </div>
  </div>
</div>
<div class="row">
	<div class="col-xs-12">
		<?php echo $this->Html->link('Podgląd kliencki',
	    array('controller' => 'Order', 'action' => 'preview', 'plugin' => CLIENT, 'admin' => false, $order['Order']['hash']),
	    array('class' => 'btn btn-success col-xs-12', 'target' => '_blank')
    ); ?>
	</div>
</div>