<?php echo $this->Form->create('Order'); ?>
<div class="row">
	<div class="col-xs-6">
		<div class="form-group">
			<?php echo $this->Form->input('order_status_id', array('class' => 'form-control', 'placeholder' => 'Status', 'label' => 'Status')); ?>
    </div>
	</div>
	<div class="col-xs-6">
		<div class="form-group">			
			<?php echo $this->Form->input('shipment_no', array('type' => 'text', 'class' => 'form-control', 'placeholder' => 'Numer listu przewozowego', 'label' => 'Numer listu przewozowego')); ?>
    </div>
	</div>
</div>
<hr/>
<div class="row">	
	<div class="col-xs-12">
		<div class="form-group">
			<div class="form-group">
        <label for="OrderSendInvoice"><?php echo __('Wyślij fakturę:'); ?></label>
        <label class="checkbox-inline c-checkbox">
          <?php echo $this->Form->input('send_invoice', array('type' => 'checkbox', 'label' => false, 'div' => false)); ?><span class="fa fa-check"></span>
        </label>
      </div>
    </div>
	</div>
</div>
<?php echo $this->Form->end(); ?>