<?php echo $this->Form->create(null, array('url' => array('admin' => true, 'plugin' => 'KeyAdmin', 'controller' => 'KeyOrders', 'action' => 'generate_invoice', $orderId))); ?>
<div class="row">
	<div class="col-xs-6">
		<div class="form-group">
			<?php echo $this->Form->input('Invoice.sell_date', array('type' => 'text','class' => 'form-control', 'placeholder' => 'Data sprzedaży', 'label' => 'Data sprzedaży')); ?>
    </div>
	</div>
	<div class="col-xs-6">
		<div class="form-group">
      <?php echo $this->Form->input('Invoice.set_date', array('type' => 'text', 'class' => 'form-control', 'placeholder' => 'Data wystawienia', 'label' => 'Data wystawienia')); ?>
    </div>
	</div>
</div>
<hr/>
<div class="row">
	<div class="col-xs-6">
		<div class="form-group">			
      <?php echo $this->Form->input('Invoice.payment_id', array('class' => 'form-control', 'placeholder' => 'Sposób zapłaty', 'label' => 'Sposób zapłaty')); ?>
    </div>
	</div>
</div>
<hr/>
<div class="row">
	<div class="col-xs-6">
		<div class="form-group">			
			<?php $option = array('Zapłacono' => 'Zapłacono', 'Termin płatności' => 'Termin płatności'); ?>
			<?php echo $this->Form->input('Invoice.payment_type', array('options' => $option, 'class' => 'form-control', 'placeholder' => 'Opis', 'label' => 'Opis daty')); ?>
    </div>
	</div>
	<div class="col-xs-6">
		<div class="form-group">
			<?php echo $this->Form->input('Invoice.paid_date', array('type' => 'text', 'class' => 'form-control', 'placeholder' => 'Data sprzedaży', 'label' => 'Data')); ?>
    </div>
	</div>
</div>
<?php echo $this->Form->end(); ?>