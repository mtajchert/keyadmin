<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <title><?php echo empty($title) ? __('KeyShop') : str_replace('%title%', $title, __('%title% - KeyShop')); ?></title>

    <?php echo $this->fetch('meta'); ?>
    <?php echo $this->Html->meta('icon'); ?>

    <?php $this->AssetCompress->config(AssetConfig::buildFromIniFile(APP.'Plugin'.DS.'KeyAdmin'.DS.'Config'.DS.'asset_compress.ini')); ?>
    
    <?php echo $this->AssetCompress->css('KeyAdmin', array('raw' => true)); ?>
    <?php echo $this->AssetCompress->script('KeyAdmin-header', array('raw' => true)); ?>
  </head>

  <body class="layout-fixedx">
    <div class="wrapper">
      <!-- Page content-->
      <div class="content-wrapper">
        <?php
        echo $this->Session->flash();
        echo $this->fetch('content');
        echo $this->element('sql_dump');
        ?>
      </div>
    </div>

    <?php echo $this->element('templates'); ?>

    <?php echo $this->AssetCompress->script('KeyAdmin-bottom', array('raw' => true)); ?>
  </body>

</html>
