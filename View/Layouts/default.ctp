<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    
    <?php if(isset($redirect_url) && !empty($redirect_url)): ?>
      <meta http-equiv="refresh" content="0; url=<?php echo $redirect_url; ?>" />
    <?php endif; ?>
    
    <title><?php echo empty($title) ? __('KeyShop') : str_replace('%title%', $title, __('%title% - KeyShop')); ?></title>

    <?php echo $this->fetch('meta'); ?>
    <?php echo $this->Html->meta('icon'); ?>

    <?php $this->AssetCompress->config(AssetConfig::buildFromIniFile(APP.'Plugin'.DS.'KeyAdmin'.DS.'Config'.DS.'asset_compress.ini')); ?>
    
    <?php echo $this->AssetCompress->css('KeyAdmin', array('raw' => true)); ?>
    <?php echo $this->AssetCompress->script('KeyAdmin-header', array('raw' => true)); ?>
  </head>

  <body class="layout-fixed">
    <div class="wrapper">
      <!-- top navbar-->
      <header class="topnavbar-wrapper">
        <!-- START Top Navbar-->
        <nav role="navigation" class="navbar topnavbar">
          <!-- START navbar header-->
          <div class="navbar-header">
            <a href="/admin/dashboard" class="navbar-brand">
              <div class="brand-logo">
                <img src="/key_admin/img/logo.png" style="height: 25px;" alt="Image" class="block-center img-rounded">&nbsp;&nbsp;KeyAdmin v.1.0
              </div>
              <div class="brand-logo-collapsed" style="height: 55px;">
                <img src="/key_admin/img/logo.png" alt="App Logo" class="img-responsive" style="max-height: 100%;">
              </div>
            </a>
          </div>
          <!-- END navbar header-->
          <!-- START Nav wrapper-->
          <div class="nav-wrapper">
            <!-- START Left navbar-->
            <ul class="nav navbar-nav">
              <li>
                <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops-->
                <a href="#" data-toggle-state="aside-collapsed" class="hidden-xs">
                  <em class="fa fa-navicon"></em>
                </a>
                <!-- Button to show/hide the sidebar on mobile. Visible on mobile only.-->
                <a href="#" data-toggle-state="aside-toggled" data-no-persist="true" class="visible-xs sidebar-toggle">
                  <em class="fa fa-navicon"></em>
                </a>
              </li>
              <!-- START User avatar toggle-->
              <li>
                <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops-->
                <a id="user-block-toggle" href="#user-block" data-toggle="collapse">
                  <em class="icon-user"></em>
                </a>
              </li>
              <!-- END User avatar toggle-->
              <!-- START lock screen-->
              <li>
                <?php echo $this->Html->link('<em class="icon-logout"></em>', array('controller' => 'KeyAuth', 'action' => 'logout', 'admin' => true), array('escape' => false, 'title' => __('Wyloguj'))); ?>
              </li>
              <!-- END lock screen-->
            </ul>
            <!-- END Left navbar-->
            <!-- START Right Navbar-->
            <ul class="nav navbar-nav navbar-right">
              <!-- Search icon-->
              <?php /*
              <li>
                <a href="#" data-search-open="">
                  <em class="icon-magnifier"></em>
                </a>
              </li>              
              <!-- START Alert menu-->
              <li class="dropdown dropdown-list">
                <a href="#" data-toggle="dropdown">
                  <em class="icon-bell"></em>
                  <div class="label label-danger">11</div>
                </a>
                <!-- START Dropdown menu-->
                <ul class="dropdown-menu animated flipInX">
                  <li>
                    <!-- START list group-->
                    <div class="list-group">
                      <!-- list item-->
                      <a href="#" class="list-group-item">
                        <div class="media-box">
                          <div class="pull-left">
                            <em class="fa fa-twitter fa-2x text-info"></em>
                          </div>
                          <div class="media-box-body clearfix">
                            <p class="m0">New followers</p>
                            <p class="m0 text-muted">
                              <small>1 new follower</small>
                            </p>
                          </div>
                        </div>
                      </a>
                      <!-- list item-->
                      <a href="#" class="list-group-item">
                        <div class="media-box">
                          <div class="pull-left">
                            <em class="fa fa-envelope fa-2x text-warning"></em>
                          </div>
                          <div class="media-box-body clearfix">
                            <p class="m0">New e-mails</p>
                            <p class="m0 text-muted">
                              <small>You have 10 new emails</small>
                            </p>
                          </div>
                        </div>
                      </a>
                      <!-- list item-->
                      <a href="#" class="list-group-item">
                        <div class="media-box">
                          <div class="pull-left">
                            <em class="fa fa-tasks fa-2x text-success"></em>
                          </div>
                          <div class="media-box-body clearfix">
                            <p class="m0">Pending Tasks</p>
                            <p class="m0 text-muted">
                              <small>11 pending task</small>
                            </p>
                          </div>
                        </div>
                      </a>
                      <!-- last list item -->
                      <a href="#" class="list-group-item">
                        <small>More notifications</small>
                        <span class="label label-danger pull-right">14</span>
                      </a>
                    </div>
                    <!-- END list group-->
                  </li>
                  
                </ul>
                <!-- END Dropdown menu-->
              </li>
              */ ?>
              <!-- END Alert menu-->
              <!-- Fullscreen (only desktops)-->
              <li class="visible-lg">
                <a href="#" data-toggle-fullscreen="">
                  <em class="fa fa-expand"></em>
                </a>
              </li>
            </ul>
            <!-- END Right Navbar-->
          </div>
          <!-- END Nav wrapper-->
          <!-- START Search form-->
          <form role="search" action="search.html" class="navbar-form">
            <div class="form-group has-feedback">
              <input type="text" placeholder="Type and hit enter ..." class="form-control">
              <div data-search-dismiss="" class="fa fa-times form-control-feedback"></div>
            </div>
            <button type="submit" class="hidden btn btn-default"><?php echo __('Szukaj'); ?></button>
          </form>
          <!-- END Search form-->
        </nav>
        <!-- END Top Navbar-->
      </header>
      <!-- sidebar-->
      <aside class="aside">
        <!-- START Sidebar (left)-->
        <div class="aside-inner">
          <nav data-sidebar-anyclick-close="" class="sidebar">
            <!-- START sidebar nav-->
            <ul class="nav">
              <!-- START user info-->
              <li class="has-user-block">
                <div id="user-block" class="collapse">
                  <div class="item user-block">
                    <!-- User picture-->
                    <div class="user-block-picture">
                      <div class="user-block-status">
                        <img src="/key_admin/img/user/02.jpg" alt="Avatar" width="60" height="60" class="img-thumbnail img-circle">
                        <div class="circle circle-success circle-lg"></div>
                      </div>
                    </div>
                    <!-- Name and Job-->
                    <div class="user-block-info">
                      <span class="user-block-name">Hello, Mike</span>
                      <span class="user-block-role">Designer</span>
                    </div>
                  </div>
                </div>
              </li>
              <!-- END user info-->
              <!-- Iterates over all sidebar items-->
              <li class="nav-heading ">
                <span><?php echo __('Menu'); ?></span>
              </li>

              <li class="<?php echo ($this->request->params['controller'] == 'KeyDashboard' && $this->request->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                <?php echo $this->Html->link('<em class="icon-speedometer"></em><span>'.__('Dashboard').'</span>', array('controller' => 'KeyDashboard', 'action' => 'index', 'admin' => true), array('escape' => false, 'title' => __('Dashboard'))); ?>
              </li>

              <li>
                <?php echo $this->Html->link('<em class="icon-grid"></em><span>'.__('Asortyment').'</span>', '#assortment', array('escape' => false, 'title' => 'Asortyment', 'data-toggle' => 'collapse')); ?>
                <ul id="assortment" class="nav sidebar-subnav collapse">
                  <li class="sidebar-subnav-header"><?php echo __('Asortyment'); ?></li>
                  <li class="<?php echo ($this->request->params['controller'] == 'KeyCategories') ? 'active' : ''; ?>">
                    <?php echo $this->Html->link('<span>'.__('Kategorie').'</span>', array('controller' => 'KeyCategories', 'action' => 'index', 'admin' => true), array('escape' => false, 'title' => __('Kategorie'))); ?>
                  </li>
                  <li class="<?php echo ($this->request->params['controller'] == 'KeyProducts' && $this->request->params['action'] == 'index') ? 'active' : ''; ?>">
                    <?php echo $this->Html->link('<span>'.__('Produkty').'</span>', array('controller' => 'KeyProducts', 'action' => 'index', 'admin' => true), array('escape' => false, 'title' => __('Produkty'))); ?>
                  </li>
                  <li class="<?php echo ($this->request->params['controller'] == 'KeyProducts' && $this->request->params['action'] == 'index-list') ? 'active' : ''; ?>">
                    <?php echo $this->Html->link('<span>'.__('Szybka lista produktów').'</span>', array('controller' => 'KeyProducts', 'action' => 'index_list', 'admin' => true), array('escape' => false, 'title' => __('Szybka lista produktów'))); ?>
                  </li>
                  <li class="<?php echo ($this->request->params['controller'] == 'KeyManufacturers') ? 'active' : ''; ?>">
                    <?php echo $this->Html->link('<span>'.__('Producenci').'</span>', array('controller' => 'KeyManufacturers', 'action' => 'index', 'admin' => true), array('escape' => false, 'title' => __('Producenci'))); ?>
                  </li>
                  <li class="<?php echo ($this->request->params['controller'] == 'KeyPromotions') ? 'active' : ''; ?>">
                    <?php echo $this->Html->link('<span>'.__('Promocje').'</span>', array('controller' => 'KeyPromotions', 'action' => 'index', 'admin' => true), array('escape' => false, 'title' => __('Promocje'))); ?>
                  </li>
                  <li class="<?php echo ($this->request->params['controller'] == 'KeyOurHits') ? 'active' : ''; ?>">
                    <?php echo $this->Html->link('<span>'.__('Nasz hit').'</span>', array('controller' => 'KeyOurHits', 'action' => 'index', 'admin' => true), array('escape' => false, 'title' => __('Nasz hit'))); ?>
                  </li>
                </ul>
              </li>

              <li>
                <?php echo $this->Html->link('<em class="icon-basket"></em><span>'.__('Sprzedaż').'</span>', '#sale', array('escape' => false, 'title' => __('Sprzedaż'), 'data-toggle' => 'collapse')); ?>
                <ul id="sale" class="nav sidebar-subnav collapse">
                  <li class="sidebar-subnav-header"><?php echo __('Sprzedaż'); ?></li>
                  <li class="<?php echo ($this->request->params['controller'] == 'KeyOrders' && $this->request->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                    <?php echo $this->Html->link('<span>'.__('Zamówienia').'</span>', array('controller' => 'KeyOrders', 'action' => 'index', 'admin' => true), array('escape' => false, 'title' => __('Zamówienia'))); ?>
                  </li>
									<li class="<?php echo ($this->request->params['controller'] == 'KeyOrders' && $this->request->params['action'] == 'admin_archive') ? 'active' : ''; ?>">
                    <?php echo $this->Html->link('<span>'.__('Archiwum').'</span>', array('controller' => 'KeyOrders', 'action' => 'archive', 'admin' => true), array('escape' => false, 'title' => __('Archiwum'))); ?>
                  </li>
                  <li class="<?php echo ($this->request->params['controller'] == 'KeyCustomers') ? 'active' : ''; ?>">
                    <?php echo $this->Html->link('<span>'.__('Klienci').'</span>', array('controller' => 'KeyCustomers', 'action' => 'index', 'admin' => true), array('escape' => false, 'title' => __('Klienci'))); ?>
                  </li>
                  <li class="<?php echo ($this->request->params['controller'] == 'KeyDiscounts') ? 'active' : ''; ?>">
                    <?php echo $this->Html->link('<span>'.__('Kupony rabatowe').'</span>', array('controller' => 'KeyDiscounts', 'action' => 'index', 'admin' => true), array('escape' => false, 'title' => __('Klienci'))); ?>
                  </li>
                  <li class="<?php echo ($this->request->params['controller'] == 'KeyCarts') ? 'active' : ''; ?>">
                    <?php echo $this->Html->link('<span>'.__('Porzucone koszyki').'</span>', array('controller' => 'KeyCarts', 'action' => 'index', 'admin' => true), array('escape' => false, 'title' => __('Porzucone koszyki'))); ?>
                  </li>
                </ul>
              </li>
              
              <?php if (Configure::read('Invoice.quote_enabled')): ?>
                <li>
                  <?php echo $this->Html->link('<em class="icon-calculator"></em><span>'.__('Księgowość').'</span>', '#bookkeeping', array('escape' => false, 'title' => __('Księgowość'), 'data-toggle' => 'collapse')); ?>
                  <ul id="bookkeeping" class="nav sidebar-subnav collapse">
                    <li class="sidebar-subnav-header"><?php echo __('Księgowość'); ?></li>
                    <li class="<?php echo ($this->request->params['controller'] == 'KeyInvoices') && $this->request->params['action'] == 'admin_index_invoices' ? 'active' : ''; ?>">
                      <?php echo $this->Html->link('<span>'.__('Faktury').'</span>', array('controller' => 'KeyInvoices', 'action' => 'index_invoices', 'admin' => true), array('escape' => false, 'title' => __('Faktury VAT'))); ?>
                    </li>
                    <li class="<?php echo ($this->request->params['controller'] == 'KeyInvoices') && $this->request->params['action'] == 'admin_index_quotes' ? 'active' : ''; ?>">
                      <?php echo $this->Html->link('<span>'.__('Faktury Proforma').'</span>', array('controller' => 'KeyInvoices', 'action' => 'index_quotes', 'admin' => true), array('escape' => false, 'title' => __('Faktury Proforma'))); ?>
                    </li>
                  </ul>
                </li>
              <?php endif; ?>

              <li>
                <?php echo $this->Html->link('<em class="icon-picture"></em><span>'.__('Wygląd').'</span>', '#layout', array('escape' => false, 'title' => __('Wygląd'), 'data-toggle' => 'collapse')); ?>
                <ul id="layout" class="nav sidebar-subnav collapse">
                  <li class="sidebar-subnav-header"><?php echo __('Wygląd'); ?></li>
                  <li>
                    <?php echo $this->Html->link('<span>'.__('Strony tekstowe').'</span>', array('controller' => 'KeyPages', 'action' => 'index', 'admin' => true), array('escape' => false, 'title' => __('Strony tekstowe'))); ?>
                  </li>
                  <li>
                    <?php echo $this->Html->link('<span>'.__('Slider').'</span>', array('controller' => 'KeySliders', 'action' => 'index', 'admin' => true), array('escape' => false, 'title' => __('Slider'))); ?>
                  </li>
                  <li>
                    <?php echo $this->Html->link('<span>'.__('Blog').'</span>', array('controller' => 'KeyPages', 'action' => 'blog_index', 'admin' => true), array('escape' => false, 'title' => __('Blog'))); ?>
                  </li>
                  <li>
                    <?php echo $this->Html->link('<span>'.__('Blog - kategorie').'</span>', array('controller' => 'KeyBlogs', 'action' => 'category_index', 'admin' => true), array('escape' => false, 'title' => __('Blog - kategorie'))); ?>
                  </li>
                </ul>
              </li>
              
              <li>
                <?php echo $this->Html->link('<em class="icon-settings"></em><span>'.__('Konfiguracja').'</span>', '#config', array('escape' => false, 'title' => __('Konfiguracja'), 'data-toggle' => 'collapse')); ?>
                <ul id="config" class="nav sidebar-subnav collapse">
                  <li class="sidebar-subnav-header"><?php echo __('Konfiguracja'); ?></li>
                  <?php /*
                  <li class="<?php echo ($this->request->params['controller'] == 'KeyProductOptions' && $this->request['action'] == 'admin_index_car') ? 'active' : ''; ?>">
                    <?php echo $this->Html->link('<span>'.__('Marki/modele aut').'</span>', array('controller' => 'KeyProductOptions', 'action' => 'index_cars', 'admin' => true), array('escape' => false, 'title' => __('Marki/modele aut'))); ?>
                  </li>*/ ?>
                  <li>
                    <?php echo $this->Html->link('<span>'.__('Dane teleadresowe').'</span>', '#', array('escape' => false, 'title' => __('Dane teleadresowe'))); ?>
                  </li>
                  <li class="<?php echo ($this->request->params['controller'] == 'KeyTaxRates') ? 'active' : ''; ?>">
                    <?php echo $this->Html->link('<span>'.__('Stawki VAT').'</span>', array('controller' => 'KeyTaxRates', 'action' => 'index', 'admin' => true), array('escape' => false, 'title' => __('Stawki VAT'))); ?>
                  </li>
                  <li class="<?php echo ($this->request->params['controller'] == 'KeyShippingTimes') ? 'active' : ''; ?>">
                    <?php echo $this->Html->link('<span>'.__('Terminy wysyłki').'</span>', array('controller' => 'KeyShippingTimes', 'action' => 'index', 'admin' => true), array('escape' => false, 'title' => __('Terminy wysyłki'))); ?>
                  </li>
                  <li class="<?php echo ($this->request->params['controller'] == 'KeyProductOptions' && $this->request['action'] == 'admin_index') ? 'active' : ''; ?>">
                    <?php echo $this->Html->link('<span>'.__('Cechy produktów').'</span>', array('controller' => 'KeyProductOptions', 'action' => 'index', 'admin' => true), array('escape' => false, 'title' => __('Cechy produktów'))); ?>
                  </li>
                  <li class="<?php echo ($this->request->params['controller'] == 'KeyProductConditions') ? 'active' : ''; ?>">
                    <?php echo $this->Html->link('<span>'.__('Stany produktów').'</span>', array('controller' => 'KeyProductConditions', 'action' => 'index', 'admin' => true), array('escape' => false, 'title' => __('Stany produktów'))); ?>
                  </li>
                  <li class="<?php echo ($this->request->params['controller'] == 'KeyProductWarranties') ? 'active' : ''; ?>">
                    <?php echo $this->Html->link('<span>'.__('Gwarancje produktów').'</span>', array('controller' => 'KeyProductWarranties', 'action' => 'index', 'admin' => true), array('escape' => false, 'title' => __('Gwarancje produktów'))); ?>
                  </li>
                  <li class="<?php echo ($this->request->params['controller'] == 'KeyProductAvailabilities') ? 'active' : ''; ?>">
                    <?php echo $this->Html->link('<span>'.__('Dostępności produktów').'</span>', array('controller' => 'KeyProductAvailabilities', 'action' => 'index', 'admin' => true), array('escape' => false, 'title' => __('Dostępności produktów'))); ?>
                  </li>
                  <li class="<?php echo ($this->request->params['controller'] == 'KeyUnits') ? 'active' : ''; ?>">
                    <?php echo $this->Html->link('<span>'.__('Jednostki miary').'</span>', array('controller' => 'KeyUnits', 'action' => 'index', 'admin' => true), array('escape' => false, 'title' => __('Jednostki miary'))); ?>
                  </li>
                  <li class="<?php echo ($this->request->params['controller'] == 'KeyShippings') ? 'active' : ''; ?>">
                    <?php echo $this->Html->link('<span>'.__('Formy dostawy').'</span>', array('controller' => 'KeyShippings', 'action' => 'index', 'admin' => true), array('escape' => false, 'title' => __('Formy dostawy'))); ?>
                  </li>
                  <li class="<?php echo ($this->request->params['controller'] == 'KeyPayments') ? 'active' : ''; ?>">
                    <?php echo $this->Html->link('<span>'.__('Formy płatności').'</span>', array('controller' => 'KeyPayments', 'action' => 'index', 'admin' => true), array('escape' => false, 'title' => __('Formy płatności'))); ?>
                  </li>
                  <li class="<?php echo ($this->request->params['controller'] == 'KeyNotifications') ? 'active' : ''; ?>">
                    <?php echo $this->Html->link('<span>'.__('Powiadomienia').'</span>', array('controller' => 'KeyNotifications', 'action' => 'index', 'admin' => true), array('escape' => false, 'title' => __('Powiadomienia'))); ?>
                  </li>
                  <li class="<?php echo ($this->request->params['controller'] == 'KeyConfigs') ? 'active' : ''; ?>">
                    <?php echo $this->Html->link('<span>'.__('Ustawienia').'</span>', array('controller' => 'KeyConfigs', 'action' => 'index', 'admin' => true), array('escape' => false, 'title' => __('Ustawienia'))); ?>
                  </li>
                </ul>
              </li>
              
              <?php /*
              <li>
                <?php echo $this->Html->link('<em class="icon-graph"></em><span>'.__('Statystyki - Sprzedaż').'</span>', '#stats-sale', array('escape' => false, 'title' => __('Statystyki - Sprzedaż'), 'data-toggle' => 'collapse')); ?>
                <ul id="stats-sale" class="nav sidebar-subnav collapse">
                  <li class="sidebar-subnav-header"><?php echo __('Statystyki - Sprzedaż'); ?></li>
                  <li>
                    <?php echo $this->Html->link('<span>'.__('Klienci').'</span>', '#', array('escape' => false, 'title' => __('Klienci'))); ?>
                  </li>
                  <li>
                    <?php echo $this->Html->link('<span>'.__('Asortyment').'</span>', '#', array('escape' => false, 'title' => __('Asortyment'))); ?>
                  </li>
                </ul>
              </li>
              */ ?>
            </ul>
            <!-- END sidebar nav-->
          </nav>
        </div>
        <!-- END Sidebar (left)-->
      </aside>
      <!-- Main section-->
      <section>
        <!-- Page content-->
        <div class="content-wrapper">
          <?php if (isset($content_title) || isset($content_subtitle) || (isset($buttons_template))): ?>
            <div class="content-heading">
              <?php if (isset($buttons_template) && !empty($buttons_template)): ?>
                <div class="pull-right">
                  <div class="col-xs-12">
                    <?php echo $this->element($buttons_template); ?>
                  </div>
                </div>
              <?php endif; ?>
              <?php echo $content_title; ?>
              <?php if (isset($content_subtitle)): ?><small><?php echo $content_subtitle; ?></small><?php endif; ?>
            </div>
          <?php endif; ?>
          <?php
          echo $this->Session->flash();
          echo $this->fetch('content');
          echo $this->element('sql_dump');
          ?>
        </div>
      </section>
      <!-- Page footer-->
      <footer>
        <span><?php echo __('&copy; %s by KeyShop', date('Y')); ?></span>
      </footer>
    </div>

    <?php echo $this->element('templates'); ?>

    <?php echo $this->AssetCompress->script('KeyAdmin-bottom', array('raw' => true)); ?>
  </body>

</html>