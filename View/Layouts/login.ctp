<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <title>Login - KeyShop</title>

    <?php echo $this->fetch('meta'); ?>
    <?php echo $this->Html->meta('icon'); ?>

    <?php $this->AssetCompress->config(AssetConfig::buildFromIniFile(APP.'Plugin'.DS.'KeyAdmin'.DS.'Config'.DS.'asset_compress.ini')); ?>
    <?php echo $this->AssetCompress->css('KeyAdmin-login', array('raw' => true)); ?>
  </head>

  <body>
    <div class="wrapper">
      <div class="block-center mt-xl wd-xl">
        <!-- START panel-->
        <div class="panel panel-dark panel-flat">
          <div class="panel-heading text-center login-header">
            <img src="/key_admin/img/logo.png" style="height: 30px;" alt="Image" class="block-center img-rounded">&nbsp;&nbsp;KeyAdmin v.1.0
          </div>
          <div class="panel-body">
            <?php echo $this->fetch('content'); ?>

            <?php echo $this->element('sql_dump'); ?>
          </div>
        </div>
        <!-- END panel-->
        <div class="p-lg text-center">
            <span>&copy;</span>
            <span>2015</span>
            <span>-</span>
            <span>M&M Solutions</span>
         </div>
      </div>
      
      <?php echo $this->AssetCompress->script('KeyAdmin-login', array('raw' => true)); ?>
  </body>
</html>