<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <?php echo $this->Form->create('Manufacturer', array('class' => 'form-horizontal', 'type' => 'file')); ?>
      <div class="col-md-8">
        <div class="form-group <?php echo $this->Form->isFieldError('name') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('name', array('label' => __('Nazwa producenta: *'), 'placeholder' => __('Wpisz nazwę producenta'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
        <div class="form-group <?php echo $this->Form->isFieldError('url') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('url', array('label' => __('Adres strony: *'), 'placeholder' => __('Wpisz adres strony producenta'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
        <div class="form-group <?php echo $this->Form->isFieldError('meta_title_tag') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('meta_title_tag', array('label' => __('Meta tagi - tytuł:'), 'placeholder' => __('Wpisz meta tag - tytuł'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
        <div class="form-group <?php echo $this->Form->isFieldError('meta_desc_tag') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('meta_desc_tag', array('label' => __('Meta tagi - opis:'), 'placeholder' => __('Wpisz meta tag - opis'), 'class' => 'form-control', 'rows' => 3, 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
        <div class="form-group <?php echo $this->Form->isFieldError('meta_keywords_tag') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('meta_keywords_tag', array('label' => __('Meta tagi - słowa kluczowe:'), 'placeholder' => __('Wpisz meta tag - słowa kluczowe'), 'class' => 'form-control', 'rows' => 3, 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>

      </div>
      <div class="col-md-4">
        <div class="form-group form-group-margin-left-0">
          <div class="col-xs-12">
            <label>Zdjęcie:</label>
            <?php if (isset($this->data['Manufacturer']['image']) && !empty($this->data['Manufacturer']['image'])): ?>
              <?php echo $this->App->showManufacturerImage($this->data['Manufacturer']['image'], 'THUMB'); ?>
            <?php endif; ?>
            <input type="file" name="data[Manufacturer][image]" class="image-choosen" data-show-upload="false" data-show-caption="true"/>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group <?php echo $this->Form->isFieldError('description') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('description', array('label' => __('Opis producenta:'), 'placeholder' => __('Wpisz opis producenta'), 'class' => 'form-control', 'rows' => 10, 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
      </div>
      <div class="col-md-12">
        <div class="pull-right">

          <?php echo $this->Html->link(__('Anuluj'), array('controller' => 'KeyManufacturers', 'action' => 'index', 'admin' => true), array('title' => __('Anuluj'), 'class' => 'btn btn-default')); ?>
          <button type="submit" class="btn btn-primary"><?php echo __('Zapisz'); ?></button>
        </div>
      </div>
      <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
    </div>
  </div>
</div>

<script>
  $(document).ready(function () {
    CKEDITOR.replace('ManufacturerDescription', {
      filebrowserBrowseUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'browse')); ?>',
			filebrowserUploadUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'upload')); ?>',
    });
    
    <?php echo $this->element('Scripts/AppManufacturersDefaultSettings'); ?>
  });
</script>