<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th><?php echo $this->Paginator->sort('id', __('ID')); ?></th>
              <th><?php echo $this->Paginator->sort('name', __('Nazwa')); ?></th>
              <th><?php echo $this->Paginator->sort('days', __('Ilość dni')); ?></th>
              <th><?php echo __('Opcje'); ?></th>
            </tr>
          </thead>
          <tbody>
            <?php if (isset($shipping_times) && !empty($shipping_times)): ?>
              <?php foreach ($shipping_times as $shipping_time): ?>
                <tr>
                  <td><?php echo h($shipping_time['ShippingTime']['id']); ?>&nbsp;</td>
                  <td><?php echo h($shipping_time['ShippingTime']['name']); ?>&nbsp;</td>
                  <td><?php echo h($shipping_time['ShippingTime']['days']); ?>&nbsp;</td>
                  <td class="options-buttons text-center">
                    <?php echo $this->Html->link('<em class="icon-note"></em>', array('controller' => 'KeyShippingTimes', 'action' => 'edit', $shipping_time['ShippingTime']['id']), array('escape' => false, 'title' => __('Edytuj'))); ?>
                    &nbsp;
                    <?php echo $this->Html->link('<em class="icon-trash"></em>', 'javascript:void(0);', array('escape' => false, 'title' => __('Usuń'), 'onclick' => 'App.ShippingTimes.deleteShippingTime(\''.h($shipping_time['ShippingTime']['name']).'\', \''.$this->Html->url(array('controller' => 'KeyShippingTimes', 'action' => 'delete', $shipping_time['ShippingTime']['id'])).'\')')); ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <td colspan="4"><?php echo __('Brak terminów wysyłki do wyświetlenia'); ?></td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="panel-footer text-center">
      <div class="col-sm-6">
        <div class="pull-left">
          <?php echo $this->Paginator->counter(array('format' => __('Strona {:page} z {:pages}'))); ?>
        </div>
      </div>
      <div class="col-sm-6 pagination pagination-large">
        <ul class="pagination pull-right">
          <?php
          $this->Paginator->options['url'] = array('controller' => 'KeyShippingTimes', 'action' => 'index');
          echo $this->Paginator->prev(__('poprzednia'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1, 'modulus' => 5, 'ellipsis' => '<li class="disabled"><a>...</a></li>'));
          echo $this->Paginator->next(__('następna'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          ?>
        </ul>
      </div>
      <span class="clearfix"></span>
    </div>
  </div>
</div>

<script>
  $(document).ready(function () {
    <?php echo $this->element('Scripts/AppShippingTimesDefaultSettings'); ?>
    App.ShippingTimes.onDocReady();
  });
</script>