<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-body">
            <?php echo $this->Form->create('ProductTemplate', array('type' => 'file')); ?>
                <?php echo $this->Form->hidden('id'); ?>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <div class="checkbox c-checkbox">
                                <label>
                                    <?php echo $this->Form->input('is_public', array('type' => 'checkbox', 'label' => false, 'div' => false, 'error' => false)); ?>
                                    <span class="fa fa-check"></span><?php echo __('Opublikuj'); ?>
                                </label>
                            </div>
                        </div>
                    </div>     
                    <div class="col-xs-6 text-right" style="padding: 5px 30px;">
                        Ostatnio zapisano: <span id="last-saved">---</span>
                    </div>               
                </div>
                <hr style="margin-top: 0px;"/>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <strong>Nagłowek tekst</strong>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <?php echo $this->Form->input('header_text', array('label' => false)); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <strong>Nagłowek zdjęcie</strong>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="form-image <?php echo (!empty($this->data['ProductTemplate']['header_image']))? 'hide': ''; ?>">
                                        <?php echo $this->Form->input('header_image', array('label' => 'Wybierz plik', 'type' => 'file', 'class' => 'form-control', 'name' => 'header_image')); ?>
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">0%</div>
                                        </div>
                                    </div>    
                                    <div class="view-image <?php echo (empty($this->data['ProductTemplate']['header_image']))? 'hide': ''; ?>">
                                        <?php echo $this->Html->link('Usuń', array(
                                            'admin' => true,
                                            'controller' => 'KeyProducts',
                                            'action' => 'delete_image',
                                            'header_image', 
                                            $this->data['ProductTemplate']['id']
                                        ), array('class' => 'center-block btn btn-danger btn-delate-template-image')); ?>
                                        <img src="/img/description/<?php echo $this->data['ProductTemplate']['header_image']?>" class="img-responsive center-block" />
                                    </div>                                                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <?php /* Pierwsza linia */ ?>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <strong>Pierwsza linia</strong>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <?php echo $this->Form->input('first_position', array('options' => [
                                                'top' => 'Do góry',
                                                'middle' => 'Do środka',
                                                'bottom' => 'Do dołu'
                                            ], 'class' => 'form-control', 'label' => false)); ?>
                                        </div>
                                        <div class="form-group">
                                            <?php echo $this->Form->input('first_text', array('label' => false)); ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <div class="form-image <?php echo (!empty($this->data['ProductTemplate']['first_image']))? 'hide': ''; ?>">
                                                <?php echo $this->Form->input('first_image', array(
                                                    'label' => 'Wybierz plik', 'type' => 'file', 'class' => 'form-control', 'name' => 'first_image')); ?>
                                                <div class="progress">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">0%</div>
                                                </div>
                                            </div>    
                                            <div class="view-image <?php echo (empty($this->data['ProductTemplate']['first_image']))? 'hide': ''; ?>">
                                                <?php echo $this->Html->link('Usuń', array(
                                                    'admin' => true,
                                                    'controller' => 'KeyProducts',
                                                    'action' => 'delete_image',
                                                    'first_image', 
                                                    $this->data['ProductTemplate']['id']
                                                ), array('class' => 'center-block btn btn-danger btn-delate-template-image')); ?>
                                                <img src="/img/description/<?php echo $this->data['ProductTemplate']['first_image']?>" class="img-responsive center-block" />
                                            </div>    
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <?php /* Druga linia */ ?>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <strong>Druga linia</strong>
                            </div>
                            <div class="panel-body">
                                <div class="row">                                    
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <div class="form-image <?php echo (!empty($this->data['ProductTemplate']['sec_image']))? 'hide': ''; ?>">
                                                <?php echo $this->Form->input('sec_image', array(
                                                    'label' => 'Wybierz plik', 'type' => 'file', 'class' => 'form-control', 'name' => 'sec_image')); ?>
                                                <div class="progress">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">0%</div>
                                                </div>
                                            </div>    
                                            <div class="view-image <?php echo (empty($this->data['ProductTemplate']['sec_image']))? 'hide': ''; ?>">
                                                <?php echo $this->Html->link('Usuń', array(
                                                    'admin' => true,
                                                    'controller' => 'KeyProducts',
                                                    'action' => 'delete_image',
                                                    'first_image', 
                                                    $this->data['ProductTemplate']['id']
                                                ), array('class' => 'center-block btn btn-danger btn-delate-template-image')); ?>
                                                <img src="/img/description/<?php echo $this->data['ProductTemplate']['sec_image']?>" class="img-responsive center-block" />
                                            </div>    
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <?php echo $this->Form->input('sec_position', array('options' => [
                                                'top' => 'Do góry',
                                                'middle' => 'Do środka',
                                                'bottom' => 'Do dołu'
                                            ], 'class' => 'form-control', 'label' => false)); ?>
                                        </div>
                                        <div class="form-group">
                                            <?php echo $this->Form->input('sec_text', array('label' => false)); ?>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>
                
                
                
                <?php /* Trzecia linia */ ?>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <strong>Trzecia linia</strong>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <?php echo $this->Form->input('third_position', array('options' => [
                                                'top' => 'Do góry',
                                                'middle' => 'Do środka',
                                                'bottom' => 'Do dołu'
                                            ], 'class' => 'form-control', 'label' => false)); ?>
                                            <?php echo $this->Form->input('third_text', array('label' => false)); ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <div class="form-image <?php echo (!empty($this->data['ProductTemplate']['third_image']))? 'hide': ''; ?>">
                                                <?php echo $this->Form->input('third_image', array(
                                                    'label' => 'Wybierz plik', 'type' => 'file', 'class' => 'form-control', 'name' => 'third_image')); ?>
                                                <div class="progress">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">0%</div>
                                                </div>
                                            </div>    
                                            <div class="view-image <?php echo (empty($this->data['ProductTemplate']['third_image']))? 'hide': ''; ?>">
                                                <?php echo $this->Html->link('Usuń', array(
                                                    'admin' => true,
                                                    'controller' => 'KeyProducts',
                                                    'action' => 'delete_image',
                                                    'first_image', 
                                                    $this->data['ProductTemplate']['id']
                                                ), array('class' => 'center-block btn btn-danger btn-delate-template-image')); ?>
                                                <img src="/img/description/<?php echo $this->data['ProductTemplate']['third_image']?>" class="img-responsive center-block" />
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
      <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
    </div>
    </div>
</div>


<script type="text/javascript">
function saveForm() {
    $.each(CKEDITOR.instances, function(item){
        this.updateElement();
    });
    $.ajax({
        evalScripts: true,
        url: $('#ProductTemplateAdminEditTemplateForm').attr('action'),
        type: 'POST',
        data: $('#ProductTemplateAdminEditTemplateForm').serialize(),
        success: function (data, textStatus) {
            $('#last-saved').html(moment().locale('pl').format('hh:mm:ss'));
        },
        error: function (data, textStatus) {
          	alert('Skontaktuj się z administratiorem, bo jest syf');
        }
    });
}
$('document').ready(function(){
    CKEDITOR.replace('ProductTemplateHeaderText', {
        filebrowserBrowseUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'browse')); ?>',
		filebrowserUploadUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'upload')); ?>',
    });
    CKEDITOR.replace('ProductTemplateFirstText', {
        filebrowserBrowseUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'browse')); ?>',
		filebrowserUploadUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'upload')); ?>',
    });
    CKEDITOR.replace('ProductTemplateSecText', {
        filebrowserBrowseUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'browse')); ?>',
		filebrowserUploadUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'upload')); ?>',
    });
    CKEDITOR.replace('ProductTemplateThirdText', {
        filebrowserBrowseUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'browse')); ?>',
		filebrowserUploadUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'upload')); ?>',
    });


    setInterval(function(){
        saveForm();
    }, 10000);


    var fileName = '';
    
    $('#ProductTemplateHeaderImage, #ProductTemplateFirstImage, #ProductTemplateSecImage, #ProductTemplateThirdImage').each(function(){
        var url = '/admin/products/image_upload/' + $(this).attr('name');
        var name = $(this).attr('name');
        var $container = $(this).closest('.form-group');
        var $progressBar = $(this).closest('.form-group').find('.progress .progress-bar');
        $(this).fileupload({
            maxChunkSize: 100000, // 10 MB
            url: url,
            add: function (e, data) {
                var that = this;
                var time = new Date().getTime();
                fileName = time+data.files[0].name;      
                
                $progressBar.html('0%');
                $progressBar.css('width', '0%');
                
                $.getJSON(url, {file: fileName}, function (result) {
                    var file = result.file;
                    data.uploadedBytes = file && file.size;
                    data.files[0]['new_name'] = fileName;
                    
                    $.blueimp.fileupload.prototype
                        .options.add.call(that, e, data);
                });
            },
            progressall: function (e, data) {
              var progress = parseInt( data.loaded / data.total * 100, 10);
              $progressBar.html(progress+'%');
              $progressBar.css('width', progress+'%');
              
              if (progress == 100){
                setTimeout( function(){            
                    $progressBar.html('<strong>Przetwarzanie...</strong>').addClass('progress-bar-success');            
                }, 100 ); 
              }
            },
            done: function(e,data) {
              var response = $.parseJSON(data._response.result);
              $.each(response, function(key, value){
                  $.ajax({
                      evalScripts: true,
                      url: '/admin/products/save_image/' + $('#ProductTemplateId').val() + '/' + key,
                      type: 'POST',
                      data: {file_name: value[0].name},
                      success: function (data, textStatus) {                          
                          $container.find('.form-image').addClass('hide');
                          $container.find('.view-image').removeClass('hide');
                          $container.find('.view-image img').attr('src', data);
                      },
                      error: function (data, textStatus) {
                        	alert('Skontaktuj się z administratiorem, bo jest syf');
                      }
                  });
              });
              
            }  
          });
    });
    
});
</script>