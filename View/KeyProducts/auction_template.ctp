<style type="text/css">body{margin: 0px; background:url('https://sweetsleep.pl/sweet_sleep/img/body-background.jpg') repeat;}
.navbar{background-color: #DDDDDD; text-align: center; color: black;font-size 16px;font-weight: bold;padding: 5px;}


.menu-item:hover{color: black;}


.alca-col{min-height: 1px; padding: 0px 15px;text-align: center;}
li{text-align: left;}
table{width: 100%;max-width: 100%;border-spacing: 0;border-collapse: collapse;}
th{vertical-align: bottom;border-top: 2px solid #ddd;padding: 8px;line-height: 1.42857143;text-align: center;}
td{padding: 8px;line-height: 1.42857143;vertical-align: top;border-top: 1px solid #ddd;text-align: center;}

.auction-content{
  padding: 30px 20px;
  font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
}
.menu{padding: 0px 0px;}
.logo{display: block; float: left; height: 100px;padding: 0px 20px;margin-right: 10px;}
.nav{float: right;margin-top: 30px;margin-right: 30px;}
.menu-item{font-size: 12px;color: #78828d;text-decoration:none;display: block;float: left;padding: 10px 8px;}
.menu-item:hover{text-decoration: underline;color: #78828d;}
.left-column{width:33%;float:left;margin-top: 20px;}
.col{min-height: 1px; padding: 0px 15px;text-align: center;}
.panel{margin-bottom:20px;padding: 10px;background-color: rgba(221,221,221, 0.2);}
.panel .header {color:#fff;font-weight:bold;background-color:#8ce2e7;border-color:#8ce2e7;padding:13px 15px 10px 15px;border-bottom:1px solid transparent;}
.panel .content {padding:15px 0px 5px 0px;text-align: justify;line-height: 22px;}
.realization-ico{display:block;max-width: 70%; text-align: center; margin: 0 auto;max-height: 110px;}
.right-column{width: 66%;float: right;margin-top: 20px;}
.header-separator {
  display: block;
  margin: 10px auto;
}
h1 {
  font-size: 40px;
  color: #34373c;
  text-transform: uppercase;
  text-align: center;
}
.price-box{
  color: #34373c;
  font-size: 17px;
  font-weight: bold;
  text-align: center;
}
.price-box .price-val{
  font-size: 58px;
  color: #8ce2e7;
}
.price-box .price-desc {
  font-size: 40px;
  color: #8ce2e7;
}
.product-part-title {
  border-top: 2px solid #d5d5d5;
  color: #78828d;
  font-size: 17px;
  text-align: right;
  margin-top: 10px;
  margin-bottom: 20px;
  padding-top: 10px;
  padding-right: 20px;
}
.center-block{
  display: block;
  margin: 0 auto;
}
.img-responsive{
  max-width: 100%;
}
.col-ico-shoe{
  width: 20%;
  float: left;
}
p {
  line-height: 25px;
  font-size: 14pt;
}
</style>

<div class="auction-content">
	<div class="menu">
  	<img class="logo" src="https://sweetsleep.pl/logo-trans.png" />
  	<div class="nav">
  		<div style="float: left;color: #78828d;font-size: 18px;"><img src="https://sweetsleep.pl/img/allegro/phone-ico.jpg" style="float: left;margin-top: 3px;"/>&nbsp;500 424 829</div>
  		<div style="float: right;color: #78828d;font-size: 18px;"><img src="https://sweetsleep.pl/img/allegro/mail-ico.jpg" style="float: left;margin-top: 2px;"/>&nbsp;kontakt@sweetsleep.pl</div>
  		<div style="clear:both"></div>
  		<div class="nav-menu-item" style="border-top: 1px solid #d5d5d5;margin-top: 10px;">
  			<a class="menu-item" href="http://allegro.pl/listing/user/listing.php?us_id=42457370">Nasze aukcje</a>
  			<a class="menu-item" href="http://allegro.pl/show_user.php?uid=42457370">Komentarze</a>
  			<a class="menu-item" href="http://allegro.pl/myaccount/favourites/favourites_sellers.php/addNew/?userId=42457370">Dodaj do ulubionych</a>
  			<a class="menu-item" href="http://allegro.pl/SendMailToUser.php?userId=42457370">Zadaj pytanie</a>
  			<div style="clear:both"></div>
  		</div>
  	</div>
  </div>
  
  <div style="clear: both;">&nbsp;</div>
  
  <div class="left-column">
    <div class="col">
    	
    	<div class="panel">
    		<div class="header"><span style="font-size:16px;">O NAS</span></div>    
    		<div class="content"><span style="font-size:15px;">Zajmujemy się sprzedażą najlepszej bielizny nocnej i dodatk&oacute;w. Nasz asortyment obejmuje piżamy damskie, koszule nocne, jak r&oacute;wnież oryginalne kapcie baleriny oraz skarpety. Wszystkie produkty pochodzą od renomowanych, gł&oacute;wnie polskich producent&oacute;w. Stawiamy na jakość, czego dowodem jest wciąż powiększająca się rzesza zadowolonych klient&oacute;w.</span></div>
    	</div>
    	
    	<div class="panel">
    		<div class="header"><span style="font-size:16px;">REALIZACJA ZAMÓWIEŃ</span></div>    
    		<div class="content" style="text-align: center;">
    			<img class="realization-ico" style="max-width: 100%;" src="https://sweetsleep.pl/img/allegro/realization-ico.png" />
    			<div style="clear: both;">&nbsp;</div>
    			<img class="realization-ico" src="https://inpost.pl/images/big-logo-inpost-black-8de51bf.png" />
    			<div style="clear: both;">&nbsp;</div>
    			<img class="realization-ico" src="https://s-media-cache-ak0.pinimg.com/236x/3d/c8/b1/3dc8b1630a25dba8fde55ce7e27a5cc6.jpg" />
    			<div style="clear: both;">&nbsp;</div>
    			<img class="realization-ico" src="https://sweetsleep.pl/img/allegro/logotyp_pp.jpg" />
    			<div style="clear: both;">&nbsp;</div>
  			</div>
    	</div>
    	
    	
    	
    	<div class="panel">
    		<div class="header"><span style="font-size:16px;">PŁATNOŚCI</span></div>    
    		<div class="content"><span style="font-size:15px;">Płatności w naszym sklepie można dokonywać za pomocą systemu szybkich płatności PayU lub za pomocą zwykłego przelewu na poniższy numer konta bankowego:<br/>
    		SweetSleep Natalia Tajchert<br/>ul. Czarnieckiego 14<br/>84-104 Jastrzębia Góra<br/>
    		<div style="clear: both;">&nbsp;</div>
    		<img style="height: 50px;text-align: center;display: block;margin: 0 auto;"  src="https://www.aliorbank.pl/dam/jcr:f0edda60-0383-48b7-a21b-1aaae29d9b80/logo.png" />
    		<div style="text-align: center;font-weight: bold;">83 2490 0005 0000 4500 8092 1522</div>
    		</span>    		
    		</div>
    	</div>
    	
    	<div class="panel">
    		<div class="header"><span style="font-size:16px;">DBAMY O JAKOŚĆ OBSŁUGI</span></div>    
    		<div class="content" style="text-align: center;">
    			<a href="https://prokonsumencki.pl/certyfikat/sweetsleep.pl" target="_blank"><img style="max-width: 100%;" src="https://sweetsleep.pl/img/allegro/prokonsumencki.jpg" /></a>    			
  			</div>
    	</div>
  	</div>
	</div>
	
	<div class="right-column">
		<img class="header-separator" src="https://sweetsleep.pl/img/allegro/header-separator.png" alt="separator" />
		<h1><?php echo $post['Product']['name']; ?></h1>
		<div class="price-box">TERAZ W SUPER CENIE&nbsp;&nbsp;<span class="price-val"><?php echo $post['Product']['price']; ?></span>&nbsp;&nbsp;<span class="price-desc">zł</span></div>
		<div class="product-part-title">Opis produktu</div>
		<p><img alt="" src="https://sweetsleep.pl/manufacturers/origin/<?php echo $product['Manufacturer']['image']; ?>" style="display:block;margin: 0 auto;text-align: center;" /></p>
		<?php if ($post['Product']['show_dreex'] == 1): ?>
  		<div class="row">
        <div class="col-ico-shoe"><img alt="Oddychający materiał" class="center-block img-responsive" src="https://sweetsleep.pl/img/dreex/material.jpg"></div>
        <div class="col-ico-shoe"><img alt="Wysoka rozciągliwość" class="center-block img-responsive" src="https://sweetsleep.pl/img/dreex/rozciagliwosc.jpg"></div>
        <div class="col-ico-shoe"><img alt="Wysoka rozciągliwość" class="center-block img-responsive" src="https://sweetsleep.pl/img/dreex/TPR.jpg"></div>
        <div class="col-ico-shoe"><img alt="Podwójne wypełnienie" class="center-block img-responsive" src="https://sweetsleep.pl/img/dreex/wype_nienie.jpg"></div>
        <div class="col-ico-shoe"><img alt="Dxcloth system" class="center-block img-responsive" src="https://sweetsleep.pl/img/dreex/dxcloth.jpg"></div>
        <div style="clear: both;">&nbsp;</div>
      </div>
    <?php endif; ?>
    
    <?php if ($post['Product']['show_size_table']): ?>
      <table class="table table-striped text-center">
      	<thead>
      		<tr>
        		<th class="text-center">Rozmiar</th>
        		<th class="text-center">Wzrost</th>
        		<th class="text-center">Obwód klatki piersiowej</th>
        		<th class="text-center">Obwód bioder</th>
        	</tr>
      	</thead>
      	<tbody>
        	<tr>
        		<td>S</td>
        		<td>158 - 164</td>
        		<td>84 - 88</td>
        		<td>88 - 92</td>
        	</tr>
        	<tr>
        		<td>M</td>
        		<td>164 - 170</td>
        		<td>92 - 96</td>
        		<td>96 - 100</td>
        	</tr>
        	<tr>
        		<td>L</td>
        		<td>164 -170</td>
        		<td>100 -104</td>
        		<td>104 - 108</td>
        	</tr>
        	<tr>
        		<td>XL</td>
        		<td>170 -176</td>
        		<td>108 - 112</td>
        		<td>112 - 116</td>
        	</tr>    	
        </tbody>
      </table>
    <?php endif; ?>
    
    <div style="clear: both;">&nbsp;</div>
    <?php /*
    <table>
    	<thead>
    		<tr>
    			<th colspan="5">Tabela rozmiarów/długości</th>
    		</tr>
    	</thead>
    	<thead>
    		<tr>
    			<th>37-38 - 24,5 cm</th>
    			<th>39-40 -26 cm</th>
    		</tr>
    	</thead>
    </table>*/ ?>
    
    <?php echo $post['Product']['description']; ?>

    <div class="product-part-title">Galeria produktu</div>
    <?php foreach ($product['ProductsImage'] as $image): ?>
    	<p style="margin-bottom: 20px;"><img alt="" src="https://sweetsleep.pl/SweetSleep/products/origin/<?php echo $image['image']; ?>" style="max-width: 100%;" /></p>
    <?php endforeach; ?>
	</div>

	<div style="clear: both;">&nbsp;</div>
</div>
<div style="clear: both;">&nbsp;</div>

<!-- Panele Start --><div style="clear:both;"></div><div id="t_322528" style="text-align:center;"><style> div#t_322528 div#panelHolder {display:inline-block;} div#t_322528, div#t_322528 div {position:relative;} div#t_322528 div div div.button a {text-decoration:none;color:#000000;} div#t_322528 div div div.button a:hover {text-decoration:underline;} div#t_322528 div div.panelHeader {position:relative;width:100%;font-family:Arial;font-weight:bold;overflow:hidden;} div#t_322528 div div div div div {position:relative;float:left;font-size:0;padding:1px 0 0 1px;margin:0;border:0;z-index:5;opacity:0;} div#t_322528 div div div div div:nth-of-type(odd) {background-image:url("http://p9.llog.pl/a/322528/animin1.jpg");} div#t_322528 div div div div div:nth-of-type(even) {background-image:url("http://p9.llog.pl/a/322528/animin2.jpg");} div#t_322528 div div div div:hover {opacity:1;font-size:0;padding:1px 0 0 1px;margin:0;border:0;} div#t_322528, div#t_322528 div, div#t_322528 div div, div#t_322528 div div a, div#t_322528 div#panelHolder div img, div#t_322528 div div div, div#t_322528 div div div a, div#t_322528 div#panelHolder div div a img, div#t_322528 div div div div, div#t_322528 div div div div a, div#t_322528 div#panelHolder div div div a img, div#t_322528 div div div div div a, div#t_322528 div#panelHolder div div div div a img {width:auto;padding:0;margin:0;border:0;} div#t_322528 div div div.button {float:left;margin:0 6px 0 0;font-size:12px;padding:13px;border:0;} </style><div id="panelHolder"><div class="panelHeader"><div style="float:left;font-size:16px;padding:12px;color:#000000;">Zobacz nasze pozostałe aukcje:</div></div><div class="panelContent" style="overflow:hidden;min-width:120px;min-height:45px;background-image:url(http://llog.pl//grafika/loading_circle.gif);background-position:10px 10px;background-size:120px 25px;background-repeat:no-repeat;"><div style="position:relative;display:inline-block;background-image:url(http://panele9.llog.pl/panele9/322528_border.gif);background-position:0 0;background-repeat:no-repeat;"><img style="z-index:1;" src="http://p9.llog.pl/i/322528/allin1.jpg"/><div style="position:absolute;width:inherit;height:100%;top:0;left:0;overflow:hidden;"><div style="background-position:0 500px;"><a href="http://llog.pl/g/4Ul1/1" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 500px;"><a href="http://llog.pl/g/4Ul1/2" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 1000px;"><a href="http://llog.pl/g/4Ul1/3" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 1000px;"><a href="http://llog.pl/g/4Ul1/4" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 1500px;"><a href="http://llog.pl/g/4Ul1/5" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 1500px;"><a href="http://llog.pl/g/4Ul1/6" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 2000px;"><a href="http://llog.pl/g/4Ul1/7" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 2000px;"><a href="http://llog.pl/g/4Ul1/8" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 2500px;"><a href="http://llog.pl/g/4Ul1/9" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 2500px;"><a href="http://llog.pl/g/4Ul1/10" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 3000px;"><a href="http://llog.pl/g/4Ul1/11" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 3000px;"><a href="http://llog.pl/g/4Ul1/12" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 3500px;"><a href="http://llog.pl/g/4Ul1/13" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 3500px;"><a href="http://llog.pl/g/4Ul1/14" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 4000px;"><a href="http://llog.pl/g/4Ul1/15" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 4000px;"><a href="http://llog.pl/g/4Ul1/16" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 4500px;"><a href="http://llog.pl/g/4Ul1/17" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 4500px;"><a href="http://llog.pl/g/4Ul1/18" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 5000px;"><a href="http://llog.pl/g/4Ul1/19" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 5000px;"><a href="http://llog.pl/g/4Ul1/20" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 5500px;"><a href="http://llog.pl/g/4Ul1/21" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 5500px;"><a href="http://llog.pl/g/4Ul1/22" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 6000px;"><a href="http://llog.pl/g/4Ul1/23" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 6000px;"><a href="http://llog.pl/g/4Ul1/24" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 6500px;"><a href="http://llog.pl/g/4Ul1/25" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 6500px;"><a href="http://llog.pl/g/4Ul1/26" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 7000px;"><a href="http://llog.pl/g/4Ul1/27" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 7000px;"><a href="http://llog.pl/g/4Ul1/28" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 7500px;"><a href="http://llog.pl/g/4Ul1/29" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 7500px;"><a href="http://llog.pl/g/4Ul1/30" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 8000px;"><a href="http://llog.pl/g/4Ul1/31" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 8000px;"><a href="http://llog.pl/g/4Ul1/32" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 8500px;"><a href="http://llog.pl/g/4Ul1/33" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 8500px;"><a href="http://llog.pl/g/4Ul1/34" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 9000px;"><a href="http://llog.pl/g/4Ul1/35" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 9000px;"><a href="http://llog.pl/g/4Ul1/36" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 9500px;"><a href="http://llog.pl/g/4Ul1/37" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 9500px;"><a href="http://llog.pl/g/4Ul1/38" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 10000px;"><a href="http://llog.pl/g/4Ul1/39" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 10000px;"><a href="http://llog.pl/g/4Ul1/40" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 10500px;"><a href="http://llog.pl/g/4Ul1/41" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 10500px;"><a href="http://llog.pl/g/4Ul1/42" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 11000px;"><a href="http://llog.pl/g/4Ul1/43" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 11000px;"><a href="http://llog.pl/g/4Ul1/44" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 11500px;"><a href="http://llog.pl/g/4Ul1/45" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 11500px;"><a href="http://llog.pl/g/4Ul1/46" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 12000px;"><a href="http://llog.pl/g/4Ul1/47" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 12000px;"><a href="http://llog.pl/g/4Ul1/48" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 12500px;"><a href="http://llog.pl/g/4Ul1/49" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 12500px;"><a href="http://llog.pl/g/4Ul1/50" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 13000px;"><a href="http://llog.pl/g/4Ul1/51" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 13000px;"><a href="http://llog.pl/g/4Ul1/52" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 13500px;"><a href="http://llog.pl/g/4Ul1/53" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 13500px;"><a href="http://llog.pl/g/4Ul1/54" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 14000px;"><a href="http://llog.pl/g/4Ul1/55" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 14000px;"><a href="http://llog.pl/g/4Ul1/56" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 14500px;"><a href="http://llog.pl/g/4Ul1/57" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 14500px;"><a href="http://llog.pl/g/4Ul1/58" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 15000px;"><a href="http://llog.pl/g/4Ul1/59" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div><div style="background-position:0 15000px;"><a href="http://llog.pl/g/4Ul1/60" target="_blank"><img src="http://p9.llog.pl/b/4Ul1/"/></a></div></div></div></div><div class="panelComments"><a href="http://llog.pl/showuser/42457370/322528/" target="_blank"><img src="http://panele9.llog.pl/panele9/322528_kom.gif" style="background-image:url(http://panele9.llog.pl/panele9/322528_comment.png);"/></a><img src="http://panele9.llog.pl/panele9/views/322528/42457370/3/" style="width:1px;height:1px;margin:0px;padding:0px;border:none;background:none;"/></div><div class="panelHeader"><div class="button"><a href="http://llog.pl/mypage/42457370/322528" target="_blank">Strona "o mnie"</a></div><div class="button"><a href="http://llog.pl/showuserauctions/42457370/322528" target="_blank">Wszystkie aukcje</a></div><div style="float:right;padding:6px;margin:0px;border:0px;"><a href="http://www.paneleallegro.pl" title="darmowy panel allegro"><img src="http://panele9.llog.pl/panele9/foot_322528.png" style="background:none;border:none;margin:0;padding:0;display:inline;" alt="panele aukcyjne"/></a></div></div></div></div><!-- Panele Koniec -->