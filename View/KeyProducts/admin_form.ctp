<div class="table-grid table-grid-desktop">
	<div class="col col-md">
		<div class="pr">
			<div class="panel panel-default">
				<div class="panel-body">
					<ul class="list-group key-side-tabs" role="tablist">
						<li role="presentation"
							class="list-group-item node-categories_tree active"><a
							href="#tab-basic-data" aria-controls="tab-basic-data" role="tab"
							data-toggle="tab"><?php echo __('Dane podstawowe'); ?></a></li>
						<li role="presentation"
							class="list-group-item node-categories_tree"><a
							href="#tab-categories" aria-controls="tab-categories" role="tab"
							data-toggle="tab"><?php echo __('Kategorie'); ?></a></li>
						<li role="presentation"
							class="list-group-item node-categories_tree"><a
							href="#tab-description" aria-controls="tab-description"
							role="tab" data-toggle="tab"><?php echo __('Opis'); ?></a></li>
						<li role="presentation"
							class="list-group-item node-categories_tree"><a
							href="#tab-photos" aria-controls="tab-photos" role="tab"
							data-toggle="tab"><?php echo __('Zdjęcia'); ?></a></li>
						<li role="presentation"
							class="list-group-item node-categories_tree"><a href="#tab-seo"
							aria-controls="tab-seo" role="tab" data-toggle="tab"><?php echo __('Pozycjonowanie'); ?></a>
						</li>
						<li role="presentation"
							class="list-group-item node-categories_tree"><a
							href="#tab-options" aria-controls="tab-options" role="tab"
							data-toggle="tab"><?php echo __('Cechy produktu'); ?></a></li>
						<li role="presentation"
							class="list-group-item node-categories_tree"><a
							href="#tab-shippings" aria-controls="tab-shippings" role="tab"
							data-toggle="tab"><?php echo __('Formy dostawy'); ?></a></li>
						<li role="presentation"
							class="list-group-item node-categories_tree"><a
							href="#tab-rel-products" aria-controls="tab-rel-products" role="tab"
							data-toggle="tab"><?php echo __('Powiązane produkty'); ?></a></li>
					</ul>
          <?php //print_r($this->data); ?>
        </div>
			</div>
		</div>
	</div>
	<div class="col">
    <?php echo $this->Form->create('Product', array('class' => '', 'type' => 'file')); ?>
      <div class="panel panel-default">
        <?php echo $this->Form->input('id'); ?>
        <div class="panel-body">
				<div class="tab-content no-border">
					
					<?php echo $this->element('Products/tab_basic'); ?>
					
					<?php echo $this->element('Products/tab_categories'); ?>
					
					<?php echo $this->element('Products/tab_description'); ?>
					
					<?php echo $this->element('Products/tab_photos'); ?>
					
					<?php echo $this->element('Products/tab_seo'); ?>
					
					<?php 
					$defaultProductOptionId = 0;
					$choosedProductOptionValueIds = [ ];
					if (isset ( $this->data ['ProductsProductsOption'] ) && ! empty ( $this->data ['ProductsProductsOption'] )) {
					  foreach ( $this->data ['ProductsProductsOption'] as $item ) {
					    $defaultProductOptionId = $item ['product_option_id'];
					    $choosedProductOptionValueIds [] = $item ['product_option_value_id'];
					  }
				  }?>
					<?php echo $this->element('Products/tab_options', array('defaultProductOptionId' => $defaultProductOptionId, 'choosedProductOptionValueIds' => $choosedProductOptionValueIds)); ?>
					
					<?php echo $this->element('Products/tab_shippings')?>
					
					<?php echo $this->element('Products/tab_rel_products'); ?>
				</div>
			</div>
			<div class="panel-footer">
				<div class="clearfix">
					<div class="pull-right">
              <?php echo $this->Html->link(__('Anuluj'), array('controller' => 'KeyProducts', 'action' => 'index', 'admin' => true), array('title' => __('Anuluj'), 'class' => 'btn btn-default')); ?>
              <button type="submit" class="btn btn-primary"><?php echo __('Zapisz'); ?></button>
					</div>
				</div>
			</div>
		</div>
    <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
  </div>
</div>

<script>
  $(document).ready(function () {
    $('#ProductOurHitDateBox, #ProductOurHitDateEnd, #ProductFeaturedDate, #ProductFeaturedDateEnd, #ProductPromotionDate, #ProductPromotionDateEnd').datetimepicker({
      icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-arrow-up",
        down: "fa fa-arrow-down"
      },
      locale: 'pl',
      format: 'YYYY-MM-DD HH:mm'
    });
    
    $('#ProductAvailableDateBox').datetimepicker({
      icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-arrow-up",
        down: "fa fa-arrow-down"
      },
      locale: 'pl',
      format: 'YYYY-MM-DD'
    });

    $('.current_unit').text(parseInt($('#ProductUnitId').find('option:selected').val()) > 0 ? $('#ProductUnitId').find('option:selected').text() : '');

    CKEDITOR.replace('ProductDescription', {
      filebrowserBrowseUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'browse')); ?>',
			filebrowserUploadUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'upload')); ?>',
    });
    CKEDITOR.replace('ProductShortDescription', {
      filebrowserBrowseUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'browse')); ?>',
			filebrowserUploadUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'upload')); ?>',
    });
    CKEDITOR.replace('ProductTechnicalDescription', {
      filebrowserBrowseUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'browse')); ?>',
			filebrowserUploadUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'upload')); ?>',
    });

    
    <?php echo $this->element('Scripts/AppProductsDefaultSettings'); ?>
    App.Products.toggleAllShippings($('#ProductAllShippings').is(':checked'));

<?php echo $this->element('Scripts/AppCategoriesTreeDefaultSettings'); ?>
    App.CategoriesTree.chooseMode = true;
    App.CategoriesTree.multiSelect = true;

    App.CategoriesTree.onDocReady();
    App.Products.loadProductForm();
    
    App.Products.disableProductOptionValues([<?php echo implode(',', $choosedProductOptionValueIds); ?>]);

    $('.key-side-tabs a').click( function(e) {
      e.preventDefault();
      history.pushState( null, null, $(this).attr('href') );
      $( "body" ).scrollTop( 0 );
      $(this).tab('show');
      return false;
    });
  });
</script>