<?php //print_r($productOption); ?>

<tr>
	<td
		class="<?php echo $this->Form->isFieldError("ProductsProductsOption.{$key}.price") ? 'has-error' : ''; ?>">
    <?php echo $this->Form->input("ProductsProductsOption.{$key}.id"); ?>
    <?php echo $this->Form->input("ProductsProductsOption.{$key}.product_id", array('type' => 'hidden')); ?>
    <?php echo $this->Form->input("ProductsProductsOption.{$key}.product_option_id", array('type' => 'hidden')); ?>
    <?php echo $this->Form->input("ProductsProductsOption.{$key}.product_option_value_id", array('type' => 'hidden')); ?>
    #<?php echo (isset($productOption['id']))? $productOption['id']: '-'; ?><br/><?php echo h($productOption['ProductOptionValue']['name']); ?>
  </td>
	<td>
    <?php echo $this->Form->input("ProductsProductsOption.{$key}.change_price", array('label' => false, 'placeholder' => __('Zmiana ceny netto'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'div' => false, 'data-toggle' => 'tooltip', 'data-title' => __('Zmiana ceny netto'), 'onchange' => "App.Products.changeProductOptionPrice($(this).closest('tr'), 'price')")); ?>
  </td>
	<td>
    <?php echo $this->Form->input("ProductsProductsOption.{$key}.change_price_tax", array('label' => false, 'placeholder' => __('Zmiana ceny brutto'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'div' => false, 'data-toggle' => 'tooltip', 'data-title' => __('Zmiana ceny brutto'), 'onchange' => "App.Products.changeProductOptionPrice($(this).closest('tr'), 'price_tax')")); ?>
  </td>
  <td>
    <?php echo $this->Form->input("ProductsProductsOption.{$key}.store_quantity", array('label' => false, 'placeholder' => __('Nr katalogowy'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'div' => false, 'data-toggle' => 'tooltip', 'data-title' => __('Nr katalogowy'))); ?>
  </td>
  <td>
    <?php echo $this->Form->input("ProductsProductsOption.{$key}.product_availability_id", array('label' => false, 'placeholder' => __('Dostępność'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'div' => false, 'data-toggle' => 'tooltip', 'data-title' => __('Dostępność'))); ?>
  </td>
  <td>
    <?php echo $this->Form->input("ProductsProductsOption.{$key}.sort_order", array('label' => false, 'placeholder' => __('Dostępność'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'div' => false, 'data-toggle' => 'tooltip', 'data-title' => __('Dostępność'))); ?>
  </td>
	<td>
    <?php echo $this->Form->input("ProductsProductsOption.{$key}.catalog_no", array('label' => false, 'placeholder' => __('Kolejność'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'div' => false, 'data-toggle' => 'tooltip', 'data-title' => __('Kolejność'))); ?>
  </td>

	<td class="options-buttons text-center">
    <?php echo $this->Html->link('<em class="icon-trash"></em>', 'javascript:void(0);', array('escape' => false, 'title' => __('Usuń pozycję'), 'onclick' => 'App.Products.deleteProductOption(this)')); ?>
  </td>
</tr>