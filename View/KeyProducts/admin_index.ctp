<div class="table-grid table-grid-desktop">
	<div class="col col-md">
		<div class="pr">
			<div class="panel panel-default">
				<div class="panel-body">
					<p id="categories_tree_loading"><?php echo __('Pobieranie kategorii...'); ?></p>
					<ul id="categories_tree"></ul>
				</div>
			</div>
		</div>
	</div>
	<div class="col">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th data-check-all="">
									<div title="" data-original-title="" data-toggle="tooltip"
										data-title="<?php echo __('Zaznacz wszystkie'); ?>"
										class="checkbox c-checkbox">
										<label> <input type="checkbox"> <span class="fa fa-check"></span>
										</label>
									</div>
								</th>
								<th><?php echo $this->Paginator->sort('Product.id', 'ID'); ?></th>
								<th><?php echo __('Zdjęcie'); ?></th>
								<th><?php echo $this->Paginator->sort('Product.name', __('Produkt')); ?></th>
								<th><?php echo $this->Paginator->sort('Product.price_tax', __('Cena')); ?></th>
								<th><?php echo __('Opcje'); ?></th>
								<th><?php echo $this->Paginator->sort('Product.sort_order', __('Sort.')); ?></th>
								<th><?php echo $this->Paginator->sort('Product.status', __('Status')); ?></th>
								<th style="width: 100px;"><?php echo __('Opcje'); ?></th>
							</tr>
						</thead>
						<tbody>
              <?php if (isset($products) && !empty($products)): ?>
                <?php foreach ($products as $product): ?>
                  <tr>
								<td>
									<div class="checkbox c-checkbox">
										<label> <input type="checkbox" name="check_product_id[]"
											value="<?php echo h($product['Product']['id']); ?>"> <span
											class="fa fa-check"></span>
										</label>
									</div>
								</td>
								<td><?php echo h($product['Product']['id']); ?>&nbsp;</td>
								<td>
                      <?php if (!empty($product['Product']['ProductsImage'])): ?>
                        <?php echo $this->App->showProductsImage($product['Product']['ProductsImage'][0]['image'], 'THUMB', [
                            'style' => 'max-height: 100px;'
                        ]); ?>
                      <?php endif; ?>
                    </td>
								<td><strong><?php echo h($product['Product']['name']); ?></strong><br />

                      <?php
                  /*
                   * $categories = array();
                   * foreach ($product['Product']['Category'] as $category) {
                   * $categories[] = $category['name'];
                   * }
                   */
                  ?>

                      <?php //echo __('Kategoria:').'&nbsp;'.h(implode(', ', $categories)); ?>
                      <?php if (!empty($product['Product']['catalog_no'])): ?>
                        <br />
                        <?php echo __('Nr kat.:').'&nbsp;'.h($product['Product']['catalog_no']); ?>
                      <?php endif; ?>
                      <?php if (!empty($product['Product']['ext_code'])): ?>
                        <br />
                        <?php echo __('Kod prod.:').'&nbsp;'.h($product['Product']['ext_code']); ?>
                      <?php endif; ?>
                      <?php if (isset($product['Manufacturer']) && !empty($product['Manufacturer'])): ?>
                        <br />
                        <?php echo __('Producent:').'&nbsp;'.h($product['Manufacturer']['name']); ?>
                      <?php endif; ?>
                    </td>
								<td><label>Cena brutto</label> <input
									class="form-control list-price-input input-sm" type="text"
									value="<?php echo h($product['Product']['price_tax']); ?>"
									onchange="App.Products.saveProductPriceTax(<?php echo h($product['Product']['id']); ?>, this.value);">
									<label>Cena poprzednia</label> <input
									class="form-control list-price-input input-sm" type="text"
									value="<?php echo h($product['Product']['old_price']); ?>"
									onchange="App.Products.saveProductOldPrice(<?php echo h($product['Product']['id']); ?>, this.value);">
                      <?php echo __('zł').', '.h($product['Product']['TaxRate']['description']); ?>
                    </td>
								<td class="record-options">
									<div class="checkbox c-checkbox">
										<label> <input
											<?php echo h($product['Product']['new_status'] ? 'checked="checked"' : ''); ?>
											value="1" type="checkbox"
											onchange="App.Products.saveProductFeatureStatus(<?php echo h($product['Product']['id']); ?>, 'new', this.checked ? 1 : 0);">
											<span class="fa fa-check"></span>nowość
										</label>
									</div>
									<div class="checkbox c-checkbox">
										<label> <input
											<?php echo h($product['Product']['our_hit_status'] ? 'checked="checked"' : ''); ?>
											value="1" type="checkbox"
											onchange="App.Products.saveProductFeatureStatus(<?php echo h($product['Product']['id']); ?>, 'our_hit', this.checked ? 1 : 0);">
											<span class="fa fa-check"></span>nasz hit
										</label>
									</div>
									<div class="checkbox c-checkbox">
										<label> <input
											<?php echo h($product['Product']['promotion_status'] ? 'checked="checked"' : ''); ?>
											value="1" type="checkbox"
											onchange="App.Products.saveProductFeatureStatus(<?php echo h($product['Product']['id']); ?>, 'promotion', this.checked ? 1 : 0);">
											<span class="fa fa-check"></span>promocja
										</label>
									</div>
									<div class="checkbox c-checkbox">
										<label> <input
											<?php echo h($product['Product']['featured_status'] ? 'checked="checked"' : ''); ?>
											value="1" type="checkbox"
											onchange="App.Products.saveProductFeatureStatus(<?php echo h($product['Product']['id']); ?>, 'featured', this.checked ? 1 : 0);">
											<span class="fa fa-check"></span>polecany
										</label>
									</div>
									<div class="checkbox c-checkbox">
										<label> <input
											<?php echo h($product['Product']['free_shipping_status'] ? 'checked="checked"' : ''); ?>
											value="1" type="checkbox"
											onchange="App.Products.saveProductFeatureStatus(<?php echo h($product['Product']['id']); ?>, 'free_shipping', this.checked ? 1 : 0);">
											<span class="fa fa-check"></span>darmowa wysyłka
										</label>
									</div>
								</td>
								<td><input class="form-control sort-order-input input-sm"
									type="text"
									value="<?php echo h($product['Product']['sort_order']); ?>"
									onchange="App.Products.saveSortOrder(<?php echo h($product['Product']['id']); ?>, this.value);">
								</td>

								<td><span
									class="checkbox c-checkbox c-checkbox-rounded c-checkbox-no-label">
										<input
										<?php echo $product['Product']['status'] ? 'checked' : ''; ?>
										type="checkbox"
										onchange="App.Products.saveStatus(<?php echo h($product['Product']['id']); ?>, this.checked ? 1 : 0);">
										<span class="fa fa-check"
										onclick="$(this).parent().find('input').click();"></span>
								</span></td>
								<td class="options-buttons text-center">
                                    <?php if (CLIENT == 'Floper'): ?>
                                    <?php else: ?>
                                        <?php 
                                        echo $this->Html->link('<em class="icon-list"></em>',
                                            array(
                                                'controller' => 'KeyProducts',
                                                'action' => 'assign_car_models',
                                                $product['Product']['id'],
                                                'admin' => true
                                            ), array(
                                                'escape' => false, 'title' => __('Przypisz marke/model'), 'class' => 'option-link'
                                            )); ?>&nbsp;
                                        <?php echo $this->Html->link('<span class="glyphicon glyphicon-align-center" aria-hidden="true"></span>',
                                            array(
                                                'controller' => 'KeyCustomizations',
                                                'action' => 'index',
                                                'product',
                                                $product['Product']['id'],
                                                'admin' => true
                                            ), array(
                                                'escape' => false, 'title' => __('Szablon'), 'class' => 'option-link'
                                            )); ?>&nbsp;
                                            <?php echo $this->Html->link('<em class="icon-basket"></em>', 
                                                'javascript:void(0);', array(
                                                  'escape' => false, 'title' => __('Allegro'), 'class' => 'option-link',
                                                  'onclick' => "App.Products.getAuctionTemplate({$product['Product']['id']});"                                        
                                            )); ?>&nbsp;
                                    <?php endif; ?>
                                    
                                    <?php echo $this->Html->link('<i class="fa fa-files-o" aria-hidden="true"></i>', array(
                                        'controller' => 'KeyProducts', 'action' => 'duplicate', $product['Product']['id']                                        
                                    ), array(
                                        'escape' => false, 'title' => __('Duplikuj'), 'class' => 'option-link'                                        
                                    )); ?>&nbsp;
                                    <?php echo $this->Html->link('<em class="icon-note"></em>', array('controller' => 'KeyProducts', 'action' => 'edit', $product['Product']['id']), array('escape' => false, 'title' => __('Edytuj'), 'class' => 'option-link')); ?>&nbsp;
                                    <?php echo $this->Html->link('<em class="icon-trash"></em>', 'javascript:void(0);', array('escape' => false, 'title' => __('Usuń'), 'onclick' => 'App.Products.deleteProduct(\''.h($product['Product']['name']).'\', \''.$this->Html->url(array('controller' => 'KeyProducts', 'action' => 'delete', $product['Product']['id'])).'\')', 'class' => 'option-link')); ?>
                                </td>
							</tr>
                <?php endforeach; ?>
              <?php else: ?>
                <tr>
								<td colspan="9"><?php echo __('Brak produktów do wyświetlenia'); ?></td>
							</tr>
              <?php endif; ?>
            </tbody>
					</table>
				</div>
			</div>
			<div class="panel-footer text-center">
				<div class="col-sm-6">
					<div class="pull-left">
            <?php echo $this->Paginator->counter(array('format' => __('Strona {:page} z {:pages}'))); ?>
          </div>
				</div>
				<div class="col-sm-6 pagination pagination-large">
					<ul class="pagination pull-right">
            <?php
            $this->Paginator->options ['url'] = array (
              'controller' => 'KeyProducts',
              'action' => 'index',
              'parent_id' => $parent_id 
            );
            echo $this->Paginator->prev ( __ ( 'poprzednia' ), array (
              'tag' => 'li' 
            ), null, array (
              'tag' => 'li',
              'class' => 'disabled',
              'disabledTag' => 'a' 
            ) );
            echo $this->Paginator->numbers ( array (
              'separator' => '',
              'currentTag' => 'a',
              'currentClass' => 'active',
              'tag' => 'li',
              'first' => 1,
              'modulus' => 5,
              'ellipsis' => '<li class="disabled"><a>...</a></li>' 
            ) );
            echo $this->Paginator->next ( __ ( 'następna' ), array (
              'tag' => 'li',
              'currentClass' => 'disabled' 
            ), null, array (
              'tag' => 'li',
              'class' => 'disabled',
              'disabledTag' => 'a' 
            ) );
            ?>
          </ul>
				</div>
				<span class="clearfix"></span>
			</div>
		</div>
	</div>
</div>

<script>
  $(document).ready(function () {
<?php echo $this->element('Scripts/AppProductsDefaultSettings'); ?>
<?php echo $this->element('Scripts/AppCategoriesTreeDefaultSettings'); ?>

    App.CategoriesTree.withRoot = true;

    App.Products.onDocReady();
    App.CategoriesTree.onDocReady();
  });
</script>