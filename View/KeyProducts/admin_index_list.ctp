<div class="table-grid table-grid-desktop">
	<div class="col">
		<div class="panel panel-default">
			<div class="panel-body">
				<div>
					<table class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th>ID</th>
								<th>Nazwa</th>
								<th style="width: 60%;">Kategoria</th>
								<th style="width: 100px;"><?php echo __('Aktywny'); ?></th>
							</tr>
						</thead>
						<tbody>
                            <?php foreach ($products as $product): ?>
                                <tr>
								    <td><?php echo h($product['Product']['id']); ?>&nbsp;</td>
								    <td><?php echo h($product['Product']['name']); ?>&nbsp;</td>
								    <td>
                                        <?php echo $this->Form->input('ProductsCategory.'.$product['Product']['id'].'.category_id', array(
                                            'options' => $list, 'label' => false, 'class' => 'chosen save-fast-category',
                                            'value' => $product['MainCategory']['category_id']
                                        ))?>
                                    </td>
                                    <td>
                                        <div class="checkbox c-checkbox">
										<label> <input type="checkbox" name="status[]"
											value="<?php echo h($product['Product']['id']); ?>" <?php if ($product['Product']['status'] == 1): ?>checked="checked"<?php endif; ?>> <span
											class="fa fa-check"></span>
										</label>
									</div>
                                    </td>
								<tr>
                            <?php endforeach; ?>
                        </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    $('.chosen').chosen();
});
</script>