<?php echo $this->Form->create(null, array('url' => array('admin' => true, 'plugin' => 'KeyAdmin', 'controller' => 'KeyProducts', 'action' => 'get_auction_template', $productId))); ?>
<div class="row">
	<div class="col-xs-6">
		<div class="form-group">
			<?php echo $this->Form->input('Product.name', array('type' => 'text','class' => 'form-control', 'label' => 'Tytuł')); ?>
    </div>
	</div>	
	<div class="col-xs-6">
		<div class="form-group">
			<?php echo $this->Form->input('Product.price', array('type' => 'text','class' => 'form-control', 'label' => 'Cena')); ?>
    </div>
	</div>	
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="form-group">
			<?php echo $this->Form->input('Product.description', array('type' => 'textarea','class' => 'form-control', 'label' => 'Opis')); ?>
    </div>
	</div>	
</div>
<div class="row">
	<div class="col-xs-4">
		<div class="form-group">
			<?php $options = array('0' => 'NIE', '1' => 'TAK'); ?>
			<?php echo $this->Form->input('Product.show_dreex', array('options' => $options, 'class' => 'form-control', 'label' => 'Pokaż ikonki dreex')); ?>
		</div>
	</div>
	<div class="col-xs-4">
		<div class="form-group">
			<?php $options = array('0' => 'NIE', '1' => 'TAK'); ?>
			<?php echo $this->Form->input('Product.show_soxo', array('options' => $options, 'class' => 'form-control', 'label' => 'Pokaż ikonki soxo')); ?>
		</div>
	</div>
	<div class="col-xs-4">
		<div class="form-group">
			<?php echo $this->Form->input('Product.show_size_table', array('options' => $options, 'class' => 'form-control', 'label' => 'Pokaż tabelę rozmiarów')); ?>
		</div>
	</div>
</div>

<?php echo $this->Form->end(); ?>