<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">   
                <div class="col-xs-12 text-left" style="padding: 5px 10px;">
                    <p><strong>Ostatnio zapisano: <u><span id="last-saved">---</span></u></strong></p>
                    <hr/>
                </div>               
            </div>
            <?php echo $this->Form->create('Product'); ?>
                <?php foreach ($cars as $car): ?>
                    <div class="row">
                        <div class="col-xs-4">
                            <strong><?php echo $car['CarManufacturer']['name']; ?></strong>
                        </div>
                        <div class="col-xs-8">
                            <div class="form-group">
                                <?php echo $this->Form->input('car_model_rel', array(
                                    'options' => Hash::combine($car, 'CarModel.{n}.id', 'CarModel.{n}.name'),
                                    'label' => false, 
                                    'name' => "data[CarModelRel][]",
                                    'placeholder' => __('Wybierz'), 
                                    'class' => 'form-control chosen',
                                    'multiple' => true,
                                    'checked' => $selected,
                                    'selected' => $selected
                                )); ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                
                <hr/>
                <div class="row">
                    <div class="col-xs-6">
                        <?php echo $this->Html->link('Powrót',
                            array('controller' => 'KeyProducts', 'action' => 'index', 'admin' => true),
                            array('class' => 'btn btn-default center-block')
                        ); ?>
                    </div>
                    <div class="col-xs-6">
                        <input type="submit" class="btn btn-primary center-block col-xs-12" value="ZATWIERDŹ"/>
                    </div>
                </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>


<script type="text/javascript">    
    function saveForm() {
        $.ajax({
            evalScripts: true,
            url: $('#ProductAdminAssignCarModelsForm').attr('action'),
            type: 'POST',
            data: $('#ProductAdminAssignCarModelsForm').serialize(),
            success: function (data, textStatus) {
                $('#last-saved').html(moment().locale('pl').format('hh:mm:ss'));
            },
            error: function (data, textStatus) {
                alert('Skontaktuj się z administratiorem, bo jest syf');
            }
        });
    }
  $(document).ready(function () {
    $('.chosen').chosen();

    setInterval(function(){
        saveForm();
    }, 10000);
  });
</script>