<div class="col-sm-12">
    <input type="hidden" id="customization-id" value="<?= $customization['Customization']['id']; ?>" />
    <input type="hidden" id="model" value="<?= $model; ?>" />
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12">
                    <?php echo $this->Html->link('<i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;&nbsp;Zapisz', '#', [
                        'class' => 'btn btn-primary btn-block btn-save-tab',
                        'escape' => false,
                    ]); ?>
                </div> 
                <div>&nbsp;</div>  
                <div class="form-group col-xs-12">
                  <label for="CustomizationActive"><?php echo __('Aktywny:'); ?></label>
                  <label class="checkbox-inline c-checkbox">
                    <?php echo $this->Form->input('is_active', array(
                        'checked' => (!empty($customization['Customization']['is_active'])),
                        'type' => 'checkbox', 'label' => false, 'div' => false)); ?><span class="fa fa-check"></span>
                  </label>
                </div>   
                <div>&nbsp;</div>              
            </div>
                    
            <div class="line-container">
                <?php foreach ($customization['CustomizationItem'] as $item): ?>
                    <?= $this->element('customized_line', [
                        'customization' => [
                            'CustomizationItem' => $item
                        ]
                    ]); ?>
                <?php endforeach; ?>
            </div>
            <div class="clear">&nbsp;</div>
            <div class="button-box">
                <div class="btn-group col-xs-12">
                    <div class="row">
                        <div class="col-xs-3">
                            <button class="btn btn-default col-xs-6" data-type="text">
                                <i class="fa fa-align-justify" aria-hidden="true"></i>
                            </button>
                            <button class="btn btn-default col-xs-6" data-type="text-text">
                                <i class="fa fa-align-justify" aria-hidden="true"></i>&nbsp;-&nbsp;
                                <i class="fa fa-align-justify" aria-hidden="true"></i>
                            </button>
                        </div>
                        <div class="col-xs-3">
                            <button class="btn btn-default col-xs-6" data-type="image">
                                <i class="fa fa-picture-o" aria-hidden="true"></i>
                            </button>
                            <button class="btn btn-default col-xs-6" data-type="image-image">
                                <i class="fa fa-picture-o" aria-hidden="true"></i>&nbsp;-&nbsp;
                                <i class="fa fa-picture-o" aria-hidden="true"></i>
                            </button>
                        </div>
                        
                        <div class="col-xs-4">
                            <button class="btn btn-default col-xs-4" data-type="text-image">
                                <i class="fa fa-align-justify" aria-hidden="true"></i>&nbsp;-&nbsp;
                                <i class="fa fa-picture-o" aria-hidden="true"></i>
                            </button>
                            <button class="btn btn-default col-xs-4" data-type="image-text">
                                <i class="fa fa-picture-o" aria-hidden="true"></i>&nbsp;-&nbsp;
                                <i class="fa fa-align-justify" aria-hidden="true"></i>
                            </button>
                            <button class="btn btn-default col-xs-4" data-type="youtube">
                                <i class="fa fa-video-camera" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>

<script type="text/template" id="product-tab-template">
<?= $this->Form->create('ProductTab'); ?>
    <div class="row">
        <div class="col-md-6">
    		<div class="form-group">
                <?php echo $this->Form->input('name', array(
                    'label' => __('Nazwa: *'), 'placeholder' => __('Wpisz nazwę'), 
                    'class' => 'form-control'                    
                )); ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <?php echo $this->Form->input('sort_order', array(
                    'label' => __('Kolejność: *'), 'placeholder' => __('Podaj kolejność'), 
                    'class' => 'form-control', 'type' => 'number', 'default' => 1                    
                )); ?>
            </div>
        </div>
    </div>
    <input type="submit" name="data[submit]" value="product-tab" class="hide" />
<?= $this->Form->end(); ?>
</script>

<script type="text/template" id="product-edit-tab-template">
    <form action="/admin/products/customized/1" id="ProductTabAdminEditCustomizedForm" method="post" accept-charset="utf-8">
        <div style="display:none;"><input type="hidden" name="_method" value="POST"/></div>
        <input type="hidden" name="data[ProductTab][id]" value="<%= id %>" />
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="input text">
                        <label for="ProductTabName">Nazwa: *</label>
                        <input value="<%= name %>" name="data[ProductTab][name]" placeholder="Wpisz nazwę" class="form-control" maxlength="255" type="text" id="ProductTabName"/>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="input number">
                        <label for="ProductTabSortOrder">Kolejność: *</label>
                        <input value="<%= order %>" name="data[ProductTab][sort_order]" placeholder="Podaj kolejność" class="form-control" type="number" value="1" id="ProductTabSortOrder" />
                    </div>
                </div>
            </div>
        </div>
        <input type="submit" name="data[submit]" value="product-edit-tab" class="hide" />
    </form>
</script>