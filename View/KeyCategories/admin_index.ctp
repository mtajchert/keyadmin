<div class="col-sm-3">
  <div class="panel panel-default">
    <div class="panel-body">
      <p id="categories_tree_loading"><?php echo __('Pobieranie kategorii...'); ?></p>
      <ul id="categories_tree"></ul>
    </div>
  </div>
</div>
<div class="col-sm-9">
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th><?php echo $this->Paginator->sort('id', 'ID'); ?></th>
              <th><?php echo __('Zdjęcie'); ?></th>
              <th><?php echo $this->Paginator->sort('name', __('Nazwa')); ?></th>
              <th><?php echo __('Ilość produktów / aktywnych'); ?></th>
              <th><?php echo $this->Paginator->sort('sort_order', __('Kolej. sort.')); ?></th>
              <th><?php echo $this->Paginator->sort('color', __('Kolor')); ?></th>
              <th><?php echo $this->Paginator->sort('status', __('Status')); ?></th>
              <th><?php echo __('Opcje'); ?></th>
            </tr>
          </thead>
          <tbody>
            <?php if (isset($categories) && !empty($categories)): ?>
              <?php foreach ($categories as $category): ?>
                <tr>
                  <td><?php echo h($category['Category']['id']); ?>&nbsp;</td>
                  <td><?php echo $this->App->showCategoryImage($category['Category']['image'], 'LIST'); ?></td>
                  <td><?php echo h($category['Category']['name']); ?>&nbsp;</td>
                  <td>0/0</td>
                  <td>
                    <input class="form-control sort-order-input input-sm" type="text" value="<?php echo h($category['Category']['sort_order']); ?>" onchange="App.Categories.saveSortOrder(<?php echo h($category['Category']['id']); ?>, this.value);">
                  </td>
                  <td class="text-center">
                    <?php if ((int) $category['Category']['color_status'] && !empty($category['Category']['color'])): ?>
                      <span class="color-square" style="background-color:<?php echo h($category['Category']['color']); ?>"></span>
                    <?php endif; ?>
                  </td>
                  <td>
                    <span class="checkbox c-checkbox c-checkbox-rounded c-checkbox-no-label">
                      <input <?php echo $category['Category']['status'] ? 'checked' : ''; ?> type="checkbox" onchange="App.Categories.saveStatus(<?php echo h($category['Category']['id']); ?>, this.checked ? 1 : 0);">
                      <span class="fa fa-check" onclick="$(this).parent().find('input').click();"></span>
                    </span>
                  </td>
                  <td class="options-buttons text-center" style="width: 100px;">
                    <?php echo $this->Html->link('<em class="icon-picture"></em>', '#', array('data-id' =>  $category['Category']['id'], 'class' => 'category-image-add', 'escape' => false, 'title' => __('Dodaj/zmień zdjęcie'))); ?>
                    &nbsp;
                    <?php echo $this->Html->link('<em class="icon-note"></em>', array('controller' => 'KeyCategories', 'action' => 'edit', $category['Category']['id']), array('escape' => false, 'title' => __('Edytuj'))); ?>
                    &nbsp;
                    <?php echo $this->Html->link('<em class="icon-trash"></em>', 'javascript:void(0);', array('escape' => false, 'title' => __('Usuń'), 'onclick' => 'App.Categories.deleteCategory(\''.h($category['Category']['name']).'\', \''.$this->Html->url(array('controller' => 'KeyCategories', 'action' => 'delete', $category['Category']['id'])).'\')')); ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <td colspan="9"><?php echo __('Brak kategorii do wyświetlenia'); ?></td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="panel-footer text-center">
      <div class="col-sm-6">
        <div class="pull-left">
          <?php echo $this->Paginator->counter(array('format' => __('Strona {:page} z {:pages}'))); ?>
        </div>
      </div>
      <div class="col-sm-6 pagination pagination-large">
        <ul class="pagination pull-right">
          <?php
          $this->Paginator->options['url'] = array('controller' => 'KeyCategories', 'action' => 'index', 'parent_id' => $parent_id);
          echo $this->Paginator->prev(__('poprzednia'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1, 'modulus' => 5, 'ellipsis' => '<li class="disabled"><a>...</a></li>'));
          echo $this->Paginator->next(__('następna'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          ?>
        </ul>
      </div>
      <span class="clearfix"></span>
    </div>
  </div>
</div>

<script>
  $(document).ready(function () {
    <?php echo $this->element('Scripts/AppCategoriesDefaultSettings'); ?>
    <?php echo $this->element('Scripts/AppCategoriesTreeDefaultSettings'); ?>

    App.CategoriesTree.onDocReady();
  });
</script>
<script type="text/template" id="category-image-modal-body">
  <?php echo $this->element('Templates/addCategoryImageModalBody'); ?>
</script>