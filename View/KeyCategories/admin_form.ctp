<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <?php echo $this->Form->create('Category', array('class' => 'form-horizontal category-form', 'type' => 'file')); ?>
      <div class="col-md-8">
        <div class="form-group <?php echo $this->Form->isFieldError('name') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('name', array('label' => __('Nazwa kategorii: *'), 'placeholder' => __('Wpisz nazwę kategorii'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
        <div class="form-group <?php echo $this->Form->isFieldError('parent_id') ? 'has-error' : ''; ?>">
          <label for="exampleInputEmail1"><?php echo __('Kategoria nadrzędna'); ?></label>
          <div class="input-group">
            <?php echo $this->Form->input('parent_id', array('label' => false, 'div' => false, 'type' => 'hidden')); ?>
            <input id="CategoryParentIdNid" type="hidden">
            <input id="CategoryParentIdPath" class="form-control" type="text" readonly placeholder="<?php echo __('Wybierz kategorię nadrzędną'); ?>">
            <span class="input-group-btn">
              <button type="button" class="btn btn-default" onclick="App.Categories.openParentCategoryBox();"><?php echo __('wybierz'); ?></button>
            </span>
          </div>
        </div>
        <div class="form-group <?php echo $this->Form->isFieldError('seo_url') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('seo_url', array('label' => __('Przyjazny adres URL:'), 'placeholder' => __('Wpisz przyjazny adres URL'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
        <div class="form-group <?php echo $this->Form->isFieldError('meta_title_tag') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('meta_title_tag', array('label' => __('Meta tagi - tytuł:'), 'placeholder' => __('Wpisz meta tag - tytuł'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
        <div class="form-group <?php echo $this->Form->isFieldError('meta_desc_tag') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('meta_desc_tag', array('label' => __('Meta tagi - opis:'), 'placeholder' => __('Wpisz meta tag - opis'), 'class' => 'form-control', 'rows' => 3, 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
        <div class="form-group <?php echo $this->Form->isFieldError('meta_keywords_tag') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('meta_keywords_tag', array('label' => __('Meta tagi - słowa kluczowe:'), 'placeholder' => __('Wpisz meta tag - słowa kluczowe'), 'class' => 'form-control', 'rows' => 3, 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
        
        <div class="form-group <?php echo $this->Form->isFieldError('main_page_position') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('main_page_position', array(
            'label' => __('Pozycja na stronie głównej'), 'placeholder' => __('Pozycja na stronie głównej'), 
            'options' => [
              0 => 'Ukryte',
              1 => 'Lewa główna',
              2 => 'Prawa głowna',
              3 => 'Dodatkowa'
            ],
            'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
        
        <div class="form-group <?php echo $this->Form->isFieldError('promotion_text') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('promotion_text', array(
            'label' => __('Tekst promujący'), 'placeholder' => __('Tekst promujący'), 
            'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>

      </div>
      <div class="col-md-4">
        <div class="form-group form-group-margin-left-0 <?php echo $this->Form->isFieldError('status') ? 'has-error' : ''; ?>">
          <label class="col-xs-5 control-label text-left-force" for="exampleInputEmail1"><?php echo __('Status: *'); ?></label>
          <div class="col-xs-7">
            <label class="checkbox-inline c-checkbox" for="CategoryStatus">
              <?php echo $this->Form->input('status', array('type' => 'checkbox', 'label' => false, 'div' => false)); ?>
              <span class="fa fa-check"></span><?php echo __('aktywny'); ?>
            </label>

          </div>
        </div>
        <div class="form-group form-group-margin-left-0">
          <div class="col-xs-12 img-box">
            <label><?php echo __('Zdjęcie:'); ?></label>
            <?php if (isset($this->data['Category']['image']) && !empty($this->data['Category']['image'])): ?>
              <?php echo $this->App->showCategoryImage($this->data['Category']['image'], 'THUMB'); ?>
            <?php endif; ?>
            <input type="file" name="data[Category][image]" class="image-choosen" data-show-upload="false" data-show-caption="true"/>
          </div>
        </div>
        <div class="form-group form-group-margin-left-0">
          <div class="col-xs-12 img-box">
            <label><?php echo __('Baner:'); ?></label>
            <?php if (isset($this->data['Category']['banner']) && !empty($this->data['Category']['banner'])): ?>
              <?php echo $this->Html->image('/'.CLIENT.'/categories-banners/show/'.$this->data['Category']['banner']); ?>
            <?php endif; ?>
            <input type="hidden" name="data[Category][banner_]" value="<?php echo (isset($this->data['Category']['banner']))? h($this->data['Category']['banner']): ''; ?>" />
            <input type="file" name="data[Category][banner]" class="image-choosen" data-show-upload="false" data-show-caption="true"/>
          </div>
        </div>
        <div class="form-group form-group-margin-left-0 <?php echo $this->Form->isFieldError('banner_redirect_type') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->label('banner_redirect_type', __('Baner - rodzaj przekierowania:'), array('type' => 'radio', 'class' => 'col-xs-12 control-label text-left-force')); ?>
          <div class="col-xs-12">
            <label class="radio-inline c-radio">
              <input id="CategoryBannerRedirectTypeNo" name="data[Category][banner_redirect_type]" value="no" <?php echo (!isset($this->data['Category']['banner_redirect_type']) || !in_array($this->data['Category']['banner_redirect_type'], array('product', 'url')) ) ? 'checked' : ''; ?> type="radio" onchange="App.Categories.changeBannerRedirectType();">
              <span class="fa fa-circle"></span><?php echo(__('brak')); ?>
            </label>
            <label class="radio-inline c-radio">
              <input id="CategoryBannerRedirectTypeProduct" name="data[Category][banner_redirect_type]" value="product" <?php echo ( isset($this->data['Category']['banner_redirect_type']) && $this->data['Category']['banner_redirect_type'] == 'product') ? 'checked' : ''; ?> type="radio" onchange="App.Categories.changeBannerRedirectType();">
              <span class="fa fa-circle"></span><?php echo(__('produkt')); ?>
            </label>
            <label class="radio-inline c-radio">
              <input id="CategoryBannerRedirectTypeUrl" name="data[Category][banner_redirect_type]" value="url" <?php echo ( isset($this->data['Category']['banner_redirect_type']) && $this->data['Category']['banner_redirect_type'] == 'url') ? 'checked' : ''; ?> type="radio" onchange="App.Categories.changeBannerRedirectType();">
              <span class="fa fa-circle"></span><?php echo(__('strona')); ?>
            </label>
          </div>
        </div>
        <div class="form-group form-group-margin-left-0 banner-redirect-product <?php echo $this->Form->isFieldError('banner_product_id') ? 'has-error' : ''; ?>" <?php echo ( !isset($this->data['Category']['banner_redirect_type']) || $this->data['Category']['banner_redirect_type'] != 'product')? 'style="display:none;"' : ''; ?>>
          <div class="col-xs-12 img-box">
            <label><?php echo __('Baner - produkt: *'); ?></label>
            <?php echo $this->Form->input('banner_product_id', array('type' => 'hidden')); ?>
            <?php echo $this->Form->input('banner_product_name', array('label' => false, 'placeholder' => __('Wybierz produkt'), 'readonly' => 'true', 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'div' => array('class' => 'input-group m-b'), 'after' => '<span class="input-group-btn"><button type="button" class="btn btn-default" onclick="App.Categories.openChooseProductBox();">'.__('wybierz').'</button></span>', 'value' => (isset($banner_product['Product']) ? $banner_product['Product']['name'] : ''))); ?>
            <?php echo $this->Form->isFieldError('banner_product_id') ? $this->Form->error('banner_product_id', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
          </div>
        </div>
        <div class="form-group form-group-margin-left-0 banner-redirect-url <?php echo $this->Form->isFieldError('banner_url') ? 'has-error' : ''; ?>" <?php echo (!isset($this->data['Category']['banner_redirect_type']) || $this->data['Category']['banner_redirect_type'] != 'url') ? 'style="display:none;"' : ''; ?>>
          <div class="col-xs-12">
            <?php echo $this->Form->input('banner_url', array('label' => __('Baner - adres strony: *'), 'placeholder' => __('Wpisz adres strony'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
          </div>
        </div>
        <div class="form-group form-group-margin-left-0 <?php echo $this->Form->isFieldError('sort_order') ? 'has-error' : ''; ?>">
          <label class="col-xs-5 control-label text-left-force" for="exampleInputEmail1"><?php echo __('Kolejność sort.:'); ?></label>
          <div class="col-xs-7">
            <?php echo $this->Form->input('sort_order', array('min' => '0', 'step' => 1, 'label' => false, 'div' => false, 'class' => 'form-control')); ?>
          </div>
        </div>
        <div class="form-group form-group-margin-left-0 <?php echo $this->Form->isFieldError('color') || $this->Form->isFieldError('color_status') ? 'has-error' : ''; ?>">
          <label class="col-xs-3 control-label text-left-force" for="exampleInputEmail1"><?php echo __('Kolor:'); ?></label>
          <div class="col-xs-9">
            <div id="CategoryColorBox" class="input-group">
              <span class="input-group-addon">
                <label class="checkbox-inline c-checkbox" for="CategoryColorStatus">
                  <?php echo $this->Form->input('color_status', array('type' => 'checkbox', 'label' => false, 'div' => false)); ?>
                  <span class="fa fa-check"></span>
                </label>
              </span>  
              <?php echo $this->Form->input('color', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
              <span class="input-group-addon"><i></i></span>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group <?php echo $this->Form->isFieldError('description') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('description', array('label' => __('Opis kategorii:'), 'placeholder' => __('Wpisz opis kategorii:'), 'class' => 'form-control', 'rows' => 10, 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
      </div>
      <div class="col-md-12">
        <div class="pull-right">
          <?php echo $this->Html->link(__('Anuluj'), array('controller' => 'KeyCategories', 'action' => 'index', 'admin' => true), array('title' => __('Anuluj'), 'class' => 'btn btn-default')); ?>
          <button type="submit" class="btn btn-primary"><?php echo __('Zapisz'); ?></button>
        </div>
      </div>
      <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
    </div>
  </div>
</div>

<script>
  $(document).ready(function () {
    CKEDITOR.replace('CategoryDescription', {
      filebrowserBrowseUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'browse')); ?>',
			filebrowserUploadUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'upload')); ?>',
    });

    $('#CategoryColorStatus').change(function () {
      $(this).closest('div').find('input[type=text]').prop('disabled', !this.checked);
      $('#CategoryColorBox').colorpicker(this.checked ? 'enable' : 'disable');
      $(this).prop('disabled', false);
      var input = $(this).closest('div').find('input[type=text]');
      if (this.checked && input.val().length === 0) {
        input.val('#000000');
        $('#CategoryColorBox').colorpicker('setValue', '#000000');
      }
    });
    $('#CategoryColorStatus').closest('div').find('input[type=text]').prop('disabled', !$('#CategoryColorStatus').is(':checked'));
    $('#CategoryColorBox').colorpicker({format: 'hex', component: '.input-group-addon:last-child,input[type=text]', input: 'input[type="text"]', customClass: 'colorpicker-big', sliders: {saturation: {maxLeft: 130, maxTop: 130}, hue: {maxTop: 130}}, template: '<div class="colorpicker dropdown-menu"><div class="colorpicker-saturation"><i><b></b></i></div><div class="colorpicker-hue"><i></i></div></div>', });
    
    <?php echo $this->element('Scripts/AppCategoriesDefaultSettings'); ?>
    
    <?php echo $this->element('Scripts/AppCategoriesTreeDefaultSettings'); ?>
    App.CategoriesTree.chooseMode = true;
    
    App.Categories.onDocReady();
    App.CategoriesTree.onDocReady();
  });
</script>