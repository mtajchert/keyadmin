<?php $error = $this->Session->flash('auth'); ?>
<?php if (strlen($error)): ?>
<div class="alert alert-danger"><?php echo $error; ?></div>
<?php endif; ?>
<p class="text-center pv"><?php echo __('ZALOGUJ SIĘ, ABY KONTYNUOWAĆ'); ?></p>
<form role="form" data-parsley-validate="" novalidate="" class="mb-lg" method="post">
  <div class="form-group has-feedback">
    <input id="inputEmail" name="data[User][email]" type="email" placeholder="<?php echo __('Wpisz e-mail'); ?>" required autofocus class="form-control">
    <span class="fa fa-envelope form-control-feedback text-muted"></span>
  </div>
  <div class="form-group has-feedback">
    <input id="inputPassword" name="data[User][password]" type="password" placeholder="<?php echo __('Wpisz hasło'); ?>" required class="form-control">
    <span class="fa fa-lock form-control-feedback text-muted"></span>
  </div>
  <!--<div class="clearfix">
    <div class="checkbox c-checkbox pull-left mt0">
      <label>
        <input type="checkbox" value="1" name="data[User][remember]">
        <span class="fa fa-check"></span><?php echo __('Zapamiętaj mnie'); ?></label>
    </div>
    <div class="pull-right"><a href="recover.html" class="text-muted">Forgot your password?</a>
  </div>-->
  <button type="submit" class="btn btn-block btn-primary mt-lg"><?php echo __('Zaloguj'); ?></button>
</form>
<!-- <p class="pt-lg text-center">Need to Signup?</p><a href="register.html" class="btn btn-block btn-default">Register Now</a> -->