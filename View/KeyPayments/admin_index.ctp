<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th><?php echo $this->Paginator->sort('id', __('ID')); ?></th>
              <th><?php echo __('Logo'); ?></th>
              <th><?php echo $this->Paginator->sort('name', __('Nazwa')); ?></th>
              <th><?php echo $this->Paginator->sort('margin', __('Marża')); ?></th>
              <th><?php echo $this->Paginator->sort('sort_order', __('Kolejność sort.')); ?></th>
              <th><?php echo $this->Paginator->sort('status', __('Status')); ?></th>
              <th><?php echo __('Opcje'); ?></th>
            </tr>
          </thead>
          <tbody>
            <?php if (isset($payments) && !empty($payments)): ?>
              <?php foreach ($payments as $payment): ?>
                <tr>
                  <td><?php echo h($payment['Payment']['id']); ?>&nbsp;</td>
                  <td>
                    <?php if (!empty($payment['Payment']['image'])): ?>
                      <?php echo $this->App->showPaymentImage($payment['Payment']['image'], 'LIST'); ?>
                    <?php endif; ?>
                  </td>
                  <td><?php echo h($payment['Payment']['name']); ?>&nbsp;</td>
                  <td><?php echo h($payment['Payment']['margin']); ?> %&nbsp;</td>
                  <td>
                    <input class="form-control sort-order-input input-sm" type="text" value="<?php echo h($payment['Payment']['sort_order']); ?>" onchange="App.Payments.saveSortOrder(<?php echo h($payment['Payment']['id']); ?>, this.value);">
                  </td>
                  <td>
                    <span class="checkbox c-checkbox c-checkbox-rounded c-checkbox-no-label">
                      <input <?php echo $payment['Payment']['status'] ? 'checked' : ''; ?> type="checkbox" onchange="App.Payments.saveStatus(<?php echo h($payment['Payment']['id']); ?>, this.checked ? 1 : 0);">
                      <span class="fa fa-check" onclick="$(this).parent().find('input').click();"></span>
                    </span>
                  </td>
                  <td class="options-buttons text-center">
                    <?php echo $this->Html->link('<em class="icon-note"></em>', array('controller' => 'KeyPayments', 'action' => 'edit', $payment['Payment']['id']), array('escape' => false, 'title' => __('Edytuj'))); ?>
                    &nbsp;
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <td colspan="6"><?php echo __('Brak form płatności do wyświetlenia'); ?></td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="panel-footer text-center">
      <div class="col-sm-6">
        <div class="pull-left">
          <?php echo $this->Paginator->counter(array('format' => __('Strona {:page} z {:pages}'))); ?>
        </div>
      </div>
      <div class="col-sm-6 pagination pagination-large">
        <ul class="pagination pull-right">
          <?php
          $this->Paginator->options['url'] = array('controller' => 'KeyPayments', 'action' => 'index');
          echo $this->Paginator->prev(__('poprzednia'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1, 'modulus' => 5, 'ellipsis' => '<li class="disabled"><a>...</a></li>'));
          echo $this->Paginator->next(__('następna'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          ?>
        </ul>
      </div>
      <span class="clearfix"></span>
    </div>
  </div>
</div>

<script>
  $(document).ready(function () {
    <?php echo $this->element('Scripts/AppPaymentsDefaultSettings'); ?>
    App.Payments.onDocReady();
  });
</script>