<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <?php echo $this->Form->create('Payment', array('class' => 'form-horizontal', 'type' => 'file')); ?>
      <div class="col-md-8">
        <fieldset class="fgmb15">
          <div class="form-group <?php echo $this->Form->isFieldError('name') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('name', array('label' => __('Nazwa: *'), 'placeholder' => __('Wpisz nazwę'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
          </div>
          <div class="form-group <?php echo $this->Form->isFieldError('description') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('description', array('label' => __('Opis w sklepie:'), 'placeholder' => __('Wpisz opis wyświetlany w sklepie'), 'class' => 'form-control', 'rows' => 3, 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
          </div>
          <div class="form-group <?php echo $this->Form->isFieldError('info') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('info', array('label' => __('Informacje w potwierdzeniu zamówienia:'), 'placeholder' => __('Wpisz informacje wyświetlane w sklepie w podsumowaniu zamówienia'), 'class' => 'form-control', 'rows' => 3, 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
          </div>
          <div class="form-group <?php echo $this->Form->isFieldError('confirm_btn_text') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('confirm_btn_text', array('label' => __('Tekst przycisku potwierdzenia zamówienia:'), 'placeholder' => __('Wpisz tekst przycisku potwierdzenia zamówienia'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
          </div>
          <div class="form-group <?php echo $this->Form->isFieldError('margin') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('margin', array('label' => false, 'placeholder' => __('Wpisz wysokość marży'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'div' => array('class' => 'input-group m-b unit-div'), 'data-toggle' => 'tooltip', 'data-title' => __('Wpisz wysokość marży'), 'after' => '<span class="input-group-addon">%</span>')); ?>
          </div>
        </fieldset>
      </div>
      <div class="col-md-4">
        <div class="form-group form-group-margin-left-0 <?php echo $this->Form->isFieldError('status') ? 'has-error' : ''; ?>">
          <label class="col-xs-5 control-label text-left-force" for="exampleInputEmail1"><?php echo __('Status: *'); ?></label>
          <div class="col-xs-7">
            <label class="checkbox-inline c-checkbox">
              <?php echo $this->Form->input('status', array('type' => 'checkbox', 'label' => false, 'div' => false)); ?>
              <span class="fa fa-check"></span><?php echo __('aktywny'); ?>
            </label>
          </div>
        </div>
        <div class="form-group form-group-margin-left-0 <?php echo $this->Form->isFieldError('sort_order') ? 'has-error' : ''; ?>">
          <label class="col-xs-5 control-label text-left-force" for="exampleInputEmail1"><?php echo __('Kolejność sort.:'); ?></label>
          <div class="col-xs-7">
            <?php echo $this->Form->input('sort_order', array('min' => '0', 'step' => 1, 'label' => false, 'div' => false, 'class' => 'form-control')); ?>
          </div>
        </div>
        <div class="form-group form-group-margin-left-0">
          <div class="col-xs-12 img-box">
            <label><?php echo __('Logo:'); ?></label>
            <?php if (isset($this->data['Payment']['image']) && !empty($this->data['Payment']['image'])): ?>
              <?php echo $this->App->showPaymentImage($this->data['Payment']['image'], 'THUMB'); ?>
            <?php endif; ?>
            <input type="file" name="data[Payment][image]" class="image-choosen" data-show-upload="false" data-show-caption="true"/>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="pull-right">
          <?php echo $this->Html->link(__('Anuluj'), array('controller' => 'KeyPayments', 'action' => 'index', 'admin' => true), array('title' => __('Anuluj'), 'class' => 'btn btn-default')); ?>
          <button type="submit" class="btn btn-primary"><?php echo __('Zapisz'); ?></button>
        </div>
      </div>
      <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
    </div>
  </div>
</div>

<script>
  $(document).ready(function () {
    <?php echo $this->element('Scripts/AppPaymentsDefaultSettings'); ?>	
    CKEDITOR.replace('PaymentInfo', {
      filebrowserBrowseUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'browse')); ?>',
    	filebrowserUploadUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'upload')); ?>',
    });
    App.Payments.onDocReady();
  });
</script>