<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th><?php echo $this->Paginator->sort('id', __('ID')); ?></th>
              <th><?php echo $this->Paginator->sort('created', __('Data utworzenia')); ?></th>
              <th><?php echo $this->Paginator->sort('number_all', __('Numer faktury')); ?></th>
              <th><?php echo $this->Paginator->sort('Order.number', __('Numer zamówienia')); ?></th>
              <th><?php echo __('Opcje'); ?></th>
            </tr>
          </thead>
          <tbody>
            <?php if (isset($invoices) && !empty($invoices)): ?>
              <?php foreach ($invoices as $invoice): ?>
                <tr>
                  <td><?php echo h($invoice['Invoice']['id']); ?>&nbsp;</td>
                  <td><?php echo h($this->Time->format($invoice['Invoice']['created'], '%Y-%m-%d %H:%M')); ?>&nbsp;</td>
                  <td><?php echo h($invoice['Invoice']['number_all']); ?>&nbsp;</td>
                  <td><?php echo isset($invoice['Order']) ? h($invoice['Order']['number']) : '-'; ?>&nbsp;</td>
                  <td class="options-buttons text-center">
                    <?php echo $this->Html->link('<em class="icon-printer"></em>', array('controller' => 'KeyInvoices', 'action' => 'download_pdf', $invoice['Invoice']['id']), array('escape' => false, 'title' => __('Pobierz PDF'), 'class' => 'option-link')); ?>
                    &nbsp;
                    <?php echo $this->Html->link('<em class="icon-trash"></em>', 'javascript:void(0);', array('escape' => false, 'title' => __('Usuń'), 'onclick' => 'App.Invoices.deleteInvoice(\''.h($invoice['Invoice']['number_all']).'\', \''.$this->Html->url(array('controller' => 'KeyInvoices', 'action' => 'delete_quote', $invoice['Invoice']['id'])).'\')', 'class' => 'option-link')); ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <td colspan="5"><?php echo __('Brak faktur do wyświetlenia'); ?></td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="panel-footer text-center">
      <div class="col-sm-6">
        <div class="pull-left">
          <?php echo $this->Paginator->counter(array('format' => __('Strona {:page} z {:pages}'))); ?>
        </div>
      </div>
      <div class="col-sm-6 pagination pagination-large">
        <ul class="pagination pull-right">
          <?php
          $this->Paginator->options['url'] = array('controller' => 'KeyInvoices', 'action' => 'index_quotes');
          echo $this->Paginator->prev(__('poprzednia'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1, 'modulus' => 5, 'ellipsis' => '<li class="disabled"><a>...</a></li>'));
          echo $this->Paginator->next(__('następna'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          ?>
        </ul>
      </div>
      <span class="clearfix"></span>
    </div>
  </div>
</div>

<?php echo $this->element('Scripts/AppInvoicesDefaultSettings'); ?>
<script>
  $(document).ready(function () {
    App.Invoices.onDocReady();
  });
</script>