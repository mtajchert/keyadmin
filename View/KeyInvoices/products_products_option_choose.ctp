<?php if(isset($product['ProductsProductsOption']) && !empty($product['ProductsProductsOption'])): ?>
  <h4><?php echo h($product['ProductsProductsOption'][0]['ProductOption']['name']); ?></h4>
  <?php foreach ($product['ProductsProductsOption'] as $productOption): ?>
    <tr>
      <td>
        <div class="radio c-radio">
          <label>
            <input type="radio" name="ChooseProductsProductsOptionBoxRadio" value="<?php echo h($productOption['id']); ?>">
            <span class="fa fa-check"></span>
          </label>
        </div>
      </td>
      <td>
        <?php echo h($productOption['ProductOptionValue']['name']); ?>
      </td>
      <td>
        <?php echo empty($productOption['change_price']) ? '' : h($productOption['change_price']).' '.__('zł'); ?>
      </td>
      <td>
        <?php echo empty($productOption['change_price_tax']) ? '' : h($productOption['change_price_tax']).' '.__('zł'); ?>
      </td>
      <td>
        <?php echo h($productOption['catalog_no']); ?>
      </td>
    </tr>
  <?php endforeach; ?>
<?php endif; ?>