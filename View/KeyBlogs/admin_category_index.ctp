<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th><?php echo $this->Paginator->sort('id', __('ID')); ?></th>
              <th><?php echo $this->Paginator->sort('title', __('Tytuł')); ?></th>
              <th><?php echo $this->Paginator->sort('modified', __('Zmodyfikowano')); ?></th>
              <th><?php echo __('Opcje'); ?></th>
            </tr>
          </thead>
          <tbody>
            <?php if (isset($categories) && !empty($categories)): ?>
              <?php foreach ($categories as $category): ?>
                <tr>
                  <td><?php echo h($category['BlogCategory']['id']); ?>&nbsp;</td>
                  <td><?php echo h($category['BlogCategory']['title']); ?>&nbsp;</td>
                  <td><?php echo h($category['BlogCategory']['modified']); ?>&nbsp;</td>
                  <td class="options-buttons text-center">
                    <?php //echo $this->Html->link('<em class="icon-picture"></em>', '#', array('escape' => false, 'title' => __('Dodaj/zmień zdjęcie'))); ?>                    
                    <?php echo $this->Html->link('<em class="icon-note"></em>', array('controller' => 'KeyBlogs', 'action' => 'category_edit', $category['BlogCategory']['id']), array('escape' => false, 'title' => __('Edytuj'))); ?>
                    &nbsp;
                    <?php echo $this->Html->link('<em class="icon-trash"></em>', 
                        array('controller' => 'KeyBlogs', 'action' => 'category_delete', $category['BlogCategory']['id']),
                        array('escape' => false, 'title' => __('Usuń'), 'confirm' => 'Na pewno chcesz usunąć kategorie?')); ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <td colspan="7"><?php echo __('Brak stron do wyświetlenia'); ?></td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="panel-footer text-center">
      <div class="col-sm-6">
        <div class="pull-left">
          <?php echo $this->Paginator->counter(array('format' => __('Strona {:page} z {:pages}'))); ?>
        </div>
      </div>
      <div class="col-sm-6 pagination pagination-large">
        <ul class="pagination pull-right">
          <?php
          $this->Paginator->options['url'] = array('controller' => 'KeyPages', 'action' => 'index');
          echo $this->Paginator->prev(__('poprzednia'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1, 'modulus' => 5, 'ellipsis' => '<li class="disabled"><a>...</a></li>'));
          echo $this->Paginator->next(__('następna'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          ?>
        </ul>
      </div>
      <span class="clearfix"></span>
    </div>
  </div>
</div>

<script>
  $(document).ready(function () {
    <?php echo $this->element('Scripts/AppManufacturersDefaultSettings'); ?>
  });
</script>