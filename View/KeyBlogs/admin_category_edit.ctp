<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <?php echo $this->Form->create('BlogCategory'); ?>
        
      <div class="col-md-6">
        <div class="form-group <?php echo $this->Form->isFieldError('title') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('title', array('label' => __('Tytuł: *'), 'placeholder' => __('Wpisz tytuł'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
      </div>
      <div class="col-md-6">
      	<div class="form-group <?php echo $this->Form->isFieldError('parent_id') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('parent_id', array('label' => __('Kategoria nadrzędna: '), 
            'class' => 'form-control', 'options' => $categories, 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>        
      </div>
      <div class="col-md-12">
        <div class="pull-right">

          <?php echo $this->Html->link(__('Anuluj'), array('controller' => 'KeyManufacturers', 'action' => 'index', 'admin' => true), array('title' => __('Anuluj'), 'class' => 'btn btn-default')); ?>
          <button type="submit" class="btn btn-primary"><?php echo __('Zapisz'); ?></button>
        </div>
      </div>
      <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
    </div>
  </div>
</div>