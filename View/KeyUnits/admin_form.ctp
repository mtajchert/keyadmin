<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <?php echo $this->Form->create('Unit', array('class' => 'form-horizontal')); ?>
      <div class="col-md-8">
        <div class="form-group <?php echo $this->Form->isFieldError('name') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('name', array('label' => __('Nazwa: *'), 'placeholder' => __('Wpisz nazwę'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
        <div class="form-group <?php echo $this->Form->isFieldError('quantity_type') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->label('quantity_type', __('Typ liczby: *'), array('type' => 'radio', 'class' => 'col-xs-3 control-label text-left-force')); ?>
          <div class="col-xs-9">
            <label class="radio-inline c-radio">
              <input id="UnitQuantityTypeInt" name="data[Unit][quantity_type]" value="int" <?php echo $unit['Unit']['quantity_type'] == 'int' ? 'checked' : ''; ?> type="radio">
              <span class="fa fa-circle"></span><?php echo(__('liczba całkowita')); ?>
            </label>
            <label class="radio-inline c-radio">
              <input id="UnitQuantityTypeFloat" name="data[Unit][quantity_type]" value="float" <?php echo $unit['Unit']['quantity_type'] == 'float' ? 'checked' : ''; ?> type="radio">
              <span class="fa fa-circle"></span><?php echo(__('liczba ułamkowa')); ?>
            </label>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group form-group-margin-left-0 <?php echo $this->Form->isFieldError('default') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->label('default', __('Domyślna: *'), array('type' => 'radio', 'class' => 'col-xs-5 control-label text-left-force')); ?>
          <div class="col-xs-7">
            <label class="radio-inline c-radio">
              <input id="UnitDefault1" name="data[Unit][default]" value="1" <?php echo $unit['Unit']['default'] ? 'checked' : ''; ?> type="radio">
              <span class="fa fa-circle"></span><?php echo(__('tak')); ?>
            </label>
            <label class="radio-inline c-radio">
              <input id="UnitDefault0" name="data[Unit][default]" value="0" <?php echo !$unit['Unit']['default'] ? 'checked' : ''; ?> type="radio">
              <span class="fa fa-circle"></span><?php echo(__('nie')); ?>
            </label>
          </div>
        </div>
        <div class="form-group form-group-margin-left-0 <?php echo $this->Form->isFieldError('sort_order') ? 'has-error' : ''; ?>">
          <label class="col-xs-5 control-label text-left-force" for="exampleInputEmail1"><?php echo __('Kolejność sort.:'); ?></label>
          <div class="col-xs-7">
            <?php echo $this->Form->input('sort_order', array('min' => '0', 'step' => 1, 'label' => false, 'div' => false, 'class' => 'form-control')); ?>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="pull-right">
          <?php echo $this->Html->link(__('Anuluj'), array('controller' => 'KeyUnits', 'action' => 'index', 'admin' => true), array('title' => __('Anuluj'), 'class' => 'btn btn-default')); ?>
          <button type="submit" class="btn btn-primary"><?php echo __('Zapisz'); ?></button>
        </div>
      </div>
      <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
    </div>
  </div>
</div>

<script>
  $(document).ready(function () {
    <?php echo $this->element('Scripts/AppUnitsDefaultSettings'); ?>
    App.Units.onDocReady();
  });
</script>