<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th><?php echo $this->Paginator->sort('id', __('ID')); ?></th>
              <th><?php echo $this->Paginator->sort('name', __('Nazwa')); ?></th>
              <th><?php echo $this->Paginator->sort('quantity_type', __('Typ liczby')); ?></th>
              <th><?php echo $this->Paginator->sort('sort_order', __('Kolejność sort.')); ?></th>
              <th><?php echo $this->Paginator->sort('default', __('Domyślna')); ?></th>
              <th><?php echo __('Opcje'); ?></th>
            </tr>
          </thead>
          <tbody>
            <?php if (isset($units) && !empty($units)): ?>
              <?php foreach ($units as $unit): ?>
                <tr>
                  <td><?php echo h($unit['Unit']['id']); ?>&nbsp;</td>
                  <td><?php echo h($unit['Unit']['name']); ?>&nbsp;</td>
                  <td><?php echo h($unit['Unit']['quantity_type'] == 'int' ? 'liczba całkowita' : 'liczba ułamkowa'); ?>&nbsp;</td>
                  <td>
                    <input class="form-control sort-order-input input-sm" type="text" value="<?php echo h($unit['Unit']['sort_order']); ?>" onchange="App.Units.saveSortOrder(<?php echo h($unit['Unit']['id']); ?>, this.value);">
                  </td>
                  <td>
                    <span class="checkbox c-checkbox c-checkbox-rounded c-checkbox-no-label">
                      <input <?php echo $unit['Unit']['default'] ? 'checked' : ''; ?> type="checkbox" disabled>
                      <span class="fa fa-check"></span>
                    </span>
                  </td>
                  <td class="options-buttons text-center">
                    <?php echo $this->Html->link('<em class="icon-note"></em>', array('controller' => 'KeyUnits', 'action' => 'edit', $unit['Unit']['id']), array('escape' => false, 'title' => __('Edytuj'))); ?>
                    &nbsp;
                    <?php echo $this->Html->link('<em class="icon-trash"></em>', 'javascript:void(0);', array('escape' => false, 'title' => __('Usuń'), 'onclick' => 'App.Units.deleteUnit(\''.h($unit['Unit']['name']).'\', \''.$this->Html->url(array('controller' => 'KeyUnits', 'action' => 'delete', $unit['Unit']['id'])).'\')')); ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <td colspan="6"><?php echo __('Brak jednostek miary do wyświetlenia'); ?></td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="panel-footer text-center">
      <div class="col-sm-6">
        <div class="pull-left">
          <?php echo $this->Paginator->counter(array('format' => __('Strona {:page} z {:pages}'))); ?>
        </div>
      </div>
      <div class="col-sm-6 pagination pagination-large">
        <ul class="pagination pull-right">
          <?php
          $this->Paginator->options['url'] = array('controller' => 'KeyUnits', 'action' => 'index');
          echo $this->Paginator->prev(__('poprzednia'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1, 'modulus' => 5, 'ellipsis' => '<li class="disabled"><a>...</a></li>'));
          echo $this->Paginator->next(__('następna'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          ?>
        </ul>
      </div>
      <span class="clearfix"></span>
    </div>
  </div>
</div>

<script>
  $(document).ready(function () {
    <?php echo $this->element('Scripts/AppUnitsDefaultSettings'); ?>
    App.Units.onDocReady();
  });
</script>