<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <?php echo $this->Form->create('Notification', array('class' => 'form-horizontal')); ?>
      <div class="col-md-12">
        <div class="form-group <?php echo $this->Form->isFieldError('subject') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('subject', array('label' => __('Temat wiadomości: *'), 'placeholder' => __('Wpisz temat wiadomości'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
        <div class="form-group <?php echo $this->Form->isFieldError('message') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('message', array('label' => __('Treść wiadomości: *'), 'placeholder' => __('Wpisz treść wiadomości:'), 'class' => 'form-control', 'rows' => 10, 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label><?php echo __('Dostępne znaczniki:'); ?></label>
          <p>(znaczników możesz używać zarówno w treści jak i w temacie wiadomości)</p>
          <?php foreach ($fields as $fieldsGroup): ?>
            <p><?php echo h($fieldsGroup['name']); ?></p>
            <p>
              <?php foreach ($fieldsGroup['fields'] as $field): ?>
                <span class="text-bold"><?php echo h($field[0]); ?></span> - <?php echo h($field[1]); ?><br/>
              <?php endforeach; ?>
            </p>
          <?php endforeach; ?>
        </div>
        <div class="col-md-12">
          <div class="pull-right">
            <?php echo $this->Html->link(__('Anuluj'), array('controller' => 'KeyNotifications', 'action' => 'index', 'admin' => true), array('title' => __('Anuluj'), 'class' => 'btn btn-default')); ?>
            <button type="submit" class="btn btn-primary"><?php echo __('Zapisz'); ?></button>
          </div>
        </div>
        <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function () {
    CKEDITOR.replace('NotificationMessage', {
      filebrowserBrowseUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'browse')); ?>',
			filebrowserUploadUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'upload')); ?>',
			height: 700,
      removePlugins: 'resize'
    });
    
    <?php echo $this->element('Scripts/AppNotificationsDefaultSettings'); ?>
    App.Notifications.onDocReady();
  });
</script>