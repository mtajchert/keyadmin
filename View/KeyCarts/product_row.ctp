<?php //print_r($cartProduct); ?>
<tr>
  <td rowspan="2">
    <?php echo h($cartProduct['Product']['id']); ?>
  </td>
  <td colspan="3">
    <?php echo $this->Form->input("CartProduct.{$key}.name", array('label' => false, 'placeholder' => __('Nazwa produktu'), 'class' => 'form-control', 'disabled' => true, 'div' => false, 'data-toggle' => 'tooltip', 'data-title' => __('Nazwa produktu'))); ?>
  </td>
  <td colspan="2">
    <?php echo $this->Form->input("CartProduct.{$key}.add_info", array('label' => false, 'placeholder' => __('Informacje dodatkowe'), 'class' => 'form-control', 'disabled' => true, 'div' => false, 'data-toggle' => 'tooltip', 'data-title' => __('Informacje dodatkowe'), 'type' => 'text')); ?>
  </td>
  <td rowspan="2">
    <?php if (isset($cartProduct['id']) && $cartProduct['id'] > 0): ?>
      &nbsp;
      <a href="javascript:void(0);" class="option-link" onclick="App.Carts.previewCartProduct(<?php echo h($cartProduct['id']); ?>);" title="<?php echo __('Podgląd'); ?>"><em class="icon-magnifier"></em></a>
    <?php endif; ?>
  </td>
</tr>
<tr>
  <td>
    <?php echo $this->Form->input("CartProduct.{$key}.price", array('label' => false, 'class' => 'form-control', 'disabled' => true, 'div' => false, 'data-toggle' => 'tooltip', 'data-title' => __('Cena netto'), 'min' => 0)); ?>
    <?php echo $this->Form->input("CartProduct.{$key}.tax", array('type' => 'hidden')); ?>
  </td>
  <td>
      <?php echo $this->Form->input("CartProduct.{$key}.tax_rate_id", array('label' => false, 'class' => 'form-control', 'disabled' => true, 'data-toggle' => 'tooltip', 'data-title' => __('Stawka VAT'), 'value' => $cartProduct['TaxRate']['description'], 'type' => 'text')); ?>
  </td>
  <td>
    <?php echo $this->Form->input("CartProduct.{$key}.price_tax", array('label' => false, 'placeholder' => __('Cena brutto'), 'class' => 'form-control', 'disabled' => true, 'div' => false, 'data-toggle' => 'tooltip', 'data-title' => __('Cena brutto'), 'min' => 0)); ?>
  </td>
  <td>
    <?php echo $this->Form->input("CartProduct.{$key}.amount", array('label' => false, 'placeholder' => __('Ilość'), 'class' => 'form-control', 'disabled' => true, 'div' => array('class' => 'input-group m-b unit-div'), 'data-toggle' => 'tooltip', 'data-title' => __('Ilość'), 'after' => '<span class="input-group-addon">'.h($cartProduct['Unit']['name']).'</span>')); ?>
  </td>
  <td class="<?php echo $this->Form->isFieldError("CartProduct.{$key}.total_price_tax") ? 'has-error' : ''; ?>">
    <?php echo $this->Form->input("CartProduct.{$key}.total_price_tax", array('label' => false, 'placeholder' => __('Wartość brutto'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'div' => false, 'data-toggle' => 'tooltip', 'data-title' => __('Wartość brutto'), 'min' => 0, 'readonly' => true, 'type' => 'text')); ?>
  </td>
</tr>