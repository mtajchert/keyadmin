<div class="col-sm-12">
  <?php echo $this->Form->create('Cart'); ?>
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="row">
        <div class="col-md-6">
          <fieldset>
            <?php echo $this->Form->input('id'); ?>
            
            <div class="form-group">
              <?php echo $this->Form->label('choose_customer', __('Klient:')); ?>
              <?php echo $this->Form->input('user_id', array('type' => 'hidden')); ?>
              <p id="customer_info">
                <?php if (isset($this->data['Customer']) && !empty($this->data['Customer'])): ?>
                  <?php echo $this->element('../KeyCarts/customer_info', array('user' => $cart['Customer'], 'userData' => (isset($cart['Customer']['UserData']))? $cart['Customer']['UserData']: array(), 'userAddress' => isset($cart['Customer']['UserAddress'][0]) ? $cart['Customer']['UserAddress'][0] : null)); ?>
                <?php endif; ?>
              </p>
            </div>
          </fieldset>
          <fieldset class="hide"></fieldset>
        </div>
        <div class="col-md-6"></div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <fieldset>
            <div class="form-group">
              <?php echo $this->Form->label('products', __('Produkty:')); ?>
              <table class="table products-table table-bordered">
                <thead>
                  <tr>
                    <th rowspan="2" class="text-v-center"><?php echo __('Id'); ?></th>
                    <th colspan="3"><?php echo __('Nazwa produktu'); ?></th>
                    <th colspan="2"><?php echo __('Dod. informacje'); ?></th>
                    <th rowspan="2" class="text-v-center"><?php echo __('Opcje'); ?></th>
                  </tr>
                  <tr>
                    <th><?php echo __('Cena netto'); ?></th>
                    <th><?php echo __('VAT'); ?></th>
                    <th><?php echo __('Cena brutto'); ?></th>
                    <th><?php echo __('Ilość'); ?></th>
                    <th><?php echo __('Wartość brutto'); ?></th>
                  </tr>
                </thead>
                <tbody>
                  <?php if(isset($this->data['CartProduct'])): ?>
                    <?php foreach ($this->data['CartProduct'] as $key => $cartProduct): ?>
                      <?php echo $this->element('../KeyCarts/product_row', array('key' => $key, 'cartProduct' => $cartProduct)); ?>
                    <?php endforeach; ?>
                  <?php endif; ?>
                  <tr class="no-products" <?php echo empty($this->data['CartProduct']) ? '' : 'style="display:none;"'; ?>>
                    <td colspan="7" class="text-center">Brak pozycji do wyświetlenia</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </fieldset>
          <fieldset class="hide"></fieldset>
        </div>
      </div>
      <div class="row">
        <div class="address_update">
          <?php echo $this->Form->input('address_update', array('type' => 'hidden')); ?>
        </div>
        <div class="col-md-6">
          <fieldset>
            <div class="form-group address">
              <div class="row">
                <div class="col-xs-12">
                  <label><?php echo __('Dane do rozliczenia'); ?></label>
                </div>
              </div>
              <div class="col-xs-12">
                <br>
                <p class="address-content">
                  <?php if (isset($this->data['BillingOrderUserAddress']['id']) && $this->data['BillingOrderUserAddress']['id'] > 0): ?>
                    <?php echo $this->User->getFormattedAddress($this->data['BillingOrderUserAddress']); ?>
                  <?php endif; ?>
                </p>
              </div>
            </div>
          </fieldset>
          <fieldset class="hide"></fieldset>
        </div>
        <div class="col-md-6">
          <fieldset>
            <div class="form-group address <?php echo $this->Form->isFieldError('shipping_order_address_id') ? 'has-error' : ''; ?>">
              <div class="row">
                <div class="col-xs-12">
                  <label><?php echo __('Dane do dostawy'); ?></label>
                </div>
              </div>
              <div class="col-xs-12">
                <br>
                <p class="address-content">
                  <?php if (isset($this->data['ShippingOrderUserAddress']['id']) && $this->data['ShippingOrderUserAddress']['id'] > 0): ?>
                    <?php echo $this->User->getFormattedAddress($this->data['ShippingOrderUserAddress']); ?>
                  <?php endif; ?>
                </p>
              </div>
            </div>
          </fieldset>
          <fieldset class="hide"></fieldset>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <fieldset>
            <div class="form-group">
              <div class="row">
                <div class="col-xs-12">
                  <label><?php echo __('Forma dostawy:'); ?></label>
                </div>
              </div>
              <div class="col-xs-12">
                <?php if (!empty($cart['Shipping']['id'])): ?>
                  <p><?php echo h($cart['Shipping']['name']).' - '.$cart['Cart']['shipping_price_tax'].' '.__('zł'); ?></p>
                <?php endif; ?>
              </div>
            </div>
          </fieldset>
          <fieldset class="hide"></fieldset>
        </div>
        <div class="col-md-6">
          <fieldset>
            <div class="form-group">
              <div class="row">
                <div class="col-xs-12">
                  <label><?php echo __('Forma płatności:'); ?></label>
                </div>
              </div>
              <div class="col-xs-12">
                <?php if (!empty($cart['Payment']['id'])): ?>
                  <p><?php echo h($cart['Payment']['name']); ?></p>
                <?php endif; ?>
              </div>
            </div>
          </fieldset>
          <fieldset class="hide"></fieldset>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <fieldset>
            <div class="form-group">
              <div class="row">
                <div class="col-xs-11">
                  <?php echo $this->Form->input('add_info', array('label' => __('Uwagi dotyczące zamówienia:'), 'class' => 'form-control', 'disabled' => true, 'rows' => 3)); ?>
                  <br>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-11">
                  <?php echo $this->Form->input('Customer.UserData.add_info', array('label' => __('Uwagi dotyczące klienta:'), 'class' => 'form-control', 'rows' => 3, 'disabled' => true)); ?>
                </div>
            </div>
          </fieldset>
        </div>
        <div class="col-md-6">
          <fieldset>
            <?php echo $this->Form->label('choose_customer', __('Podsumowanie koszyka:')); ?>
            <div class="col-xs-11">
              <table class="table table-bordered table-striped">
                <tr>
                  <th width="65%" class="text-right">
                    <?php echo $this->Form->input('products_price_tax', array('type' => 'hidden')); ?>
                    <?php echo __('Produkty brutto'); ?>
                  </th>
                  <td width="35%" class="text-right products_price_tax_display">
                    <?php echo h($this->data['Cart']['products_price_tax']).' '.__('zł'); ?>
                  </td>
                </tr>
                <tr>
                  <th width="65%" class="text-right">
                    <?php echo $this->Form->input('shipping_price_tax', array('type' => 'hidden')); ?>
                    <?php echo __('Dostawa brutto'); ?>
                  </th>
                  <td width="35%" class="text-right shipping_price_tax_display">
                    <?php echo h($this->data['Cart']['shipping_price_tax']).' '.__('zł'); ?>
                  </td>
                </tr>
                <tr>
                  <th width="65%" class="text-right">
                    <?php echo $this->Form->input('order_price_tax', array('type' => 'hidden')); ?>
                    <?php echo __('Razem brutto'); ?>
                  </th>
                  <td width="35%" class="text-right order_price_tax_display">
                    <?php echo h($this->data['Cart']['order_price_tax']).' '.__('zł'); ?>
                  </td>
                </tr>
              </table>
            </div>
          </fieldset>
        </div>
      </div>
    </div>
    <div class="panel-footer">
      <div class="clearfix">
        <div class="pull-right">
          <?php echo $this->Html->link(__('Anuluj'), array('controller' => 'KeyCarts', 'action' => 'index', 'admin' => true), array('title' => __('Anuluj'), 'class' => 'btn btn-default')); ?>
        </div>
      </div>
    </div>
  </div>
  <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
</div>

<?php echo $this->element('Scripts/AppCartsDefaultSettings'); ?>
<script>
  $(document).ready(function () {
    App.Carts.onDocReady();
  });
</script>