<?php
$string = '';
if (isset($user) && !empty($user)) {
  if (!empty($user['company'])) {
    $string .= '<strong>'.h($user['company']).'</strong>';
  }
  if (!empty($user['first_name']) || !empty($user['last_name'])) {
    $string .= (empty($string) ? '' : ', ').h(trim($user['first_name'].' '.$user['last_name'])).'</strong>';
  }
  if (!empty($user['email'])) {
    $string .= (empty($string) ? '' : ', ').h(trim($user['email'])).'</strong>';
  }
  if (!empty($user['contact_phone'])) {
    $string .= (empty($string) ? '' : ', ').h(__('tel.').' '.trim($user['contact_phone'])).'</strong>';
  }
  if ($user['guest_account']) {
    $string .= '<br/><span class="text-danger"><strong>'.__('Konto bez rejestracji').'</strong></span>';
  }
}
echo $string;
?>
