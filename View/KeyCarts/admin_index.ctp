<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th><?php echo $this->Paginator->sort('id', __('ID')); ?></th>
              <th><?php echo __('Klient'); ?></th>
              <th><?php echo $this->Paginator->sort('created', __('Data utworzenia')); ?></th>
              <th><?php echo $this->Paginator->sort('order_price_tax', __('Wartość')); ?></th>
              <th><?php echo $this->Paginator->sort('Payment.name', __('Forma płatności')); ?></th>
              <th><?php echo $this->Paginator->sort('Shipping.name', __('Forma dostawy')); ?></th>
              <th><?php echo __('Opcje'); ?></th>
            </tr>
          </thead>
          <tbody>
            <?php if (isset($carts) && !empty($carts)): ?>
              <?php foreach ($carts as $cart): ?>
                <tr class="<?php echo (!empty($cart['Cart']['paid']))? 'success': '';?>">
                  <td><?php echo h($cart['Cart']['id']); ?>&nbsp;</td>
                  <td>
                    <?php if (isset($cart['Cart']['email']) && !empty($cart['Cart']['email'])): ?>
                        <?php echo $cart['Cart']['email']; ?>
                    <?php else: ?>
                        <?php echo @$this->element('customer_standard_data', array('user' => $cart['Customer'], 'userData' => $cart['Customer']['UserData'], 'userAddress' => isset($cart['Customer']['UserAddress'][0]) ? $cart['Customer']['UserAddress'][0] : null)); ?>
                    <?php endif; ?>
                    
                  </td>
                  <td><?php echo h($this->Time->format($cart['Cart']['created'], '%Y-%m-%d %H:%M')); ?>&nbsp;</td>
                  <td><?php echo h($cart['Cart']['order_price_tax']); ?>&nbsp;</td>
                  <td><?php echo h($cart['Payment']['name']); ?>&nbsp;</td>
                  <td><?php echo h($cart['Shipping']['name']); ?>&nbsp;</td>
                  <td class="options-buttons text-center">
                    <?php echo $this->Html->link('<em class="icon-magnifier"></em>', array('controller' => 'KeyCarts', 'action' => 'preview', $cart['Cart']['id']), array('escape' => false, 'title' => __('Podgląd'), 'class' => 'option-link')); ?>
                    &nbsp;
                    <?php echo $this->Html->link('<em class="icon-trash"></em>', 'javascript:void(0);', array('escape' => false, 'title' => __('Usuń'), 'onclick' => 'App.Carts.deleteCart(\''.h($cart['Cart']['id']).'\', \''.$this->Html->url(array('controller' => 'KeyCarts', 'action' => 'delete', $cart['Cart']['id'])).'\')', 'class' => 'option-link')); ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <td colspan="9"><?php echo __('Brak koszyków do wyświetlenia'); ?></td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="panel-footer text-center">
      <div class="col-sm-6">
        <div class="pull-left">
          <?php echo $this->Paginator->counter(array('format' => __('Strona {:page} z {:pages}'))); ?>
        </div>
      </div>
      <div class="col-sm-6 pagination pagination-large">
        <ul class="pagination pull-right">
          <?php
          $this->Paginator->options['url'] = array('controller' => 'KeyCarts', 'action' => 'index');
          echo $this->Paginator->prev(__('poprzednia'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1, 'modulus' => 5, 'ellipsis' => '<li class="disabled"><a>...</a></li>'));
          echo $this->Paginator->next(__('następna'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          ?>
        </ul>
      </div>
      <span class="clearfix"></span>
    </div>
  </div>
</div>

<?php echo $this->element('Scripts/AppCartsDefaultSettings'); ?>
<script>
  $(document).ready(function () {
    
    App.Carts.onDocReady();
  });
</script>