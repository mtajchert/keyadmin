<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th><?php echo $this->Paginator->sort('id', __('ID')); ?></th>
              <th><?php echo $this->Paginator->sort('title', __('Tytuł')); ?></th>
              <th><?php echo $this->Paginator->sort('code', __('Kod')); ?></th>
              <th style="width: 300px;"><?php echo __('Kategorie'); ?></th>
              <th style="width: 100px;"><?php echo __('Producent'); ?></th>
              <th><?php echo $this->Paginator->sort('amount', __('Rabat (%)')); ?></th>
              <th><?php echo $this->Paginator->sort('active', __('Aktywny')); ?></th>
              <th><?php echo $this->Paginator->sort('valid_date', __('Ważny do')); ?></th>
              <th><?php echo __('Opcje'); ?></th>
            </tr>
          </thead>
          <tbody>
            <?php if (isset($discounts) && !empty($discounts)): ?>
              <?php foreach ($discounts as $discount): ?>
                <tr>
                  <td><?php echo h($discount['Discount']['id']); ?>&nbsp;</td>
                  <td><?php echo h($discount['Discount']['title']); ?>&nbsp;</td>
                  <td><?php echo h($discount['Discount']['code']); ?>&nbsp;</td>
                  <td>
                    <?php $categories = Hash::extract($discount, 'Category.{n}.name'); ?>
                    <?php echo (empty($categories))? 'Wszystkie': implode('<br/>', $categories); ?>
                  </td>
                  <td>
                    <?php $categories = Hash::extract($discount, 'Manufacturer.{n}.name'); ?>
                    <?php echo (empty($categories))? 'Wszystkie': implode('<br/>', $categories); ?>
                  </td>
                  <td><?php echo h($discount['Discount']['amount']); ?>&nbsp;</td>
                  <td><?php echo (empty($discount['Discount']['active']))? 'NIE': 'TAK'; ?>&nbsp;</td>
                  <td><?php echo h($discount['Discount']['valid_date']); ?>&nbsp;</td>
                  <td class="options-buttons text-center">
                    <?php //echo $this->Html->link('<em class="icon-picture"></em>', '#', array('escape' => false, 'title' => __('Dodaj/zmień zdjęcie'))); ?>                    
                    <?php echo $this->Html->link('<em class="icon-note"></em>', 
                        array('controller' => 'KeyDiscounts', 'action' => 'edit', $discount['Discount']['id']), array('escape' => false, 'title' => __('Edytuj'))); ?>
                    &nbsp;
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <td colspan="7"><?php echo __('Brak kuponów do wyświetlenia'); ?></td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="panel-footer text-center">
      <div class="col-sm-6">
        <div class="pull-left">
          <?php echo $this->Paginator->counter(array('format' => __('Strona {:page} z {:pages}'))); ?>
        </div>
      </div>
      <div class="col-sm-6 pagination pagination-large">
        <ul class="pagination pull-right">
          <?php
          $this->Paginator->options['url'] = array('controller' => 'KeyDiscount', 'action' => 'index');
          echo $this->Paginator->prev(__('poprzednia'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1, 'modulus' => 5, 'ellipsis' => '<li class="disabled"><a>...</a></li>'));
          echo $this->Paginator->next(__('następna'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          ?>
        </ul>
      </div>
      <span class="clearfix"></span>
    </div>
  </div>
</div>