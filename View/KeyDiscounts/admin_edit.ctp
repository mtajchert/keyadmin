<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <?php echo $this->Form->create('Discount'); ?>
        
      <div class="row">
      	<div class="col-md-6">
          <div class="form-group <?php echo $this->Form->isFieldError('title') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('title', array('label' => __('Tytuł: *'), 'placeholder' => __('Wpisz tytuł'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group <?php echo $this->Form->isFieldError('code') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('code', array('label' => __('Kod: *'), 
              'placeholder' => __('Wpisz kod'), 'class' => 'form-control', 
              'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
          </div>
        </div>
      </div>
      
      <div class="row">
      	<div class="col-md-6">
          <div class="form-group <?php echo $this->Form->isFieldError('amount') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('amount', array('label' => __('Rabat (%): *'), 'placeholder' => __('Podaj rabat'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group <?php echo $this->Form->isFieldError('valid_date') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('valid_date', array('label' => __('Data ważności: *'), 
              'type' => 'text',
              'placeholder' => __('Podaj datę'), 'class' => 'form-control',
              'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <?php echo $this->Form->input('Manufacturer.Manufacturer', array(
                'label' => __('Powiązany producent'), 
                'data-placeholder' => __('Wybierz producenta'), 
                'class' => 'form-control chosen',
                'options' => $manufacturers
            )); ?>
          </div>
        </div>
      </div>
      
      
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <?php echo $this->Form->input('Category.Category', array(
                'label' => __('Powiązana kategoria'), 
                'data-placeholder' => __('Wybierz kategorie'), 
                'class' => 'form-control chosen',
                'options' => $categories
            )); ?>
          </div>
        </div>
      </div>
      
      <div class="row">
      	<div class="col-md-12">
      		<div class="form-group">
            <label><?php echo __('Aktywny:'); ?></label>
            <label class="checkbox-inline c-checkbox">
              <?php echo $this->Form->input('active', array('type' => 'checkbox', 'label' => false, 'div' => false)); ?><span class="fa fa-check"></span>
            </label>
          </div>
      	</div>
      </div>
      
      <div class="col-md-12">
        <div class="pull-right">
          <?php echo $this->Html->link(__('Anuluj'), array('controller' => 'KeyManufacturers', 'action' => 'index', 'admin' => true), array('title' => __('Anuluj'), 'class' => 'btn btn-default')); ?>
          <button type="submit" class="btn btn-primary"><?php echo __('Zapisz'); ?></button>
        </div>
      </div>
      <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
    </div>
  </div>
</div>