<table>
  <?php foreach ($products as $product): ?>
    <tr>
      <td>
        <div class="radio c-radio">
          <label>
            <input type="radio" name="ChooseCategoryProductBoxRadio" value="<?php echo h($product['Product']['id']); ?>">
            <span class="fa fa-check"></span>
          </label>
        </div>
      </td>
      <td><?php echo h($product['Product']['id']); ?></td>
      <td>Zdjęcie</td>
      <td><?php echo h($product['Product']['name']); ?></td>
      <td><?php echo h($product['Product']['catalog_no']); ?></td>
      <td>
        <div class="checkbox c-checkbox">
          <label>
            <input type="checkbox" disabled <?php echo $product['Product']['status'] ? 'checked' : ''; ?>>
            <span class="fa fa-check"></span>
          </label>
        </div>
      </td>
    </tr>
  <?php endforeach; ?>
</table>
