<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <?php echo $this->Form->create('Product', array('class' => 'form-horizontal')); ?>
      <div class="col-md-7">
        <?php echo $this->Form->input('id', array('label' => false, 'div' => false, 'type' => 'hidden')); ?>
        <div class="form-group <?php echo $this->Form->isFieldError('id') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->label('name', __('Produkt: *')); ?>
          <?php echo $this->Form->input('name', array('label' => false, 'placeholder' => __('Wybierz produkt'), 'readonly' => 'true', 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'div' => array('class' => (isset($create) && $create) ? 'input-group m-b' : ''), 'after' => (isset($create) && $create) ? '<span class="input-group-btn"><button type="button" class="btn btn-default" onclick="App.OurHits.openChooseProductBox();">'.__('wybierz').'</button></span>' : '')); ?>
          <?php echo $this->Form->isFieldError('id') ? $this->Form->error('id', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
        </div>
        <div class="form-group <?php echo $this->Form->isFieldError('our_hit_status') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->label('our_hit_status', __('Status:'), array('class' => 'col-xs-5 control-label text-left-force')); ?>
          <div class="col-xs-7">
            <label class="checkbox-inline c-checkbox">
              <?php echo $this->Form->input('our_hit_status', array('type' => 'checkbox', 'label' => false, 'div' => false)); ?>
              <span class="fa fa-check"></span><?php echo __('aktywny'); ?>
            </label>
          </div>
        </div>
        <div class="form-group <?php echo $this->Form->isFieldError('our_hit_date') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->label('our_hit_date', __('Data rozpoczęcia:')); ?>
          <?php echo $this->Form->input('our_hit_date', array('type' => 'text', 'label' => false, 'placeholder' => __('Wybierz datę rozpoczęcia'), 'class' => 'form-control', 'error' => false, 'div' => array('class' => 'input-group date', 'id' => 'ProductOurHitDateBox'), 'after' => '<span class="input-group-addon"><span class="fa fa-calendar"></span></span>')); ?>
          <?php echo $this->Form->isFieldError('our_hit_date') ? $this->Form->error('our_hit_date', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
        </div>
        <div class="form-group <?php echo $this->Form->isFieldError('our_hit_date_end') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->label('our_hit_date_end', __('Data zakończenia:')); ?>
          <?php echo $this->Form->input('our_hit_date_end', array('type' => 'text', 'label' => false, 'placeholder' => __('Wybierz datę zakończenia'), 'class' => 'form-control', 'error' => false, 'div' => array('class' => 'input-group date', 'id' => 'ProductOurHitDateEndBox'), 'after' => '<span class="input-group-addon"><span class="fa fa-calendar"></span></span>')); ?>
          <?php echo $this->Form->isFieldError('our_hit_date_end') ? $this->Form->error('our_hit_date_end', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')) : ''; ?>
        </div>
      </div>
      <div class="col-md-12">
        <div class="pull-right">
          <?php echo $this->Html->link(__('Anuluj'), array('controller' => 'KeyOurHits', 'action' => 'index', 'admin' => true), array('title' => __('Anuluj'), 'class' => 'btn btn-default')); ?>
          <button type="submit" class="btn btn-primary"><?php echo __('Zapisz'); ?></button>
        </div>
      </div>
      <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
    </div>
  </div>
</div>

<script>
  $(document).ready(function () {
    $('#ProductOurHitDateBox, #ProductOurHitDateEndBox').datetimepicker({
      icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-arrow-up",
        down: "fa fa-arrow-down"
      },
      locale: 'pl',
      format: 'YYYY-MM-DD HH:mm'
    });

    <?php echo $this->element('Scripts/AppOurHitsDefaultSettings'); ?>
    
    <?php echo $this->element('Scripts/AppCategoriesTreeDefaultSettings'); ?>
    App.CategoriesTree.chooseMode = true;
    
    App.OurHits.onDocReady();
    App.CategoriesTree.onDocReady();
  });
</script>