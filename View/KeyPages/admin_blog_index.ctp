<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th><?php echo $this->Paginator->sort('id', __('ID')); ?></th>
              <th><?php echo $this->Paginator->sort('id', __('Miniaturka')); ?></th>
              <th><?php echo $this->Paginator->sort('id', __('Kategoria')); ?></th>
              <th><?php echo $this->Paginator->sort('title', __('Tytuł')); ?></th>
              <th><?php echo $this->Paginator->sort('created', __('Utworzono')); ?></th>
              <th><?php echo $this->Paginator->sort('modified', __('Zmodyfikowano')); ?></th>
              <th><?php echo __('Opcje'); ?></th>
            </tr>
          </thead>
          <tbody>
            <?php if (isset($pages) && !empty($pages)): ?>
              <?php foreach ($pages as $page): ?>
                <tr>
                  <td><?php echo h($page['Page']['id']); ?>&nbsp;</td>
                  <td><?php echo $this->App->showBlogImage($page['Page']['thumb'], 'THUMB', array('style' => 'max-height: 50px;')) ?>&nbsp;</td>
                  <td><?php echo h($page['BlogCategory']['title']); ?>&nbsp;</td>
                  <td><?php echo h($page['Page']['title']); ?>&nbsp;</td>
                  <td><?php echo h($page['Page']['created']); ?>&nbsp;</td>
                  <td><?php echo h($page['Page']['modified']); ?>&nbsp;</td>
                  <td class="options-buttons text-center">
                    <?php //echo $this->Html->link('<em class="icon-picture"></em>', '#', array('escape' => false, 'title' => __('Dodaj/zmień zdjęcie'))); ?>        
                    <?php echo $this->Html->link('<span class="glyphicon glyphicon-align-center" aria-hidden="true"></span>',
                    array(
                        'controller' => 'KeyCustomizations',
                        'action' => 'index',
                        'pages',
                        $page['Page']['id'],
                        'admin' => true
                    ), array(
                        'escape' => false, 'title' => __('Szablon'), 'class' => 'option-link'                                        
                    )); ?>&nbsp;&nbsp;&nbsp;            
                    <?php echo $this->Html->link('<em class="icon-note"></em>', array('controller' => 'KeyPages', 'action' => 'blog_edit', $page['Page']['id']), array('escape' => false, 'title' => __('Edytuj'))); ?>
                    &nbsp;
                    <?php echo $this->Html->link('<em class="icon-trash"></em>', 
                        array('controller' => 'KeyPages', 'action' => 'blog_delete', $page['Page']['id']),
                        array('escape' => false, 'title' => __('Usuń'), 'confirm' => 'Na pewno chcesz usunąć kategorie?')); ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <td colspan="7"><?php echo __('Brak stron do wyświetlenia'); ?></td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="panel-footer text-center">
      <div class="col-sm-6">
        <div class="pull-left">
          <?php echo $this->Paginator->counter(array('format' => __('Strona {:page} z {:pages}'))); ?>
        </div>
      </div>
      <div class="col-sm-6 pagination pagination-large">
        <ul class="pagination pull-right">
          <?php          
          $this->Paginator->options['url'] = array('controller' => 'KeyPages', 'action' => 'blog_index');
          echo $this->Paginator->prev(__('poprzednia'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1, 'modulus' => 5, 'ellipsis' => '<li class="disabled"><a>...</a></li>'));
          echo $this->Paginator->next(__('następna'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          ?>
        </ul>
      </div>
      <span class="clearfix"></span>
    </div>
  </div>
</div>

<script>
  $(document).ready(function () {
    <?php echo $this->element('Scripts/AppManufacturersDefaultSettings'); ?>
  });
</script>