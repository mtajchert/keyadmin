<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <?php echo $this->Form->create('Page', array()); ?>
        
      <div class="col-md-6">
        <div class="form-group <?php echo $this->Form->isFieldError('title') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('title', array('label' => __('Tytuł: *'), 'placeholder' => __('Wpisz tytuł'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
        <div class="form-group <?php echo $this->Form->isFieldError('meta_title_tag') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('meta_title_tag', array('label' => __('Meta tagi - tytuł:'), 'placeholder' => __('Wpisz meta tag - tytuł'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
        <div class="form-group <?php echo $this->Form->isFieldError('meta_desc_tag') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('meta_desc_tag', array('label' => __('Meta tagi - opis:'), 'placeholder' => __('Wpisz meta tag - opis'), 'class' => 'form-control', 'rows' => 3, 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
        <div class="form-group <?php echo $this->Form->isFieldError('meta_keywords_tag') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('meta_keywords_tag', array('label' => __('Meta tagi - słowa kluczowe:'), 'placeholder' => __('Wpisz meta tag - słowa kluczowe'), 'class' => 'form-control', 'rows' => 3, 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group <?php echo $this->Form->isFieldError('code') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('code', array('label' => __('Kod:'), 'placeholder' => __('Wpisz kod'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
        <div class="form-group <?php echo $this->Form->isFieldError('group') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('group', array('label' => __('Grupa:'), 'placeholder' => __('Wpisz grupę'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
      </div>
      
      <div class="col-md-12">
        <div class="form-group <?php echo $this->Form->isFieldError('short_content') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('short_content', array('label' => __('Krótka treść:'), 'placeholder' => __('Wpisz krótką treść'), 'class' => 'form-control', 'rows' => 10, 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
        <div class="form-group <?php echo $this->Form->isFieldError('content') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('content', array('label' => __('Treść:'), 'placeholder' => __('Wpisz treść'), 'class' => 'form-control', 'rows' => 10, 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
      </div>
      <div class="col-md-12">
        <div class="pull-right">

          <?php echo $this->Html->link(__('Anuluj'), array('controller' => 'KeyManufacturers', 'action' => 'index', 'admin' => true), array('title' => __('Anuluj'), 'class' => 'btn btn-default')); ?>
          <button type="submit" class="btn btn-primary"><?php echo __('Zapisz'); ?></button>
        </div>
      </div>
      <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
    </div>
  </div>
</div>

<script>
  $(document).ready(function () {
    CKEDITOR.replace('PageContent', {
      filebrowserBrowseUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'browse')); ?>',
			filebrowserUploadUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'upload')); ?>',
    });
    CKEDITOR.replace('PageShortContent', {
      filebrowserBrowseUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'browse')); ?>',
			filebrowserUploadUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'upload')); ?>',
    });
    
    <?php echo $this->element('Scripts/AppManufacturersDefaultSettings'); ?>
  });
</script>