<?php $isError = false; ?>
<div class="address-item col-xs-12 no-padding" id="address-item-<?php echo $key; ?>">
  <?php echo $this->Form->input("UserAddress.{$key}.id"); ?>
  <?php echo $this->Form->input("UserAddress.{$key}.user_id", array('type' => 'hidden')); ?>
  <table class="table table-bordered table-striped">
    <tbody>
      <tr>
        <td width="60%" class="address-alias">
          <?php echo h($userAddress['alias']); ?>
        </td>
        <td width="30%" class="address-defaults">
          <?php
            $defaults = [];
            if ($userAddress['default_billing']) {
              $defaults[] = __('Domyślny adres rozliczeniowy');
            }
            if ($userAddress['default_shipping']) {
              $defaults[] = __('Domyślny adres dostawy');
            }
            echo implode('<br>', $defaults);
          ?>
        <td width="10%">
          <a href="javascript:void(0);" title="Edytuj" class="option-link" onclick="App.Customers.editAddress($(this).closest('.address-item'));"><em class="icon-note"></em></a>
          &nbsp;
          <a href="javascript:void(0);" title="Usuń" class="option-link"  onclick="App.Customers.removeAddress($(this).closest('.address-item'));"><em class="icon-trash"></em></a>
        </td>
      </tr>
    </tbody>
  </table>
  <div class="col-md-12 item-content" style="display:none;">
    <br/>
    <fieldset class="fgmb15">
      <div class="col-md-12">
        <?php $this->Form->isFieldError("UserAddress.{$key}.alias") ? $isError = true : null; ?>
        <div class="form-group mr7 <?php echo $this->Form->isFieldError("UserAddress.{$key}.alias") ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input("UserAddress.{$key}.alias", array('type' => 'text', 'label' => __('Nazwa adresu: *'), 'placeholder' => __('Wpisz nazwę adresu'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
      </div>
      <div class="col-md-6">
        <?php $this->Form->isFieldError("UserAddress.{$key}.default_billing") ? $isError = true : null; ?>
        <div class="form-group <?php echo $this->Form->isFieldError("UserAddress.{$key}.default_billing") ? 'has-error' : ''; ?>">
          <label class="checkbox-inline c-checkbox">
            <?php echo $this->Form->input("UserAddress.{$key}.default_billing", array('type' => 'checkbox', 'label' => false, 'div' => false)); ?>
            <span class="fa fa-check"></span><?php echo __('domyślny adres rozliczeniowy'); ?>
          </label>
        </div>
      </div>
      <div class="col-md-6">
        <?php $this->Form->isFieldError("UserAddress.{$key}.default_shipping") ? $isError = true : null; ?>
        <div class="form-group <?php echo $this->Form->isFieldError("UserAddress.{$key}.default_shipping") ? 'has-error' : ''; ?>">
          <label class="checkbox-inline c-checkbox">
            <?php echo $this->Form->input("UserAddress.{$key}.default_shipping", array('type' => 'checkbox', 'label' => false, 'div' => false)); ?>
            <span class="fa fa-check"></span><?php echo __('domyślny adres dostawy'); ?>
          </label>
        </div>
      </div>
    </fieldset>
    <fieldset class="fgmb15">
      <?php $this->Form->isFieldError("UserAddress.{$key}.quantity_type") ? $isError = true : null; ?>
      <div class="form-group <?php echo $this->Form->isFieldError('quantity_type') ? 'has-error' : ''; ?>">
        <?php echo $this->Form->label("UserAddress.{$key}.is_company", __('Osobowość prawna: *'), array('type' => 'radio', 'class' => 'col-xs-4 control-label text-left-force')); ?>
        <div class="col-xs-8">
          <label class="radio-inline c-radio">
            <input id="UserAddress0IsCompany0" name="data[UserAddress][<?php echo $key; ?>][is_company]" value="0" <?php echo !$userAddress['is_company'] ? 'checked' : ''; ?> type="radio" onchange="App.Form.toggleShow(this.checked, '.xxx', '.is_company', $(this).closest('.item-content'));">
            <span class="fa fa-circle"></span><?php echo(__('osoba fizyczna')); ?>
          </label>
          <label class="radio-inline c-radio">
            <input id="UserAddress0IsCompany1" name="data[UserAddress][<?php echo $key; ?>][is_company]" value="1" <?php echo $userAddress['is_company'] ? 'checked' : ''; ?> type="radio" onchange="App.Form.toggleShow(this.checked, '.is_company', '.xxx', $(this).closest('.item-content'));">
            <span class="fa fa-circle"></span><?php echo(__('firma')); ?>
          </label>
        </div>
      </div>
    </fieldset>
    <fieldset class="fgmb15 is_company" <?php echo !$userAddress['is_company'] ? 'style="display:none;"' : ''; ?>>
      <div class="col-md-6">
        <?php $this->Form->isFieldError("UserAddress.{$key}.nip") ? $isError = true : null; ?>
        <div class="form-group mr7 <?php echo $this->Form->isFieldError("UserAddress.{$key}.nip") ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input("UserAddress.{$key}.nip", array('type' => 'text', 'label' => __('Numer NIP: *'), 'placeholder' => __('Wpisz numer NIP'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'required' => false)); ?>
        </div>
      </div>
      <div class="col-md-6">
        <?php $this->Form->isFieldError("UserAddress.{$key}.company") ? $isError = true : null; ?>
        <div class="form-group mr7 <?php echo $this->Form->isFieldError("UserAddress.{$key}.company") ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input("UserAddress.{$key}.company", array('type' => 'text', 'label' => __('Firma: *'), 'placeholder' => __('Wpisz nazwę firmy'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'required' => false)); ?>
        </div>
      </div>
    </fieldset>
    <fieldset class="fgmb15">
      <div class="col-md-6">
        <?php $this->Form->isFieldError("UserAddress.{$key}.first_name") ? $isError = true : null; ?>
        <div class="form-group mr7 <?php echo $this->Form->isFieldError("UserAddress.{$key}.first_name") ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input("UserAddress.{$key}.first_name", array('label' => __('Imię: *'), 'placeholder' => __('Wpisz imię'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
      </div>
      <div class="col-md-6">
        <?php $this->Form->isFieldError("UserAddress.{$key}.last_name") ? $isError = true : null; ?>
        <div class="form-group mr7 <?php echo $this->Form->isFieldError("UserAddress.{$key}.last_name") ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input("UserAddress.{$key}.last_name", array('label' => __('Nazwisko:'), 'placeholder' => __('Wpisz nazwisko'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
      </div>
      <div class="col-md-6 col-md-offset-right-6">
        <?php $this->Form->isFieldError("UserAddress.{$key}.street_address") ? $isError = true : null; ?>
        <div class="form-group mr7 <?php echo $this->Form->isFieldError("UserAddress.{$key}.street_address") ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input("UserAddress.{$key}.street_address", array('label' => __('Ulica i nr domu: *'), 'placeholder' => __('Wpisz ulicę i nr domu'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
      </div>
      <div class="col-md-6">
        <?php $this->Form->isFieldError("UserAddress.{$key}.post_code") ? $isError = true : null; ?>
        <div class="form-group mr7 <?php echo $this->Form->isFieldError("UserAddress.{$key}.post_code") ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input("UserAddress.{$key}.post_code", array('label' => __('Kod pocztowy: *'), 'placeholder' => __('Wpisz kod pocztowy'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
      </div>
      <div class="col-md-6">
        <?php $this->Form->isFieldError("UserAddress.{$key}.city") ? $isError = true : null; ?>
        <div class="form-group mr7 <?php echo $this->Form->isFieldError("UserAddress.{$key}.city") ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input("UserAddress.{$key}.city", array('label' => __('Miejscowość: *'), 'placeholder' => __('Wpisz miejscowość'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
      </div>
      <div class="col-md-6">
        <?php $this->Form->isFieldError("UserAddress.{$key}.country_id") ? $isError = true : null; ?>
        <div class="form-group mr7 <?php echo $this->Form->isFieldError("UserAddress.{$key}.country_id") ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input("UserAddress.{$key}.country_id", array('label' => __('Kraj: *'), 'empty' => __('Wybierz kraj'), 'default' => $defaultCountryId, 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'onchange' => 'App.Customers.changeCountry($(this).val());')); ?>
        </div>
      </div>
      <div class="col-md-6">
        <?php $this->Form->isFieldError("UserAddress.{$key}.zone_id") ? $isError = true : null; ?>
        <div class="form-group mr7 <?php echo $this->Form->isFieldError("UserAddress.{$key}.zone_id") ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input("UserAddress.{$key}.zone_id", array('label' => __('Województwo:'), 'empty' => __('Wybierz województwo'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
      </div>
    </fieldset>
    <fieldset class="fgmb15">
      <div class="col-md-6">
        <?php $this->Form->isFieldError("UserAddress.{$key}.phone") ? $isError = true : null; ?>
        <div class="form-group mr7 <?php echo $this->Form->isFieldError("UserAddress.{$key}.phone") ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input("UserAddress.{$key}.phone", array('label' => __('Telefon: *'), 'placeholder' => __('Wpisz numer telefonu'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
      </div>
    </fieldset>
    <div class="col-md-12 no-padding">
      <div class="pull-right">
        <a href="javascript:void(0);" title="<?php echo __('Zapisz adres'); ?>" class="btn btn-primary" onclick="App.Customers.saveAddress($(this).closest('.address-item'))"><?php echo __('Zapisz adres'); ?></a>
      </div>
    </div>
    <div class="col-md-12">&nbsp;</div>
  </div>
</div>
<?php if ($isError): ?>
    <script>
      $('#address-item-<?php echo $key; ?> .item-content').show();
    </script>
<?php endif; ?>