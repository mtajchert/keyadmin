<div class="table-grid table-grid-desktop">
  <div class="col col-md">
    <div class="pr">
      <div class="panel panel-default">
        <div class="panel-body">
          <ul class="list-group key-side-tabs" role="tablist">
            <li role="presentation" class="list-group-item node-categories_tree <?php echo empty($action) ? 'active' : ''; ?>">
              <a href="#tab-basic-data" aria-controls="tab-basic-data" role="tab" data-toggle="tab"><?php echo __('Dane podstawowe'); ?></a>
            </li>
            <li role="presentation" class="list-group-item node-categories_tree <?php echo $action == 'add_address' ? 'active' : ''; ?>">
              <a href="#tab-addresses" aria-controls="tab-addresses" role="tab" data-toggle="tab"><?php echo __('Dane adresowe'); ?></a>
            </li>
            <li role="presentation" class="list-group-item node-categories_tree">
              <a href="#tab-description" aria-controls="tab-description" role="tab" data-toggle="tab"><?php echo __('Uwagi'); ?></a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="col">
    <div class="panel panel-default">
      <div class="panel-body">
        <?php echo $this->Form->create('Customer', array('class' => 'form-horizontal')); ?>
        <div class="tab-content no-border">
          <div role="tabpanel" class="tab-pane <?php echo empty($action) ? 'active' : ''; ?>" id="tab-basic-data">
            <div class="col-md-8">
                <fieldset class="fgmb15">
                  <?php if ($this->data['Customer']['guest_account']): ?>
                  <p class="text-danger"><strong><?php echo __('Uwaga! Konto bez rejestracji - wyłącznie do realizacji zamówienia.'); ?></strong></p>
                  <?php endif; ?>
                  
                  <div class="form-group <?php echo $this->Form->isFieldError('User.guest_account') ? 'has-error' : ''; ?>">
                    <?php echo $this->Form->label('User.guest_account', __('Rodzaj konta: *'), array('type' => 'radio', 'class' => 'col-xs-3 control-label text-left-force no-padding')); ?>
                    <div class="col-xs-9">
                      <div class="radio c-radio">
                        <label>
                          <input id="UserGuestAccount0" name="data[User][guest_account]" value="0" <?php echo !$this->data['Customer']['guest_account'] ? 'checked' : ''; ?> type="radio">
                          <span class="fa fa-circle"></span><?php echo(__('konto z rejestracją')); ?>
                        </label>
                      </div>
                      <div class="radio c-radio">
                        <label>
                          <input id="UserGuestAccount1" name="data[User][guest_account]" value="1" <?php echo $this->data['Customer']['guest_account'] ? 'checked' : ''; ?> type="radio">
                          <span class="fa fa-circle"></span><?php echo(__('konto bez rejestracji (tylko do realizacji zamówienia)')); ?>
                        </label>
                      </div>
                    </div>
                  </div>
                </fieldset>
              <fieldset class="fgmb15">
                <div class="form-group <?php echo $this->Form->isFieldError('first_name') ? 'has-error' : ''; ?>">
                  <?php echo $this->Form->input('first_name', array('label' => __('Imię: *'), 'placeholder' => __('Wpisz imię'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
                <div class="form-group <?php echo $this->Form->isFieldError('last_name') ? 'has-error' : ''; ?>">
                  <?php echo $this->Form->input('last_name', array('label' => __('Nazwisko:'), 'placeholder' => __('Wpisz nazwisko'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
                <div class="form-group <?php echo $this->Form->isFieldError('company') ? 'has-error' : ''; ?>">
                  <?php echo $this->Form->input('company', array('label' => __('Firma:'), 'placeholder' => __('Wpisz nazwę firmy'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
                <div class="form-group <?php echo $this->Form->isFieldError('email') ? 'has-error' : ''; ?>">
                  <?php echo $this->Form->input('email', array('label' => __('Adres e-mail: *'), 'placeholder' => __('Wpisz adres e-mail'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
                <div class="form-group <?php echo $this->Form->isFieldError('password') ? 'has-error' : ''; ?>">
                  <?php echo $this->Form->input('password', array('label' => __('Hasło: *'), 'placeholder' => __('Wpisz hasło'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'required' => false, 'value' => '')); ?>
                </div>
                <div class="form-group <?php echo $this->Form->isFieldError('password2') ? 'has-error' : ''; ?>">
                  <?php echo $this->Form->input('password2', array('type' => 'password', 'label' => __('Powtórz hasło: *'), 'placeholder' => __('Wpisz hasło ponownie'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')), 'required' => false, 'value' => '')); ?>
                </div>
                <div class="form-group <?php echo $this->Form->isFieldError('contact_phone') ? 'has-error' : ''; ?>">
                  <?php echo $this->Form->input('contact_phone', array('label' => __('Numer telefonu: *'), 'placeholder' => __('Wpisz numer telefonu'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </fieldset>
              <fieldset class="fgmb15">
                <div class="form-group <?php echo $this->Form->isFieldError('UserData.ext_store_id') ? 'has-error' : ''; ?>">
                  <?php echo $this->Form->input('UserData.ext_store_id', array('type' => 'text', 'label' => __('Id klienta w programie magazynowym:'), 'placeholder' => __('Wpisz id klienta w programie magazynowym'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                </div>
              </fieldset>
            </div>
            <div class="col-md-4">
              <fieldset class="fgmb15">
                <?php if (isset($customer['Customer']['created']) && $customer['Customer']['created'] > 0): ?>
                  <div class="form-group form-group-margin-left-0">
                    <em class="col-xs-5 control-label text-left-force"><?php echo __('Data rejestracji:'); ?></em>
                    <div class="col-xs-7 control-label text-left-force">
                      <em><?php echo $customer['Customer']['created']; ?></em>
                    </div>
                  </div>
                <?php endif; ?>
                <?php if (isset($customer['Customer']['id']) && $customer['Customer']['id'] > 0): ?>
                  <div class="form-group form-group-margin-left-0">
                    <em class="col-xs-5 control-label text-left-force"><?php echo __('Ilość zamówień:'); ?></em>
                    <div class="col-xs-7 control-label text-left-force">
                      <em>0</em>
                    </div>
                  </div>
                <?php endif; ?>
                <div class="form-group form-group-margin-left-0 <?php echo $this->Form->isFieldError('status') ? 'has-error' : ''; ?>">
                  <?php echo $this->Form->label('UserData.status', __('Status:'), array('class' => 'col-xs-5 control-label text-left-force')); ?>
                  <div class="col-xs-7">
                    <label class="checkbox-inline c-checkbox">
                      <?php echo $this->Form->input('UserData.status', array('type' => 'checkbox', 'label' => false, 'div' => false)); ?>
                      <span class="fa fa-check"></span><?php echo __('aktywny'); ?>
                    </label>
                  </div>
                </div>
              </fieldset>
            </div>
          </div>


          <div role="tabpanel" class="tab-pane <?php echo $action == 'add_address' ? 'active' : ''; ?>" id="tab-addresses">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th width="60%">Nazwa adresu</th>
                  <th width="30%">Domyślny</th>
                  <th width="10%">Opcje</th>
                </tr>
              </thead>
            </table>
            <table class="table table-bordered table-striped no-addresses" <?php echo empty($this->data['UserAddress']) ? '' : 'style="display:none;"'; ?>>
              <tbody>
                <tr>
                  <td class="text-center">Brak adresów do wyświetlenia, kliknij przycisk poniżej, aby dodać</td>
                </tr>
              </tbody>
            </table>
            <?php if(isset($this->data['UserAddress'])): ?>
              <?php foreach ($this->data['UserAddress'] as $key => $userAddress): ?>
                <?php echo $this->element('../KeyCustomers/address_item', array('key' => $key, 'userAddress' => $userAddress)); ?>
              <?php endforeach; ?>
            <?php endif; ?>
            <table class="table table-bordered table-striped">
              <tfoot>
                <tr>
                  <td >
                    <a href="javascript:void(0);" class="option-link" onclick="App.Customers.addAddress();">
                      <em class="icon-plus"></em>
                      <?php echo __('Dodaj adres'); ?>
                    </a>
                  </td>
                </tr>
              </tfoot>
            </table>
            
            <?php echo $this->Form->input('user_address', array('type' => 'hidden')); ?>
            <?php echo $this->Form->isFieldError('user_address') ? '<div class="has-error">'.$this->Form->error('user_address', null, array('wrap' => 'span', 'class' => 'help-block m-b-none')).'</div>' : ''; ?>
          </div>

          <div role="tabpanel" class="tab-pane" id="tab-description">
            <div class="col-md-12">
              <div class="form-group <?php echo $this->Form->isFieldError('add_info') ? 'has-error' : ''; ?>">
                <?php echo $this->Form->input('UserData.add_info', array('label' => __('Uwagi:'), 'placeholder' => __('Wpisz uwagi'), 'class' => 'form-control', 'rows' => 10, 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="pull-right">
            <?php echo $this->Html->link(__('Anuluj'), array('controller' => 'KeyCategories', 'action' => 'index', 'admin' => true), array('title' => __('Anuluj'), 'class' => 'btn btn-default')); ?>
            <button type="submit" class="btn btn-primary"><?php echo __('Zapisz'); ?></button>
          </div>
        </div>
        <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function () {
    <?php echo $this->element('Scripts/AppCustomersDefaultSettings'); ?>

    App.Customers.onDocReady();
    
    <?php if (isset($after) && $after): ?>
      <?php if ($from == 'order' && $action == 'add_address'): ?>
        window.opener.App.Orders.afterAddAddress(window, {param: '<?php echo $param; ?>'});
      <?php endif; ?>
    <?php elseif ($action == 'add_address'): ?>
      App.Customers.addAddress();
    <?php endif; ?>
  });
</script>