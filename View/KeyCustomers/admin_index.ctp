<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-body">
            <?php echo $this->Form->create('Customer'); ?>
                <div class="col-xs-6">
                    <div class="form-group">
                        <?php echo $this->Form->input('phraze', array(
                            'label' => __('Fraze'), 'class' => 'form-control', 'div' => false,
                            'placeholder' => 'np. Nazwisko', 'data-validation' => 'required'
                        ));?>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label>&nbsp;</label>
                        <button class="btn btn-primary btn-block">Zatwierdź</button>
                    </div>
                </div>                
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th><?php echo $this->Paginator->sort('id', __('ID')); ?></th>
              <th><?php echo $this->Paginator->sort('customer', __('Klient')); ?></th>
              <th><?php echo $this->Paginator->sort('email', __('Kontakt')); ?></th>
              <th><?php echo $this->Paginator->sort('created', __('Data utworzenia')); ?></th>
              <th><?php echo __('Zamówienia'); ?></th>
              <th><?php echo $this->Paginator->sort('status', __('Status')); ?></th>
              <th><?php echo __('Opcje'); ?></th>
            </tr>
          </thead>
          <tbody>
            <?php if (isset($customers) && !empty($customers)): ?>
              <?php foreach ($customers as $customer): ?>
                <tr>
                  <td><?php echo h($customer['Customer']['id']); ?>&nbsp;</td>
                  <td>
                    <?php echo $this->element('customer_standard_data', array('user' => $customer['Customer'], 'userData' => $customer['UserData'], 'userAddress' => isset($customer['UserAddress'][0]) ? $customer['UserAddress'][0] : null)); ?>
                  </td>
                  <td>
                    <?php echo $this->element('customer_standard_contact', array('user' => $customer['Customer'], 'userData' => $customer['UserData'], 'userAddress' => isset($customer['UserAddress'][0]) ? $customer['UserAddress'][0] : null)); ?>
                  </td>
                  <td>
                    <?php echo strtotime($customer['Customer']['created']) > 0 ? h($this->Time->format($customer['Customer']['created'], '%Y-%m-%d %H:%M')) : '-'; ?>&nbsp;
                  </td>
                  <td>0</td>
                  <td>
                    <span class="checkbox c-checkbox c-checkbox-rounded c-checkbox-no-label">
                      <input <?php echo $customer['UserData']['status'] ? 'checked' : ''; ?> type="checkbox" onchange="App.Customers.saveStatus(<?php echo h($customer['Customer']['id']); ?>, this.checked ? 1 : 0);">
                      <span class="fa fa-check" onclick="$(this).parent().find('input').click();"></span>
                    </span>
                  </td>
                  <td class="options-buttons text-center">
                    <?php echo $this->Html->link('<em class="icon-note"></em>', array('controller' => 'KeyCustomers', 'action' => 'edit', $customer['Customer']['id']), array('escape' => false, 'title' => __('Edytuj'), 'class' => 'option-link')); ?>
                    &nbsp;
                    <?php echo $this->Html->link('<em class="icon-trash"></em>', 'javascript:void(0);', array('escape' => false, 'title' => __('Usuń'), 'onclick' => 'App.Customers.deleteCustomer(\''.h($customer['Customer']['customer']).'\', \''.$this->Html->url(array('controller' => 'KeyCustomers', 'action' => 'delete', $customer['Customer']['id'])).'\')', 'class' => 'option-link')); ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <td colspan="7"><?php echo __('Brak klientów do wyświetlenia'); ?></td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="panel-footer text-center">
      <div class="col-sm-6">
        <div class="pull-left">
          <?php echo $this->Paginator->counter(array('format' => __('Strona {:page} z {:pages}'))); ?>
        </div>
      </div>
      <div class="col-sm-6 pagination pagination-large">
        <ul class="pagination pull-right">
          <?php
          $this->Paginator->options['url'] = array('controller' => 'KeyCustomers', 'action' => 'index');
          echo $this->Paginator->prev(__('poprzednia'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1, 'modulus' => 5, 'ellipsis' => '<li class="disabled"><a>...</a></li>'));
          echo $this->Paginator->next(__('następna'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          ?>
        </ul>
      </div>
      <span class="clearfix"></span>
    </div>
  </div>
</div>

<script>
  $(document).ready(function () {
    <?php echo $this->element('Scripts/AppCustomersDefaultSettings'); ?>
    App.Customers.onDocReady();
  });
</script>