<?php

App::uses('AppHelper', 'View/Helper');

class CategoryHelper extends AppHelper {

  public function __construct(\View $View, $settings = array()) {
    parent::__construct($View, $settings);
    $this->Category = ClassRegistry::init('KeyAdmin.Category');
  }

  public function getNestedId($categoryId) {
    return $this->Category->getNestedId($categoryId);
  }

  public function getNestedName($categoryId) {
    return $this->Category->getNestedName($categoryId);
  }

}
