<?php
App::uses ( 'AppHelper', 'View/Helper' );
class ProductHelper extends AppHelper {
  public function __construct(\View $View, $settings = array()) {
    parent::__construct ( $View, $settings );
    $this->Product = ClassRegistry::init ( 'KeyAdmin.Product' );
  }
  
  
  public function getProductOptions ($productId) {
    $this->Product->contain(array('ProductsProductsOption', 'ProductsProductsOption.ProductOptionValue'));
    
    $product = $this->Product->read(null, $productId);
    $keys = Hash::extract($product, 'ProductsProductsOption.{n}.product_option_value_id');
    $values = Hash::extract($product, 'ProductsProductsOption.{n}.ProductOptionValue.name');
    
    return array_combine($keys, $values);
  }
}
