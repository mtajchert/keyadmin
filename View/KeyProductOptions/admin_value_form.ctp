<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <?php echo $this->Form->create('ProductOptionValue', array('class' => 'form-horizontal')); ?>
      <div class="col-md-8">
        <div class="form-group <?php echo $this->Form->isFieldError('product_option_id') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('product_option_id', array('label' => __('Cecha: *'), 'empty' => __('Wybierz cechę'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
        <div class="form-group <?php echo $this->Form->isFieldError('name') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('name', array('label' => __('Nazwa: *'), 'placeholder' => __('Wpisz nazwę'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
      </div>
      <div class="col-md-12">
        <div class="pull-right">
          <?php echo $this->Html->link(__('Anuluj'), array('controller' => 'KeyProductOptions', 'action' => 'index', 'admin' => true), array('title' => __('Anuluj'), 'class' => 'btn btn-default')); ?>
          <button type="submit" class="btn btn-primary"><?php echo __('Zapisz'); ?></button>
        </div>
      </div>
      <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
    </div>
  </div>
</div>

<?php echo $this->element('Scripts/AppProductOptionsDefaultSettings'); ?>
<script>
  $(document).ready(function () {
    App.ProductOptions.onDocReady();
  });
</script>