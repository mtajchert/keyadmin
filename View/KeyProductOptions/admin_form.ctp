<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <?php echo $this->Form->create('ProductOption', array('class' => 'form-horizontal')); ?>
      <div class="col-md-8">
        <div class="form-group <?php echo $this->Form->isFieldError('name') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('name', array('label' => __('Nazwa: *'), 'placeholder' => __('Wpisz nazwę'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
        <div class="form-group <?php echo $this->Form->isFieldError('type') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->label('type', __('Typ: *'), array('type' => 'radio', 'class' => 'col-xs-3 control-label text-left-force no-padding')); ?>
          <div class="col-xs-9">
            <label class="radio-inline c-radio">
              <input id="ProductOptionTypeList" name="data[ProductOption][type]" value="list" <?php echo $this->data['ProductOption']['type'] == 'list' ? 'checked' : ''; ?> type="radio">
              <span class="fa fa-circle"></span><?php echo(__('lista rozwijalna')); ?>
            </label>
            <label class="radio-inline c-radio">
              <input id="ProductOptionTypeRadio" name="data[ProductOption][type]" value="radio" <?php echo $this->data['ProductOption']['type'] == 'radio' ? 'checked' : ''; ?> type="radio">
              <span class="fa fa-circle"></span><?php echo(__('pola wyboru')); ?>
            </label>
          </div>
        </div>
        <div class="form-group <?php echo $this->Form->isFieldError('description') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('description', array('label' => __('Opis:'), 'placeholder' => __('Wpisz opis'), 'class' => 'form-control', 'rows' => 3, 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group form-group-margin-left-0 <?php echo $this->Form->isFieldError('sort_order') ? 'has-error' : ''; ?>">
          <label class="col-xs-5 control-label text-left-force" for="exampleInputEmail1"><?php echo __('Kolejność sort.:'); ?></label>
          <div class="col-xs-7">
            <?php echo $this->Form->input('sort_order', array('min' => '0', 'step' => 1, 'label' => false, 'div' => false, 'class' => 'form-control')); ?>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="pull-right">
          <?php echo $this->Html->link(__('Anuluj'), array('controller' => 'KeyProductOptions', 'action' => 'index', 'admin' => true), array('title' => __('Anuluj'), 'class' => 'btn btn-default')); ?>
          <button type="submit" class="btn btn-primary"><?php echo __('Zapisz'); ?></button>
        </div>
      </div>
      <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
    </div>
  </div>
</div>

<?php echo $this->element('Scripts/AppProductOptionsDefaultSettings'); ?>
<script>
  $(document).ready(function () {
    App.ProductOptions.onDocReady();
  });
</script>