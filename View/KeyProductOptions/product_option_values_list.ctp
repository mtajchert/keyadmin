<tr id="product_option_values_list_<?php echo h($product_option_id); ?>">
  <td colspan="5">
    <div class="anim" style="display:none;">
      <table class="table table-bordered table-striped">
        <?php foreach ($productOptionValues as $productOptionValue): ?>
          <tr>
            <td width="15%"><?php echo h($productOptionValue['ProductOptionValue']['id']); ?></td>
            <td width="65%"><?php echo h($productOptionValue['ProductOptionValue']['name']); ?></td>
            <td width="20%" class="options-buttons text-center">
              <?php echo $this->Html->link('<em class="icon-note"></em>', array('controller' => 'KeyProductOptions', 'action' => 'edit_value', $productOptionValue['ProductOptionValue']['id']), array('escape' => false, 'title' => __('Edytuj'), 'class' => 'option-link')); ?>
              &nbsp;
              <?php echo $this->Html->link('<em class="icon-trash"></em>', 'javascript:void(0);', array('escape' => false, 'title' => __('Usuń'), 'onclick' => 'App.ProductOptions.deleteProductOptionValue(\''.h($productOptionValue['ProductOptionValue']['name']).'\', \''.$this->Html->url(array('controller' => 'KeyProductOptions', 'action' => 'delete_value', $productOptionValue['ProductOptionValue']['id'])).'\')', 'class' => 'option-link')); ?>
            </td>
          </tr>
        <?php endforeach; ?>
      </table>
    </div>
  </td>
</tr>