<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th><?php echo $this->Paginator->sort('id', __('ID')); ?></th>
              <th><?php echo $this->Paginator->sort('name', __('Nazwa')); ?></th>
              <th><?php echo $this->Paginator->sort('type', __('Typ')); ?></th>
              <th><?php echo $this->Paginator->sort('sort_order', __('Kolejność sort.')); ?></th>
              <th><?php echo __('Opcje'); ?></th>
            </tr>
          </thead>
          <tbody>
            <?php if (isset($product_options) && !empty($product_options)): ?>
              <?php foreach ($product_options as $product_option): ?>
                <tr>
                  <td><?php echo h($product_option['ProductOption']['id']); ?>&nbsp;</td>
                  <td><?php echo h($product_option['ProductOption']['name']); ?>&nbsp;</td>
                  <td><?php echo h($product_option['ProductOption']['type'] == 'list' ? 'lista rozwijalna' : 'pola wyboru'); ?>&nbsp;</td>
                  <td>
                    <input class="form-control sort-order-input input-sm" type="text" value="<?php echo h($product_option['ProductOption']['sort_order']); ?>" onchange="App.ProductOptions.saveSortOrder(<?php echo h($product_option['ProductOption']['id']); ?>, this.value);">
                  </td>
                  <td class="options-buttons text-center">
                    <?php echo $this->Html->link('<em class="icon-list"></em>', 'javascript:void(0);', array('escape' => false, 'title' => __('Wartości'), 'onclick' => 'App.ProductOptions.toggleValuesList(this, '.h($product_option['ProductOption']['id']).')', 'class' => 'option-link')); ?>
                    &nbsp;
                    <?php echo $this->Html->link('<em class="icon-plus"></em>', array('controller' => 'KeyProductOptions', 'action' => 'create_value', $product_option['ProductOption']['id']), array('escape' => false, 'title' => __('Dodaj wartość'), 'class' => 'option-link')); ?>
                    &nbsp;
                    &nbsp;
                    <?php echo $this->Html->link('<em class="icon-note"></em>', array('controller' => 'KeyProductOptions', 'action' => 'edit', $product_option['ProductOption']['id']), array('escape' => false, 'title' => __('Edytuj'), 'class' => 'option-link')); ?>
                    &nbsp;
                    <?php echo $this->Html->link('<em class="icon-trash"></em>', 'javascript:void(0);', array('escape' => false, 'title' => __('Usuń'), 'onclick' => 'App.ProductOptions.deleteProductOption(\''.h($product_option['ProductOption']['name']).'\', \''.$this->Html->url(array('controller' => 'KeyProductOptions', 'action' => 'delete', $product_option['ProductOption']['id'])).'\')', 'class' => 'option-link')); ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <td colspan="5"><?php echo __('Brak cech produktów do wyświetlenia'); ?></td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="panel-footer text-center">
      <div class="col-sm-6">
        <div class="pull-left">
          <?php echo $this->Paginator->counter(array('format' => __('Strona {:page} z {:pages}'))); ?>
        </div>
      </div>
      <div class="col-sm-6 pagination pagination-large">
        <ul class="pagination pull-right">
          <?php
          $this->Paginator->options['url'] = array('controller' => 'KeyProductOptions', 'action' => 'index');
          echo $this->Paginator->prev(__('poprzednia'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1, 'modulus' => 5, 'ellipsis' => '<li class="disabled"><a>...</a></li>'));
          echo $this->Paginator->next(__('następna'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          ?>
        </ul>
      </div>
      <span class="clearfix"></span>
    </div>
  </div>
</div>

<?php echo $this->element('Scripts/AppProductOptionsDefaultSettings'); ?>
<script>
  $(document).ready(function () {
    App.ProductOptions.onDocReady();
  });
</script>