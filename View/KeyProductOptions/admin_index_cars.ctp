<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th><?php echo __('ID'); ?></th>
              <th><?php echo __('Nazwa'); ?></th>
              <th><?php echo __('Modele'); ?></th>
            </tr>
          </thead>
          <tbody>
              <?php foreach ($cars as $car): ?>
                <tr>
                  <td><?php echo $car['CarManufacturer']['id']; ?></td>
                  <td><?php echo $car['CarManufacturer']['name']; ?></td>
                  <td>
                    <?php $models = Hash::extract($car, 'CarModel.{n}.name'); echo implode(', ', $models); ?>
                  </td>
                </tr>
              <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>