<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <?php echo $this->Form->create('Config', array('class' => 'form-horizontal')); ?>
      <div class="col-md-8">
        <div class="form-group <?php echo $this->Form->isFieldError('Config.'.$keys['shop_name'].'.value') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('Config.'.$keys['shop_name'].'.id'); ?>
            <?php echo $this->Form->input('Config.'.$keys['shop_name'].'.code', array('type' => 'hidden')); ?>
          <?php echo $this->Form->input('Config.'.$keys['shop_name'].'.value', array('label' => __('Nazwa sklepu: *'), 'placeholder' => __('Wpisz nazwę sklepu'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
      </div>
      <div class="col-md-8">
        <div class="form-group <?php echo $this->Form->isFieldError('Config.'.$keys['email_signature'].'.valueText') ? 'has-error' : ''; ?>">
            <?php echo $this->Form->input('Config.'.$keys['email_signature'].'.id'); ?>
            <?php echo $this->Form->input('Config.'.$keys['email_signature'].'.code', array('type' => 'hidden')); ?>
          <?php echo $this->Form->input('Config.'.$keys['email_signature'].'.valueText', array('label' => __('Podpis powiadomień e-mail: *'), 'placeholder' => __('Wpisz podpis powiadomień e-mail'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
      </div>
      <div class="col-md-12">
        <div class="pull-right">
          <?php echo $this->Html->link(__('Anuluj'), array('controller' => 'KeyConfigs', 'action' => 'index', 'admin' => true), array('title' => __('Anuluj'), 'class' => 'btn btn-default')); ?>
          <button type="submit" class="btn btn-primary"><?php echo __('Zapisz'); ?></button>
        </div>
      </div>
      <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
    </div>
  </div>
</div>

<script>
  $(document).ready(function () {
    CKEDITOR.replace('Config0ValueText', {
      filebrowserBrowseUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'browse')); ?>',
			filebrowserUploadUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'upload')); ?>',
			height: 700,
      removePlugins: 'resize'
    });
    <?php echo $this->element('Scripts/AppConfigsDefaultSettings'); ?>
    App.Configs.onDocReady();
  });
</script>