<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-body">
            <?php echo $this->Form->create('Slider', array('class' => '', 'type' => 'file')); ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group <?php echo $this->Form->isFieldError('url') ? 'has-error' : ''; ?>">
                                    <?php echo $this->Form->input('url', array('label' => __('Url: *'), 'placeholder' => __('Podaj adres url'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group <?php echo $this->Form->isFieldError('btn_label') ? 'has-error' : ''; ?>">
                                    <?php echo $this->Form->input('btn_label', array(
                                        'label' => __('Opis przycisku'), 'placeholder' => __('Podaj adres url'), 
                                        'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none'))                                        
                                    )); ?>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group <?php echo $this->Form->isFieldError('sort') ? 'has-error' : ''; ?>">
                            <?php echo $this->Form->input('sort', array('label' => __('Kolejność: *'), 'type' => 'number', 'placeholder' => __('Kolejność'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                        </div>
                        
                        <div class="form-group">
                            <label>Zdjęcie:</label>
                            <input type="file" name="data[Slider][image]" class="image-choosen" data-show-upload="false" data-show-caption="true"/>
                        </div>
                        
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group <?php echo $this->Form->isFieldError('background_color') ? 'has-error' : ''; ?>">
                                    <?php echo $this->Form->input('background_color', array('label' => __('Kolor Tła [HEX]'), 'type' => 'text', 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <?php echo $this->Form->input('description', array(
                                'label' => __('Tekst: *'), 'type' => 'textarea', 'placeholder' => __('Opis'), 
                                'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none'))                                
                            )); ?>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-12">
                    <div class="pull-right">
                        <?php echo $this->Html->link(__('Anuluj'), array('controller' => 'KeySliders', 'action' => 'index', 'admin' => true), array('title' => __('Anuluj'), 'class' => 'btn btn-default')); ?>
                        <button type="submit" class="btn btn-primary"><?php echo __('Zapisz'); ?></button>
                    </div>
                </div>
                
            <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
        </div>
    </div>
</div>


<script type="text/javascript">
$(document).ready(function () {
    CKEDITOR.replace('SliderDescription', {
       filebrowserBrowseUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'browse')); ?>',
	   filebrowserUploadUrl: '<?php echo Router::url(array('controller' => 'KeyCkeditor', 'action' => 'upload')); ?>',    
    });
});
</script>