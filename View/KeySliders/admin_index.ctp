<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th><?php echo $this->Paginator->sort('id', __('ID')); ?></th>
              <th>Zdjęcie</th>
              <th><?php echo $this->Paginator->sort('url', __('Adres strony')); ?></th>
              <th><?php echo __('Tekst'); ?></th>
              <th><?php echo __('Kolejność'); ?></th>
              <th><?php echo __('Opcje'); ?></th>
            </tr>
          </thead>
          <tbody>
            <?php if (isset($sliders) && !empty($sliders)): ?>
              <?php foreach ($sliders as $slider): ?>
                <tr>
                  <td><?php echo h($slider['Slider']['id']); ?>&nbsp;</td>
                  <td><?php echo $this->App->showSliderImage($slider['Slider']['image'], 'THUMB'); ?></td>
                  <td><?php echo h($slider['Slider']['url']); ?>&nbsp;</td>
                  <td><?php echo (!empty($slider['Slider']['description']))? $slider['Slider']['description']: '---'; ?>&nbsp;</td>
                  <td><?php echo h($slider['Slider']['sort']); ?>&nbsp;</td>
                  <td class="options-buttons text-center">
                    <?php //echo $this->Html->link('<em class="icon-picture"></em>', '#', array('escape' => false, 'title' => __('Dodaj/zmień zdjęcie'))); ?>
                    
                    <?php echo $this->Html->link('<em class="icon-note"></em>', array('controller' => 'KeySliders', 'action' => 'edit', $slider['Slider']['id']), array('escape' => false, 'title' => __('Edytuj'))); ?>
                    &nbsp;
                    <?php echo $this->Html->link('<em class="icon-trash"></em>', 'javascript:void(0);', array('escape' => false, 'title' => __('Usuń'), 'onclick' => 'App.Sliders.deleteSlider(\''.h($slider['Slider']['url']).'\', \''.$this->Html->url(array('controller' => 'KeySliders', 'action' => 'delete', $slider['Slider']['id'])).'\')')); ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <td colspan="7"><?php echo __('Brak sliderów do wyświetlenia'); ?></td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="panel-footer text-center">
      <div class="col-sm-6">
        <div class="pull-left">
          <?php echo $this->Paginator->counter(array('format' => __('Strona {:page} z {:pages}'))); ?>
        </div>
      </div>
      <div class="col-sm-6 pagination pagination-large">
        <ul class="pagination pull-right">
          <?php
          $this->Paginator->options['url'] = array('controller' => 'KeyManufacturers', 'action' => 'index');
          echo $this->Paginator->prev(__('poprzednia'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1, 'modulus' => 5, 'ellipsis' => '<li class="disabled"><a>...</a></li>'));
          echo $this->Paginator->next(__('następna'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          ?>
        </ul>
      </div>
      <span class="clearfix"></span>
    </div>
  </div>
</div>

<script>
  $(document).ready(function () {
    <?php echo $this->element('Scripts/AppManufacturersDefaultSettings'); ?>
  });
</script>