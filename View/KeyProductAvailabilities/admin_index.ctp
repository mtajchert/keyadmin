<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th><?php echo $this->Paginator->sort('id', __('ID')); ?></th>
              <th><?php echo $this->Paginator->sort('name', __('Nazwa')); ?></th>
              <th><?php echo $this->Paginator->sort('shipping_mode', __('Można kupować?')); ?></th>
              <th><?php echo __('Opcje'); ?></th>
            </tr>
          </thead>
          <tbody>
            <?php if (isset($product_availabilities) && !empty($product_availabilities)): ?>
              <?php foreach ($product_availabilities as $product_availability): ?>
                <tr>
                  <td><?php echo h($product_availability['ProductAvailability']['id']); ?>&nbsp;</td>
                  <td><?php echo h($product_availability['ProductAvailability']['name']); ?>&nbsp;</td>
                  <td><?php echo h($product_availability['ProductAvailability']['shipping_mode'] ? 'tak' : 'nie'); ?>&nbsp;</td>
                  <td class="options-buttons text-center">
                    <?php echo $this->Html->link('<em class="icon-note"></em>', array('controller' => 'KeyProductAvailabilities', 'action' => 'edit', $product_availability['ProductAvailability']['id']), array('escape' => false, 'title' => __('Edytuj'))); ?>
                    &nbsp;
                    <?php echo $this->Html->link('<em class="icon-trash"></em>', 'javascript:void(0);', array('escape' => false, 'title' => __('Usuń'), 'onclick' => 'App.ProductAvailabilities.deleteProductAvailability(\''.h($product_availability['ProductAvailability']['name']).'\', \''.$this->Html->url(array('controller' => 'KeyProductAvailabilities', 'action' => 'delete', $product_availability['ProductAvailability']['id'])).'\')')); ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <td colspan="4"><?php echo __('Brak dostępności produktów do wyświetlenia'); ?></td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="panel-footer text-center">
      <div class="col-sm-6">
        <div class="pull-left">
          <?php echo $this->Paginator->counter(array('format' => __('Strona {:page} z {:pages}'))); ?>
        </div>
      </div>
      <div class="col-sm-6 pagination pagination-large">
        <ul class="pagination pull-right">
          <?php
          $this->Paginator->options['url'] = array('controller' => 'KeyProductAvailabilities', 'action' => 'index');
          echo $this->Paginator->prev(__('poprzednia'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1, 'modulus' => 5, 'ellipsis' => '<li class="disabled"><a>...</a></li>'));
          echo $this->Paginator->next(__('następna'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
          ?>
        </ul>
      </div>
      <span class="clearfix"></span>
    </div>
  </div>
</div>

<script>
  $(document).ready(function () {
    <?php echo $this->element('Scripts/AppProductAvailabilitiesDefaultSettings'); ?>
    App.ProductAvailabilities.onDocReady();
  });
</script>