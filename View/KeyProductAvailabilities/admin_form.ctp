<div class="col-sm-12">
  <div class="panel panel-default">
    <div class="panel-body">
      <?php echo $this->Form->create('ProductAvailability', array('class' => 'form-horizontal')); ?>
      <div class="col-md-8">
        <div class="form-group <?php echo $this->Form->isFieldError('name') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->input('name', array('label' => __('Nazwa: *'), 'placeholder' => __('Wpisz nazwę'), 'class' => 'form-control', 'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-block m-b-none')))); ?>
        </div>
        <div class="form-group <?php echo $this->Form->isFieldError('shipping_mode') ? 'has-error' : ''; ?>">
          <?php echo $this->Form->label('default', __('Można kupować w tej dostępności? *'), array('type' => 'radio', 'class' => 'col-xs-5 control-label text-left-force')); ?>
          <div class="col-xs-7">
            <label class="radio-inline c-radio">
              <input id="ProductAvailabilityShippingMode1" name="data[ProductAvailability][shipping_mode]" value="1" <?php echo $product_availability['ProductAvailability']['shipping_mode'] ? 'checked' : ''; ?> type="radio">
              <span class="fa fa-circle"></span><?php echo(__('tak')); ?>
            </label>
            <label class="radio-inline c-radio">
              <input id="ProductAvailabilityShippingMode0" name="data[ProductAvailability][shipping_mode]" value="0" <?php echo !$product_availability['ProductAvailability']['shipping_mode'] ? 'checked' : ''; ?> type="radio">
              <span class="fa fa-circle"></span><?php echo(__('nie')); ?>
            </label>
          </div>
        </div>
      </div>
      <div class="col-md-4"></div>
      <div class="col-md-12">
        <div class="pull-right">
          <?php echo $this->Html->link(__('Anuluj'), array('controller' => 'KeyProductAvailabilities', 'action' => 'index', 'admin' => true), array('title' => __('Anuluj'), 'class' => 'btn btn-default')); ?>
          <button type="submit" class="btn btn-primary"><?php echo __('Zapisz'); ?></button>
        </div>
      </div>
      <?php echo $this->Form->end(array('div' => array('class' => 'hide'))); ?>
    </div>
  </div>
</div>

<script>
  $(document).ready(function () {
    <?php echo $this->element('Scripts/AppProductAvailabilitiesDefaultSettings'); ?>
    App.ProductAvailabilities.onDocReady();
  });
</script>