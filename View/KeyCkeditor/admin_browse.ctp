<div class="ckeditor-browse">
  <?php foreach ($files as $file): ?>
    <a href="javascript:void(0);" onclick="returnFileUrl('/<?php echo CLIENT; ?>/ckeditor/<?php echo $file; ?>')">
      <img src="/<?php echo CLIENT; ?>/ckeditor/<?php echo $file; ?>" />
    </a>
  <?php endforeach; ?>
</div>

<script>
  // Helper function to get parameters from the query string.
  function getUrlParam(paramName) {
    var reParam = new RegExp('(?:[\?&]|&)' + paramName + '=([^&]+)', 'i');
    var match = window.location.search.match(reParam);

    return (match && match.length > 1) ? match[1] : null;
  }
  
  // Simulate user action of selecting a file to be returned to CKEditor.
  function returnFileUrl(fileUrl) {
    var funcNum = getUrlParam('CKEditorFuncNum');
    window.opener.CKEDITOR.tools.callFunction(funcNum, fileUrl);
    window.close();
  }
</script>
