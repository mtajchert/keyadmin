<?php
App::uses('AppModel', 'Model');

/**
 * Page Model
 */
class PromoteBlog extends AppModel {
  
  /**
   * Validation rules
   *
   * @var array
   */
  var $validate = array();
  
  
  var $belongsTo = array(
    'Blog' => array(
      'className' => 'KeyAdmin.Blog',
      'foreignKey' => 'blog_id'
    ),
    'Category' => array(
      'className' => 'KeyAdmin.BlogCategory',
      'foreignKey' => 'blog_category_id'
    )
  );
}
