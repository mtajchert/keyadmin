<?php

App::uses('AppModel', 'Model');

/**
 * OrderStatus Model
 *
 */
class OrderStatus extends AppModel {

  /**
   * Display field
   *
   * @var string
   */
  public $displayField = 'name';
  
  /**
   * Validation rules
   *
   * @var array
   */
  var $validate = array();

}
