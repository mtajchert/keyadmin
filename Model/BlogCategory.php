<?php
App::uses( 'AppModel', 'Model' );

/**
 * Page Model
 */
class BlogCategory extends AppModel {
  
  /**
   * Display field
   *
   * @var string
   */
  public $displayField = 'title';
  
  /**
   * Validation rules
   *
   * @var array
   */
  var $validate = array();
  
  public $hasMany = array(
    'ChildCategory' => array(
      'className' => 'KeyAdmin.BlogCategory',
      'foreignKey' => 'parent_id'
    )
  );
  /**
   */
  public function beforeSave($options = array()) {

    $this->data['BlogCategory']['seo_url'] = mb_strtolower(Inflector::slug($this->data['BlogCategory']['title'], '-'));
    
    return $this->data;
  }
}
