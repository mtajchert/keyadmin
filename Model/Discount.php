<?php
App::uses('AppModel', 'Model');

/**
 * Category Model
 */
class Discount extends AppModel
{

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';

    /**
     * Validation rules
     *
     * @var array
     */
    var $validate = array(
        'title' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'allowEmpty' => false,
                'required' => true,
                'message' => 'Tytuł kuponu jest wymagany'
            )
        ),
        'code' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'allowEmpty' => false,
                'required' => true,
                'message' => 'Kod jest wymagany'
            )
        ),
        'valid_date' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'allowEmpty' => false,
                'required' => true,
                'message' => 'Data ważności jest wymagana'
            )
        ),
        'status' => array(
            'rule1' => array(
                'rule' => array(
                    'range',
                    0,
                    100
                ),
                'allowEmpty' => false,
                'message' => 'Niepoprawny wartości rabatu'
            )
        )
    );
    
    public $hasAndBelongsToMany = array (
        'Category' => array (
            'className' => 'KeyAdmin.Category',
            'joinTable' => 'discounts_categories',
            'associationForeignKey' => 'category_id',
            'foreignKey' => 'discount_id',
            'unique' => 'keepExisting'
        ),
        'Manufacturer' => array (
            'className' => 'KeyAdmin.Manufacturer',
            'joinTable' => 'discounts_manufacturers',
            'associationForeignKey' => 'manufacturer_id',
            'foreignKey' => 'discount_id',
            'unique' => 'keepExisting'
        )
    );

    public function generateNewCode($prefix = '', $length = 6)
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i ++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $prefix . $randomString;
    }
}
