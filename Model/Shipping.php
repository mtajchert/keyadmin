<?php

App::uses('AppModel', 'Model');

/**
 * Shipping model
 *
 */
class Shipping extends AppModel {

  /**
   * Display field
   *
   * @var string
   */
  public $displayField = 'name';
  
  public $belongsTo = array(
    'TaxRate' => array(
      'className' => 'KeyAdmin.TaxRate',
      'foreignKey' => 'tax_rate_id'
    )
  );
  public $hasMany = array(
    'ShippingPrice' => array(
      'className' => 'KeyAdmin.ShippingPrice',
      'foreignKey' => 'shipping_id',
      'order' => 'ShippingPrice.to_weight ASC, ShippingPrice.to_price_tax ASC, ShippingPrice.to_product_amount ASC'
    )
  );
  public $hasAndBelongsToMany = array(
    'Payment' => array(
      'className' => 'KeyAdmin.Payment',
      'joinTable' => 'shippings_payments',
      'associationForeignKey' => 'payment_id',
      'foreignKey' => 'shipping_id',
      'unique' => true,
    ),
    'Country' => array(
      'className' => 'KeyAdmin.Country',
      'joinTable' => 'shippings_countries',
      'associationForeignKey' => 'country_id',
      'foreignKey' => 'shipping_id',
      'unique' => 'keepExisting',
    )
  );

  /**
   * Validation rules
   *
   * @var array
   */
  var $validate = array(
    'name' => array(
      'notEmpty' => array(
        'rule' => 'notEmpty',
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Nazwa jest wymagana'
      )
    ),
    'shop_name' => array(
      'notEmpty' => array(
        'rule' => 'notEmpty',
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Nazwa w sklepie jest wymagana'
      )
    ),
    'sort_order' => array(
      'int' => array(
        'rule' => array('comparison', '>=', 0),
        'allowEmpty' => true,
        'message' => 'Kolejność sortowania musi być liczbą większą lub równą 0'
      ),
    ),
    'status' => array(
      '0_or_1' => array(
        'rule' => array('range', -1, 2),
        'allowEmpty' => false,
        'message' => 'Niepoprawny status'
      ),
    ),
    'price_type' => array(
      'enum' => array(
        'rule' => array('inList', ['const', 'depend_weight', 'depend_price', 'depend_amount']),
        'required' => true,
        'message' => 'Nieprawidłowa wartość'
      )
    ),
    'price_tax' => array(
      'decimal' => array(
        'rule' => '/^([0-9]+([.,][0-9]{1,2}){0,1})$/i',
        'required' => false,
        'allowEmpty' => true,
        'message' => 'Wpisz wartość do 2 miejsc po przecinku'
      )
    ),
    'free_from' => array(
      'decimal' => array(
        'rule' => '/^([0-9]+([.,][0-9]{1,2}){0,1})$/i',
        'required' => false,
        'allowEmpty' => true,
        'message' => 'Wpisz wartość do 2 miejsc po przecinku'
      )
    ),
    'tax_rate_id' => array(
      'decimal' => array(
        'rule' => array('range', -1),
        'required' => true,
        'allowEmpty' => false,
        'message' => 'Wybierz stawkę VAT'
      )
    ),
    'large_scale' => array(
      '0_or_1' => array(
        'rule' => array('range', -1, 2),
        'allowEmpty' => false,
        'message' => 'Niepoprawna wartość'
      ),
    ),
    'max_weight' => array(
      'decimal' => array(
        'rule' => '/^([0-9]+([.,][0-9]{1,2}){0,1})$/i',
        'required' => false,
        'allowEmpty' => true,
        'message' => 'Wpisz wartość do 2 miejsc po przecinku'
      )
    ),
    'max_price_tax' => array(
      'decimal' => array(
        'rule' => '/^([0-9]+([.,][0-9]{1,2}){0,1})$/i',
        'required' => false,
        'allowEmpty' => true,
        'message' => 'Wpisz wartość do 2 miejsc po przecinku'
      )
    ),
    'all_payments' => array(
      '0_or_1' => array(
        'rule' => array('range', -1, 2),
        'allowEmpty' => false,
        'message' => 'Niepoprawna wartość'
      ),
    ),
    'all_countries' => array(
      '0_or_1' => array(
        'rule' => array('range', -1, 2),
        'allowEmpty' => false,
        'message' => 'Niepoprawna wartość'
      ),
    )
  );
  
  public function beforeValidate($options = array()) {
    if ($this->data['Shipping']['price_type'] == 'const') {
      $this->validator()->remove('price_tax');
      $this->validator()->add('price_tax', array(
        'decimal' => array(
          'rule' => '/^([0-9]+([.,][0-9]{1,2}){0,1})$/i',
          'required' => true,
          'allowEmpty' => false,
          'message' => 'Wpisz wartość do 2 miejsc po przecinku'
        )
      ));
    }
    
    if (empty($this->data['Shipping']['all_payments']) && (!isset($this->data['Payment']['Payment']) || empty($this->data['Payment']['Payment']))) {
      $this->invalidate('payment_id', __('Wybierz co najmniej jedną formę płatności'));
    }
    
    if (empty($this->data['Shipping']['all_countries']) && (!isset($this->data['Country']['Country']) || empty($this->data['Country']['Country']))) {
      $this->invalidate('country_id', __('Wybierz co najmniej jeden kraj'));
    }
  }
  
  public function beforeSave($options = array()) {
    if (is_array($this->data['Shipping']['image']) && $this->data['Shipping']['image']['error'] != UPLOAD_ERR_NO_FILE) {
      if (empty($this->data['Shipping']['image']['size'])) {
        $this->data['Shipping']['image'] = null;
        return true;
      }
      if (!is_uploaded_file($this->data['Shipping']['image']['tmp_name']) || $this->data['Shipping']['image']['error'] != UPLOAD_ERR_OK) {
        return false;
      }

      App::import('Component', 'KeyAdmin.Image');
      $oImage = new ImageComponent(new ComponentCollection());

      $shippingsDir = CLIENT_WWW.'shippings/origin/';
      if (!is_dir($shippingsDir)) {
        mkdir($shippingsDir, 0777, true);
      }
      $shippingsDirList = CLIENT_WWW.'shippings/list/';
      if (!is_dir($shippingsDirList)) {
        mkdir($shippingsDirList, 0777, true);
      }
      $shippingsDirThumb = CLIENT_WWW.'shippings/thumb/';
      if (!is_dir($shippingsDirThumb)) {
        mkdir($shippingsDirThumb, 0777, true);
      }
      $fileName = md5(time().$this->data['Shipping']['image']['name']).'.jpg';
      if (move_uploaded_file($this->data['Shipping']['image']['tmp_name'], $shippingsDir.$fileName)) {
        $oImage->resizeImageMaxDimensions($shippingsDir.$fileName, $shippingsDirList.$fileName, 50, 34);
        $oImage->resizeImageMaxDimensions($shippingsDir.$fileName, $shippingsDirThumb.$fileName, 100, 40);
        $this->data['Shipping']['image'] = $fileName;
      } else {
        return false;
      }
    } else {
      unset($this->data['Shipping']['image']);
    }

    return true;
  }

}
