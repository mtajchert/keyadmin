<?php
App::uses('User', 'KeyAdmin.Model');
App::uses('AppModel', 'Model');

/**
 * Customer Model
 */
class Customer extends User {
    public $useTable = 'users';
    
    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'first_name';
    public $virtualFields = array(
        'customer' => 'CONCAT_WS(" ", Customer.company, Customer.first_name, Customer.last_name)'
    );
    
    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'user_group_id' => array(
            'int_gt_0' => array(
                'rule' => array(
                    'comparison',
                    '>=',
                    0
                ),
                'allowEmpty' => true,
                'message' => 'Nie wybrano grupy'
            )
        ),
        'first_name' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'allowEmpty' => false,
                'required' => true,
                'message' => 'Imię jest wymagane'
            ),
            'length' => array(
                'rule' => array(
                    'lengthBetween',
                    1,
                    64
                ),
                'message' => 'Imię może mieć długość 1-64 znaków'
            )
        ),
        'last_name' => array(
            'length' => array(
                'rule' => array(
                    'lengthBetween',
                    0,
                    64
                ),
                'allowEmpty' => true,
                'required' => false,
                'message' => 'Nazwisko może mieć długość 0-64 znaków'
            )
        ),
        'email' => array(
            'email' => array(
                'rule' => array(
                    'email',
                    false
                ),
                'allowEmpty' => false,
                'required' => true,
                'message' => 'Podany e-mail jest nieprawidłowy'
            ),
            'emailUnique' => array(
                'rule' => 'validateEmailUnique',
                'allowEmpty' => false,
                'required' => true,
                'message' => 'Podany e-mail jest już zarejestrowany'
            )
        ),
        'company' => array(
            'length' => array(
                'rule' => array(
                    'lengthBetween',
                    0,
                    128
                ),
                'allowEmpty' => true,
                'required' => false,
                'message' => 'Nazwa firmy może mieć długość 0-128 znaków'
            )
        ),
        'contact_phone' => array(
            /* 'phone' => array(
                'rule' => '/^([0-9 ,+\-\(\)])*$/i',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'Wpisz poprawny nr telefonu (dopuszczalne znaki: +, -, (, ), 0-9, spacja i przecinek)'
            ), */
            'length' => array(
                'rule' => array(
                    'lengthBetween',
                    0,
                    32
                ),
                'allowEmpty' => true,
                'required' => false,
                'message' => 'Nr telefonu może mieć długość 0-32 znaków'
            )
        ),
        'user_address' => array(
            'notEmpty' => array(
                'rule' => array(
                    'validateAddress',
                    'notEmpty'
                ),
                'required' => true,
                'message' => 'Dodaj co najmniej jeden adres domyślny do rozliczeń i dostawy'
            ),
            'defaultShipping' => array(
                'rule' => array(
                    'validateAddress',
                    'defaultShipping'
                ),
                'required' => true,
                'message' => 'Wybierz domyślny adres do dostawy'
            )
        )
    );



    public function validatePasswordConfirm($value) {
        if (empty($this->data['Customer']['password']) || strcmp($value['password2'], $this->data['Customer']['password']) == 0) {
            return true;
        }
        
        $this->data['Customer']['password'] = $this->data['Customer']['password2'] = '';
        
        return false;
    }



    public function validateAddress($value, $type) {
        switch ($type) {
            case 'notEmpty' :
                if (! empty($this->data['UserAddress'])) {
                    return true;
                }
                break;
            case 'defaultBilling' :
                foreach ( $this->data['UserAddress'] as $userAddress ) {
                    if ($userAddress['default_billing']) {
                        return true;
                    }
                }
                break;
            case 'defaultShipping' :
                foreach ( $this->data['UserAddress'] as $userAddress ) {
                    if ($userAddress['default_shipping']) {
                        return true;
                    }
                }
                break;
        }
        
        return false;
    }



    public function validateEmailUnique($value, $type) {
        $conditions = array(
            'Customer.email' => $value['email'],
            'Customer.guest_account' => 0,
            'UserData.status' => 1
        );
        if (isset($this->data['Customer']['id']) && ! empty($this->data['Customer']['id'])) {
            $conditions['Customer.id !='] = $this->data['Customer']['id'];
        }
        $this->contain('UserData');
        $exists = $this->find('first', array(
            'conditions' => $conditions
        ));
        return empty($exists);
    }



    public function beforeValidate($options = array()) {
        parent::beforeValidate($options);
        
        if (empty($this->data['Customer']['guest_account']) && (! isset($this->data['Customer']['id']) || $this->data['Customer']['id'] <= 0 || ! empty($this->data['Customer']['password']))) {
            $this->validator()->add('password', array(
                'minLength' => array(
                    'rule' => array(
                        'minLength',
                        '3'
                    ),
                    'allowEmpty' => false,
                    'required' => true,
                    'message' => 'Hasło jest wymagane, min. 3 znaki'
                )
            ));
            $this->validator()->add('password2', array(
                'same' => array(
                    'rule' => 'validatePasswordConfirm',
                    'allowEmpty' => false,
                    'required' => true,
                    'message' => 'Hasła różnią się od siebie'
                )
            ));
        } elseif (! empty($this->data['Customer']['password'])) {
            $this->validator()->add('password', array(
                'minLength' => array(
                    'rule' => array(
                        'minLength',
                        '3'
                    ),
                    'allowEmpty' => true,
                    'required' => false,
                    'message' => 'Hasło musi mieć min. 3 znaki'
                )
            ));
            $this->validator()->add('password2', array(
                'same' => array(
                    'rule' => 'validatePasswordConfirm',
                    'allowEmpty' => false,
                    'required' => true,
                    'message' => 'Hasła różnią się od siebie'
                )
            ));
        } else {
            $this->validator()->remove('password');
            $this->validator()->remove('password2');
        }
        
        if (isset($options['noAddress']) && $options['noAddress']) {
            $this->validator()->remove('user_address');
        }
        if (isset($options['noContactPhone']) && $options['noContactPhone']) {
            $this->validator()->remove('contact_phone');
        }
        
        return true;
    }



    public function parseRequestData($data, $isGuest = 1) {
        $group = $this->UserGroup->find('first', array(
            'conditions' => array(
                'admin' => 0
            )
        ));
        $data['Customer']['user_group_id'] = $group['UserGroup']['id'];
        $data['Customer']['user_address'] = '';
        $data['Customer']['guest_account'] = $isGuest;
        
        $data['UserData'] = array(
            'status' => 1
        );
        
        $data['UserAddress'][0]['alias'] = __('Adres główny');
        $data['UserAddress'][0]['default_billing'] = false;
        $data['UserAddress'][0]['default_shipping'] = true;
        $data['UserAddress'][0]['is_company'] = 0;
        $data['UserAddress'][0]['company'] = (isset($data['Customer']['company'])) ? $data['Customer']['company'] : null;
        $data['UserAddress'][0]['first_name'] = $data['Customer']['first_name'];
        $data['UserAddress'][0]['last_name'] = $data['Customer']['last_name'];
        $data['UserAddress'][0]['phone'] = $data['Customer']['contact_phone'];
        $data['UserAddress'][0]['country_id'] = 170;
        
        if (! empty($data['Customer']['guest_account'])) {
            $data['Customer']['password'] = '';
            $data['Customer']['password2'] = '';
        }
        
        if (empty($data['User']['invoice'])) {
            unset($data['UserAddress'][1]);
            $data['UserAddress'][0]['default_billing'] = true;
        } else {
            $data['UserAddress'][1]['alias'] = __('Adres dodatkowy');
            $data['UserAddress'][1]['default_billing'] = true;
            $data['UserAddress'][1]['default_shipping'] = false;
            $data['UserAddress'][1]['is_company'] = 1;
            $data['UserAddress'][1]['phone'] = $data['Customer']['contact_phone'];
            $data['UserAddress'][1]['country_id'] = 170;
        }
        
        if (empty($data['User']['shipping_address'])) {
            unset($data['UserAddress'][2]);
        } else {
            $data['UserAddress'][2]['alias'] = __('Adres dodatkowy');
            $data['UserAddress'][2]['default_billing'] = false;
            $data['UserAddress'][2]['default_shipping'] = false;
            $data['UserAddress'][2]['is_company'] = 0;
            $data['UserAddress'][2]['country_id'] = 170;
        }
        
        return $data;
    }
}
