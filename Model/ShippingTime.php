<?php

App::uses('AppModel', 'Model');

/**
 * Shipping time model
 *
 */
class ShippingTime extends AppModel {

  /**
   * Display field
   *
   * @var string
   */
  public $displayField = 'name';

  /**
   * Validation rules
   *
   * @var array
   */
  var $validate = array(
    'days' => array(
      'int_gt_0' => array(
        'rule' => array('comparison', '>', 0),
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Ilość dni musi być liczbą większą od 0'
      )
    ),
    'name' => array(
      'notEmpty' => array(
        'rule' => 'notEmpty',
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Nazwa jest wymagana'
      )
    )
  );
}
