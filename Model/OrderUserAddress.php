<?php

App::uses('AppModel', 'Model');

/**
 * OrderUserAddress Model
 *
 */
class OrderUserAddress extends AppModel {

  /**
   * Display field
   *
   * @var string
   */
  public $displayField = 'alias';
  
  /**
   * belongsTo associations
   *
   * @var array
   */
  public $belongsTo = array(
    'Country' => array(
      'className' => 'KeyAdmin.Country',
      'foreignKey' => 'country_id'
    ),
    'Zone' => array(
      'className' => 'KeyAdmin.Zone',
      'foreignKey' => 'zone_id'
    ),
    'UserAddress' => array(
      'className' => 'KeyAdmin.UserAddress',
      'foreignKey' => 'user_address_id'
    )
  );

  /**
   * Validation rules
   *
   * @var array
   */
  var $validate = array();
  
  public function importUserAddress($userAddressId) {
    $this->UserAddress->contain(array('User'));
    $userAddress = $this->UserAddress->findById($userAddressId);
    
    $data = array(
        'user_address_id' => $userAddress['UserAddress']['id'],
        'alias' => $userAddress['UserAddress']['alias'],
        'is_company' => $userAddress['UserAddress']['is_company'],
        'first_name' => $userAddress['UserAddress']['first_name'],
        'last_name' => $userAddress['UserAddress']['last_name'],
        'email' => $userAddress['User']['email'],
        'company' => $userAddress['UserAddress']['company'],
        'nip' => $userAddress['UserAddress']['nip'],
        'street_address' => $userAddress['UserAddress']['street_address'],
        'post_code' => $userAddress['UserAddress']['post_code'],
        'city' => $userAddress['UserAddress']['city'],
        'country_id' => $userAddress['UserAddress']['country_id'],
        'zone_id' => $userAddress['UserAddress']['zone_id'],
        'phone' => $userAddress['UserAddress']['phone']
    );
    $this->create();
    $this->save($data);
    
    return $this->getLastInsertID();
  }
  
  public function updateByUserAddress($orderUserAddressId, $userAddressId) {
    $orderUserAddress = $this->findById($orderUserAddressId);
    $userAddress = $this->UserAddress->findById($userAddressId);
    
    $orderUserAddress[$this->alias]['user_address_id'] = $userAddress['UserAddress']['id'];
    $orderUserAddress[$this->alias]['alias'] = $userAddress['UserAddress']['alias'];
    $orderUserAddress[$this->alias]['is_company'] = $userAddress['UserAddress']['is_company'];
    $orderUserAddress[$this->alias]['first_name'] = $userAddress['UserAddress']['first_name'];
    $orderUserAddress[$this->alias]['last_name'] = $userAddress['UserAddress']['last_name'];
    $orderUserAddress[$this->alias]['company'] = $userAddress['UserAddress']['company'];
    $orderUserAddress[$this->alias]['nip'] = $userAddress['UserAddress']['nip'];
    $orderUserAddress[$this->alias]['street_address'] = $userAddress['UserAddress']['street_address'];
    $orderUserAddress[$this->alias]['post_code'] = $userAddress['UserAddress']['post_code'];
    $orderUserAddress[$this->alias]['city'] = $userAddress['UserAddress']['city'];
    $orderUserAddress[$this->alias]['country_id'] = $userAddress['UserAddress']['country_id'];
    $orderUserAddress[$this->alias]['zone_id'] = $userAddress['UserAddress']['zone_id'];
    $orderUserAddress[$this->alias]['phone'] = $userAddress['UserAddress']['phone'];
    
    return $this->save($orderUserAddress);
  }
  
  public function getOrderUserAddressData($id, $fromUser = false) {
    $orderUserAddress = array('id' => 0);
    if (!empty($id)) {
      if (is_numeric($id)) {
        $this->contain('Country', 'Zone');
        $orderUserAddress = $this->find('first', array(
          'conditions' => array(
            $this->alias.'.id' => (int) $id
          )
        ));
        $orderUserAddress = $orderUserAddress[$this->alias];
      }
      
      if ($fromUser || substr($id, 0, 2) == '0-') {
        if (substr($id, 0, 2) == '0-') {
          $user_address_id = (int) substr($id, 2);
        } else {
          $user_address_id = (int) $orderUserAddress['user_address_id'];
        }
        
        $userAddress = $this->UserAddress->find('first', array(
          'conditions' => array(
            'id' => $user_address_id
          )
        ));
        $userAddress = $userAddress['UserAddress'];
        
        $orderUserAddress['user_address_id'] = $user_address_id;
        $orderUserAddress['alias'] = $userAddress['alias'];
        $orderUserAddress['is_company'] = $userAddress['is_company'];
        $orderUserAddress['first_name'] = $userAddress['first_name'];
        $orderUserAddress['last_name'] = $userAddress['last_name'];
        $orderUserAddress['company'] = $userAddress['company'];
        $orderUserAddress['nip'] = $userAddress['nip'];
        $orderUserAddress['street_address'] = $userAddress['street_address'];
        $orderUserAddress['post_code'] = $userAddress['post_code'];
        $orderUserAddress['city'] = $userAddress['city'];
        $orderUserAddress['country_id'] = $userAddress['country_id'];
        $orderUserAddress['zone_id'] = $userAddress['zone_id'];
        $orderUserAddress['phone'] = $userAddress['phone'];
        
        if ($orderUserAddress['country_id'] > 0) {
          $country = $this->Country->findById($orderUserAddress['country_id']);
          $orderUserAddress['Country'] = $country['Country'];
        }
        if ($orderUserAddress['zone_id'] > 0) {
          $country = $this->Zone->findById($orderUserAddress['zone_id']);
          $orderUserAddress['Zone'] = $country['Zone'];
        }
      }
    }
    
    return $orderUserAddress;
  }

}
