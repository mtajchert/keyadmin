<?php

App::uses('AppModel', 'Model');

/**
 * Product review Model
 *
 */
class ProductReview extends AppModel {

  /**
   * Validation rules
   *
   * @var array
   */
  var $validate = array(
    'product_id' => array(
      'gt0' => array(
        'rule' => array('naturalNumber', false),
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Wybierz produkt'
      )
    ),
    'rate' => array(
      'decimal' => array(
        'rule' => array('decimal', 2),
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Wybierz ocenę'
      ),
      'range' => array(
        'rule' => array('range', 0.99, 5.01),
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Wybierz wartość oceny od 0 do 5'
      )
    ),
    'author_name' => array(
      'notEmpty' => array(
        'rule' => 'notEmpty',
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Wpisz swoje imię'
      )
    ),
    'review' => array(
      'notEmpty' => array(
        'rule' => 'notEmpty',
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Wpisz opinię'
      )
    )
  );

}
