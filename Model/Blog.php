<?php
App::uses('Page', 'KeyAdmin.Model');

/**
 * Page Model
 */
class Blog extends Page
{

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'title';

    public $useTable = 'pages';

    /**
     * Validation rules
     *
     * @var array
     */
    var $validate = array();

    var $belongsTo = array(
        'BlogCategory' => array(
            'className' => 'KeyAdmin.BlogCategory',
            'foreignKey' => 'category_id'
        )
    );
    
    var $hasOne = array(
        'Customization' => array(
            'className' => 'KeyAdmin.Customization',
            'foreignKey' => 'customization_id'
        )
    );

    var $hasMany = array(
        'PromoteBlog' => array(
            'className' => 'KeyAdmin.PromoteBlog',
            'foreignKey' => 'blog_id'
        ),
        'Customizations' => array(
            'foreignKey' => 'model_id',
            'conditions' => array(
                'Customizations.model' => 'Blog'
            )
        )
    );

    /**
     */
    public function beforeSave($options = array())
    {
        if (isset($this->data['Blog']['image']) && ! empty($this->data['Blog']['image']['size'])) {
            App::import('Component', 'KeyAdmin.Image');
            $oImage = new ImageComponent(new ComponentCollection());
            
            $blogDir = CLIENT_WWW . 'blog/origin/';
            if (! is_dir($blogDir)) {
                mkdir($blogDir, 0777, true);
            }
            
            $blogDirThumb = CLIENT_WWW . 'blog/thumb/';
            if (! is_dir($blogDirThumb)) {
                mkdir($blogDirThumb, 0777, true);
            }
            $fileName = md5(time() . $this->data['Blog']['image']['name']) . '.jpg';
            if (move_uploaded_file($this->data['Blog']['image']['tmp_name'], $blogDir . $fileName)) {
                $oImage->resizeImage($blogDir . $fileName, $blogDirThumb . $fileName, 200, 150);
            }
            
            $this->data['Blog']['thumb'] = $fileName;
        } else {
            unset($this->data['Blog']['image']);
        }
        
        $this->data['Blog']['seo_url'] = mb_strtolower(Inflector::slug($this->data['Blog']['title'], '-'));
        
        if (! empty($this->data['Blog']['tags'])) {
            $tags = explode(';', $this->data['Blog']['tags']);
            $formated = array();
            foreach ($tags as $tag) {
                $key = mb_strtolower(Inflector::slug($tag, '-'));
                $formated[$key] = $tag;
            }
            $this->data['Blog']['tags'] = serialize($formated);
        }
        
        return $this->data;
    }
}
