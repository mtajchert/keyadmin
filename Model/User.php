<?php

App::uses('AppModel', 'Model');

/**
 * User Model
 *
 * @property UserGroup $UserGroup
 */
class User extends AppModel {

  /**
   * Display field
   *
   * @var string
   */
  public $displayField = 'first_name';

  /**
   * Validation rules
   *
   * @var array
   */
  public $validate = array();

  //The Associations below have been created with all possible keys, those that are not needed can be removed

  /**
   * belongsTo associations
   *
   * @var array
   */
  public $belongsTo = array(
    'UserGroup' => array(
      'className' => 'KeyAdmin.UserGroup',
      'foreignKey' => 'user_group_id',
      'conditions' => '',
      'fields' => '',
      'order' => ''
    )
  );
  
  /**
   * hasOne associations
   *
   * @var array
   */
  public $hasOne = array(
    'UserData' => array(
      'className' => 'KeyAdmin.UserData',
      'foreignKey' => 'user_id'
    )
  );
  
  /**
   * hasMany associations
   *
   * @var array
   */
  public $hasMany = array(
    'UserAddress' => array(
      'className' => 'KeyAdmin.UserAddress',
      'foreignKey' => 'user_id',
      'order' => 'UserAddress.default_shipping DESC'
    )
  );

  /**
   * (non-PHPdoc)
   * @see Model::beforeSave()
   */
  public function beforeSave($options = array()) {
    if (isset($this->data[$this->alias]['password']) && !empty($this->data[$this->alias]['password'])) {
      $this->data[$this->alias]['password'] = $this->hashPassword($this->data[$this->alias]['password']);
    } else {
      unset($this->data[$this->alias]['password']);
    }
    return true;
  }
  
  public function hashPassword($pass) {
    App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
    $passwordHasher = new SimplePasswordHasher(array('hashType' => 'sha1'));
    return $passwordHasher->hash($pass);
  }

  /**
   *
   * @param string $id
   */
  public static function isAdmin($id) {

    if (!intval($id)) {
      throw new Exception("Invalid user");
    }

    $userModel = new User();
    $result = $userModel->find(
        'first', array(
          'conditions' => array('User.id' => $id, 'UserGroup.admin' => 1),
          'recursive' => 0
        )
    );

    if (empty($result)) {
      return false;
    }

    return true;
  }

}
