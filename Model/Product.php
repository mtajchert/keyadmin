<?php
App::uses ( 'AppModel', 'Model' );

/**
 * Category Model
 */
class Product extends AppModel {
  
  /**
   * Display field
   *
   * @var string
   */
  public $displayField = 'name';
  public $belongsTo = array (
    'Manufacturer' => array (
      'className' => 'KeyAdmin.Manufacturer',
      'foreignKey' => 'manufacturer_id' 
    ),
    'TaxRate' => array (
      'className' => 'KeyAdmin.TaxRate',
      'foreignKey' => 'tax_rate_id' 
    ),
    'ProductAvailability' => array (
      'className' => 'KeyAdmin.ProductAvailability',
      'foreignKey' => 'product_availability_id' 
    ),
    'ShippingTime' => array (
      'className' => 'KeyAdmin.ShippingTime',
      'foreignKey' => 'shipping_time_id' 
    ),
    'ProductCondition' => array (
      'className' => 'KeyAdmin.ProductCondition',
      'foreignKey' => 'product_condition_id' 
    ),
    'ProductWarranty' => array (
      'className' => 'KeyAdmin.ProductWarranty',
      'foreignKey' => 'product_warranty_id' 
    ),
    'Unit' => array (
      'className' => 'KeyAdmin.Unit',
      'foreignKey' => 'unit_id' 
    ) 
  );
  public $hasAndBelongsToMany = array (
    'Category' => array (
      'className' => 'KeyAdmin.Category',
      'joinTable' => 'products_categories',
      'associationForeignKey' => 'category_id',
      'foreignKey' => 'product_id',
      'unique' => 'keepExisting' 
    ),
    'Shipping' => array (
      'className' => 'KeyAdmin.Shipping',
      'joinTable' => 'products_shippings',
      'associationForeignKey' => 'shipping_id',
      'foreignKey' => 'product_id',
      'unique' => 'keepExisting' 
    ),
    'RelProduct' => array (
      'className' => 'KeyAdmin.Product',
      'joinTable' => 'product_products_rel',
      'associationForeignKey' => 'rel_product_id',
      'foreignKey' => 'product_id',
      'unique' => 'keepExisting'
    )
  );
  public $hasMany = array (
    'ProductsImage',
    'ProductsProductsOption' => array (
      'className' => 'KeyAdmin.ProductsProductsOption' 
    ),      
  'ProdOptions' => array (
      'className' => 'KeyAdmin.ProductsProductsOption'
  ),
    'ProductReview' => array (
      'className' => 'KeyAdmin.ProductReview' 
    ) 
  );
  
  public $hasOne = array(
      'ProductTemplate',
      'MainCategory' => array(
          'className' => 'KeyAdmin.ProductsCategory',
          'conditions' => array(
              'MainCategory.category_default' => 1
          )
      ),
      'Customization' => array(
          'className' => 'KeyAdmin.Customization',
          'foreignKey' => 'model_id',
          'conditions' => [
              'Customization.model' => 'product',
              'Customization.is_active' => 1,
          ]
      )
  );
  
  /**
   * Validation rules
   *
   * @var array
   */
  var $validate = array (
    'name' => array (
      'notEmpty' => array (
        'rule' => 'notEmpty',
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Nazwa produktu jest wymagana' 
      ) 
    ),
    'price' => array (
      'decimal' => array (
        'rule' => '/^([0-9]+([.,][0-9]{1,2}){0,1})$/i',
        'required' => true,
        'allowEmpty' => false,
        'message' => 'Wpisz wartość do 2 miejsc po przecinku' 
      ),
      'int_gt_0' => array (
        'rule' => array (
          'comparison',
          '>',
          0 
        ),
        'required' => true,
        'allowEmpty' => false,
        'message' => 'Wpisz wartość większą od 0' 
      ) 
    ),
    'price_tax' => array (
      'decimal' => array (
        'rule' => '/^([0-9]+([.,][0-9]{1,2}){0,1})$/i',
        'required' => true,
        'allowEmpty' => false,
        'message' => 'Wpisz wartość do 2 miejsc po przecinku' 
      ),
      'int_gt_0' => array (
        'rule' => array (
          'comparison',
          '>',
          0 
        ),
        'required' => true,
        'allowEmpty' => false,
        'message' => 'Wpisz wartość większą od 0' 
      ) 
    ),
    'retail_price' => array (
      'decimal' => array (
        'rule' => '/^([0-9]+([.,][0-9]{1,2}){0,1})$/i',
        'required' => false,
        'allowEmpty' => true,
        'message' => 'Wpisz wartość do 2 miejsc po przecinku' 
      ) 
    ),
    'store_quantity' => array (
      'decimal' => array (
        'rule' => '/^([0-9]+([.,][0-9]{1,2}){0,1})$/i',
        'required' => false,
        'allowEmpty' => true,
        'message' => 'Wpisz wartość do 2 miejsc po przecinku' 
      ) 
    ),
    'order_min_quantity' => array (
      'decimal' => array (
        'rule' => '/^([0-9]+([.,][0-9]{1,2}){0,1})$/i',
        'required' => false,
        'allowEmpty' => true,
        'message' => 'Wpisz wartość do 2 miejsc po przecinku' 
      ) 
    ),
    'order_max_quantity' => array (
      'decimal' => array (
        'rule' => '/^([0-9]+([.,][0-9]{1,2}){0,1})$/i',
        'required' => false,
        'allowEmpty' => true,
        'message' => 'Wpisz wartość do 2 miejsc po przecinku' 
      ) 
    ),
    'order_step_quantity' => array (
      'decimal' => array (
        'rule' => '/^([0-9]+([.,][0-9]{1,2}){0,1})$/i',
        'required' => false,
        'allowEmpty' => true,
        'message' => 'Wpisz wartość do 2 miejsc po przecinku' 
      ) 
    ),
    'ind_shipping_price_tax' => array (
      'decimal' => array (
        'rule' => '/^([0-9]+([.,][0-9]{1,2}){0,1})$/i',
        'required' => false,
        'allowEmpty' => true,
        'message' => 'Wpisz wartość do 2 miejsc po przecinku' 
      ) 
    ),
    'weight' => array (
      'decimal' => array (
        'rule' => '/^([0-9]+([.,][0-9]{1,2}){0,1})$/i',
        'required' => false,
        'allowEmpty' => true,
        'message' => 'Wpisz wartość do 2 miejsc po przecinku' 
      ) 
    ),
    'sort_order' => array (
      'decimal' => array (
        'rule' => array (
          'naturalNumber',
          true 
        ),
        'allowEmpty' => true,
        'message' => 'Kolejność sortowania musi być liczbą większą lub równą 0' 
      ),
      'gt_0' => array (
        'rule' => array (
          'comparison',
          '>=',
          0 
        ),
        'allowEmpty' => true,
        'message' => 'Kolejność sortowania musi być liczbą większą lub równą 0' 
      ) 
    ),
    'status' => array (
      '0_or_1' => array (
        'rule' => array (
          'range',
          - 1,
          2 
        ),
        'allowEmpty' => false,
        'message' => 'Niepoprawny status' 
      ) 
    ),
    'type' => array (
      'type' => array (
        'rule' => array (
          'inList',
          [ 
            'standard',
            'custom',
            'service',
            'digital' 
          ] 
        ),
        'required' => true,
        'message' => 'Wybierz typ produktu' 
      ) 
    ),
    'available_date' => array (
      'date' => array (
        'rule' => array (
          'date',
          'ymd' 
        ),
        'required' => false,
        'allowEmpty' => true,
        'message' => 'Nieprawidłowy format (RRRR-MM-DD)' 
      ) 
    ),
    'our_hit_date' => array (
      'rule1' => array (
        'rule' => array (
          'datetime',
          'ymd' 
        ),
        'required' => false,
        'allowEmpty' => true,
        'message' => 'Nieprawidłowy format (RRRR-MM-DD GG:MM)' 
      ) 
    ),
    'our_hit_date_end' => array (
      'rule1' => array (
        'rule' => array (
          'datetime',
          'ymd' 
        ),
        'required' => false,
        'allowEmpty' => true,
        'message' => 'Nieprawidłowy format (RRRR-MM-DD GG:MM)' 
      ) 
    ),
    'featured_date' => array (
      'rule1' => array (
        'rule' => array (
          'datetime',
          'ymd' 
        ),
        'required' => false,
        'allowEmpty' => true,
        'message' => 'Nieprawidłowy format (RRRR-MM-DD GG:MM)' 
      ) 
    ),
    'featured_date_end' => array (
      'rule1' => array (
        'rule' => array (
          'datetime',
          'ymd' 
        ),
        'required' => false,
        'allowEmpty' => true,
        'message' => 'Nieprawidłowy format (RRRR-MM-DD GG:MM)' 
      ) 
    ),
    'old_price' => array (
      'decimal' => array (
        'rule' => '/^([0-9]+([.,][0-9]{1,2}){0,1})$/i',
        'required' => false,
        'allowEmpty' => true,
        'message' => 'Wpisz wartość do 2 miejsc po przecinku' 
      ) 
    ),
    'promotion_date' => array (
      'rule1' => array (
        'rule' => array (
          'datetime',
          'ymd' 
        ),
        'required' => false,
        'allowEmpty' => true,
        'message' => 'Nieprawidłowy format (RRRR-MM-DD GG:MM)' 
      ) 
    ),
    'promotion_date_end' => array (
      'rule1' => array (
        'rule' => array (
          'datetime',
          'ymd' 
        ),
        'required' => false,
        'allowEmpty' => true,
        'message' => 'Nieprawidłowy format (RRRR-MM-DD GG:MM)' 
      ) 
    ),
    'promotion_info' => array (
      'maxLength' => array (
        'rule' => array (
          'maxLength',
          16
        ),
        'required' => false,
        'allowEmpty' => true,
        'message' => 'Wpisz tekst do 16 znaków' 
      ) 
    ),
    'default_category_id' => array (
      'decimal' => array (
        'rule' => array (
          'range',
          - 1 
        ),
        'required' => true,
        'allowEmpty' => false,
        'message' => 'Wybierz kategorię domyślną' 
      ) 
    ),
    'unit_id' => array (
      'type' => array (
        'rule' => array (
          'range',
          - 1 
        ),
        'required' => true,
        'message' => 'Wybierz jednostkę miary' 
      ) 
    ),
    'ext_code' => array(
        'extCodeUnique' => array(
            'rule' => 'validateExtCode',
            'allowEmpty' => true,
            'required' => false,
            'message' => 'Podany kod jest już w bazie'
        )
    ), 
  );
  
  
  
  public function validateExtCode($value, $type) {
      
      $conditions = array(
          'Product.ext_code' => $value['ext_code']
      );
      if (!empty($this->id)) {
          $conditions['Product.id !='] = $this->id;
  }
      $exists = $this->find('first', array(
          'conditions' => $conditions
      ));
      return empty($exists);
  }
  
  /**
   */
  public function beforeSave($options = array()) {
    $productsImageIds = Hash::extract ( $this->data, 'ProductsImage.{n}.ProductsImage.id' );
    $productsImageIds = array_filter ( $productsImageIds );
    $this->ProductsImage->deleteAll ( array (
      'ProductsImage.product_id' => $this->data ['Product'] ['id'],
      'ProductsImage.id !=' => $productsImageIds 
    ), false );
    
    if (empty ( $this->data ['Product'] ['seo_url'] )) {
      $this->data ['Product'] ['seo_url'] = Inflector::slug ( $this->data ['Product'] ['name'], '-' );
    }
    
    $productsProductsOptionsIds = Hash::extract ( $this->data, 'ProductsProductsOption.{n}.ProductsProductsOption.id' );
    $productsProductsOptionsIds = array_filter ( $productsProductsOptionsIds );
    $this->ProductsProductsOption->deleteAll ( array (
      'ProductsProductsOption.product_id' => $this->data ['Product'] ['id'],
      'ProductsProductsOption.id !=' => $productsProductsOptionsIds 
    ), false );
    
    return true;
    return $this->data;
  }
  
  /**
   */
  public function afterSave($created, $options = array()) {
    if (empty ( $this->id )) {
      return false;
    }
    
    if (isset ( $this->data ['Product'] ['image'] ) && ! empty ( $this->data ['Product'] ['image'] [0] ['size'] )) {
      App::import ( 'Component', 'KeyAdmin.Image' );
      $oImage = new ImageComponent ( new ComponentCollection () );
      
      $productsDir = CLIENT_WWW . 'products/origin/';
      if (! is_dir ( $productsDir )) {
        mkdir ( $productsDir, 0777, true );
      }
      $productsDirList = CLIENT_WWW . 'products/list/';
      if (! is_dir ( $productsDirList )) {
        mkdir ( $productsDirList, 0777, true );
      }
      $productsDirThumb = CLIENT_WWW . 'products/thumb/';
      if (! is_dir ( $productsDirThumb )) {
        mkdir ( $productsDirThumb, 0777, true );
      }
      foreach ( $this->data ['Product'] ['image'] as $image ) {
        $fileName = md5 ( time () . $image ['name'] ) . '.jpg';
        if (move_uploaded_file ( $image ['tmp_name'], $productsDir . $fileName )) {
          $oImage->resizeImage ( $productsDir . $fileName, $productsDirList . $fileName, 0, 50 );
          if (CLIENT != 'Floper') {
              $oImage->resizeImage ( $productsDir . $fileName, $productsDirThumb . $fileName, 0, 360 );              
          } else {
              $oImage->resizeImage ( $productsDir . $fileName, $productsDirThumb . $fileName, 0, 460 );
          }
          $this->ProductsImage->create ();
          $this->ProductsImage->save ( array (
            'ProductsImage' => array (
              'product_id' => $this->data ['Product'] ['id'],
              'image' => $fileName 
            ) 
          ) );
        }
      }
    }
    unset ( $this->data ['Product'] ['image'] );
  }
  public function deletePromotion($ids, $restorePrice = true) {
    $ids = ( array ) $ids;
    $products = $this->find ( 'all', array (
      'conditions' => array (
        'Product.id' => $ids 
      ) 
    ) );
    
    $ds = $this->getDataSource ();
    $ds->begin ();
    foreach ( $products as $product ) {
      $this->id = $product ['Product'] ['id'];
      $data = array (
        'Product' => array (
          'promotion_status' => 0,
          'promotion_date' => '',
          'promotion_date_end' => '',
          'price_tax' => $restorePrice ? $product ['Product'] ['old_price'] : $product ['Product'] ['price_tax'],
          'old_price' => 0 
        ) 
      );
      if (! $this->save ( $data )) {
        $ds->rollback ();
        return false;
      }
    }
    $ds->commit ();
    return true;
  }
  public function manipulatePromotionDate($ids, $value, $endDate = false) {
    $ids = ( array ) $ids;
    $products = $this->find ( 'all', array (
      'conditions' => array (
        'Product.id' => $ids 
      ) 
    ) );
    
    $date_change = ($value > 0 ? '+' : '') . $value . ' days';
    
    $ds = $this->getDataSource ();
    $ds->begin ();
    
    $postfix = $endDate ? '_end' : '';
    $data = array (
      'Product' => array (
        'promotion_date' . $postfix => null 
      ) 
    );
    
    foreach ( $products as $product ) {
      $this->id = $product ['Product'] ['id'];
      $data ['Product'] ['promotion_date' . $postfix] = date ( 'Y-m-d H:i:s', strtotime ( $date_change, $product ['Product'] ['promotion_date' . $postfix] == '0000-00-00 00:00:00' ? time () : strtotime ( $product ['Product'] ['promotion_date' . $postfix] ) ) );
      if (! $this->save ( $data )) {
        $ds->rollback ();
        return false;
      }
    }
    $ds->commit ();
    return true;
  }
  public function manipulatePromotionDateEnd($ids, $value) {
    return $this->manipulatePromotionDate ( $ids, $value, true );
  }
  public function manipulateOldPrice($ids, $value, $type) {
    $ids = ( array ) $ids;
    $products = $this->find ( 'all', array (
      'conditions' => array (
        'Product.id' => $ids 
      ) 
    ) );
    
    $change = $type == 'amount' ? $value : (100 + $value) / 100;
    
    $ds = $this->getDataSource ();
    $ds->begin ();
    
    $data = array (
      'Product' => array (
        'old_price' => null 
      ) 
    );
    
    foreach ( $products as $product ) {
      $this->id = $product ['Product'] ['id'];
      if ($type == 'amount') {
        $data ['Product'] ['old_price'] = ( float ) $product ['Product'] ['old_price'] + $change;
      } else {
        $data ['Product'] ['old_price'] = ( float ) $product ['Product'] ['old_price'] * $change;
      }
      if (! $this->save ( $data )) {
        $ds->rollback ();
        return false;
      }
    }
    $ds->commit ();
    return true;
  }
  public function deleteOurHit($ids) {
    $ids = ( array ) $ids;
    $products = $this->find ( 'all', array (
      'conditions' => array (
        'Product.id' => $ids 
      ) 
    ) );
    
    $ds = $this->getDataSource ();
    $ds->begin ();
    foreach ( $products as $product ) {
      $this->id = $product ['Product'] ['id'];
      $data = array (
        'Product' => array (
          'our_hit_status' => 0,
          'our_hit_date' => '',
          'our_hit_date_end' => '' 
        ) 
      );
      if (! $this->save ( $data )) {
        $ds->rollback ();
        return false;
      }
    }
    $ds->commit ();
    return true;
  }
  public function manipulateOurHitDate($ids, $value, $endDate = false) {
    $ids = ( array ) $ids;
    $products = $this->find ( 'all', array (
      'conditions' => array (
        'Product.id' => $ids 
      ) 
    ) );
    
    $date_change = ($value > 0 ? '+' : '') . $value . ' days';
    
    $ds = $this->getDataSource ();
    $ds->begin ();
    
    $postfix = $endDate ? '_end' : '';
    $data = array (
      'Product' => array (
        'our_hit_date' . $postfix => null 
      ) 
    );
    
    foreach ( $products as $product ) {
      $this->id = $product ['Product'] ['id'];
      $data ['Product'] ['our_hit_date' . $postfix] = date ( 'Y-m-d H:i:s', strtotime ( $date_change, $product ['Product'] ['our_hit_date' . $postfix] == '0000-00-00 00:00:00' ? time () : strtotime ( $product ['Product'] ['our_hit_date' . $postfix] ) ) );
      if (! $this->save ( $data )) {
        $ds->rollback ();
        return false;
      }
    }
    $ds->commit ();
    return true;
  }
  public function manipulateOurHitDateEnd($ids, $value) {
    return $this->manipulateOurHitDate ( $ids, $value, true );
  }
  public function beforeValidate($options = array()) {
    if (! isset ( $this->data ['Category'] ['Category'] ) || empty ( $this->data ['Category'] ['Category'] )) {
      $this->invalidate ( 'categories', __ ( 'Wybierz co najmniej jedną kategorię' ) );
    }
    if (empty ( $this->data ['Product'] ['all_shippings'] ) && (! isset ( $this->data ['Shipping'] ['Shipping'] ) || empty ( $this->data ['Shipping'] ['Shipping'] ))) {
      $this->invalidate ( 'shippings', __ ( 'Wybierz co najmniej jedną formę dostawy' ) );
    }
  }
  public function isPromotionActive($product) {
    if (is_numeric ( $product )) {
      $product = $this->find ( 'first', array (
        'fields' => array (
          'Product.promotion_status',
          'Product.promotion_date',
          'Product.promotion_date_end' 
        ),
        'conditions' => array (
          'Product.id' => $product 
        ) 
      ) );
    }
    
    if ($product) {
      if (! $product ['Product'] ['promotion_status']) {
        return false;
      }
      $time = time ();
      if (! empty ( $product ['Product'] ['promotion_date'] ) && $product ['Product'] ['promotion_date'] != '0000-00-00 00:00') {
        if (strtotime ( $product ['Product'] ['promotion_date'] > $time )) {
          return false;
        }
      }
      if (! empty ( $product ['Product'] ['promotion_date_end'] ) && $product ['Product'] ['promotion_date_end'] != '0000-00-00 00:00') {
        if (strtotime ( $product ['Product'] ['promotion_date_end'] <= $time )) {
          return false;
        }
      }
    }
    
    return true;
  }
  public function beforeFind($queryData) {
    if (isset ( $queryData ['sort'] ) && ! empty ( $queryData ['sort'] )) {
      switch ($queryData ['sort']) {
        case 'al_default' :
          $queryData ['order'] = 'Product.sort_order ASC, Product.id ASC';
          break;
        case 'al_price' :
          $queryData ['order'] = 'Product.price_tax ASC, Product.sort_order ASC, Product.id ASC';
          break;
        case 'al_price-desc' :
          $queryData ['order'] = 'Product.price_tax DESC, Product.sort_order ASC, Product.id ASC';
          break;
      }
    }
    
    return $queryData;
  }
}
