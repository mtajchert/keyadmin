<?php

App::uses('AppModel', 'Model');

/**
 * Category Model
 *
 */
class Slider extends AppModel {

  /**
   * Display field
   *
   * @var string
   */
  public $displayField = 'url';

  /**
   * Validation rules
   *
   * @var array
   */
  var $validate = array(
    'url' => array(
      'rule1' => array(
        'rule' => 'notEmpty',
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Url jest wymagany'
      )
    )
  );

  
  /**
   *
   */
  public function beforeSave($options = array()){
    if (is_array($this->data['Slider']['image'])){
      if (is_uploaded_file($this->data['Slider']['image']['tmp_name'])){
        App::import('Component','KeyAdmin.Image');
        $oImage = new ImageComponent(new ComponentCollection());
    
        $categoriesDir = CLIENT_WWW.'sliders/origin/';
        if (!is_dir($categoriesDir)){
          mkdir($categoriesDir, 0777, true);
        }
        $categoriesDirThumb = CLIENT_WWW.'sliders/thumb/';
        if (!is_dir($categoriesDirThumb)){
          mkdir($categoriesDirThumb, 0777, true);
        }
        $fileName = md5(time().$this->data['Slider']['image']['name']).'.jpg';
        
        if (move_uploaded_file($this->data['Slider']['image']['tmp_name'], $categoriesDir.$fileName)){
            \Tinify\setKey("PlInAEA7woXRGlSf4h-3KdEA7wHW8JRf"); //biuro@alcatras.pl
            $dirFile = $categoriesDir . $fileName;
            $source = \Tinify\fromFile($dirFile);
            $resized = $source->resize(array(
                "method" => "scale",
                "width" => 1200
            ));
            $resized->toFile($dirFile);
            
            $resized = $source->resize(array(
                "method" => "scale",
                "height" => 50
            ));
            $resized->toFile($categoriesDirThumb . $fileName);
            
            $this->data['Slider']['image'] = $fileName;
        }else{
          return false;
        }
      }else{
        unset($this->data['Slider']['image']);
      }
    }
    return true;
  }
}
