<?php

App::uses('AppModel', 'Model');

/**
 * Payment Model
 *
 */
class Payment extends AppModel {

  /**
   * Display field
   *
   * @var string
   */
  public $displayField = 'name';

  /**
   * Validation rules
   *
   * @var array
   */
  var $validate = array(
    'name' => array(
      'notEmpty' => array(
        'rule' => 'notEmpty',
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Nazwa jest wymagana'
      )
    ),
    'margin' => array (
      'decimal' => array (
        'rule' => '/^([0-9]+([.,][0-9]{1,2}){0,1})$/i',
        'required' => true,
        'allowEmpty' => false,
        'message' => 'Wpisz wartość do 2 miejsc po przecinku' 
      )
    ),
    'sort_order' => array(
      'rule1' => array(
        'rule' => array('range', -1),
        'allowEmpty' => true,
        'message' => 'Kolejność sortowania musi być liczbą większą lub równą 0'
      ),
    ),
    'status' => array(
      'rule1' => array(
        'rule' => array('range', -1, 2),
        'allowEmpty' => false,
        'message' => 'Niepoprawny status'
      ),
    )
  );
  
  public function beforeSave($options = array()) {
    if (is_array($this->data['Payment']['image']) && $this->data['Payment']['image']['error'] != UPLOAD_ERR_NO_FILE) {
      if (empty($this->data['Payment']['image']['size'])) {
        $this->data['Payment']['image'] = null;
        return true;
      }
      if (!is_uploaded_file($this->data['Payment']['image']['tmp_name']) || $this->data['Payment']['image']['error'] != UPLOAD_ERR_OK) {
        return false;
      }

      App::import('Component', 'KeyAdmin.Image');
      $oImage = new ImageComponent(new ComponentCollection());

      $paymentsDir = CLIENT_WWW.'payments/origin/';
      if (!is_dir($paymentsDir)) {
        mkdir($paymentsDir, 0777, true);
      }
      $paymentsDirList = CLIENT_WWW.'payments/list/';
      if (!is_dir($paymentsDirList)) {
        mkdir($paymentsDirList, 0777, true);
      }
      $paymentsDirThumb = CLIENT_WWW.'payments/thumb/';
      if (!is_dir($paymentsDirThumb)) {
        mkdir($paymentsDirThumb, 0777, true);
      }
      $fileName = md5(time().$this->data['Payment']['image']['name']).'.jpg';
      if (move_uploaded_file($this->data['Payment']['image']['tmp_name'], $paymentsDir.$fileName)) {
        $oImage->resizeImage($paymentsDir.$fileName, $paymentsDirList.$fileName, 50, 34);
        $oImage->resizeImage($paymentsDir.$fileName, $paymentsDirThumb.$fileName, 0, 100, 100);
        $this->data['Payment']['image'] = $fileName;
      } else {
        return false;
      }
    } else {
      unset($this->data['Payment']['image']);
    }

    return true;
  }

}
