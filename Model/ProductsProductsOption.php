<?php

App::uses('AppModel', 'Model');

/**
 * ProductsProductsOption option Model
 *
 */
class ProductsProductsOption extends AppModel {

  /**
   * Display field
   *
   * @var string
   */
  public $displayField = 'catalog_no';
  
  public $belongsTo = array(
    'ProductOption' => array(
      'className' => 'KeyAdmin.ProductOption',
      'foreignKey' => 'product_option_id'
    ),
    'ProductOptionValue' => array(
      'className' => 'KeyAdmin.ProductOptionValue',
      'foreignKey' => 'product_option_value_id'
    ),
    'ProductAvailability' => array (
      'className' => 'KeyAdmin.ProductAvailability',
      'foreignKey' => 'product_availability_id'
    ),
  );

  /**
   * Validation rules
   *
   * @var array
   */
  var $validate = array(
    'product_option_id' => array(
      'gt0' => array(
        'rule' => array('naturalNumber', false),
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Wybierz cechę produktu'
      )
    ),
    'product_option_value_id' => array(
      'gt0' => array(
        'rule' => array('naturalNumber', false),
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Wybierz wartość cechy'
      )
    ),
    'change_price' => array(
      'decimal' => array(
        'rule' => '/^([0-9]+([.,][0-9]{1,2}){0,1})$/i',
        'required' => true,
        'allowEmpty' => true,
        'message' => 'Wpisz wartość do 2 miejsc po przecinku'
      )
    ),
    'change_price_tax' => array(
      'decimal' => array(
        'rule' => '/^([0-9]+([.,][0-9]{1,2}){0,1})$/i',
        'required' => true,
        'allowEmpty' => true,
        'message' => 'Wpisz wartość do 2 miejsc po przecinku'
      )
    )
  );

}
