<?php

App::uses('AppModel', 'Model');
App::uses('Product', 'Model');

/**
 * Category Model
 *
 */
class PromotionManage extends AppModel {

  public $useTable = false;
  protected $_schema = array(
    'action' => array(
      'type' => 'string'
    ),
    'price_change_kind' => array(
      'type' => 'string'
    ),
    'price_change_value' => array(
      'type' => 'integer'
    ),
    'price_change_type' => array(
      'type' => 'string'
    ),
    'restore_price' => array(
      'type' => 'boolean'
    ),
    'round_price' => array(
      'type' => 'boolean'
    ),
    'no_change_promotion_date' => array(
      'type' => 'boolean'
    ),
    'promotion_date' => array(
      'type' => 'datetime'
    ),
    'no_change_promotion_date_end' => array(
      'type' => 'boolean'
    ),
    'promotion_date_end' => array(
      'type' => 'datetime'
    ),
    'without_promotion' => array(
      'type' => 'boolean'
    ),
    'price_tax_from' => array(
      'type' => 'float'
    ),
    'price_tax_to' => array(
      'type' => 'float'
    ),
    'name_contain' => array(
      'type' => 'string'
    ),
    'all_categories' => array(
      'type' => 'boolean'
    ),
    'categories' => array(
      'type' => 'integer'
    ),
    'categories_nid' => array(
      'type' => 'string'
    ),
    'all_manufacturers' => array(
      'type' => 'boolean'
    ),
    'manufacturers' => array(
      'type' => 'integer'
    ),
  );

  /**
   * Validation rules
   *
   * @var array
   */
  var $validate = array(
    'action' => array(
      'inList' => array(
        'rule' => array('inList', array('create', 'delete')),
        'required' => true,
        'allowEmpty' => false,
        'message' => 'Wybierz akcję'
      )
    ),
    'price_change_value' => array(
      'decimal2' => array(
        'rule' => '/^([-+]){0,1}([0-9]+([.,][0-9]{1,2}){0,1})$/i',
        'required' => false,
        'allowEmpty' => true,
        'message' => 'Wpisz wartość o jaką zmienić cenę (do 2 miejsc po przecinku)'
      )
    ),
    'action' => array(
      'inList' => array(
        'rule' => array('inList', array('create', 'delete')),
        'required' => true,
        'allowEmpty' => false,
        'message' => 'Wybierz akcję'
      )
    ),
    'restore_price' => array(
      'boolean' => array(
        'rule' => 'boolean',
        'message' => 'Nieprawidłowa wartość'
      )
    ),
    'round_price' => array(
      'boolean' => array(
        'rule' => 'boolean',
        'message' => 'Nieprawidłowa wartość'
      )
    ),
    'promotion_date' => array(
      'rule1' => array(
        'rule' => array('datetime', 'ymd'),
        'required' => false,
        'allowEmpty' => true,
        'message' => 'Nieprawidłowy format (RRRR-MM-DD GG:MM)'
      )
    ),
    'promotion_date_end' => array(
      'rule1' => array(
        'rule' => array('datetime', 'ymd'),
        'required' => false,
        'allowEmpty' => true,
        'message' => 'Nieprawidłowy format (RRRR-MM-DD GG:MM)'
      )
    ),
    'product_status' => array(
      'inList' => array(
        'rule' => array('inList', array('only_active', 'only_unactive')),
        'required' => false,
        'allowEmpty' => true,
        'message' => 'Nieprawidłowa wartość'
      )
    ),
    'without_promotion' => array(
      'boolean' => array(
        'rule' => 'boolean',
        'message' => 'Nieprawidłowa wartość'
      )
    ),
    'price_tax_from' => array(
      'decimal' => array(
        'rule' => '/^([-+]){0,1}([0-9]+([.,][0-9]{1,2}){0,1})$/i',
        'required' => false,
        'allowEmpty' => true,
        'message' => 'Wpisz wartość do 2 miejsc po przecinku'
      )
    ),
    'price_tax_to' => array(
      'decimal' => array(
        'rule' => '/^([-+]){0,1}([0-9]+([.,][0-9]{1,2}){0,1})$/i',
        'required' => false,
        'allowEmpty' => true,
        'message' => 'Wpisz wartość do 2 miejsc po przecinku'
      )
    ),
    'all_categories' => array(
      'boolean' => array(
        'rule' => 'boolean',
        'message' => 'Nieprawidłowa wartość'
      )
    ),
    'all_manufacturers' => array(
      'boolean' => array(
        'rule' => 'boolean',
        'message' => 'Nieprawidłowa wartość'
      )
    ),
  );

  public function beforeValidate($options = array()) {
    if ($this->data['PromotionManage']['action'] == 'create') {
      $this->validator()->add('price_change_kind', array(
        'inList' => array(
          'rule' => array('inList', array('no_change', 'price_tax', 'old_price')),
          'required' => true,
          'allowEmpty' => false,
          'message' => 'Wybierz sposób zmiany cen'
        )
      ));
    }
    if (!empty($this->data['PromotionManage']['price_change_value'])) {
      $this->validator()->add('price_change_type', array(
        'inList' => array(
          'rule' => array('inList', array('percent', 'value')),
          'required' => true,
          'allowEmpty' => false,
          'message' => 'Wybierz jednostkę podanej wartości'
        )
      ));
      if (isset($this->data['PromotionManage']['price_change_type']) && $this->data['PromotionManage']['price_change_type'] == 'percent') {
        $this->validator()->remove('price_change_value', 'decimal2');
        $this->validator()->add('price_change_value', array(
          'decimal5' => array(
            'rule' => '/^([-+]){0,1}([0-9]+([.,][0-9]{1,5}){0,1})$/i',
            'required' => false,
            'allowEmpty' => true,
            'message' => 'Wpisz wartość o jaką zmienić cenę (do 5 miejsc po przecinku)'
          )
        ));
      }
    }
    if (empty($this->data['PromotionManage']['all_categories'])) {
      $this->validator()->add('categories', array(
        'notEmpty' => array(
          'rule' => array('multiple', array('min' => 1)),
          'required' => true,
          'allowEmpty' => false,
          'message' => 'Wybierz kategorie'
        )
      ));
    }
    
    if (empty($this->data['PromotionManage']['all_manufacturers'])) {
      $this->validator()->add('manufacturers', array(
        'notEmpty' => array(
          'rule' => array('multiple', array('min' => 1)),
          'required' => true,
          'allowEmpty' => false,
          'message' => 'Wybierz producenta/ów'
        )
      ));
    }
  }
  
  public function save($data = null, $validate = true, $fieldList = array()) {
    $this->set($data);
    if ($validate) {
      if (!$this->validates()) {
        return false;
      }
    }
    
    $counter = 0;
    $conditions = array();
    
    $this->Product = ClassRegistry::init('Product');
    
    if (isset($this->data['PromotionManage']['product_status'])) {
      if ($this->data['PromotionManage']['product_status'] == 'only_active') {
        $conditions['Product.status'] = 1;
      } elseif ($this->data['PromotionManage']['product_status'] == 'only_unactive') {
        $conditions['Product.status'] = 0;
      }
    }
    
    if (isset($this->data['PromotionManage']['without_promotion']) && !empty($this->data['PromotionManage']['without_promotion'])) {
      $conditions[] = array(
        'Product.promotion_status' => 0,
        'OR' => array(
          array('Product.old_price' => 0),
          array('Product.old_price' => null)
        )
      );
    }
    
    if (isset($this->data['PromotionManage']['price_tax_from']) && !empty($this->data['PromotionManage']['price_tax_from'])) {
      $conditions['price_tax >='] = $this->data['PromotionManage']['price_tax_from'];
    }
    if (isset($this->data['PromotionManage']['price_tax_to']) && !empty($this->data['PromotionManage']['price_tax_to'])) {
      $conditions['price_tax <='] = $this->data['PromotionManage']['price_tax_to'];
    }
    
    if (isset($this->data['PromotionManage']['name_contain']) && !empty($this->data['PromotionManage']['name_contain'])) {
      $conditions['name LIKE'] = '%'.$this->data['PromotionManage']['name_contain'].'%';
    }
    
    if (!isset($this->data['PromotionManage']['all_categories']) || empty($this->data['PromotionManage']['all_categories'])) {
      $db = $this->Product->getDataSource();
      $subQuery = $db->buildStatement(array(
        'fields' => array('`PC`.`product_id`'),
        'table' => $db->fullTableName('products_categories'),
        'alias' => 'PC',
        'limit' => null,
        'offset' => null,
        'joins' => array(),
        'conditions' => array(
          '`PC`.`category_id` IN ' => $this->data['PromotionManage']['categories']
        ),
        'order' => null,
        'group' => null
          ), $this->Product
      );
      $subQuery = ' `Product`.`id` IN ('.$subQuery.') ';
      $subQueryExpression = $db->expression($subQuery);
      
      $conditions[] = $subQueryExpression;
    }
    
    if (!isset($this->data['PromotionManage']['all_manufacturers']) || empty($this->data['PromotionManage']['all_manufacturers'])) {
      $conditions['manufacturer_id'] = $this->data['PromotionManage']['manufacturers'];
    }
    
    
    $products = $this->Product->find('all', array(
      'conditions' => $conditions,
      'fields' => array('Product.id', 'Product.old_price', 'Product.price_tax')
    ));
    
    $ds = $this->Product->getDataSource();
    $ds->begin();
    
    $percent = isset($this->data['PromotionManage']['price_change_type']) && $this->data['PromotionManage']['price_change_type'] == 'percent' ? (100 + (float) $this->data['PromotionManage']['price_change_value']) / 100 : 1;
    $value = isset($this->data['PromotionManage']['price_change_type']) && $this->data['PromotionManage']['price_change_type'] == 'value' ? (float) $this->data['PromotionManage']['price_change_value'] : 0;
    
    if ($this->data['PromotionManage']['action'] == 'create') {
      $promotion_date = empty($this->data['PromotionManage']['promotion_date']) ? '0000-00-00 00:00:00' : $this->data['PromotionManage']['promotion_date'];
      $promotion_date_end = empty($this->data['PromotionManage']['promotion_date_end']) ? '0000-00-00 00:00:00' : $this->data['PromotionManage']['promotion_date_end'];
      
      $data = array('Product' => array('promotion_status' => 1));
      if (empty($this->data['PromotionManage']['no_change_promotion_date'])) {
        $data['Product']['promotion_date'] = $promotion_date;
      }
      if (empty($this->data['PromotionManage']['no_change_promotion_date_end'])) {
        $data['Product']['promotion_date_end'] = $promotion_date_end;
      }

      foreach ($products as $product) {
        if ($this->data['PromotionManage']['price_change_kind'] == 'price_tax') {
          $data['Product']['old_price'] = $product['Product']['price_tax'];
          $data['Product']['price_tax'] = (float) $product['Product']['price_tax'] * $percent + $value;
          if ($this->data['PromotionManage']['round_price']) {
            $data['Product']['price_tax'] = round($data['Product']['price_tax']);
          }
        } elseif ($this->data['PromotionManage']['price_change_kind'] == 'old_price') {
          $data['Product']['old_price'] = (float) $product['Product']['price_tax'] * $percent + $value;
          if ($this->data['PromotionManage']['round_price']) {
            $data['Product']['old_price'] = round($data['Product']['old_price']);
          }
        }
        
        $this->Product->id = $product['Product']['id'];
        if ($this->Product->save($data)) {
          $counter++;
        } else {
          $ds->rollback($data);
          return false;
        }
      }
    } elseif ($this->data['PromotionManage']['action'] == 'delete') {
      $data = array('Product' => array(
        'promotion_status' => 0,
        'old_price' => 0,
        'promotion_date' => '0000-00-00 00:00:00',
        'promotion_date_end' => '0000-00-00 00:00:00'
      ));
      
      foreach ($products as $product) {
        if ($this->data['PromotionManage']['restore_price']) {
          $data['Product']['price_tax'] = $product['Product']['old_price'];
        }
        
        $this->Product->id = $product['Product']['id'];
        if ($this->Product->save($data)) {
          $counter++;
        } else {
          $ds->rollback($data);
          return false;
        }
      }
    }
    
    $ds->commit();
    return $counter;
  }
}
