<?php
App::uses('AppModel', 'Model');

/**
 * UserData Model
 */
class UserAddress extends AppModel {
    
    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'User' => array(
            'className' => 'KeyAdmin.User'
        ),
        'Country' => array(
            'className' => 'KeyAdmin.Country',
            'foreignKey' => 'country_id'
        ),
        'Zone' => array(
            'className' => 'KeyAdmin.Zone',
            'foreignKey' => 'zone_id'
        )
    );
    
    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'is_company' => array(
            '0_or_1' => array(
                'rule' => array(
                    'range',
                    - 1,
                    2
                ),
                'allowEmpty' => false,
                'message' => 'Niepoprawna wartość'
            )
        ),
        'alias' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'allowEmpty' => false,
                'required' => true,
                'message' => 'Nazwa adresu jest wymagana'
            ),
            'length' => array(
                'rule' => array(
                    'lengthBetween',
                    1,
                    64
                ),
                'message' => 'Nazwa adresu może mieć długość 1-64 znaków'
            )
        ),
        'first_name' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'allowEmpty' => false,
                'required' => true,
                'message' => 'Imię jest wymagane'
            ),
            'length' => array(
                'rule' => array(
                    'lengthBetween',
                    1,
                    64
                ),
                'message' => 'Imię może mieć długość 1-64 znaków'
            )
        ),
        'last_name' => array(
            'length' => array(
                'rule' => array(
                    'lengthBetween',
                    0,
                    64
                ),
                'allowEmpty' => true,
                'required' => false,
                'message' => 'Nazwisko może mieć długość 0-64 znaków'
            )
        ),
        'street_address' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'allowEmpty' => false,
                'required' => true,
                'message' => 'Ulica i nr domu są wymagane'
            ),
            'length' => array(
                'rule' => array(
                    'lengthBetween',
                    1,
                    64
                ),
                'message' => 'Ulica i nr domu mogą mieć długość 1-64 znaków'
            )
        ),
        'post_code' => array(
            'length' => array(
                'rule' => 'validatePostCode',
                'allowEmpty' => false,
                'required' => true,
                'message' => 'Kod pocztowy jest nieprawidłowy'
            )
        ),
        'city' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'allowEmpty' => false,
                'required' => true,
                'message' => 'Miejscowość jest wymagana'
            ),
            'length' => array(
                'rule' => array(
                    'lengthBetween',
                    1,
                    64
                ),
                'message' => 'Miejscowość może mieć długość 1-64 znaków'
            )
        ),
        'country_id' => array(
            'decimal' => array(
                'rule' => array(
                    'range',
                    - 1
                ),
                'required' => true,
                'allowEmpty' => false,
                'message' => 'Wybierz kraj'
            )
        ),
        'phone' => array(
            'phone' => array(
                'rule' => '/^([0-9 ,+\-\(\)])*$/i',
                'required' => true,
                'allowEmpty' => true,
                'message' => 'Wpisz poprawny nr telefonu (dopuszczalne znaki: +, -, (, ), 0-9), spacja i przecinek'
            ),
            'length' => array(
                'rule' => array(
                    'lengthBetween',
                    0,
                    32
                ),
                'allowEmpty' => true,
                'required' => false,
                'message' => 'Nr telefonu może mieć długość 0-32 znaków'
            )
        )
    );



    public function validateNip($value) {
        $poland = $this->Country->find('first', array(
            'conditions' => array(
                'Country.iso_code_2' => 'PL'
            )
        ));
        
        if ($poland && $this->data['UserAddress']['country_id'] == $poland['Country']['id']) {
            $value = trim($value['nip']);
            if (! preg_match('/^([0-9])+$/', $value)) {
                return false;
            }
            
            if (strlen($value) != 10) {
                return false;
            }
            
            $arrSteps = array(
                6,
                5,
                7,
                2,
                3,
                4,
                5,
                6,
                7
            );
            $intSum = 0;
            for($i = 0; $i < 9; $i ++) {
                $intSum += $arrSteps[$i] * $value[$i];
            }
            $int = $intSum % 11;
            
            $intControlNr = ($int == 10) ? 0 : $int;
            if ($intControlNr == $value[9]) {
                return true;
            }
            return false;
        }
        
        return true;
    }



    public function validatePostCode($value) {
        $poland = $this->Country->find('first', array(
            'conditions' => array(
                'Country.iso_code_2' => 'PL'
            )
        ));
        
        if ($poland && $this->data['UserAddress']['country_id'] == $poland['Country']['id']) {
            $value = trim($value['post_code']);
            if (preg_match('/^([0-9]{2}-[0-9]{3}){1}$/', $value)) {
                return true;
            }
            return false;
        }
        
        return true;
    }



    public function beforeValidate($options = array()) {
        // print_r($_POST);
        // print_r($this->data['UserAddress']);die;
        // need to add and remove for every run because added/removed validations is used for next associations
        if (isset($this->data['UserAddress']['is_company']) && $this->data['UserAddress']['is_company']) {
            $this->validator()->add('nip', array(
                'nip' => array(
                    'rule' => 'validateNip',
                    'allowEmpty' => true,
                    'required' => false,
                    'message' => 'Podany NIP jest nieprawidłowy'
                )
            ));
            $this->validator()->add('company', array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'allowEmpty' => false,
                    'required' => true,
                    'message' => 'Nazwa firmy jest wymagana'
                ),
                'length' => array(
                    'rule' => array(
                        'lengthBetween',
                        1,
                        64
                    ),
                    'allowEmpty' => false,
                    'required' => true,
                    'message' => 'Nazwa firmy może mieć długość 1-64 znaków'
                )
            ));
            
            $this->validator()->remove('first_name');
            $this->validator()->remove('last_name');
        } else {
            $this->data['UserAddress']['nip'] = '';
            $this->validator()->remove('nip');
            $this->validator()->remove('company');
        }
        
        if (isset($this->data['UserAddress']['is_billing']) && ! empty($this->data['UserAddress']['is_billing'])) {
            $this->validator()->remove('phone');
        }
    }



    public function getAddressString($lineSeparator = "\r\n") {
        $addressString = trim($this->data['UserAddress']['street_address']);
        if (! empty($this->data['UserAddress']['post_code']) || ! empty($this->data['UserAddress']['city'])) {
            $addressString .= $lineSeparator . trim($this->data['UserAddress']['post_code'] . ' ' . $this->data['UserAddress']['city']);
        }
        
        $defaultCountry = $this->Country->find('first', array(
            'conditions' => array(
                'default' => 1
            )
        ));
        if ($this->data['UserAddress']['country_id'] != $defaultCountry['Country']['id']) {
            $country = $this->Country->findById($this->data['UserAddress']['country_id']);
            $addressString .= $lineSeparator . $country['Country']['name'];
        }
        
        return $addressString;
    }



    public function getFormattedAddress($userAddress, $lineSeparator = "\r\n") {
        $address = '';
        
        
        if ($userAddress['is_company']) {
            $address .= $userAddress['company'] . $lineSeparator;
            if (! empty($userAddress['nip'])) {
                $address .= 'NIP: ' . $userAddress['nip'];
            }
        }
        if (! empty($userAddress['first_name'])) {
            $address .= trim($userAddress['first_name'] . ' ' . $userAddress['last_name']);
        }
        
        if (! empty($userAddress['street_address'])) {
            $address .= $lineSeparator . trim($userAddress['street_address']);
        }
        if (! empty($userAddress['post_code'])) {
            $address .= $lineSeparator . trim($userAddress['post_code'] . ' ' . $userAddress['city']);
        }
        
        if (! isset($userAddress['Country']) && $userAddress['country_id'] > 0) {
            $country = $this->Country->find('first', array(
                'conditions' => array(
                    'id' => $userAddress['country_id']
                )
            ));
            $userAddress['Country'] = $country['Country'];
        }
        if (isset($userAddress['Country']['name']) && ! empty($userAddress['Country']['name'])) {
            if (! $userAddress['Country']['default'] || ! empty($userAddress['Zone']['name'])) {
                $address .= $lineSeparator . trim($userAddress['Country']['name']);
            }
        }
        
        if (! isset($userAddress['Zone']) && $userAddress['zone_id'] > 0) {
            $zone = $this->Zone->find('first', array(
                'conditions' => array(
                    'id' => $userAddress['zone_id']
                )
            ));
            $userAddress['Zone'] = $zone['Zone'];
        }
        
        if (isset($userAddress['Zone']['name']) && ! empty($userAddress['Zone']['name'])) {
            $address .= $lineSeparator . $userAddress['Zone']['name'];
        }
        
        $address .= $lineSeparator . 'tel. ' . trim($userAddress['phone']);
        
        if (! empty($userAddress['email'])) {
            $address .= $lineSeparator . trim($userAddress['email']);
        }
        return $address;
    }
}
