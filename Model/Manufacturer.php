<?php

App::uses('AppModel', 'Model');

/**
 * Category Model
 *
 */
class Manufacturer extends AppModel {

  /**
   * Display field
   *
   * @var string
   */
  public $displayField = 'name';

  /**
   * Validation rules
   *
   * @var array
   */
  var $validate = array(
    'name' => array(
      'rule1' => array(
        'rule' => 'notEmpty',
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Nazwa producenta jest wymagana'
      )
    ),
    'url' => array(
      'rule1' => array(
        'rule' => 'url',
        'allowEmpty' => true,
        'message' => 'Podany adres www jest niepoprawny'
      )
    )
  );

  
  /**
   *
   */
  public function beforeSave($options = array()){
    if (is_array($this->data['Manufacturer']['image']) && !empty($this->data['Manufacturer']['image']['size'])){
      if (empty($this->data['Manufacturer']['image']['size'])){
        $this->data['Manufacturer']['image'] = null;
        return true;
      }
      
      if (!is_uploaded_file($this->data['Manufacturer']['image']['tmp_name'])){
        return false;
      }
  
      App::import('Component','KeyAdmin.Image');
      $oImage = new ImageComponent();
  
      $categoriesDir = CLIENT_WWW.'manufacturers/origin/';
      if (!is_dir($categoriesDir)){
        mkdir($categoriesDir, 0777, true);
      }
      $categoriesDirThumb = CLIENT_WWW.'manufacturers/thumb/';
      if (!is_dir($categoriesDirThumb)){
        mkdir($categoriesDirThumb, 0777, true);
      }
      $fileName = md5(time().$this->data['Manufacturer']['image']['name']).'.jpg';
      if (move_uploaded_file($this->data['Manufacturer']['image']['tmp_name'], $categoriesDir.$fileName)){
        $oImage->resizeImage($categoriesDir.$fileName, $categoriesDirThumb.$fileName, 50, 50);
        $this->data['Manufacturer']['image'] = $fileName;
      }else{
        return false;
      }
  
    }else{
      unset($this->data['Manufacturer']['image']);
    }
    return true;
  }
}
