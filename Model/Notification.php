<?php

App::uses('AppModel', 'Model');

/**
 * Notification Model
 *
 */
class Notification extends AppModel {

  /**
   * Display field
   *
   * @var string
   */
  public $displayField = 'name';

  /**
   * Validation rules
   *
   * @var array
   */
  var $validate = array(
    'subject' => array(
      'notEmpty' => array(
        'rule' => 'notEmpty',
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Temat jest wymagany'
      )
    ),
    'message' => array(
      'notEmpty' => array(
        'rule' => 'notEmpty',
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Wiadomość jest wymagana'
      )
    )
  );

}
