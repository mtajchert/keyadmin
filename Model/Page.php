<?php
App::uses('AppModel', 'Model');

/**
 * Page Model
 */
class Page extends AppModel {
  
  /**
   * Display field
   *
   * @var string
   */
  public $displayField = 'title';
  
  /**
   * Validation rules
   *
   * @var array
   */
  var $validate = array();
  
  
  var $belongsTo = array(
    'BlogCategory' => array(
      'className' => 'KeyAdmin.BlogCategory',
      'foreignKey' => 'category_id'
    )
  );
  
  var $hasOne = array(
      'Customization' => array(
          'className' => 'KeyAdmin.Customization',
          'foreignKey' => 'model_id',
          'conditions' => [
              'Customization.model' => 'pages',
              'Customization.is_active' => 1,
          ]
      )
  );



  /**
   */
  public function beforeSave($options = array()) {
    if (isset($this->data['Page']['image']) && ! empty($this->data['Page']['image']['size'])) {
      App::import('Component', 'KeyAdmin.Image');
      $oImage = new ImageComponent(new ComponentCollection());
      
      $blogDir = CLIENT_WWW . 'blog/origin/';
      if (! is_dir($blogDir)) {
        mkdir($blogDir, 0777, true);
      }
      
      $blogDirThumb = CLIENT_WWW . 'blog/thumb/';
      if (! is_dir($blogDirThumb)) {
        mkdir($blogDirThumb, 0777, true);
      }
      $fileName = md5(time() . $this->data['Page']['image']['name']) . '.jpg';
      if (move_uploaded_file($this->data['Page']['image']['tmp_name'], $blogDir . $fileName)) {
        $oImage->resizeImage($blogDir . $fileName, $blogDirThumb . $fileName, 200, 150);
      }
      
      $this->data['Page']['thumb'] = $fileName;
    } else {
      unset($this->data['Page']['image']);
    }
    
    $this->data['Page']['seo_url'] = mb_strtolower(Inflector::slug($this->data['Page']['title'], '-'));
    
    if (! empty($this->data['Page']['tags'])) {
      $tags = explode(';', $this->data['Page']['tags']);
      $formated = array();
      foreach ( $tags as $tag ) {
        $key = mb_strtolower(Inflector::slug($tag, '-'));
        $formated[$key] = $tag;
      }
      $this->data['Page']['tags'] = serialize($formated);
    }
    
    return $this->data;
  }
}
