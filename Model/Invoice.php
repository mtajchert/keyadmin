<?php
App::uses('AppModel', 'Model');

/**
 * Invoice Model
 */
class Invoice extends AppModel {
  
  /**
   */
  public $belongsTo = array(
    'KeyAdmin.Order',
    'KeyAdmin.Payment'
  );
  
  /**
   */
  public function beforeSave($options = array()) {
    if (!isset($this->data['Invoice']['id']) || empty($this->data['Invoice']['id'])){
      $month = date('m');
      $year = date('Y');
      $number = $this->__getNextNumber($month, $year, $this->data['Invoice']['type']);
      
      $this->data['Invoice']['number'] = $number;
      $this->data['Invoice']['month'] = $month;
      $this->data['Invoice']['year'] = $year;
      $this->data['Invoice']['number_all'] = "{$this->data['Invoice']['number']}/{$this->data['Invoice']['month']}/{$this->data['Invoice']['year']}";
      if ($this->data['Invoice']['type'] == 'QUOTE') {
        $this->data['Invoice']['number_all'] = 'FP/'.$this->data['Invoice']['number_all'];
      }
    }
    return true;
  }
  
  /**
   */
  private function __getNextNumber($month, $year, $type) {
    $this->contain(array());
    $lastInv = $this->find('first', array(
      'conditions' => array(
        'Invoice.type' => $type,
        'Invoice.month' => $month,
        'Invoice.year' => $year
      ),
      'order' => array(
        'Invoice.number' => 'DESC'
      ),
      'fields' => array(
        'Invoice.id',
        'Invoice.number'
      )
    ));
    
    if (!empty($lastInv)){
      return ++$lastInv['Invoice']['number'];
    }else{
      return 1;
    }
  }
  
  public function getPdfPath($invoice) {
    $fpath = ROOT . DS . APP_DIR . DS . 'tmp' . DS . ($invoice['Invoice']['type'] == 'QUOTE' ? 'QUOTE' : 'INVOICE') . DS;
    $fpath .= substr($invoice['Invoice']['created'], 0, 4) . DS;
    $fpath .= substr($invoice['Invoice']['created'], 5, 2) . DS;
    if (!file_exists($fpath)) {
      mkdir($fpath, 0777, true);
    }
    $fpath .= md5($invoice['Invoice']['number_all']).'.pdf';
    return $fpath;
  }
  
  public function beforeDelete($cascade = true) {
    $invoice = $this->findById($this->id);
    
    $path = $this->getPdfPath($invoice);
    if (file_exists($path)) {
      unlink($path);
    }
    
    return true;
  }

}
