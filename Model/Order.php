<?php
App::uses('AppModel', 'Model');

/**
 * Order Model
 */
class Order extends AppModel {
    
    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'number';
    
    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Customer' => array(
            'className' => 'KeyAdmin.Customer',
            'foreignKey' => 'user_id'
        ),
        'BillingOrderUserAddress' => array(
            'className' => 'KeyAdmin.OrderUserAddress',
            'foreignKey' => 'billing_order_address_id'
        ),
        'ShippingOrderUserAddress' => array(
            'className' => 'KeyAdmin.OrderUserAddress',
            'foreignKey' => 'shipping_order_address_id'
        ),
        'InvoiceOrderUserAddress' => array(
            'className' => 'KeyAdmin.OrderUserAddress',
            'foreignKey' => 'invoice_order_address_id'
        ),
        'Shipping' => array(
            'className' => 'KeyAdmin.Shipping',
            'foreignKey' => 'shipping_id'
        ),
        'Payment' => array(
            'className' => 'KeyAdmin.Payment',
            'foreignKey' => 'payment_id'
        ),
        'OrderStatus' => array(
            'className' => 'KeyAdmin.OrderStatus',
            'foreignKey' => 'order_status_id'
        ),
        'Discount' => array(
            'className' => 'KeyAdmin.Discount',
            'foreignKey' => 'discount_id'
        )
    );
    
    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasOne = array(
        'Invoice' => array(
            'className' => 'KeyAdmin.Invoice',
            'foreignKey' => 'order_id',
            'conditions' => array(
                'Invoice.type' => 'INVOICE'
            )
        ),
        'PreInvoice' => array(
            'className' => 'KeyAdmin.Invoice',
            'foreignKey' => 'order_id',
            'conditions' => array(
                'PreInvoice.type' => 'QUOTE'
            )
        )
    );
    
    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'OrderProduct' => array(
            'className' => 'KeyAdmin.OrderProduct',
            'foreignKey' => 'order_id'
        ),
        'OrderStatusHistory' => array(
            'className' => 'KeyAdmin.OrderStatusHistory',
            'foreignKey' => 'order_id',
            'order' => 'created desc'
        )
    );
    
    /**
     * Validation rules
     *
     * @var array
     */
    var $validate = array(
        'user_id' => array(
            'gt0' => array(
                'rule' => array(
                    'naturalNumber',
                    false
                ),
                'allowEmpty' => false,
                'required' => true,
                'message' => 'Wybierz klienta'
            )
        ),
        'order_status_id' => array(
            'gt0' => array(
                'rule' => array(
                    'naturalNumber',
                    false
                ),
                'allowEmpty' => false,
                'required' => true,
                'message' => 'Wybierz status zamówienia'
            )
        ),
        'shipping_id' => array(
            'gt0' => array(
                'rule' => array(
                    'naturalNumber',
                    false
                ),
                'allowEmpty' => false,
                'required' => true,
                'message' => 'Wybierz formę dostawy'
            )
        ),
        'payment_id' => array(
            'gt0' => array(
                'rule' => array(
                    'naturalNumber',
                    false
                ),
                'allowEmpty' => true,
                'message' => 'Wybierz formę płatności'
            )
        )
    );



    public function beforeValidate($options = array()) {
        if (! isset($this->data['OrderProduct']) || empty($this->data['OrderProduct'])) {
            $this->invalidate('products', __('Dodaj co najmniej jedną pozycję'));
        }
        
        if (empty($this->data['Order']['billing_order_address_id'])) {
            $this->validator()->remove('billing_order_address_id');
            $this->validator()->add('billing_order_address_id', array(
                'gt0' => array(
                    'rule' => array(
                        'naturalNumber',
                        false
                    ),
                    'allowEmpty' => false,
                    'required' => true,
                    'message' => 'Wybierz adres do rozliczenia'
                )
            ));
        }
        if (empty($this->data['Order']['shipping_order_address_id'])) {
            $this->validator()->remove('shipping_order_address_id');
            $this->validator()->add('shipping_order_address_id', array(
                'gt0' => array(
                    'rule' => array(
                        'naturalNumber',
                        false
                    ),
                    'allowEmpty' => false,
                    'required' => true,
                    'message' => 'Wybierz adres do dostawy'
                )
            ));
        }
    }



    public function getAddressesListAndData($userId, $orderId = null, $updateOrderAddress = array()) {
        $addresses = array();
        $addressesNew = array();
        $defaultBillingUserAddressId = 0;
        $defaultShippingUserAddressId = 0;
        
        if ($userId > 0) {
            if (! $this->UserAddress) {
                $this->UserAddress = ClassRegistry::init('KeyAdmin.UserAddress');
            }
            
            $userAddresses = $this->UserAddress->find('all', array(
                'fields' => array(
                    'UserAddress.id',
                    'UserAddress.alias',
                    'UserAddress.default_shipping'
                ),
                'conditions' => array(
                    'user_id' => $userId
                )
            ));
            
            foreach ( $userAddresses as $userAddress ) {
                $addresses['0-' . $userAddress['UserAddress']['id']] = $userAddress['UserAddress']['alias'];
                /*
                 * if ($userAddress['UserAddress']['default_billing']) {
                 * $defaultBillingUserAddressId = $userAddress['UserAddress']['id'];
                 * }
                 */
                if ($userAddress['UserAddress']['default_shipping']) {
                    $defaultShippingUserAddressId = $userAddress['UserAddress']['id'];
                }
            }
        }
        
        $addressesNew = $addresses;
        if ($orderId > 0) {
            if (! $this->OrderUserAddress) {
                $this->OrderUserAddress = ClassRegistry::init('KeyAdmin.OrderUserAddress');
            }
            
            $order = $this->find('first', array(
                'fields' => array(
                    'billing_order_address_id',
                    'shipping_order_address_id'
                ),
                'conditions' => array(
                    'Order.id' => $orderId
                )
            ));
            
            $orderAddresses = $this->OrderUserAddress->find('all', array(
                'fields' => array(
                    'OrderUserAddress.id',
                    'OrderUserAddress.user_address_id',
                    'OrderUserAddress.alias'
                ),
                'conditions' => array(
                    'id' => array(
                        $order['Order']['billing_order_address_id'],
                        $order['Order']['shipping_order_address_id']
                    )
                )
            ));
            
            $orderAddressesAssign = array();
            foreach ( $orderAddresses as $orderAddress ) {
                $orderAddressesAssign['0-' . $orderAddress['OrderUserAddress']['user_address_id']] = $orderAddress;
                if ($defaultBillingUserAddressId == $orderAddress['OrderUserAddress']['user_address_id']) {
                    $defaultBillingUserAddressId = $orderAddress['OrderUserAddress']['id'];
                }
                if ($defaultShippingUserAddressId == $orderAddress['OrderUserAddress']['user_address_id']) {
                    $defaultShippingUserAddressId = $orderAddress['OrderUserAddress']['id'];
                }
            }
            
            $addressesNew = array();
            $orderAddressesMapped = array();
            foreach ( $addresses as $key => $alias ) {
                if (array_key_exists($key, $orderAddressesAssign)) {
                    $orderAddressesMapped[] = $key;
                    if (in_array($orderAddressesAssign[$key]['OrderUserAddress']['id'], $updateOrderAddress)) {
                        $addressesNew[$orderAddressesAssign[$key]['OrderUserAddress']['id']] = $alias;
                    } else {
                        $addressesNew[$orderAddressesAssign[$key]['OrderUserAddress']['id']] = $orderAddressesAssign[$key]['OrderUserAddress']['alias'];
                    }
                } else {
                    $addressesNew[$key] = $alias;
                }
            }
            
            foreach ( $orderAddresses as $key => $address ) {
                if (! in_array($address['OrderUserAddress']['id'], $orderAddressesMapped)) {
                    $addressesNew[$address['OrderUserAddress']['id']] = $address['OrderUserAddress']['alias'];
                }
            }
        }
        
        return array(
            $addressesNew,
            $defaultBillingUserAddressId,
            $defaultShippingUserAddressId
        );
    }



    public function getShippingForOrder($orderId, $params = array(), $status = 1) {
        $shippingsForOrder = array();
        
        if ($orderId > 0) {
            $this->contain(array(
                'OrderProduct',
                'OrderProduct.Product',
                'ShippingOrderUserAddress' => 'country_id'
            ));
            $order = $this->findById($orderId);
        }
        
        if (! isset($params['weight'])) {
            $params['weight'] = 0;
            if (isset($order)) {
                foreach ( $order['OrderProduct'] as $orderProduct ) {
                    $params['weight'] += $orderProduct['weight'];
                }
            } else {
                return $shippingsForOrder;
            }
        }
        
        if (! isset($params['products_price_tax'])) {
            $params['products_price_tax'] = ( float ) $order['Order']['products_price_tax'];
        }
        
        if (! isset($params['products_amount'])) {
            $params['products_amount'] = 0;
            if (isset($order)) {
                foreach ( $order['OrderProduct'] as $orderProduct ) {
                    $params['products_amount'] += $orderProduct['amount'];
                }
            } else {
                return $shippingsForOrder;
            }
        }
        
        if (! isset($params['country_id'])) {
            $params['country_id'] = 0;
            if (isset($order)) {
                $params['country_id'] = $order['ShippingOrderUserAddress']['country_id'];
            } else {
                return $shippingsForOrder;
            }
        }
        
        if (empty($params['country_id'])) {
            return $shippingsForOrder;
        }
        
        $this->Shipping->contain(array(
            'ShippingPrice',
            'Payment' => array(
                'id'
            ),
            'Country' => array(
                'id'
            )
        ));
        $shippings = $this->Shipping->find('all', array(
            'conditions' => array(
                'Shipping.status' => $status
            ),
            'order' => 'Shipping.sort_order'
        ));
        
        foreach ( $shippings as $shipping ) {
            if (! $shipping['Shipping']['all_countries']) {
                $found = false;
                foreach ( $shipping['Country'] as $country ) {
                    if ($country['id'] == $params['country_id']) {
                        $found = true;
                        break;
                    }
                }
                if (! $found) {
                    continue;
                }
            }
            
            if ($shipping['Shipping']['max_price_tax'] > 0 && $params['products_price_tax'] > $shipping['Shipping']['max_price_tax']) {
                continue;
            }
            
            if ($shipping['Shipping']['max_weight'] > 0 && $params['weight'] > $shipping['Shipping']['max_weight']) {
                continue;
            }
            
            if (! empty($params['products'])) {
                $valid = true;
                foreach ( $params['products'] as $products ) {
                    if (! $products['Product']['all_shippings']) {
                        $shippings = Hash::extract($products, 'Product.Shipping.{n}.ProductsShipping.shipping_id');
                        if (! in_array($shipping['Shipping']['id'], $shippings)) {
                            $valid = false;
                        }
                    }
                }
                if (! $valid) {
                    continue;
                }
            }
            
            $freeShippingStatus = array();
            if (! empty($params['products'])) {
                $freeShippingStatus = Hash::extract($params['products'], '{n}.Product[free_shipping_status=1].free_shipping_status');
            }
            
            if (($shipping['Shipping']['free_from'] > 0 && $params['products_price_tax'] > $shipping['Shipping']['free_from']) || ! empty($freeShippingStatus)) {
                $shipping['Shipping']['for_order_price_tax'] = 0;
                $shippingsForOrder[] = $shipping;
                continue;
            }
            
            if ($shipping['Shipping']['price_type'] == 'const') {
                $shipping['Shipping']['for_order_price_tax'] = $shipping['Shipping']['price_tax'];
                $shippingsForOrder[] = $shipping;
            } elseif ($shipping['Shipping']['price_type'] == 'depend_weight') {
                foreach ( $shipping['ShippingPrice'] as $shippingPrice ) {
                    if ($shippingPrice['to_weight'] <= $params['weight']) {
                        $shipping['Shipping']['for_order_price_tax'] = $shippingPrice['price_tax'];
                        $shippingsForOrder[] = $shipping;
                        break;
                    }
                }
            } elseif ($shipping['Shipping']['price_type'] == 'depend_price') {
                foreach ( $shipping['ShippingPrice'] as $shippingPrice ) {
                    if ($shippingPrice['to_price_tax'] <= $params['products_price_tax']) {
                        $shipping['Shipping']['for_order_price_tax'] = $shippingPrice['price_tax'];
                        $shippingsForOrder[] = $shipping;
                        break;
                    }
                }
            } elseif ($shipping['Shipping']['price_type'] == 'depend_amount') {
                foreach ( $shipping['ShippingPrice'] as $shippingPrice ) {
                    if ($shippingPrice['to_product_amount'] <= $params['products_amount']) {
                        $shipping['Shipping']['for_order_price_tax'] = $shippingPrice['price_tax'];
                        $shippingsForOrder[] = $shipping;
                        break;
                    }
                }
            }
        }
        
        return $shippingsForOrder;
    }



    public function getPaymentsForOrder($orderId, $params = array(), $asList = true) {
        $paymentsForOrder = array();
        
        if ($orderId > 0) {
            $this->contain(array(
                'Shipping'
            ));
            $order = $this->findById($orderId);
        }
        
        if (! isset($params['shipping_id'])) {
            $params['shipping_id'] = 0;
            if ($order && $order['Order']['shipping_id'] > 0) {
                $params['shipping_id'] = $order['Order']['shipping_id'];
            }
        }
        if (! isset($params['payment_id'])) {
            $params['payment_id'] = 0;
            if (isset($order) && $order['Order']['payment_id'] > 0) {
                $params['payment_id'] = $order['Order']['payment_id'];
            }
        }
        
        if ($params['shipping_id'] > 0) {
            $this->Shipping->contain(array(
                'Payment' => array(
                    'id'
                )
            ));
            $shipping = $this->Shipping->findById($params['shipping_id']);
            
            if (! $shipping['Shipping']['all_payments']) {
                $payments_ids = [
                    $params['payment_id']
                ];
                foreach ( $shipping['Payment'] as $payment ) {
                    $payments_ids[] = $payment['id'];
                }
                
                $paymentsForOrder = $this->Payment->find($asList ? 'list' : 'all', array(
                    'conditions' => array(
                        'id' => $payments_ids,
                        'status' => 1
                    ),
                    'order' => 'sort_order'
                ));
            } else {
                $paymentsForOrder = $this->Payment->find($asList ? 'list' : 'all', array(
                    'conditions' => array(
                        'status' => 1
                    ),
                    'order' => 'sort_order'
                ));
            }
        }
        
        return $paymentsForOrder;
    }



    public function beforeSave($options = array()) {
        $orig_billing_address_id = $this->data['Order']['billing_order_address_id'];
        if (substr($this->data['Order']['billing_order_address_id'], 0, 2) == '0-') {
            $this->data['Order']['billing_order_address_id'] = $this->BillingOrderUserAddress->importUserAddress(substr($this->data['Order']['billing_order_address_id'], 2));
        }
        if (substr($this->data['Order']['shipping_order_address_id'], 0, 2) == '0-') {
            if ($this->data['Order']['shipping_order_address_id'] == $orig_billing_address_id) {
                $this->data['Order']['shipping_order_address_id'] = $this->data['Order']['billing_order_address_id'];
            } else {
                $this->data['Order']['shipping_order_address_id'] = $this->ShippingOrderUserAddress->importUserAddress(substr($this->data['Order']['shipping_order_address_id'], 2));
            }
        }
        
        return true;
    }



    public function afterSave($created, $options = array()) {
        $this->addOrderStatusHistory($this->data['Order']['id'], $this->data['Order']['order_status_id'], $this->data['Order']['user_id']);
        
        if ($created) {
            $this->data['Order']['number'] = '#' . $this->data['Order']['id'];
            $this->save($this->data);
        }
        
        if (! $created) {
            $lastStatus = $this->OrderStatusHistory->find('first', array(
                'fields' => 'order_status_id',
                'conditions' => array(
                    'order_id' => $this->data['Order']['order_status_id']
                ),
                'order' => 'created desc'
            ));
            
            if ($lastStatus && $lastStatus['OrderStatusHistory']['order_status_id'] == $this->data['Order']['order_status_id']) {
                return true;
            }
        }
        
        return true;
    }



    public function addOrderStatusHistory($orderId, $orderStatusId, $userId = null) {
        $data = array(
            'order_id' => $orderId,
            'order_status_id' => $orderStatusId,
            'user_id' => $userId > 0 ? $userId : AuthComponent::user('id')
        );
        $this->OrderStatusHistory->create();
        $this->OrderStatusHistory->save($data);
    }



    public function setFilters($data) {
        $conditions = array();
        if (! empty($data['status_id'])) {
            $conditions['Order.order_status_id'] = $data['status_id'];
        }
        
        if (! empty($data['payment_id'])) {
            $conditions['Order.payment_id'] = $data['payment_id'];
        }
        
        if (! empty($data['shipping_id'])) {
            $conditions['Order.shipping_id'] = $data['shipping_id'];
        }
        
        if (! empty($data['paid'])) {
            $conditions['Order.paid'] = $data['paid'];
        }
        
        if (! empty($data['shipment_no'])) {
            $conditions['Order.shipment_no LIKE'] = "%{$data['shipment_no']}%";
        }
        
        if (! empty($data['price_from']) && ! empty($data['price_to'])) {
            $priceFrom = str_replace(',', '.', $data['price_from']);
            $priceTo = str_replace(',', '.', $data['price_to']);
            $conditions['AND'][] = array(
                'Order.order_price_tax >=' => $priceFrom
            );
            $conditions['AND'][] = array(
                'Order.order_price_tax <=' => $priceTo
            );
        } else if (! empty($data['price_from'])) {
            $priceFrom = str_replace(',', '.', $data['price_from']);
            $conditions['AND'][] = array(
                'Order.order_price_tax' => $priceFrom
            );
        } else if (! empty($data['price_to'])) {
            $priceTo = str_replace(',', '.', $data['price_to']);
            $conditions['AND'][] = array(
                'Order.order_price_tax <=' => $priceTo
            );
        }
        
        if (! empty($data['date_to'])) {
            $conditions['AND'][] = array(
                'Order.order_price_tax <=' => $data['date_to'] . ' 23:59:59'
            );
        }
        
        if (! empty($data['date_from'])) {
            $conditions['AND'][] = array(
                'Order.created >=' => $data['date_from'] . ' 00:00:00'
            );
        }
        
        if (! empty($data['date_to'])) {
            $conditions['AND'][] = array(
                'Order.created <=' => $data['date_to'] . ' 23:59:59'
            );
        }
        
        if (! empty($data['phraze'])) {
            $phraze = strtolower($data['phraze']);
            $conditions['AND']['OR'][] = array(
                'LOWER(UserFiltr.first_name) LIKE' => "%{$phraze}%"
            );
            $conditions['AND']['OR'][] = array(
                'LOWER(UserFiltr.last_name) LIKE' => "%{$phraze}%"
            );
            $conditions['AND']['OR'][] = array(
                'LOWER(UserFiltr.company) LIKE' => "%{$phraze}%"
            );
            $conditions['AND']['OR'][] = array(
                'Order.id' => $phraze
            );
        }
        
        if (! empty($data['phraze_product'])) {
            $phraze = strtolower($data['phraze_product']);
            $conditions['AND']['OR'][] = array(
                'LOWER(VProducts.name) LIKE' => "%{$phraze}%"
            );
            $conditions['AND']['OR'][] = array(
                'LOWER(VProducts.catalog_no) LIKE' => "%{$phraze}%"
            );
            $conditions['AND']['OR'][] = array(
                'LOWER(VProducts.option_no) LIKE' => "%{$phraze}%"
            );
        }
        
        return $conditions;
    }
}
