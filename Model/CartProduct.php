<?php

App::uses('AppModel', 'Model');

/**
 * CartProduct Model
 *
 */
class CartProduct extends AppModel {

  /**
   * Display field
   *
   * @var string
   */
  public $displayField = 'name';

  /**
   * belongsTo associations
   *
   * @var array
   */
  public $belongsTo = array(
    'Cart' => array(
      'className' => 'KeyAdmin.Cart',
      'foreignKey' => 'cart_id'
    ),
    'Product' => array(
      'className' => 'KeyAdmin.Product',
      'foreignKey' => 'product_id'
    ),
    'Manufacturer' => array(
      'className' => 'KeyAdmin.Manufacturer',
      'foreignKey' => 'manufacturer_id'
    ),
    'TaxRate' => array(
      'className' => 'KeyAdmin.TaxRate',
      'foreignKey' => 'tax_rate_id'
    ),
    'Unit' => array(
      'className' => 'KeyAdmin.Unit',
      'foreignKey' => 'unit_id'
    ),
    'ShippingTime' => array(
      'className' => 'KeyAdmin.ShippingTime',
      'foreignKey' => 'shipping_time_id'
    ),
    'ProductCondition' => array(
      'className' => 'KeyAdmin.ProductCondition',
      'foreignKey' => 'product_condition_id'
    ),
    'ProductWarranty' => array(
      'className' => 'KeyAdmin.ProductWarranty',
      'foreignKey' => 'product_warranty_id'
    ),
    'ProductOption' => array(
      'className' => 'KeyAdmin.ProductsProductsOption',
      'foreignKey' => 'products_products_option_id'
    )
  );
  
  /**
   * Validation rules
   *
   * @var array
   */
  var $validate = array();

}
