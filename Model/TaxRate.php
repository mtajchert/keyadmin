<?php

App::uses('AppModel', 'Model');

/**
 * Tax rate Model
 *
 */
class TaxRate extends AppModel {

  /**
   * Display field
   *
   * @var string
   */
  public $displayField = 'description';

  /**
   * Validation rules
   *
   * @var array
   */
  var $validate = array(
    'default' => array(
      'zero_one' => array(
        'rule' => array('inList', [0, 1]),
        'required' => true,
        'message' => 'Nieprawidłowa wartość'
      )
    ),
    'rate' => array(
      'int' => array(
        'rule' => array('comparison', '>=', 0),
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Stawka musi być liczbą większą lub równą 0'
      )
    ),
    'description' => array(
      'notEmpty' => array(
        'rule' => 'notEmpty',
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Opis jest wymagany'
      )
    ),
    'short_description' => array(
      'notEmpty' => array(
        'rule' => 'notEmpty',
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Opis skrócony jest wymagany'
      )
    ),
    'sort_order' => array(
      'int' => array(
        'rule' => array('comparison', '>=', 0),
        'allowEmpty' => true,
        'message' => 'Kolejność sortowania musi być liczbą większą lub równą 0'
      ),
    )
  );

}
