<?php

App::uses('AppModel', 'Model');

/**
 * Product option value Model
 *
 */
class ProductOptionValue extends AppModel {

  /**
   * Display field
   *
   * @var string
   */
  public $displayField = 'name';
  
  public $belongsTo = array(
    'ProductOption' => array(
      'className' => 'KeyAdmin.ProductOption',
      'foreignKey' => 'product_option_id'
    )
  );

  /**
   * Validation rules
   *
   * @var array
   */
  var $validate = array(
    'name' => array(
      'notEmpty' => array(
        'rule' => 'notEmpty',
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Nazwa jest wymagana'
      )
    ),
    'product_option_id' => array(
      'gt0' => array(
        'rule' => array('naturalNumber', false),
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Wybierz cechę'
      )
    )
  );

}
