<?php

App::uses('AppModel', 'Model');

/**
 * Product condition Model
 *
 */
class ProductCondition extends AppModel {

  /**
   * Display field
   *
   * @var string
   */
  public $displayField = 'name';

  /**
   * Validation rules
   *
   * @var array
   */
  var $validate = array(
    'default' => array(
      'zero_one' => array(
        'rule' => array('inList', [0, 1]),
        'required' => true,
        'message' => 'Nieprawidłowa wartość'
      )
    ),
    'name' => array(
      'notEmpty' => array(
        'rule' => 'notEmpty',
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Nazwa jest wymagana'
      )
    )
  );

}
