<?php

App::uses('AppModel', 'Model');

/**
 * Product option Model
 *
 */
class ProductOption extends AppModel {

  /**
   * Display field
   *
   * @var string
   */
  public $displayField = 'name';
  
  public $hasMany = array(
    'ProductOptionValue' => array(
      'className' => 'KeyAdmin.ProductOptionValue',
      'foreignKey' => 'product_option_id'
    )
  );

  /**
   * Validation rules
   *
   * @var array
   */
  var $validate = array(
    'name' => array(
      'notEmpty' => array(
        'rule' => 'notEmpty',
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Nazwa jest wymagana'
      )
    ),
    'type' => array(
      'zero_one' => array(
        'rule' => array('inList', ['list', 'radio']),
        'required' => true,
        'message' => 'Nieprawidłowa wartość'
      )
    ),
    'sort_order' => array(
      'int' => array(
        'rule' => array('comparison', '>=', 0),
        'allowEmpty' => true,
        'message' => 'Kolejność sortowania musi być liczbą większą lub równą 0'
      ),
    )
  );

}
