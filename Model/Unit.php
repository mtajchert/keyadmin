<?php

App::uses('AppModel', 'Model');

/**
 * Unit Model
 *
 */
class Unit extends AppModel {

  /**
   * Display field
   *
   * @var string
   */
  public $displayField = 'name';

  /**
   * Validation rules
   *
   * @var array
   */
  var $validate = array(
    'name' => array(
      'notEmpty' => array(
        'rule' => 'notEmpty',
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Nazwa jest wymagana'
      )
    ),
    'quantity_type' => array(
      'zero_one' => array(
        'rule' => array('inList', ['int', 'float']),
        'required' => true,
        'message' => 'Nieprawidłowa wartość'
      )
    ),
    'default' => array(
      'zero_one' => array(
        'rule' => array('inList', [0, 1]),
        'required' => true,
        'message' => 'Nieprawidłowa wartość'
      )
    ),
    'sort_order' => array(
      'int' => array(
        'rule' => array('comparison', '>=', 0),
        'allowEmpty' => true,
        'message' => 'Kolejność sortowania musi być liczbą większą lub równą 0'
      ),
    )
  );

}
