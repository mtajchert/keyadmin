<?php

App::uses('AppModel', 'Model');

/**
 * Shipping time model
 *
 */
class ShippingPrice extends AppModel {

  public $belongsTo = array(
    'Shipping' => array(
      'className' => 'KeyAdmin.Shipping',
      'foreignKey' => 'shipping_id'
    )
  );
  
  /**
   * Validation rules
   *
   * @var array
   */
  var $validate = array(
    'price_tax' => array(
      'decimal' => array(
        'rule' => '/^([0-9]+([.,][0-9]{1,2}){0,1})$/i',
        'required' => true,
        'allowEmpty' => false,
        'message' => 'Nieprawidłowa cena'
      )
    ),
    'to_weight' => array(
      'decimal' => array(
        'rule' => '/^([0-9]+([.,][0-9]{1,2}){0,1})$/i',
        'required' => false,
        'allowEmpty' => true,
        'message' => 'Nieprawidłowa waga graniczna'
      )
    ),
    'to_price_tax' => array(
      'decimal' => array(
        'rule' => '/^([0-9]+([.,][0-9]{1,2}){0,1})$/i',
        'required' => false,
        'allowEmpty' => true,
        'message' => 'Nieprawidłowa cena graniczna'
      )
    ),
    'to_product_amount' => array(
      'decimal' => array(
        'rule' => '/^([0-9]+([.,][0-9]{1,2}){0,1})$/i',
        'required' => false,
        'allowEmpty' => true,
        'message' => 'Nieprawidłowa ilość graniczna'
      )
    )
  );
  
  public function beforeValidate($options = array()) {
    if ($options['price_type'] == 'depend_weight') {
      $this->validator()->remove('to_weight');
      $this->validator()->add('to_weight', array(
        'decimal_required' => array(
          'rule' => '/^([0-9]+([.,][0-9]{1,2}){0,1})$/i',
          'required' => true,
          'allowEmpty' => false,
          'message' => 'Nieprawidłowa waga graniczna'
        )
      ));
    } elseif ($options['price_type'] == 'depend_price') {
      $this->validator()->remove('to_price_tax');
      $this->validator()->add('to_price_tax', array(
        'decimal_required' => array(
          'rule' => '/^([0-9]+([.,][0-9]{1,2}){0,1})$/i',
          'required' => true,
          'allowEmpty' => false,
          'message' => 'Nieprawidłowa cena graniczna'
        )
      ));
    } elseif ($options['price_type'] == 'depend_amount') {
      $this->validator()->remove('to_product_amount');
      $this->validator()->add('to_product_amount', array(
        'decimal_required' => array(
          'rule' => '/^([0-9]+([.,][0-9]{1,2}){0,1})$/i',
          'required' => true,
          'allowEmpty' => false,
          'message' => 'Nieprawidłowa ilość graniczna'
        )
      ));
    }
  }

}
