<?php
App::uses ( 'AppModel', 'Model' );

/**
 * Category Model
 */
class ProductProductsRel extends AppModel {
  
  /**
   * Display field
   *
   * @var string
   */
  public $displayField = 'name';
  public $useTable = 'product_products_rel';
  
  public $belongsTo = array (
    'ParentProduct' => array (
      'className' => 'KeyAdmin.Product',
      'foreignKey' => 'product_id' 
    ),
    'RelProduct' => array (
      'className' => 'KeyAdmin.Product',
      'foreignKey' => 'rel_product_id' 
    )
  );
}
