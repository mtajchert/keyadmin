<?php

App::uses('AppModel', 'Model');

/**
 * Config Model
 *
 */
class Config extends AppModel {

  /**
   * Display field
   *
   * @var string
   */
  public $displayField = 'code';

  /**
   * Validation rules
   *
   * @var array
   */
  var $validate = array();

}
