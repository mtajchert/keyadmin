<?php

App::uses('AppModel', 'Model');

/**
 * UserData Model
 *
 */
class UserData extends AppModel {

  /**
   * Validation rules
   *
   * @var array
   */
  public $validate = array(
    'status' => array(
      '0_or_1' => array(
        'rule' => array('range', -1, 2),
        'allowEmpty' => false,
        'message' => 'Niepoprawny status'
      ),
    ),
  );

  //The Associations below have been created with all possible keys, those that are not needed can be removed

  /**
   * belongsTo associations
   *
   * @var array
   */
  public $belongsTo = array(
    'User' => array(
      'className' => 'KeyAdmin.User'
    )
  );

}
