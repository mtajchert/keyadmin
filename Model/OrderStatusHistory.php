<?php

App::uses('AppModel', 'Model');

/**
 * OrderStatusHistory Model
 *
 */
class OrderStatusHistory extends AppModel {

  /**
   * Display field
   *
   * @var string
   */
  public $displayField = 'id';
  
  /**
   * belongsTo associations
   *
   * @var array
   */
  public $belongsTo = array(
    'User' => array(
      'className' => 'KeyAdmin.User',
      'foreignKey' => 'user_id'
    ),
    'Order' => array(
      'className' => 'KeyAdmin.Order',
      'foreignKey' => 'order_id'
    ),
    'OrderStatus' => array(
      'className' => 'KeyAdmin.OrderStatus',
      'foreignKey' => 'order_status_id'
    )
  );
  
  /**
   * Validation rules
   *
   * @var array
   */
  var $validate = array();

}
