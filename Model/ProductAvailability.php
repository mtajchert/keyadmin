<?php

App::uses('AppModel', 'Model');

/**
 * Tax rate Model
 *
 */
class ProductAvailability extends AppModel {

  /**
   * Display field
   *
   * @var string
   */
  public $displayField = 'name';

  /**
   * Validation rules
   *
   * @var array
   */
  var $validate = array(
    'name' => array(
      'notEmpty' => array(
        'rule' => 'notEmpty',
        'allowEmpty' => false,
        'required' => true,
        'message' => 'Nazwa jest wymagana'
      )
    ),
    'shipping_mode' => array(
      'zero_one' => array(
        'rule' => array('inList', [0, 1]),
        'required' => true,
        'message' => 'Nieprawidłowa wartość'
      )
    )
  );

}
