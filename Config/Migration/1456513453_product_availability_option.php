<?php
class ProductAvailabilityOption extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = 'product_availability_option';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
			'create_field' => array(
				'products_products_options' => array(
					'product_availability_id' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 10, 'unsigned' => true, 'after' => 'store_quantity'),
					'sort_order' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 10, 'unsigned' => true, 'after' => 'product_availability_id'),
				),
			),
		),
		'down' => array(
			'drop_field' => array(
				'products_products_options' => array('product_availability_id', 'sort_order'),
			),
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction Direction of migration process (up or down)
 * @return bool Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction Direction of migration process (up or down)
 * @return bool Should process continue
 */
	public function after($direction) {
		return true;
	}
}
