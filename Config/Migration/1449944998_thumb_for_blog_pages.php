<?php
class ThumbForBlogPages extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = 'thumb_for_blog_pages';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
			'create_field' => array(
				'pages' => array(
					'thumb' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 36, 'collate' => 'utf8_general_ci', 'charset' => 'utf8', 'after' => 'seo_url'),
				),
			),
		),
		'down' => array(
			'drop_field' => array(
				'pages' => array('thumb'),
			),
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction Direction of migration process (up or down)
 * @return bool Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction Direction of migration process (up or down)
 * @return bool Should process continue
 */
	public function after($direction) {
		return true;
	}
}
