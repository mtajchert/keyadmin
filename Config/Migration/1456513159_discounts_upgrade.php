<?php
class DiscountsUpgrade extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = 'discounts_upgrade';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
			'create_field' => array(
				'cart_products' => array(
					'modified' => array('type' => 'datetime', 'null' => true, 'default' => null, 'after' => 'created'),
					'discount' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 10, 'unsigned' => true, 'after' => 'weight'),
				),
				'carts' => array(
					'discount_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10, 'unsigned' => true, 'after' => 'add_info'),
				),
				'orders' => array(
					'discount_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 10, 'unsigned' => true, 'after' => 'hash'),
				),
			),
			'drop_field' => array(
				'cart_products' => array('midified'),
			),
		),
		'down' => array(
			'drop_field' => array(
				'cart_products' => array('modified', 'discount'),
				'carts' => array('discount_id'),
				'orders' => array('discount_id'),
			),
			'create_field' => array(
				'cart_products' => array(
					'midified' => array('type' => 'datetime', 'null' => true, 'default' => null),
				),
			),
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction Direction of migration process (up or down)
 * @return bool Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction Direction of migration process (up or down)
 * @return bool Should process continue
 */
	public function after($direction) {
		return true;
	}
}
