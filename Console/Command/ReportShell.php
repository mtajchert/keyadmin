<?php

class ReportShell extends AppShell {
	
  public $uses = array('KeyAdmin.Order', 'KeyAdmin.ProductsCategory');
  
  
  public function main() {
    $this->Order->contain(array('OrderProduct.Product' => array('fields' => array('purchase_price', 'purchase_price_tax'))));
    $orders = $this->Order->find('all', array('conditions' => array(
      'Order.created >=' => $this->args[0],
      'Order.created <=' => $this->args[1],
      'Order.order_status_id !=' => 6
    )));        
    
    App::uses('KeyOrdersController', 'KeyAdmin.Controller');
    $ctrl = new KeyOrdersController();
    $ctrl->plugin = 'KeyAdmin';
    $view = new View($ctrl, false);
    $view->layout = false;
    $products = Hash::extract($orders, '{n}.OrderProduct.{n}');
    
    $view->set(compact('products'));
    $html = $view->render('purchase_report_pdf');
    
    $mpdf = new mPDF('BLANK', 'A4-L', '9', 'Arial', 10, 10, 15, 15, 5, 5);
    
    
    $mpdf->SetHTMLHeader('<p style="width:100%; text-align:right; margin:0; padding:0; font-family:Arial; font-size:8pt; font-weight:bold; font-style:italic;">Strona {PAGENO}/{nbpg}</span><hr style="margin:0;"/>');
    $mpdf->SetHTMLFooter('<hr style="margin-top:0;"/><p style="width:100%; text-align:right; margin:0; padding:0; font-family:Arial; font-size:8pt; font-weight:bold; font-style:italic;">Strona {PAGENO}/{nbpg}</span>');
    
    @$mpdf->WriteHTML($html);
    $data = date('Y-m-d-H-i-s');
    $mpdf->Output(WWW_ROOT."sprzedaz-{$data}.pdf", 'F');
  }
}