<?php
App::uses('ImageComponent', 'KeyAdmin.Controller/Component');

class ImageImportShell extends AppShell {
	
  public $uses = array('Category', 'Manufacturer', 'ProductsImage');
  
  public $components = array('KeyAdmin.Image');
  
  public function categories() {
    set_time_limit(0);
    $categories = $this->Category->find('all', array('image !=' => ''));

    $categoriesDir = CLIENT_WWW.'categories/origin/';
    if (!is_dir($categoriesDir)) {
      mkdir($categoriesDir, 0777, true);
    }
    $categoriesDirList = CLIENT_WWW.'categories/list/';
    if (!is_dir($categoriesDirList)) {
      mkdir($categoriesDirList, 0777, true);
    }
    $categoriesDirThumb = CLIENT_WWW.'categories/thumb/';
    if (!is_dir($categoriesDirThumb)) {
      mkdir($categoriesDirThumb, 0777, true);
    }
    
    $this->Image = new ImageComponent(new ComponentCollection());
     
     foreach ($categories as $category){
       $this->out("Sprawdzam kategorie: {$category['Category']['name']}");
       if ($this->remoteFileExists('http://alcatras.pl/images/'.$category['Category']['image'])){
         $this->out("Pobieram: ".'http://alcatras.pl/images/'.$category['Category']['image']);
         $tmp = @file_get_contents('http://alcatras.pl/images/'.$category['Category']['image']);
         
         $fileName = md5(time().$category['Category']['image']).'.jpg';
         if (file_put_contents($categoriesDir.$fileName, $tmp)){
           $this->Image->resizeImage($categoriesDir.$fileName, $categoriesDirList.$fileName, 50, 34);
           $this->Image->resizeImage($categoriesDir.$fileName, $categoriesDirThumb.$fileName, 480, 320);
         
           $this->Category->read(null, $category['Category']['id']);
           $this->Category->set('image', $fileName);
           $this->Category->save();
           $this->out("Zapisywanie zdjecia dla: {$category['Category']['name']}");
         }   
       }
     }
  }
  
  
  public function manufacturers() {
    set_time_limit(0);
    $manufacturers = $this->Manufacturer->find('all', array('image !=' => ''));
    $categoriesDir = CLIENT_WWW.'manufacturers/origin/';
    if (!is_dir($categoriesDir)){
      mkdir($categoriesDir, 0777, true);
    }
    $categoriesDirList = CLIENT_WWW.'manufacturers/list/';
    if (!is_dir($categoriesDirList)){
      mkdir($categoriesDirList, 0777, true);
    }
    $categoriesDirThumb = CLIENT_WWW.'manufacturers/thumb/';
    if (!is_dir($categoriesDirThumb)){
      mkdir($categoriesDirThumb, 0777, true);
    }
    $this->Image = new ImageComponent(new ComponentCollection());
     
    foreach ($manufacturers as $manufacturer){
      $this->out("Sprawdzam producenta: {$manufacturer['Manufacturer']['name']}");
      if ($this->remoteFileExists('http://alcatras.pl/images/'.$manufacturer['Manufacturer']['image'])){
        $this->out("Pobieram: ".'http://alcatras.pl/images/'.$manufacturer['Manufacturer']['image']);
        $tmp = @file_get_contents('http://alcatras.pl/images/'.$manufacturer['Manufacturer']['image']);
         
        $fileName = md5(time().$manufacturer['Manufacturer']['image']).'.jpg';
        if (file_put_contents($categoriesDir.$fileName, $tmp)){
          $this->Image->resizeImage($categoriesDir.$fileName, $categoriesDirList.$fileName, 50, 34);
          $this->Image->resizeImage($categoriesDir.$fileName, $categoriesDirThumb.$fileName, 480, 320);
           
          $this->Manufacturer->read(null, $manufacturer['Manufacturer']['id']);
          $this->Manufacturer->set('image', $fileName);
          $this->Manufacturer->save();
          $this->out("Zapisywanie zdjecia dla: {$manufacturer['Manufacturer']['name']}");
        }
      }
    }
  }
  
  public function products() {
    set_time_limit(0);
    $products = $this->ProductsImage->find('all');
    
    $productsDir = CLIENT_WWW.'products/origin/';
    if (!is_dir($productsDir)){
      mkdir($productsDir, 0777, true);
    }
    $productsDirList = CLIENT_WWW.'products/list/';
    if (!is_dir($productsDirList)){
      mkdir($productsDirList, 0777, true);
    }
    $productsDirThumb = CLIENT_WWW.'products/thumb/';
    if (!is_dir($productsDirThumb)){
      mkdir($productsDirThumb, 0777, true);
    }
    
    $this->Image = new ImageComponent(new ComponentCollection());
    
    foreach ($products as $product){
      $this->out("Sprawdzam zdjecie: {$product['ProductsImage']['id']}");
      if ($this->remoteFileExists('http://alcatras.pl/images/'.$product['ProductsImage']['image'])){
        $this->out("Pobieram: ".'http://alcatras.pl/images/'.$product['ProductsImage']['image']);
        $tmp = @file_get_contents('http://alcatras.pl/images/'.$product['ProductsImage']['image']);
         
        $fileName = md5(time().$product['ProductsImage']['image']).'.jpg';
        if (file_put_contents($productsDir.$fileName, $tmp)){
          $this->Image->resizeImage($productsDir.$fileName, $productsDirList.$fileName, 50, 34);
          $this->Image->resizeImage($productsDir.$fileName, $productsDirThumb.$fileName, 480, 320);
           
          $this->ProductsImage->read(null, $product['ProductsImage']['id']);
          $this->ProductsImage->set('image', $fileName);
          $this->ProductsImage->save();
          $this->out("Zapisywanie zdjecia dla: {$product['ProductsImage']['id']}");
        }
      }
    }
  }

  
  public function remoteFileExists($url) {
    $curl = curl_init($url);
  
    //don't fetch the actual page, you only want to check the connection is ok
    curl_setopt($curl, CURLOPT_NOBODY, true);
  
    //do request
    $result = curl_exec($curl);
  
    $ret = false;
  
    //if request did not fail
    if ($result !== false) {
      //if request was ok, check response code
      $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
  
      if ($statusCode == 200) {
        $ret = true;
      }
    }
  
    curl_close($curl);
  
    return $ret;
  }
}