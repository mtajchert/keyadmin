<?php
App::uses('ImageComponent', 'KeyAdmin.Controller/Component');

class ImageRescaleShell extends AppShell {
	
  public $uses = array('Category', 'Manufacturer', 'ProductsImage');
  
  public $components = array('KeyAdmin.Image');
  
  public function products() {
    set_time_limit(0);
    $images = $this->ProductsImage->find('all');
    
    $productsDir = CLIENT_WWW.'products/origin/';
    if (!is_dir($productsDir)){
      mkdir($productsDir, 0777, true);
    }
    $productsDirList = CLIENT_WWW.'products/list/';
    if (!is_dir($productsDirList)){
      mkdir($productsDirList, 0777, true);
    }
    $productsDirThumb = CLIENT_WWW.'products/thumb/';
    if (!is_dir($productsDirThumb)){
      mkdir($productsDirThumb, 0777, true);
    }
    
    $this->Image = new ImageComponent(new ComponentCollection());
     
    foreach ($images as $image){
      $fileName = $image['ProductsImage']['image'];
      if (file_exists($productsDir.$fileName)) {
        $this->Image->resizeImage($productsDir.$fileName, $productsDirList.$fileName, 50, 34);
        $this->Image->resizeImage($productsDir.$fileName, $productsDirThumb.$fileName, 480, 320);
        $this->out("Przeskalowano zdjecie id : {$image['ProductsImage']['id']}");
      }
    }
  }
}