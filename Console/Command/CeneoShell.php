<?php
App::uses('ImageComponent', 'KeyAdmin.Controller/Component');

class CeneoShell extends AppShell {
	
  public $uses = array('KeyAdmin.Category', 'KeyAdmin.ProductsCategory');
  
  public function utf8_for_xml($string)
  {
    return preg_replace ('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $string);
  }
  
  
  public function getBody($product, $options, $categoryPath){    
    App::uses('AppHelper', 'View/Helper');
    $appHelper = @new AppHelper();
    
    $description = strip_tags($product['Product']['description']);
    $description = str_replace('"', '', $description);
    $description = str_replace("'", '', $description);
    $description = str_replace("\n", '', $description);
    $description = trim($description);
    $description = $this->utf8_for_xml($description);
    $manufacturer = (isset($product['Product']['Manufacturer']['name']))? $product['Product']['Manufacturer']['name']: '';
    $productName = (!empty($options))? "{$product['Product']['name']} - {$options['ProductOptionValue']['name']}": $product['Product']['name'];
    $productId = $product['Product']['id'];
    if ( !empty($options) ){
      $productId .= '-'.$options['id'];
    }
    if (isset($options['id']) && !empty($options['id'])) {
      $url = $appHelper->getProductViewUrl($product, $options['id']);
    } else {
      $url = $appHelper->getProductViewUrl($product);
    }
    $tmp = array(
      '@id' => $productId,
      '@url' => $url,
      '@price' => (isset($options['change_price_tax']))? $options['change_price_tax']: $product['Product']['price_tax'],
      '@avail' => 1,
      '@set' => 0,
      '@basket' => 0,
      'cat' => "<![CDATA[$categoryPath]]>",
      'name' => "<![CDATA[{$productName}]]>",
      'imgs' => array('main' => array('@url' => 'https://alcatras.pl/'.CLIENT.'/products/origin/'.$product['Product']['ProductsImage'][0]['image'])),
      'desc' => "<![CDATA[{$description}]]>",
      'attrs' => array(
        'a' => array(
          array('@name' => 'Producent', '@' => "<![CDATA[{$manufacturer}]]>"),
          array('@name' => 'Kod_producenta', '@' => "<![CDATA[{$product['Product']['ext_code']}]]>"),
          array('@name' => 'EAN', '@' => "<![CDATA[{$product['Product']['ean']}]]>")
        )
      )
      );
    return $tmp;
  }
  
  
  public function main() {
    App::uses('Xml', 'Utility');
    
    
    $xmlArray = array('@name' => 'other', 'o' => array());
    
    //854
    $this->Category->contain(array('ChildCategory'));
    $this->ProductsCategory->contain(array('Product.Manufacturer', 'Product.ProductsImage' => array('order' => array('ProductsImage.sort_order' => 'ASC'))));
    
    
    
    $categories = $this->Category->find('all', array('conditions' => array('Category.parent_id' => 854, 'Category.status' => 1))); 
    $counter = 0;
    $break = 2000;
    foreach ($categories as $category) {
      $this->out($category['Category']['name']);
        if (empty($category['ChildCategory'])){
          $this->ProductsCategory->contain(array('Product.ProductsProductsOption.ProductOptionValue', 'Product.Manufacturer', 'Product.ProductsImage' => array('order' => array('ProductsImage.sort_order' => 'ASC'))));
          $products = $this->ProductsCategory->find('all', array('conditions' => array('ProductsCategory.category_id' => $category['Category']['id'])));
          foreach ($products as $product) {
            if (!isset($product['Product'])){
              continue;
            }
            if ($product['Product']['status'] == 0) {
              continue;
            }            
            $this->out($product['Product']['name']);
            if (!empty($product['Product']['ProductsProductsOption'])){
              if (!empty($product['Product']['ProductsProductsOption'])){
                foreach ($product['Product']['ProductsProductsOption'] as $options) {
                  $xmlArray['o'][] = $this->getBody($product, $options, "Akcesoria samochodowe/{$category['Category']['name']}/{$childCategory['name']}");
                }
              } else {
                $xmlArray['o'][] = $this->getBody($product, array(),"Akcesoria samochodowe/{$category['Category']['name']}/{$childCategory['name']}");
              }
            }
            $counter++;
            if ($counter > $break) break;
          }
          
        } else {
          foreach ($category['ChildCategory'] as $childCategory){
            $this->ProductsCategory->contain(array('Product.ProductsProductsOption.ProductOptionValue', 'Product.Manufacturer', 'Product.ProductsImage' => array('order' => array('ProductsImage.sort_order' => 'ASC'))));
            $products = $this->ProductsCategory->find('all', array('conditions' => array('ProductsCategory.category_id' => $childCategory['id'])));
            foreach ($products as $product) {
              if (!isset($product['Product'])){
                continue;
              }
              if ($product['Product']['status'] == 0) {
                continue;
              }
              $this->out($product['Product']['name']);
              if (!empty($product['Product']['ProductsProductsOption'])){
                foreach ($product['Product']['ProductsProductsOption'] as $options) {
                  $xmlArray['o'][] = $this->getBody($product, $options, "Akcesoria samochodowe/{$category['Category']['name']}/{$childCategory['name']}");
                }
              } else {
                $xmlArray['o'][] = $this->getBody($product, array(),"Akcesoria samochodowe/{$category['Category']['name']}/{$childCategory['name']}");
              }
              
              $counter++;
              if ($counter > $break) break;
            }
          }
        }
        if ($counter > $break) break;
    }
    
    
    $xml = array(
      'offers' => array('xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance', '@version' => 1, 'group' => $xmlArray)        
    );
    
    
    
    //print_r($xmlArray);
    $xmlObject = Xml::fromArray($xml);
    $xmlString = $xmlObject->asXML();
    file_put_contents('ceneo.xml', htmlspecialchars_decode($xmlString));
    
  }
  
  
}




