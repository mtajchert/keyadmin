<?php
App::uses('AdminController', 'KeyAdmin.Controller');
App::uses('OrderProduct', 'KeyAdmin.Model');

class KeyOrdersController extends AdminController {
    public $uses = array(
        'KeyAdmin.Order',
        'KeyAdmin.Customer',
        'KeyAdmin.Product',
        'KeyAdmin.OrderProduct',
        'KeyAdmin.OrderUserAddress',
        'KeyAdmin.Invoice'
    );
    public $components = array(
        'Paginator',
        'RequestHandler',
        'KeyAdmin.NotificationCenter',
        'KeyAdmin.Reports',
        'KeyAdmin.Invoices'
    );
    public $helpers = array(
        'KeyAdmin.User'
    );
    public $paginate = array(
        'limit' => 20,
        'order' => array(
            'Order.created' => 'DESC'
        )
    );
    public $active = 1;



    public function beforeFilter() {
        parent::beforeFilter();
        $this->Paginator->settings = $this->paginate;
    }



    public function admin_archive() {
        $this->active = 0;
        $this->admin_index();
        $this->set('buttons_template', null);
        
        $this->render('KeyOrders/admin_index');
    }



    public function admin_index() {
        if ($this->request->is('post')) {
            $url = array(
                'plugin' => 'KeyAdmin',
                'controller' => 'KeyOrders',
                'action' => 'index',
                'admin' => true
            );
            foreach ( $this->params['named'] as $key => $value ) {
                if (! is_array($value) && $key != 'page') {
                    $url[$key] = $value;
                }
            }
            foreach ( $this->request->data['Order'] as $key2 => $value2 ) {
                $url['Order[' . $key2 . ']'] = $value2;
            }
            $indexUrl = Router::url($url, true);
            $this->redirect($indexUrl);
        }
        
        $this->Order->contain(array(
            'Customer',
            'Customer.UserData',
            'Customer.UserAddress',
            'Payment',
            'Shipping',
            'OrderStatus',
            'Invoice'
        ));
        
        if (! empty($this->params['named']['Order'])) {
            $this->Paginator->settings['conditions'] = $this->Order->setFilters($this->params['named']['Order']);
            $this->request->data['Order'] = $this->params['named']['Order'];
            $this->Paginator->settings['joins'] = array(
                array(
                    'table' => 'users',
                    'type' => 'INNER',
                    'alias' => 'UserFiltr',
                    'conditions' => "Order.user_id = UserFiltr.id"
                )
            );
            if (! empty($this->params['named']['Order']['phraze_product'])) {
                $this->Paginator->settings['joins'][] = array(
                    'table' => 'v_order_products',
                    'type' => 'INNER',
                    'alias' => 'VProducts',
                    'conditions' => "Order.id = VProducts.order_id"
                );
            }
        }
        $this->Paginator->settings['conditions']['Order.active'] = $this->active;
        $this->set('orders', $this->Paginator->paginate());
        
        $this->set('title', __('Zamówienia'));
        $this->set('content_title', __('Zamówienia'));
        $this->set('content_subtitle', __('Lista zamówień'));
        $this->set('buttons_template', 'Buttons/KeyOrders');
        
        $payments = array(
            '0' => 'Wybierz'
        ) + $this->Order->Payment->find('list');
        $shippings = array(
            '0' => 'Wybierz'
        ) + $this->Order->Shipping->find('list');
        $statuses = array(
            '0' => 'Wybierz'
        ) + $this->Order->OrderStatus->find('list');
        
        $this->set(compact('payments', 'shippings', 'statuses'));
    }



    public function admin_edit($id) {
        if (! $id) {
            throw new NotFoundException(__('Wybrane zamówienie nie istnieje'));
        }
        
        $this->Order->contain(array(
            'Customer',
            'Customer.UserData',
            'Customer.UserAddress',
            'OrderProduct',
            'OrderProduct.Unit',
            'OrderProduct.Product',
            'Payment',
            'Shipping',
            'OrderStatus',
            'BillingOrderUserAddress',
            'BillingOrderUserAddress.Country',
            'BillingOrderUserAddress.Zone',
            'ShippingOrderUserAddress',
            'ShippingOrderUserAddress.Country',
            'ShippingOrderUserAddress.Zone',
            'OrderStatusHistory',
            'OrderStatusHistory.User',
            'OrderStatusHistory.OrderStatus'
        ));
        $order = $this->Order->findById($id);
        if (! $order) {
            throw new NotFoundException(__('Nie odnaleziono wybranego zamówienia'));
        }
        
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $this->Order->id = $id;
            
            $orderProductsIds = [
                - 1
            ];
            if (isset($this->request->data['OrderProduct'])) {
                foreach ( $this->request->data['OrderProduct'] as $key => $item ) {
                    $orderProductsIds[] = $this->request->data['OrderProduct'][$key]['id'];
                }
            }
            
            $existsOrderProducts = $this->OrderProduct->find('all', array(
                'conditions' => array(
                    'OrderProduct.order_id' => $id
                ),
                'fields' => array(
                    'OrderProduct.id'
                )
            ));
            $existsOrderProductsIds = [
                - 1
            ];
            foreach ( $existsOrderProducts as $item ) {
                $existsOrderProductsIds[] = $item['OrderProduct']['id'];
            }
            
            if ($this->Order->saveAll($this->request->data)) {
                $this->OrderProduct->deleteAll(array(
                    'OrderProduct.id' => $existsOrderProductsIds,
                    'OrderProduct.id !=' => $orderProductsIds
                ));
                
                if ($order['Order']['order_status_id'] != $this->request->data['Order']['order_status_id']) {
                    // notify
                    $this->Order->recursive = 2;
                    $orderForNotify = $this->Order->findById($id);
                    $userForNotify = $this->Order->Customer->findById($orderForNotify['Order']['user_id']);
                    
                    if (preg_match('/^[a-zA-Z0-9._\+-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,4}$/', $userForNotify['Customer']['email'])) {
                        $this->loadModel('Notification');
                        $notification = $this->Notification->find('first', array(
                            'conditions' => array(
                                'Notification.code' => $orderForNotify['OrderStatus']['code']
                            )
                        ));
                        if (! empty($notification)) {
                            $this->NotificationCenter->send($orderForNotify['OrderStatus']['code'], array(
                                $userForNotify['Customer']['email'],
                                trim($userForNotify['Customer']['first_name'] . ' ' . $userForNotify['Customer']['last_name'])
                            ), $userForNotify, $orderForNotify);
                        } else {
                            $this->NotificationCenter->send('order_status_change', array(
                                $userForNotify['Customer']['email'],
                                trim($userForNotify['Customer']['first_name'] . ' ' . $userForNotify['Customer']['last_name'])
                            ), $userForNotify, $orderForNotify);
                        }
                    }
                }
                
                $this->Session->setFlash(__('Zamówienie zostało zapisane.'), 'flash-success');
                return $this->redirect(array(
                    'action' => 'index'
                ));
            }
            $this->Session->setFlash(__('Nie udało się zapisać zamówienia. Sprawdź poprawność podanych danych.'), 'flash-error');
            
            $this->request->data = $this->fillRequestData($this->request->data);
            // print_r($this->request->data);die;
        }
        
        if (! $this->request->data) {
            $this->request->data = $order;
        }
        // print_r($this->request->data);die;
        // print_r($order);die;
        $this->set('title', __('Zamówienia'));
        $this->set('content_title', __('Zamówienia'));
        $this->set('content_subtitle', __('Edycja zamówienia'));
        $this->set('buttons_template', 'Buttons/KeyOrders');
        
        $this->set('order', $order);
        $this->set('create', false);
        
        $taxRatesFull = $this->Product->TaxRate->find('all', array(
            'fields' => array(
                'TaxRate.id',
                'TaxRate.description',
                'TaxRate.rate'
            )
        ));
        list($userAddresses, $defaultBillingUserAddressId, $defaultShippingUserAddressId) = $this->Order->getAddressesListAndData($order['Order']['user_id'], $order['Order']['id']);
        
        $shippingsFull = $this->Order->getShippingForOrder($id, array(), array(
            0,
            1
        ));
        $payments = $this->Order->getPaymentsForOrder($id);
        
        $orderStatusesFull = $this->Order->OrderStatus->find('all', array(
            'order' => 'OrderStatus.sort_order'
        ));
        
        $this->set(compact('taxRatesFull', 'userAddresses', 'defaultBillingUserAddressId', 'defaultShippingUserAddressId', 'shippingsFull', 'payments', 'orderStatusesFull'));
        // print_r($userAddresses);die;
        $this->render('KeyOrders/admin_form');
    }



    public function admin_create() {
        if ($this->request->is('post')) {
            $this->Order->create();
            $this->request->data['Order']['number'] = time();
            $this->request->data['Order']['hash'] = md5($this->request->data['Order']['user_id'] . time());
            ;
            
            if ($this->Order->saveAll($this->request->data)) {
                $this->Session->setFlash(__('Zamówienie zostało dodane.'), 'flash-success');
                
                if (Configure::read('Invoice.quote_enabled')) {
                    $this->Invoices->setController($this);
                    $this->Invoices->generateQuote($this->Order->getLastInsertID());
                }
                
                // notify
                $this->Order->recursive = 2;
                /*
                 * $orderForNotify = $this->Order->findById($this->Order->getLastInsertID());
                 * $userForNotify = $this->Order->Customer->findById($orderForNotify['Order']['user_id']);
                 *
                 * if (preg_match('/^[a-zA-Z0-9._\+-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,4}$/', $userForNotify['Customer']['email'])) {
                 * $this->NotificationCenter->send('new_order', array(
                 * $userForNotify['Customer']['email'],
                 * trim($userForNotify['Customer']['first_name'] . ' ' . $userForNotify['Customer']['last_name'])
                 * ), $userForNotify, $orderForNotify);
                 * }
                 */
                
                return $this->redirect(array(
                    'action' => 'index'
                ));
            }
            $this->Session->setFlash(__('Nie udało się dodać zamówienia. Sprawdź poprawność podanych danych.'), 'flash-error');
            $this->request->data = $this->fillRequestData($this->request->data);
        } else {
            $this->request->data = [
                'Order' => [
                    'shipping_id' => 0,
                    'payment_id' => 0,
                    'shipping_price_tax' => 0,
                    'products_price_tax' => 0,
                    'order_price_tax' => 0
                ]
            ];
        }
        
        $this->set('title', __('Zamówienia'));
        $this->set('content_title', __('Zamówienia'));
        $this->set('content_subtitle', __('Dodawanie zamówienia'));
        $this->set('buttons_template', 'Buttons/KeyOrders');
        
        $this->set('order', $this->request->data);
        $this->set('create', true);
        
        $taxRatesFull = $this->Product->TaxRate->find('all', array(
            'fields' => array(
                'TaxRate.id',
                'TaxRate.description',
                'TaxRate.rate'
            )
        ));
        list($userAddresses, $defaultBillingUserAddressId, $defaultShippingUserAddressId) = $this->Order->getAddressesListAndData(isset($this->request->data['Order']['user_id']) ? $this->request->data['Order']['user_id'] : 0, isset($this->request->data['Order']['id']) ? $this->request->data['Order']['id'] : 0);
        
        $shippingsFull = array();
        $payments = array();
        if ($this->request->is('post')) {
            $params = array(
                'weight' => 0,
                'products_price_tax' => $this->request->data['Order']['products_price_tax'],
                'products_amount' => 0,
                'country_id' => isset($this->request->data['ShippingOrderUserAddress']['country_id']) ? $this->request->data['ShippingOrderUserAddress']['country_id'] : 0
            );
            
            if (isset($this->request->data['OrderProduct'])) {
                foreach ( $this->request->data['OrderProduct'] as $orderProduct ) {
                    $params['weight'] += floatVal($orderProduct['weight']);
                    $params['products_amount'] += floatVal($orderProduct['amount']);
                }
            }
            
            $shippingsFull = $this->Order->getShippingForOrder(0, $params);
            $payments = $this->Order->getPaymentsForOrder(0, array(
                'shipping_id' => $this->request->data['Order']['shipping_id']
            ));
        }
        
        $orderStatusesFull = $this->Order->OrderStatus->find('all', array(
            'order' => 'OrderStatus.sort_order'
        ));
        
        $this->set(compact('taxRatesFull', 'userAddresses', 'defaultBillingUserAddressId', 'defaultShippingUserAddressId', 'shippingsFull', 'payments', 'orderStatusesFull'));
        
        $this->render('KeyOrders/admin_form');
    }



    protected function fillRequestData($data) {
        if (! isset($data['Customer']) && isset($data['Order']['user_id']) && $data['Order']['user_id'] > 0) {
            $this->Customer->contain(array(
                'UserData',
                'UserAddress'
            ));
            $customer = $this->Order->Customer->findById($data['Order']['user_id']);
            
            $data['Customer'] = $customer['Customer'];
            $data['Customer']['UserData'] = $customer['UserData'];
            $data['Customer']['UserAddress'] = $customer['UserAddress'];
        }
        
        $address_update = explode(',', $data['Order']['address_update']);
        
        if (! isset($data['BillingOrderUserAddress']) && isset($data['Order']['billing_order_address_id']) && ! empty($data['Order']['billing_order_address_id'])) {
            $update = false;
            if (substr($data['Order']['billing_order_address_id'], 0, 2) == '0-') {
                $order_address_id = substr($data['Order']['billing_order_address_id'], 2);
                if (in_array($order_address_id, $address_update)) {
                    $update = true;
                }
            }
            $billingAddress = $this->Order->BillingOrderUserAddress->getOrderUserAddressData($data['Order']['billing_order_address_id'], $update);
            
            $data['BillingOrderUserAddress'] = $billingAddress;
        }
        
        if (! isset($data['ShippingOrderUserAddress']) && isset($data['Order']['shipping_order_address_id']) && ! empty($data['Order']['shipping_order_address_id'])) {
            $update = false;
            if (substr($data['Order']['shipping_order_address_id'], 0, 2) == '0-') {
                $order_address_id = substr($data['Order']['shipping_order_address_id'], 2);
                if (in_array($order_address_id, $address_update)) {
                    $update = true;
                }
            }
            $shippingAddress = $this->Order->ShippingOrderUserAddress->getOrderUserAddressData($data['Order']['shipping_order_address_id'], $update);
            
            $data['ShippingOrderUserAddress'] = $shippingAddress;
        }
        
        if (! isset($data['Shipping']) && isset($data['Order']['shipping_id']) && $data['Order']['shipping_id'] > 0) {
            $shipping = $this->Order->Shipping->findById($data['Order']['shipping_id']);
            $data['Shipping'] = $shipping['Shipping'];
        }
        
        if (! isset($data['Payment']) && isset($data['Order']['payment_id']) && $data['Order']['payment_id'] > 0) {
            $payment = $this->Order->Payment->findById($data['Order']['payment_id']);
            $data['Payment'] = $payment['Payment'];
        }
        
        if (! isset($data['OrderStatus']) && isset($data['Order']['order_status_id']) && $data['Order']['order_status_id'] > 0) {
            $order_status = $this->Order->OrderStatus->findById($data['Order']['order_status_id']);
            $data['OrderStatus'] = $order_status['OrderStatus'];
        }
        
        if (isset($data['OrderProduct'])) {
            foreach ( $data['OrderProduct'] as $key => $orderProduct ) {
                if (! isset($orderProduct['Product']) && $orderProduct['product_id'] > 0) {
                    $product = $this->Order->OrderProduct->Product->findById($orderProduct['product_id']);
                    $data['OrderProduct'][$key]['Product'] = $product['Product'];
                }
                if (! isset($orderProduct['Unit']) && $orderProduct['unit_id'] > 0) {
                    $unit = $this->Order->OrderProduct->Unit->findById($orderProduct['unit_id']);
                    $data['OrderProduct'][$key]['Unit'] = $unit['Unit'];
                }
            }
        }
        
        if (! isset($data['OrderStatusHistory']) && isset($data['Order']['id']) && $data['Order']['id'] > 0) {
            $this->Order->OrderStatusHistory->contain(array(
                'User',
                'OrderStatus'
            ));
            $order_status_history = $this->Order->OrderStatusHistory->find('all', array(
                'conditions' => array(
                    'order_id' => $data['Order']['id']
                )
            ));
            $data['OrderStatusHistory'] = array();
            foreach ( $order_status_history as $item ) {
                $itemN = $item['OrderStatusHistory'];
                $itemN['User'] = $item['User'];
                $itemN['OrderStatus'] = $item['OrderStatus'];
                $data['OrderStatusHistory'][] = $itemN;
            }
        }
        
        return $data;
    }



    public function admin_delete($id) {
        $order = $this->Order->findById($id);
        
        if ($this->Order->delete($id, false)) {
            $this->OrderProduct->deleteAll(array(
                'OrderProduct.order_id' => $id
            ));
            $this->Session->setFlash(__('Zamówienie "%s" zostało usunięte.', h($order['Order']['number'])), 'flash-success');
        } else {
            $this->Session->setFlash(__('Nie udało się usunąć zamówienia "%s".', h($order['Order']['number'])), 'flash-error');
        }
        
        return $this->redirect(array(
            'action' => 'index'
        ));
    }



    public function admin_get_customers_table() {
        $query = $this->request->query['query'];
        $customers = $conditions = array();
        
        if (! empty($query)) {
            $conditions['OR'] = array(
                'Customer.first_name LIKE' => "%{$query}%",
                'Customer.last_name LIKE' => "%{$query}%",
                'Customer.company LIKE' => "%{$query}%",
                'Customer.contact_phone LIKE' => "%{$query}%",
                'Customer.customer LIKE' => "%{$query}%"
            );
        }
        
        if (! empty($conditions)) {
            $this->Customer->recursive = 1;
            $customers = $this->Customer->find('all', array(
                'conditions' => $conditions
            ));
        }
        
        $view = new View($this, false);
        $view->viewPath = 'KeyOrders';
        $view->layout = false;
        $view->set('customers', $customers);
        $view->set('empty_query', empty($query));
        $html = $view->render('customers_table');
        
        $this->RequestHandler->renderAs($this, 'json');
        $this->set('jsonp', true);
        $this->set('result', array(
            'html' => $html
        ));
        $this->set('_serialize', array(
            'result'
        ));
    }



    public function admin_get_customer_info() {
        $customer_id = $this->request->query['customer_id'];
        $this->Customer->recursive = 1;
        $customer = $this->Customer->findById($customer_id);
        
        $view = new View($this, false);
        $view->viewPath = 'KeyOrders';
        $view->layout = false;
        $view->set('user', $customer['Customer']);
        $view->set('userData', $customer['UserData']);
        $view->set('userAddress', isset($customer['UserAddress'][0]) ? $customer['UserAddress'][0] : null);
        $html = $view->render('customer_info');
        
        $this->RequestHandler->renderAs($this, 'json');
        $this->set('jsonp', true);
        $this->set(array(
            'html' => $html,
            'user' => $customer
        ));
        $this->set('_serialize', array(
            'html',
            'user'
        ));
    }



    public function admin_get_product_row() {
        $product_id = ( int ) $this->request->query['product_id'];
        $products_products_option_id = ( int ) $this->request->query['products_products_option_id'];
        $key = ( int ) $this->request->query['key'];
        
        $product = $this->OrderProduct->Product->find('first', array(
            'conditions' => array(
                'Product.id' => $product_id
            )
        ));
        
        if ($products_products_option_id > 0) {
            $this->OrderProduct->Product->ProductsProductsOption->contain(array(
                'ProductOption',
                'ProductOptionValue'
            ));
            $product_option = $this->OrderProduct->Product->ProductsProductsOption->find('first', array(
                'conditions' => array(
                    'ProductsProductsOption.id' => $products_products_option_id
                )
            ));
        }
        
        $unit = $this->OrderProduct->Product->Unit->find('first', array(
            'conditions' => array(
                'Unit.id' => $product['Product']['unit_id']
            )
        ));
        
        $catalog_no = $product['Product']['catalog_no'];
        if (isset($product_option) && ! empty($product_option['ProductsProductsOption']['catalog_no'])) {
            $catalog_no = $product_option['ProductsProductsOption']['catalog_no'];
        }
        
        $orderProduct = array(
            'product_id' => $product_id,
            'name' => $product['Product']['name'],
            'add_info' => empty($catalog_no) ? '' : __('Nr kat.:') . ' ' . $catalog_no,
            'catalog_no' => $product['Product']['catalog_no'],
            'ext_code' => $product['Product']['ext_code'],
            'ean' => $product['Product']['ean'],
            'pkwiu' => $product['Product']['pkwiu'],
            'weight' => $product['Product']['weight'],
            
            'price' => $product['Product']['price'],
            'tax_rate_id' => $product['Product']['tax_rate_id'],
            'tax' => $product['Product']['tax'],
            'price_tax' => $product['Product']['price_tax'],
            
            'manufacturer_id' => $product['Product']['manufacturer_id'],
            'shipping_time_id' => $product['Product']['shipping_time_id'],
            'product_warranty_id' => $product['Product']['product_warranty_id'],
            'product_condition_id' => $product['Product']['product_condition_id'],
            'unit_id' => $product['Product']['unit_id'],
            'products_products_option_id' => $products_products_option_id,
            'discount' => 0,
            
            'Product' => $product['Product'],
            'Unit' => $unit['Unit']
        );
        
        if (isset($product_option)) {
            $orderProduct['add_info'] .= (empty($orderProduct['add_info']) ? '' : __(', ')) . $product_option['ProductOption']['name'] . ': ' . $product_option['ProductOptionValue']['name'];
            $orderProduct['price'] = $product_option['ProductsProductsOption']['change_price'];
            $orderProduct['tax'] += $product_option['ProductsProductsOption']['change_price_tax'] - $product_option['ProductsProductsOption']['change_price'];
            $orderProduct['price_tax'] = $product_option['ProductsProductsOption']['change_price_tax'];
        }
        
        if (( float ) $product['Product']['order_min_quantity'] > 0) {
            $orderProduct['amount'] = $product['Product']['order_min_quantity'];
        } else {
            $orderProduct['amount'] = 1;
        }
        $orderProduct['total_price_tax'] = round($orderProduct['amount'] * $orderProduct['price_tax'], 2);
        
        $taxRatesFull = $this->OrderProduct->Product->TaxRate->find('all', array(
            'fields' => array(
                'TaxRate.id',
                'TaxRate.description',
                'TaxRate.rate'
            )
        ));
        
        $this->request->data = array(
            'OrderProduct' => array(
                $key => $orderProduct
            )
        );
        
        $view = new View($this, false);
        $view->viewPath = 'KeyOrders';
        $view->layout = false;
        $view->set(compact('key', 'orderProduct', 'taxRatesFull'));
        $html = $view->render('product_row');
        
        $this->RequestHandler->renderAs($this, 'json');
        $this->set('jsonp', true);
        $this->set(array(
            'html' => $html
        ));
        $this->set('_serialize', array(
            'html'
        ));
    }



    public function admin_get_order_product_preview() {
        $order_product_id = ( int ) $this->request->query['order_product_id'];
        
        $this->OrderProduct->contain(array(
            'Product.ProductsImage' => array(
                'limit' => 1,
                'order' => array(
                    'ProductsImage.sort_order' => 'ASC'
                )
            ),
            'Manufacturer',
            'ShippingTime',
            'ProductWarranty',
            'ProductCondition',
            'Unit'
        ));
        $orderProduct = $this->OrderProduct->findById($order_product_id);
        
        $view = new View($this, false);
        $view->viewPath = 'KeyOrders';
        $view->layout = false;
        $view->set(compact('orderProduct'));
        $html = $view->render('product_preview');
        
        $this->RequestHandler->renderAs($this, 'json');
        $this->set('jsonp', true);
        $this->set(array(
            'html' => $html,
            'lang' => array(
                'title' => __('Szczegóły pozycji zamówienia'),
                'btn_ok_title' => __('OK'),
                'btn_product_title' => __('Pokaż aktualny produkt')
            ),
            'order_product' => $orderProduct
        ));
        $this->set('_serialize', array(
            'html',
            'lang',
            'order_product'
        ));
    }



    public function admin_get_order_user_address_data() {
        $order_user_address_id = $this->request->query['order_user_address_id'];
        $update = $this->request->query['update'];
        
        $orderUserAddress = $this->Order->BillingOrderUserAddress->getOrderUserAddressData($order_user_address_id, in_array($order_user_address_id, $update));
        $html = '';
        if (! empty($order_user_address_id)) {
            $html = $this->Customer->UserAddress->getFormattedAddress($orderUserAddress, '<br>');
        }
        
        $this->RequestHandler->renderAs($this, 'json');
        $this->set('jsonp', true);
        $this->set(array(
            'html' => $html,
            'orderUserAddress' => $orderUserAddress
        ));
        $this->set('_serialize', array(
            'html',
            'orderUserAddress'
        ));
    }



    public function admin_get_order_user_addresses_list() {
        $user_id = ( int ) $this->request->query['user_id'];
        $order_id = ( int ) $this->request->query['order_id'];
        $update = $this->request->query['update'];
        
        list($userAddresses, $defaultBillingUserAddressId, $defaultShippingUserAddressId) = $this->Order->getAddressesListAndData($user_id, $order_id, $update);
        
        $this->RequestHandler->renderAs($this, 'json');
        $this->set('jsonp', true);
        $this->set(compact('userAddresses', 'defaultBillingUserAddressId', 'defaultShippingUserAddressId'));
        $this->set('_serialize', array(
            'userAddresses',
            'defaultBillingUserAddressId',
            'defaultShippingUserAddressId'
        ));
    }



    public function admin_get_shippings_list() {
        $params = array(
            'weight' => ( float ) $this->request->query['weight'],
            'products_price_tax' => ( float ) $this->request->query['products_price_tax'],
            'products_amount' => ( float ) $this->request->query['products_amount'],
            'country_id' => ( int ) $this->request->query['country_id']
        );
        
        $shippingsFull = $this->Order->getShippingForOrder(0, $params, array(
            0,
            1
        ));
        
        $this->RequestHandler->renderAs($this, 'json');
        $this->set('jsonp', true);
        $this->set(compact('shippingsFull'));
        $this->set('_serialize', array(
            'shippingsFull'
        ));
    }



    public function admin_get_payments_list() {
        $params = array(
            'shipping_id' => ( int ) $this->request->query['shipping_id']
        );
        
        $payments = $this->Order->getPaymentsForOrder(0, $params);
        
        $this->RequestHandler->renderAs($this, 'json');
        $this->set('jsonp', true);
        $this->set(compact('payments'));
        $this->set('_serialize', array(
            'payments'
        ));
    }



    public function admin_get_products_products_option_form() {
        $product_id = ( int ) $this->request->query['product_id'];
        
        $this->OrderProduct->Product->contain(array(
            'ProductsProductsOption',
            'ProductsProductsOption.ProductOption',
            'ProductsProductsOption.ProductOptionValue'
        ));
        $product = $this->OrderProduct->Product->findById($product_id);
        
        $view = new View($this, false);
        $view->viewPath = 'KeyOrders';
        $view->layout = false;
        $view->set(compact('product'));
        $html = $view->render('products_products_option_choose');
        
        $this->RequestHandler->renderAs($this, 'json');
        $this->set('jsonp', true);
        $this->set(array(
            'result' => array(
                'optionChoose' => (isset($product['ProductsProductsOption']) && ! empty($product['ProductsProductsOption'])),
                'html' => $html,
                'product' => $product
            )
        ));
        $this->set('_serialize', array(
            'result'
        ));
    }



    public function admin_generate_orders_report() {
        $date_from = $this->request->query['from'] . ' 00:00:00';
        $date_to = $this->request->query['to'] . ' 23:59:59';
        $type = $this->request->query['type'];
        
        $this->Reports->setController($this);
        $this->Reports->generateOrdersReport($date_from, $date_to, $type);
    }



    public function admin_generate_invoice($id) {
        $this->Invoice->contain(array(
            'Order',
            'Payment'
        ));
        $invoice = $this->Invoice->find('first', array(
            'conditions' => array(
                'order_id' => $id,
                'type' => 'INVOICE'
            )
        ));
        
        $this->Invoices->setController($this);
        if (empty($invoice)) {
            $response = $this->Invoices->saveOrderInvoice($id, $this->request->data);
        } else {
            $response = $this->Invoices->updateOrderInvoice($invoice['Invoice']['id'], $this->request->data);
        }
        
        $fpath = $this->Invoice->getPdfPath($response['model']);
        
        header("Cache-Control: public, must-revalidate");
        header("Pragma: hack"); // WTF? oh well, it works...
        header("Content-Type: text/csv");
        header('Content-Disposition: attachment; filename="F-' . str_replace('/', '-', $response['model']['Invoice']['number_all']) . '.pdf"');
        header("Content-Transfer-Encoding: binary\n");
        header("Content-Length: " . filesize($fpath));
        echo file_get_contents($fpath);
        die();
    }



    public function admin_invoice_template($orderId) {
        $this->Invoice->contain(array());
        $invoice = $this->Invoice->find('first', array(
            'conditions' => array(
                'order_id' => $orderId,
                'type' => 'INVOICE'
            )
        ));
        $order = $this->Order->read(null, $orderId);
        $payments = $this->Order->Payment->find('list');
        
        if (! empty($invoice)) {
            $this->request->data = $invoice;
        } else {
            $this->request->data = array(
                'Invoice' => array(
                    'payment_id' => $order['Order']['payment_id'],
                    'payment_type' => (! empty($order['Order']['paid'])) ? 'Zapłacono' : 'Termin płatności',
                    'sell_date' => date('Y-m-d', strtotime($order['Order']['created'])),
                    'set_date' => date('Y-m-d'),
                    'paid_date' => (empty($order['Order']['paid'])) ? date('Y-m-d', strtotime("+7 days")) : date('Y-m-d', strtotime($order['Order']['created']))
                )
            );
        }
        
        $this->layout = 'ajax';
        $this->set(compact('order', 'payments', 'orderId'));
    }



    public function admin_status_template($orderId) {
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            
            $this->Order->updateAll(array(
                'order_status_id' => $this->request->data['Order']['order_status_id'],
                'shipment_no' => (! empty($this->request->data['Order']['shipment_no'])) ? "'{$this->request->data['Order']['shipment_no']}'" : null
            ), array(
                'Order.id' => $orderId
            ));
            
            // $this->Order->contain(array('Invoice', 'Customer', 'Shipping', 'Payment', 'OrderStatus', 'BillingOrderUserAddress.UserAddress', 'ShippingOrderUserAddress.UserAddress'));
            $this->Order->recursive = 2;
            $orderForNotify = $this->Order->read(null, $orderId);
            $userForNotify = $this->Order->Customer->findById($orderForNotify['Order']['user_id']);
            $adminNotificationParams = $this->NotificationCenter->getBody($orderForNotify['OrderStatus']['code'], $orderForNotify, $userForNotify);
            
            $attachment = $attachmentName = null;
            if (! empty($this->request->data['Order']['send_invoice'])) {
                $attachment = $this->Invoice->getPdfPath($orderForNotify);
                $attachmentName = 'F-' . Inflector::slug($orderForNotify['Invoice']['number_all'], '-') . '.pdf';
            }
            
            $this->loadModel('QueueEmail');
            
            $this->QueueEmail->create();
            $toSave = array(
                'QueueEmail' => array(
                    'to' => $orderForNotify['Customer']['email'],
                    'subject' => $adminNotificationParams['subject'],
                    'body' => $adminNotificationParams['message'],
                    'attachment_name' => $attachmentName,
                    'attachment' => $attachment
                )
            );
            $this->QueueEmail->save($toSave);
            $this->Session->setFlash(__('Status został zmieniony.'), 'flash-success');
            return $this->redirect(array(
                'action' => 'index'
            ));
        }
        
        $order = $this->Order->read(null, $orderId);
        $orderStatuses = $this->Order->OrderStatus->find('list', array(
            'order' => 'OrderStatus.sort_order'
        ));
        
        $this->request->data = $order;
        $this->layout = 'ajax';
        $this->set(compact('order', 'orderStatuses'));
    }



    /**
     */
    public function admin_update_paid_status($orderId, $paid) {
        if (! $this->Order->exists($orderId)) {
            throw new Exception('Order is not existing');
        }
        $result = $this->Order->updateAll(array(
            'paid' => $paid
        ), array(
            'Order.id' => $orderId
        ));
        
        die($paid);
    }



    /**
     */
    public function admin_set_archive() {
        $result = $this->Order->updateAll(array(
            'active' => 0
        ), array(
            'Order.id' => $this->request->data['check_order_id']
        ));
        
        die('1');
    }



    /**
     */
    public function admin_preview($orderId) {
        $this->layout = 'ajax';
        $this->Order->contain(array(
            'BillingOrderUserAddress',
            'ShippingOrderUserAddress',
            'InvoiceOrderUserAddress',
            'Shipping',
            'Payment',
            'OrderStatus',
            'OrderProduct.Product.ProductsImage',
            'OrderProduct.ProductsProductsOption',
            'Discount'
        ));
        $order = $this->Order->read(null, $orderId);
        
        $this->set(compact('order'));
    }
}
