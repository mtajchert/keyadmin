<?php

App::uses('AdminController', 'KeyAdmin.Controller');

class KeyProductAvailabilitiesController extends AdminController {

  public $uses = array('KeyAdmin.ProductAvailability');
  public $components = array('Paginator', 'RequestHandler');
  public $paginate = array(
    'limit' => 20,
  );

  public function beforeFilter() {
    parent::beforeFilter();
    $this->Paginator->settings = $this->paginate;
  }

  public function admin_index() {
    $this->set('product_availabilities', $this->Paginator->paginate());
    
    $this->set('title', __('Dostępności produktów'));
    $this->set('content_title', __('Dostępności produktów'));
    $this->set('content_subtitle', __('Lista dostępności produktów'));
    $this->set('buttons_template', 'Buttons/KeyProductAvailabilities');
  }

  public function admin_edit($id) {
    if (!$id) {
      throw new NotFoundException(__('Wybrana dostępność produktów nie istnieje'));
    }

    $product_availability = $this->ProductAvailability->findById($id);
    if (!$product_availability) {
      throw new NotFoundException(__('Nie odnaleziono wybranej dostępności produktów'));
    }

    if ($this->request->is(array('post', 'put'))) {
      $this->ProductAvailability->id = $id;
      if ($this->ProductAvailability->save($this->request->data)) {
        $this->Session->setFlash(__('Dostępność produktów została zapisana.'), 'flash-success');
        return $this->redirect(array('action' => 'index'));
      }
      $this->Session->setFlash(__('Nie udało się zapisać dostępności produktów. Sprawdź poprawność podanych danych.'), 'flash-error');
    }

    if (!$this->request->data) {
      $this->request->data = $product_availability;
    }

    $this->set('title', __('Dostępności produktów'));
    $this->set('content_title', __('Dostępności produktów'));
    $this->set('content_subtitle', __('Edycja dostępności produktów'));
    $this->set('buttons_template', 'Buttons/KeyProductAvailabilities');
    
    $this->set('product_availability', $product_availability);
    
    $this->render('KeyProductAvailabilities/admin_form');
  }

  public function admin_create() {
    if ($this->request->is('post')) {
      $this->ProductAvailability->create();
      
      if ($this->ProductAvailability->save($this->request->data)) {
        $this->Session->setFlash(__('Dostępność produktów została dodana.'), 'flash-success');
        return $this->redirect(array('action' => 'index'));
      }
      $this->Session->setFlash(__('Nie udało się dodać dostępności produktów. Sprawdź poprawność podanych danych.'), 'flash-error');
    } else {
      $this->request->data = ['ProductAvailability' => ['shipping_mode' => 1]];
    }

    $this->set('title', __('Dostępności produktów'));
    $this->set('content_title', __('Dostępności produktów'));
    $this->set('content_subtitle', __('Dodawanie dostępności produktów'));
    $this->set('buttons_template', 'Buttons/KeyProductAvailabilities');
    
    $this->set('product_availability', $this->request->data);
    
    $this->render('KeyProductAvailabilities/admin_form');
  }

  public function admin_delete($id) {
    $product_availability = $this->ProductAvailability->findById($id);
    
    if ($this->ProductAvailability->delete($id)) {
      $this->Session->setFlash(__('Dostępność produktów "%s" została usunięta.', h($product_availability['ProductAvailability']['name'])), 'flash-success');
    } else {
      $this->Session->setFlash(__('Nie udało się usunąć dostępności produktów "%s".', h($product_availability['ProductAvailability']['name'])), 'flash-error');
    }

    return $this->redirect(array('action' => 'index'));
  }

}
