<?php

App::uses('AdminController', 'KeyAdmin.Controller');

class KeyDashboardController extends AdminController {
  
  public $components = array('KeyAdmin.NotificationCenter');
  
  /**
   * 
   */
  public function admin_index(){
    $this->set('content_title', __('Dashboard'));
  }
  
  public function admin_pass_request() {
    $found = 0;
    $sent = 0;
    
    $this->Customer = ClassRegistry::init('KeyAdmin.Customer');
    
    $this->Customer->recursive = 2;
    $customers = $this->Customer->find('all', array(
      'conditions' => array(
        'OR' => array(
          array('Customer.hash' => null),
          array('Customer.hash = "" ')
        )
      )
    ));
    
    foreach ($customers as $customer) {
      if (preg_match('/^[a-zA-Z0-9._\+-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,4}$/', $customer['Customer']['email'])) {
        $found++;
        
        $customer['Customer']['hash'] = md5(uniqid(mt_rand(), true).time());
        $this->Customer->updateAll(array('Customer.hash' => "'".$customer['Customer']['hash']."'"), array('Customer.id' => $customer['Customer']['id']));
        
        if ($this->NotificationCenter->send('new_password_request', array($customer['Customer']['email'], trim($customer['Customer']['first_name'].' '.$customer['Customer']['last_name'])), $customer)) {
          $sent++;
        }
      }
    }
    
    die("znaleziono do wyslania: {$found}\r\nwyslano do:{$sent}");
    
    $this->render('KeyDashboard/admin_index');
  }
  
  public function admin_replace_desc_links() {
    $this->Product = ClassRegistry::init('KeyAdmin.Product');
    $products = $this->Product->find('all', array(
      'conditions' => array(
        'OR' => array(
          'Product.description LIKE' => '%.html%',
          'Product.short_description LIKE' => '%.html%'
        )
      )
    ));
    
    $countProd = 0;
    $idsProd = [];
    $this->Product->belongsTo = [];
    $this->Product->hasMany = [];
    $this->Product->hasAndBelongsToMany = [];
    foreach ($products as $product) {
      if (strpos($product['Product']['short_description'], '.html') !== false) {
        $product['Product']['short_description'] = $this->replaceLinks($product['Product']['short_description']);
        $this->Product->updateAll(
            array('Product.short_description' => "'".$product['Product']['short_description']."'"), array('Product.id' => $product['Product']['id'])
        );
      }
      if (strpos($product['Product']['description'], '.html') !== false) {
        $product['Product']['description'] = $this->replaceLinks($product['Product']['description']);
        $this->Product->updateAll(
            array('Product.description' => "'".$product['Product']['description']."'"), array('Product.id' => $product['Product']['id'])
        );
      }
      $idsProd[] = $product['Product']['id'];
      $countProd++;
    }
    
    
    
    $this->Category = ClassRegistry::init('KeyAdmin.Category');
    $categories = $this->Category->find('all', array(
      'conditions' => array(
        'OR' => array(
          'Category.description LIKE' => '%.html%'
        )
      )
    ));
    
    $countCats = 0;
    $idsCats = [];
    $this->Category->belongsTo = [];
    $this->Category->hasMany = [];
    $this->Category->hasAndBelongsToMany = [];
    foreach ($categories as $category) {
      if (strpos($category['Category']['description'], '.html') !== false) {
        $category['Category']['description'] = $this->replaceLinks($category['Category']['description']);
        $this->Category->updateAll(
            array('Category.description' => "'".$category['Category']['description']."'"), array('Category.id' => $category['Category']['id'])
        );
      }
      $idsCats[] = $category['Category']['id'];
      $countCats++;
    }
    
    die("poprawiono opisy produktów: {$countProd}\r\nId produktów: ".implode(', ', $idsProd)."\r\n\r\npoprawiono opisy kategorii: {$countCats}\r\nId kategorii: ".implode(', ', $idsCats));
  }
  
  protected function replaceLinks($text) {
    $arrChange = array(
      '/regulamin-pm-5.html' => '/regulamin',
      '/kontakt-f-1.html' => '/kontakt',
      '/platnosci-i-dostawa-pm-3.html' => '/platnosc-i-dostawa-s24',
      '/dane-firmy-pm-11.html' => '/kontakt',
      '/jak-dojechac-pm-10.html' => '/kontakt',
      '/logowanie.html' => '/logowanie',
      '/rejestracja.html' => '/rejestracja',
      '/certyfikat-prokonsumencki-pm-21.html' => '/certyfikat-prokonsumencki-s29',
      '/polityka-prywatnosci-pm-19.html' => '/polityka-prywatnosci',
      '/regulamin-pm-5.html' => '/regulamin',
      '/reklamacja-towaru-pm-17.html' => '/reklamacja-towaru-s30',
      '/odstapienie-od-umowy-pm-18.html' => '/odstapienie-od-umowy-s31'
    );
    
    foreach ($arrChange as $search => $replace) {
      $text = str_replace($search, $replace, $text);
    }
    
    $text = preg_replace('@alcatras.pl/(.*?)-c-([0-9]+).html@i', 'alcatras.pl/$1-c$2', $text);
    $text = preg_replace('@alcatras.pl/(.*?)-p-([0-9]+).html@', 'alcatras.pl/$1-p$2', $text);
    
    return $text;
  }
}
