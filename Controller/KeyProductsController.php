<?php
App::uses('AdminController', 'KeyAdmin.Controller');

class KeyProductsController extends AdminController
{

    public $uses = array(
        'KeyAdmin.Product',
        'KeyAdmin.ProductsCategory',
        'KeyAdmin.ProductsProductsOption'
    );

    public $components = array(
        'Paginator',
        'RequestHandler'
    );

    public $paginate = array(
        'limit' => 20
    );

    public $helpers = array(
        'KeyAdmin.Category',
        'KeyAdmin.Product'
    );

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Paginator->settings = $this->paginate;
    }

    public function admin_index()
    {
        $this->Product->contain(array(
            'ProductsImage'
        ));
        $this->ProductsCategory->recursive = 2;
        
        $parentId = 0;
        if (isset($this->request->params['named']['parent_id']) && $this->request->params['named']['parent_id'] != 0) {
            $parentId = explode('_', $this->request->params['named']['parent_id']);
            $parentId = (int) end($parentId);
            $this->Paginator->settings['conditions'] = array(
                'ProductsCategory.category_id' => $parentId
            );
        } else {
            $this->Paginator->settings['group'] = array(
                'ProductsCategory.product_id'
            );
        }
        
        // print_r($this->Paginator->paginate('ProductsCategory'));die;
        $this->set('parent_id', $parentId);
        $this->Paginator->settings['order'] = array(
            'Product.created' => 'DESC'
        );
        $this->set('products', $this->Paginator->paginate('ProductsCategory'));
        
        $this->set('title', __('Produkty'));
        $this->set('content_title', __('Produkty'));
        $this->set('content_subtitle', __('Lista produktów'));
        $this->set('buttons_template', 'Buttons/KeyProducts');
    }

    public function admin_edit($id)
    {
        if (! $id) {
            throw new NotFoundException(__('Wybrany produkt nie istnieje'));
        }
        
        $this->Product->contain(array(
            'ProductsImage',
            'ProductsProductsOption',
            'ProductsProductsOption.ProductOption',
            'ProductsProductsOption.ProductOptionValue',
            'Category',
            'Shipping',
            'RelProduct'
        ));
        $product = $this->Product->findById($id);
        if (! $product) {
            throw new NotFoundException(__('Nie odnaleziono wybranego produktu'));
        }
        // print_r($product);die;
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $this->Product->id = $id;
            $this->request->data['Product']['id'] = $id;
            $this->request->data['RelProduct']['RelProduct'] = Hash::extract($this->request->data['RelProduct']['RelProduct'], '{n}[rel_product_id!=0]');
            if ($this->Product->saveAll($this->request->data)) {
                $this->ProductsCategory->updateAll(array(
                    'category_default' => 0
                ), array(
                    'ProductsCategory.product_id' => $id,
                    'ProductsCategory.category_default' => 1
                ));
                $this->ProductsCategory->updateAll(array(
                    'category_default' => 1
                ), array(
                    'ProductsCategory.product_id' => $id,
                    'ProductsCategory.category_id' => $this->request->data['Product']['default_category_id']
                ));
                
                $this->Session->setFlash(__('Produkt został zapisany.'), 'flash-success');
                
                $this->Product->contain(array(
                    'Category'
                ));
                $product = $this->Product->read(null, $id);
                $categoryId = Hash::extract($product, 'Category.{n}.ProductsCategory[category_default=1].category_id');
                $nested = $this->Product->Category->getNestedCategories($categoryId[0]);
                $categories = Hash::extract($nested, '{n}.Category.id');
                return $this->redirect(array(
                    'action' => 'index',
                    'parent_id' => implode('_', $categories)
                ));
            } else {
                $product['default_category_id'] = $this->request->data['Product']['default_category_id'];
                $this->Session->setFlash(__('Nie udało się zapisać produktu. Sprawdź poprawność podanych danych na wszystkich zakładkach.'), 'flash-error');
                
                $this->request->data = $this->fillRequestData($this->request->data);
            }
        } else {
            $defaultCategory = $this->Product->ProductsCategory->find('first', array(
                'conditions' => array(
                    'product_id' => $id,
                    'category_default' => 1
                )
            ));
            $product['Product']['default_category_id'] = empty($defaultCategory) ? 0 : $defaultCategory['ProductsCategory']['category_id'];
            
            $relProducts = array();
            foreach ($product['RelProduct'] as $item) {
                $relProducts[] = array(
                    'rel_product_id' => $item['ProductProductsRel']['rel_product_id'],
                    'option_id' => $item['ProductProductsRel']['option_id']
                );
            }
            $product['RelProduct'] = array(
                'RelProduct' => $relProducts
            );
        }
        
        $productsList = array(
            '0' => 'Wybierz produkt'
        ) + $this->Product->find('list', array(
            'order' => array(
                'Product.name' => 'ASC'
            ),
            'conditions' => array(
                'Product.status' => 1
            )
        ));
        $this->set('productsList', $productsList);
        
        if (! $this->request->data) {
            $this->request->data = $product;
        }
        
        $this->set('title', __('Produkty'));
        $this->set('content_title', __('Produkty'));
        $this->set('content_subtitle', __('Edycja produktu'));
        $this->set('buttons_template', 'Buttons/KeyProducts');
        
        $this->set('product', $product);
        $this->set('title', __('Produkty'));
        
        $manufacturers = $this->Product->Manufacturer->find('list', array(
            'fields' => array(
                'Manufacturer.id',
                'Manufacturer.name'
            )
        ));
        $productAvailabilities = $this->Product->ProductAvailability->find('list', array(
            'fields' => array(
                'ProductAvailability.id',
                'ProductAvailability.name'
            )
        ));
        $shippingTimes = $this->Product->ShippingTime->find('list', array(
            'fields' => array(
                'ShippingTime.id',
                'ShippingTime.name'
            )
        ));
        $productConditions = $this->Product->ProductCondition->find('list', array(
            'fields' => array(
                'ProductCondition.id',
                'ProductCondition.name'
            )
        ));
        $productWarranties = $this->Product->ProductWarranty->find('list', array(
            'fields' => array(
                'ProductWarranty.id',
                'ProductWarranty.name'
            )
        ));
        $units = $this->Product->Unit->find('list', array(
            'fields' => array(
                'Unit.id',
                'Unit.name'
            )
        ));
        $taxRatesFull = $this->Product->TaxRate->find('all', array(
            'fields' => array(
                'TaxRate.id',
                'TaxRate.description',
                'TaxRate.rate'
            )
        ));
        $shippings = $this->Product->Shipping->find('list', array(
            'fields' => array(
                'Shipping.id',
                'Shipping.name'
            )
        ));
        $productOptions = $this->Product->ProductsProductsOption->ProductOption->find('list', array(
            'fields' => array(
                'ProductOption.id',
                'ProductOption.name'
            )
        ));
        $productOptionValues = array();
        if (isset($product['ProductsProductsOption']) && ! empty($product['ProductsProductsOption'])) {
            $option_id = $product['ProductsProductsOption'][0]['product_option_id'];
            $productOptionValues = $this->Product->ProductsProductsOption->ProductOptionValue->find('list', array(
                'conditions' => array(
                    'product_option_id' => $option_id
                )
            ));
        }
        $this->set(compact('manufacturers', 'productAvailabilities', 'shippingTimes', 'productConditions', 'productWarranties', 'units', 'taxRatesFull', 'shippings', 'productOptions', 'productOptionValues'));
        
        $defaultTax = $this->Product->TaxRate->find('first', array(
            'conditions' => array(
                'TaxRate.default' => 1
            )
        ));
        $this->set('defaultTaxRateId', $defaultTax['TaxRate']['id']);
        
        $this->render('KeyProducts/admin_form');
    }

    public function admin_duplicate($productId)
    {
        if ($this->request->is('GET')) {
            $this->admin_edit($productId);
            $this->set('content_subtitle', __('Duplikuj produkt'));
        } else {
            $this->request->data['Product']['id'] = null;
            $this->request->data = Hash::insert($this->request->data, 'ProductsImage.{n}.id', null);
            $this->request->data = Hash::insert($this->request->data, 'ProductsProductsOption.{n}.id', null);
            $this->admin_create();
        }
    }

    public function admin_create()
    {
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->Product->create();
            $this->request->data['RelProduct']['RelProduct'] = Hash::extract($this->request->data['RelProduct']['RelProduct'], '{n}[rel_product_id!=0]');
            if ($this->Product->saveAll($this->request->data)) {
                $id = $this->Product->getLastInsertID();
                $this->ProductsCategory->updateAll(array(
                    'category_default' => 1
                ), array(
                    'ProductsCategory.product_id' => $id,
                    'ProductsCategory.category_id' => $this->request->data['Product']['default_category_id']
                ));
                
                $this->Session->setFlash(__('Produkt został dodany.'), 'flash-success');
                
                $this->Product->contain(array(
                    'Category'
                ));
                $product = $this->Product->read(null, $id);
                $categoryId = Hash::extract($product, 'Category.{n}.ProductsCategory[category_default=1].category_id');
                $nested = $this->Product->Category->getNestedCategories($categoryId[0]);
                $categories = Hash::extract($nested, '{n}.Category.id');
                return $this->redirect(array(
                    'action' => 'index',
                    'parent_id' => implode('_', $categories)
                ));
            }
            
            $this->Session->setFlash(__('Nie udało się dodać produktu. Sprawdź poprawność podanych danych.'), 'flash-error');
            
            $this->request->data = $this->fillRequestData($this->request->data);
        } else {
            $this->request->data = [
                'Product' => [
                    'default' => 0,
                    'sort_order' => 1
                ]
            ];
        }
        
        $productsList = array(
            '0' => 'Wybierz produkt'
        ) + $this->Product->find('list', array(
            'order' => array(
                'Product.name' => 'ASC'
            ),
            'conditions' => array(
                'Product.status' => 1
            )
        ));
        $this->set('productsList', $productsList);
        
        $this->set('title', __('Produkty'));
        $this->set('content_title', __('Produkty'));
        $this->set('content_subtitle', __('Dodawanie produktu'));
        $this->set('buttons_template', 'Buttons/KeyProducts');
        
        $this->set('product', $this->request->data);
        
        $manufacturers = $this->Product->Manufacturer->find('list', array(
            'fields' => array(
                'Manufacturer.id',
                'Manufacturer.name'
            )
        ));
        $productAvailabilities = $this->Product->ProductAvailability->find('list', array(
            'fields' => array(
                'ProductAvailability.id',
                'ProductAvailability.name'
            )
        ));
        $shippingTimes = $this->Product->ShippingTime->find('list', array(
            'fields' => array(
                'ShippingTime.id',
                'ShippingTime.name'
            )
        ));
        $productConditions = $this->Product->ProductCondition->find('list', array(
            'fields' => array(
                'ProductCondition.id',
                'ProductCondition.name'
            )
        ));
        $productWarranties = $this->Product->ProductWarranty->find('list', array(
            'fields' => array(
                'ProductWarranty.id',
                'ProductWarranty.name'
            )
        ));
        $units = $this->Product->Unit->find('list', array(
            'fields' => array(
                'Unit.id',
                'Unit.name'
            )
        ));
        $taxRatesFull = $this->Product->TaxRate->find('all', array(
            'fields' => array(
                'TaxRate.id',
                'TaxRate.description',
                'TaxRate.rate'
            )
        ));
        $shippings = $this->Product->Shipping->find('list', array(
            'fields' => array(
                'Shipping.id',
                'Shipping.name'
            )
        ));
        $productOptions = $this->Product->ProductsProductsOption->ProductOption->find('list', array(
            'fields' => array(
                'ProductOption.id',
                'ProductOption.name'
            )
        ));
        $productOptionValues = array();
        if (isset($product['ProductsProductsOption']) && ! empty($product['ProductsProductsOption'])) {
            $option_id = $product['ProductsProductsOption'][0]['product_option_id'];
            $productOptionValues = $this->Product->ProductsProductsOption->ProductOptionValue->find('list', array(
                'conditions' => array(
                    'product_option_id' => $option_id
                )
            ));
        }
        $this->set(compact('manufacturers', 'productAvailabilities', 'shippingTimes', 'productConditions', 'productWarranties', 'units', 'taxRatesFull', 'shippings', 'productOptions', 'productOptionValues'));
        
        $defaultTax = $this->Product->TaxRate->find('first', array(
            'conditions' => array(
                'TaxRate.default' => 1
            )
        ));
        $this->set('defaultTaxRateId', $defaultTax['TaxRate']['id']);
        
        $this->render('KeyProducts/admin_form');
    }

    public function admin_delete($id)
    {
        $product = $this->Product->findById($id);
        
        if ($this->Product->delete($id)) {
            $this->Session->setFlash(__('Produkt "%s" został usunięty.', h($product['Product']['name'])), 'flash-success');
        } else {
            $this->Session->setFlash(__('Nie udało się usunąć produktu "%s".', h($product['Product']['name'])), 'flash-error');
        }
        
        return $this->redirect($this->referer());
    }

    public function admin_saveProductSortOrder()
    {
        $product_id = (int) $this->request->query['product_id'];
        $value = (int) $this->request->query['value'];
        
        /*
         * $product = $this->Product->findById ( $product_id );
         * $product ['Product'] ['sort_order'] = $value;
         *
         * if ($this->Product->save ( $product, false )) {
         */
        if ($this->Product->updateAll(array(
            'sort_order' => $value
        ), array(
            'Product.id' => $product_id
        ))) {
            $result = array(
                'success' => 1
            );
        } else {
            $result = array(
                'success' => 0
            );
        }
        
        $this->RequestHandler->renderAs($this, 'json');
        $this->set('jsonp', true);
        $this->set('result', $result);
        $this->set('_serialize', array(
            'result'
        ));
    }

    public function admin_saveProductStatus()
    {
        $productId = (int) $this->request->query['product_id'];
        $value = (int) $this->request->query['value'];
        
        if ($this->Product->updateAll(array(
            'status' => $value
        ), array(
            'Product.id' => $productId
        ))) {
            $result = array(
                'success' => 1
            );
        } else {
            $result = array(
                'success' => 0
            );
        }
        
        $this->RequestHandler->renderAs($this, 'json');
        $this->set('jsonp', true);
        $this->set('result', $result);
        $this->set('_serialize', array(
            'result'
        ));
    }

    public function admin_saveProductPrice()
    {
        $productId = (int) $this->request->query['product_id'];
        $type = $this->request->query['type'];
        $price = str_replace(',', '.', $this->request->query['price']);
        
        $product = $this->Product->findById($productId);
        $toSave = array();
        if ($type == 'priceTax') {
            $toSave['price_tax'] = strlen((string) $price) == 0 ? 0 : (float) $price;
            $taxRate = $this->Product->TaxRate->findById($product['Product']['tax_rate_id']);
            $toSave['price'] = round($product['Product']['price_tax'] / round((100 + $taxRate['TaxRate']['rate']) / 100, 2), 2);
            $toSave['tax'] = round($product['Product']['price_tax'] - $product['Product']['price'], 2);
        } elseif ($type == 'oldPrice') {
            $toSave['old_price'] = strlen((string) $price) == 0 ? null : (float) $price;
        }
        
        // if ($this->Product->save($product, false)) {
        if ($this->Product->updateAll($toSave, array(
            'Product.id' => $productId
        ))) {
            $result = array(
                'success' => 1
            );
        } else {
            $result = array(
                'success' => 0
            );
        }
        
        $this->RequestHandler->renderAs($this, 'json');
        $this->set('jsonp', true);
        $this->set('result', $result);
        $this->set('_serialize', array(
            'result'
        ));
    }

    public function admin_saveProductFeatureStatus()
    {
        $productId = (int) $this->request->query['product_id'];
        $feature = $this->request->query['feature'];
        $status = (int) $this->request->query['status'];
        
        if ($this->Product->updateAll(array(
            $feature . '_status' => $status
        ), array(
            'Product.id' => $productId
        ))) {
            $result = array(
                'success' => 1
            );
        } else {
            $result = array(
                'success' => 0
            );
        }
        
        $this->RequestHandler->renderAs($this, 'json');
        $this->set('jsonp', true);
        $this->set('result', $result);
        $this->set('_serialize', array(
            'result'
        ));
    }

    public function admin_getCategoryNestedName()
    {
        $category_id = (int) $this->request->query['category_id'];
        $result = [
            'success' => 1,
            'nname' => $this->Product->Category->getNestedName($category_id)
        ];
        
        $this->RequestHandler->renderAs($this, 'json');
        $this->set('jsonp', true);
        $this->set('result', $result);
        $this->set('_serialize', array(
            'result'
        ));
    }

    public function admin_get_products_table()
    {
        $category_id = $this->request->query['category_id'];
        $query = $this->request->query['query'];
        $products = $conditions = array();
        
        if ($category_id > 0) {
            $db = $this->Product->getDataSource();
            $subQuery = $db->buildStatement(array(
                'fields' => array(
                    '`PC`.`product_id`'
                ),
                'table' => $db->fullTableName('products_categories'),
                'alias' => 'PC',
                'limit' => null,
                'offset' => null,
                'joins' => array(),
                'conditions' => array(
                    '`PC`.`category_id`' => $category_id
                ),
                'order' => null,
                'group' => null
            ), $this->Product);
            $subQuery = ' `Product`.`id` IN (' . $subQuery . ') ';
            $subQueryExpression = $db->expression($subQuery);
            
            $conditions[] = $subQueryExpression;
        }
        if (! empty($query)) {
            $conditions['Product.name LIKE'] = "%{$query}%";
        }
        
        if (! empty($conditions)) {
            $products = $this->Product->find('all', array(
                'conditions' => $conditions
            ));
        }
        
        $view = new View($this, false);
        $view->viewPath = 'KeyPromotions';
        $view->layout = false;
        $view->set('products', $products);
        $html = $view->render('category_product_table');
        
        $this->RequestHandler->renderAs($this, 'json');
        $this->set('jsonp', true);
        $this->set('result', array(
            'html' => $html
        ));
        $this->set('_serialize', array(
            'result'
        ));
    }

    public function admin_get_product_data()
    {
        $product_id = $this->request->query['product_id'];
        $product = array();
        
        if ($product_id > 0) {
            $product = $this->Product->findById($product_id);
        }
        
        $this->RequestHandler->renderAs($this, 'json');
        $this->set('jsonp', true);
        $this->set('product', $product);
        $this->set('_serialize', array(
            'product'
        ));
    }

    public function admin_get_product_option_row()
    {
        $product_id = (int) $this->request->query['product_id'];
        $product_option_id = (int) $this->request->query['product_option_id'];
        $product_option_value_id = (int) $this->request->query['product_option_value_id'];
        $key = (int) $this->request->query['key'];
        
        $productOption = $this->Product->ProductsProductsOption->ProductOption->find('first', array(
            'conditions' => array(
                'id' => $product_option_id
            )
        ));
        $productOptionValue = $this->Product->ProductsProductsOption->ProductOptionValue->find('first', array(
            'conditions' => array(
                'id' => $product_option_value_id
            )
        ));
        
        $productOption = array(
            'product_id' => $product_id,
            'product_option_id' => $product_option_id,
            'product_option_value_id' => $product_option_value_id,
            'change_price' => 0,
            'change_price_tax' => 0,
            'catalog_no' => '',
            'ProductOption' => $productOption['ProductOption'],
            'ProductOptionValue' => $productOptionValue['ProductOptionValue']
        );
        
        $this->request->data = array(
            'ProductsProductsOption' => array(
                $key => $productOption
            )
        );
        
        $productAvailabilities = $this->Product->ProductAvailability->find('list', array(
            'fields' => array(
                'ProductAvailability.id',
                'ProductAvailability.name'
            )
        ));
        
        $view = new View($this, false);
        $view->viewPath = 'KeyProducts';
        $view->layout = false;
        $view->set(compact('key', 'productOption', 'productAvailabilities'));
        $html = $view->render('product_option_row');
        
        $this->RequestHandler->renderAs($this, 'json');
        $this->set('jsonp', true);
        $this->set('result', array(
            'success' => 1,
            'html' => $html
        ));
        $this->set('_serialize', array(
            'result'
        ));
    }

    public function admin_get_product_option_values()
    {
        $product_option_id = (int) $this->request->query['product_option_id'];
        
        $productOptionValues = $this->Product->ProductsProductsOption->ProductOptionValue->find('list', array(
            'conditions' => array(
                'product_option_id' => $product_option_id
            )
        ));
        
        $this->RequestHandler->renderAs($this, 'json');
        $this->set('jsonp', true);
        $this->set('result', array(
            'success' => 1,
            'productOptionValues' => $productOptionValues
        ));
        $this->set('_serialize', array(
            'result'
        ));
    }

    public function admin_get_product_options()
    {
        $productId = (int) $this->request->query['product_id'];
        $this->Product->contain(array(
            'ProductsProductsOption',
            'ProductsProductsOption.ProductOptionValue'
        ));
        
        $product = $this->Product->read(null, $productId);
        $keys = Hash::extract($product, 'ProductsProductsOption.{n}.product_option_value_id');
        $values = Hash::extract($product, 'ProductsProductsOption.{n}.ProductOptionValue.name');
        
        $options = array_combine($keys, $values);
        
        $this->set(compact('options'));
    }

    protected function fillRequestData($data)
    {
        if (isset($data['ProductsProductsOption'])) {
            foreach ($data['ProductsProductsOption'] as $key => $productOption) {
                if (! isset($productOption['ProductOption']) && $productOption['product_option_id'] > 0) {
                    $option = $this->Product->ProductsProductsOption->ProductOption->findById($productOption['product_option_id']);
                    $data['ProductsProductsOption'][$key]['ProductOption'] = $option['ProductOption'];
                }
                if (! isset($productOption['ProductOptionValue']) && $productOption['product_option_value_id'] > 0) {
                    $value = $this->Product->ProductsProductsOption->ProductOptionValue->findById($productOption['product_option_value_id']);
                    $data['ProductsProductsOption'][$key]['ProductOptionValue'] = $value['ProductOptionValue'];
                }
            }
        }
        
        return $data;
    }

    public function admin_get_auction_template($productId)
    {
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $this->layout = 'ajax';
            $this->Product->contain(array(
                'Manufacturer',
                'ProductsImage' => array(
                    'order' => array(
                        'ProductsImage.sort_order' => 'ASC'
                    )
                )
            ));
            $product = $this->Product->read(null, $productId);
            
            $post = $this->request->data;
            $this->set(compact('post', 'product'));
            $this->render('auction_template');
        } else {
            $this->request->data = $this->Product->read(null, $productId);
            $this->set(compact('productId'));
        }
    }

    /**
     */
    public function admin_edit_template($productId)
    {
        $this->loadModel('KeyAdmin.ProductTemplate');
        $this->ProductTemplate->contain(array(
            'Product'
        ));
        $template = $this->ProductTemplate->find('first', array(
            'conditions' => array(
                'ProductTemplate.product_id' => $productId
            )
        ));
        if (empty($template)) {
            $toSave = array(
                'product_id' => $productId
            );
            $this->ProductTemplate->create();
            $this->ProductTemplate->save($toSave);
            $this->ProductTemplate->contain(array(
                'Product'
            ));
            $template = $this->ProductTemplate->find('first', array(
                'conditions' => array(
                    'ProductTemplate.product_id' => $productId
                )
            ));
        }
        ;
        
        if ($this->request->is([
            'post',
            'put'
        ])) {
            $this->ProductTemplate->save($this->request->data, true, array(
                'id',
                'header_text',
                'first_text',
                'sec_text',
                'third_text',
                'is_public',
                'first_position',
                'sec_position',
                'third_position'
            ));
            exit();
        }
        
        $this->set('title', __('Szablon produktu'));
        $this->set('content_title', __('Szablon produktu: ') . $template['Product']['name']);
        $this->set('content_subtitle', __('Zrób raz a dobrze.'));
        $this->request->data = $template;
    }

    public function admin_image_upload($paramName)
    {
        error_reporting(E_ALL | E_STRICT);
        $options = array(
            'upload_dir' => CLIENT_WWW . 'img' . DS . 'description' . DS,
            'image_versions' => array(),
            'param_name' => $paramName
        );
        App::uses('UploadHandler', 'Lib');
        new UploadHandler($options);
        
        exit();
    }

    public function admin_save_image($templateId, $column)
    {
        $this->loadModel('KeyAdmin.ProductTemplate');
        $this->ProductTemplate->updateAll(array(
            $column => "'{$this->request->data['file_name']}'"
        ), array(
            'ProductTemplate.id' => $templateId
        ));
        
        \Tinify\setKey("ZJt0gbI3pjULzGINGIkkGwZjGGrOJajL");
        $dirFile = CLIENT_WWW . 'img/description/' . $this->request->data['file_name'];
        $source = \Tinify\fromFile($dirFile);
        $resized = $source->resize(array(
            "method" => "scale",
            "width" => 1200
        ));
        $resized->toFile($dirFile);
        
        echo '/img/description/' . $this->request->data['file_name'];
        die();
    }

    public function admin_delete_image($column, $templateId)
    {
        $this->loadModel('KeyAdmin.ProductTemplate');
        $this->ProductTemplate->updateAll(array(
            $column => null
        ), array(
            'ProductTemplate.id' => $templateId
        ));
        
        die();
    }

    public function admin_assign_car_models($productId)
    {
        $this->Product->contain();
        $product = $this->Product->read(null, $productId);
        $this->loadModel('PoductsCarModelsRel');
        
        if ($this->request->is('post')) {
            $toSave = array_filter($this->request->data['CarModelRel']);
            $toSave = Hash::extract($toSave, '{n}.{n}');
            $toSave = Hash::map($toSave, '{n}', function ($item) use($productId) {
                return array(
                    'product_id' => $productId,
                    'car_model_id' => $item
                );
            });
            $this->PoductsCarModelsRel->deleteAll(array(
                'PoductsCarModelsRel.product_id' => $productId
            ));
            $this->PoductsCarModelsRel->saveAll($toSave);
            
            $this->Session->setFlash(__('Relacja zapisana.'), 'flash-success');
        }
        $this->loadModel('KeyAdmin.CarManufacturer');
        $this->CarManufacturer->contain(array(
            'CarModel'
        ));
        $cars = $this->CarManufacturer->find('all');
        
        $this->set('title', __('Marki/modele aut'));
        $this->set('content_title', __('Marki/modele aut'));
        $this->set('content_subtitle', __('Przypisz marke i model '));
        
        $selected = $this->PoductsCarModelsRel->find('list', array(
            'conditions' => array(
                'PoductsCarModelsRel.product_id' => $productId
            ),
            'fields' => array(
                'id',
                'car_model_id'
            )
        ));
        
        $this->set(compact('product', 'cars', 'selected'));
    }

    public function admin_index_list()
    {
        $this->set('title', __('Szybka lista produktów'));
        $this->set('content_title', __('Szybka lista produktów'));
        $this->set('content_subtitle', __('Operacje na liście'));
        
        $this->Product->contain(array(
            'MainCategory'
        ));
        $products = $this->Product->find('all', array(
            'order' => array(
                'Product.name' => 'ASC'
            )
        ));
        
        $this->set('products', $products);
        
        $this->loadModel('KeyAdmin.Category');
        $this->Category->contain(array(
            'ChildCategory.ChildChildCategory'
        ));
        $categories = $this->Category->find('all', array(
            'conditions' => array(
                'Category.parent_id' => 0
            ),
            'order' => array(
                'Category.name' => 'ASC'
            )
        ));
        $list = [];
        foreach ($categories as $category) {
            $list[$category['Category']['id']] = $category['Category']['name'];
            foreach ($category['ChildCategory'] as $child) {
                $list[$child['id']] = $category['Category']['name'].' > '.$child['name'];
                
                foreach ($child['ChildChildCategory'] as $child2) {
                    $list[$child2['id']] = $category['Category']['name'].' > '.$child['name'].' > '.$child2['name'];
                }
            }
        }
        $this->set('list', $list);
    }
    
    
    public function admin_save_category()
    {
        $this->loadModel('KeyAdmin.ProductsCategory');
        foreach ($this->request->data['ProductsCategory'] as $key => $value) {
            $this->ProductsCategory->deleteAll(array(
                'ProductsCategory.product_id' => $key
            ));
            
            $toSave = [
                'product_id' => $key,
                'category_id' => $value['category_id'],
                'category_default' => 1
            ];
            $this->ProductsCategory->create();
            var_dump($this->ProductsCategory->save($toSave));
        }
        exit;
    }
}
