<?php

App::uses('AdminController', 'KeyAdmin.Controller');

class KeyAuthController extends AdminController {

  public $uses = array('KeyAdmin.User', 'KeyAdmin.Customer');
  
  /**
   * (non-PHPdoc)
   * @see Controller::beforeFilter()
   */
  public function beforeFilter() {
    parent::beforeFilter();
    $this->Auth->allow('login', 'admin_logout', 'logout');
  }

  /**
   * 
   */
  public function login() {
    App::uses('User', 'KeyAdmin.Model');
    if (is_numeric($this->Auth->user('id')) && $this->Auth->user('id') > 0 && User::isAdmin($this->Auth->user('id'))) {
      return $this->redirect(array('plugin' => 'KeyAdmin', 'controller' => 'KeyDashboard', 'action' => 'index', 'admin' => true));
    }
    
    $this->layout = 'KeyAdmin.login';
    
    if ($this->request->is('post')) {
      if ($this->Auth->login()) {
        App::uses('User', 'KeyAdmin.Model');
        if (User::isAdmin($this->Auth->user('id'))) {
          $this->Session->write('Auth.User.id', $this->Auth->user('id'));
          $this->Session->write('Auth.User.admin', 1);
          
          $this->User->id = $this->Auth->user('id');
          $this->User->save(array(
            'last_login' => date('Y-m-d H:i:s')
          ), false);
          
          return $this->redirect($this->Auth->redirect());
        } else {
          $this->Session->destroy();
          $this->Auth->logout();
          $this->request->data['User']['password'] = '';
        }
      }

      $this->Session->setFlash(__('Niepoprawny e-mail i/lub hasło'), 'default', array(), 'auth');
    }
  }
  
  /**
   *
   */
  public function admin_logout() {
    $this->Session->destroy();
    $this->redirect($this->Auth->logout());
  }

}
