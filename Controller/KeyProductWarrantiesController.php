<?php

App::uses('AdminController', 'KeyAdmin.Controller');

class KeyProductWarrantiesController extends AdminController {

  public $uses = array('KeyAdmin.ProductWarranty');
  public $components = array('Paginator', 'RequestHandler');
  public $paginate = array(
    'limit' => 20,
  );

  public function beforeFilter() {
    parent::beforeFilter();
    $this->Paginator->settings = $this->paginate;
  }

  public function admin_index() {
    $this->set('product_warranties', $this->Paginator->paginate());
    
    $this->set('title', __('Gwarancje produktów'));
    $this->set('content_title', __('Gwarancje produktów'));
    $this->set('content_subtitle', __('Lista gwarancji produktów'));
    $this->set('buttons_template', 'Buttons/KeyProductWarranties');
  }

  public function admin_edit($id) {
    if (!$id) {
      throw new NotFoundException(__('Wybrana gwarancja produktów nie istnieje'));
    }

    $product_warranty = $this->ProductWarranty->findById($id);
    if (!$product_warranty) {
      throw new NotFoundException(__('Nie odnaleziono wybranej gwarancji produktów'));
    }

    if ($this->request->is(array('post', 'put'))) {
      $this->ProductWarranty->id = $id;
      if ($this->ProductWarranty->save($this->request->data)) {
        $this->Session->setFlash(__('Gwarancja produktów została zapisana.'), 'flash-success');
        return $this->redirect(array('action' => 'index'));
      }
      $this->Session->setFlash(__('Nie udało się zapisać gwarancji produktów. Sprawdź poprawność podanych danych.'), 'flash-error');
    }

    if (!$this->request->data) {
      $this->request->data = $product_warranty;
    }

    $this->set('title', __('Gwarancje produktów'));
    $this->set('content_title', __('Gwarancje produktów'));
    $this->set('content_subtitle', __('Edycja gwarancji produktów'));
    $this->set('buttons_template', 'Buttons/KeyProductWarranties');
    
    $this->set('product_warranty', $product_warranty);
    
    $this->render('KeyProductWarranties/admin_form');
  }

  public function admin_create() {
    if ($this->request->is('post')) {
      $this->ProductWarranty->create();
      if ($this->ProductWarranty->save($this->request->data)) {
        $this->Session->setFlash(__('Gwarancja produktów została dodana.'), 'flash-success');
        return $this->redirect(array('action' => 'index'));
      }
      $this->Session->setFlash(__('Nie udało się dodać gwarancji produktów. Sprawdź poprawność podanych danych.'), 'flash-error');
    }

    $this->set('title', __('Gwarancje produktów'));
    $this->set('content_title', __('Gwarancje produktów'));
    $this->set('content_subtitle', __('Dodawanie gwarancji produktów'));
    $this->set('buttons_template', 'Buttons/KeyProductWarranties');
    
    $this->set('product_warranty', $this->request->data);
    
    $this->render('KeyProductWarranties/admin_form');
  }

  public function admin_delete($id) {
    $product_warranty = $this->ProductWarranty->findById($id);
    
    if ($this->ProductWarranty->delete($id)) {
      $this->Session->setFlash(__('Gwarancja produktów "%s" została usunięta.', h($product_warranty['ProductWarranty']['name'])), 'flash-success');
    } else {
      $this->Session->setFlash(__('Nie udało się usunąć gwarancji produktów "%s".', h($product_warranty['ProductWarranty']['name'])), 'flash-error');
    }

    return $this->redirect(array('action' => 'index'));
  }

}
