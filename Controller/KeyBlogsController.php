<?php
App::uses('AdminController', 'KeyAdmin.Controller');

class KeyBlogsController extends AdminController {
  public $uses = array(
    'KeyAdmin.BlogCategory'
  );
  public $components = array(
    'Paginator',
    'RequestHandler'
  );
  public $paginate = array(
    'limit' => 20
  );



  public function beforeFilter() {
    parent::beforeFilter();
    $this->Paginator->settings = $this->paginate;
  }



  public function admin_category_index() {
    $this->set('categories', $this->Paginator->paginate('BlogCategory'));
    
    $this->set('title', __('Blog - Kategorie'));
    $this->set('content_title', __('Blog - Kategorie'));
    $this->set('content_subtitle', __('Lista kategorii'));
    $this->set('buttons_template', 'Buttons/KeyBlogCategories');
  }



  public function admin_category_edit($pageId) {
    if (! empty($pageId)) {
      $page = $this->BlogCategory->findById($pageId);
      if (! $page) {
        throw new NotFoundException(__('Nie odnaleziono wybranej strony'));
      }
    }
    
    if ($this->request->is(array(
      'post',
      'put'
    ))) {
      if (! empty($pageId)) {
        $this->request->data['BlogCategory']['id'] = $pageId;
      }
      $this->BlogCategory->create();
      
      if ($this->BlogCategory->save($this->request->data)) {
        $this->Session->setFlash(__('Kategoria została zapisany.'), 'flash-success');
        return $this->redirect(array(
          'action' => 'category_index'
        ));
      }
      
      $this->Session->setFlash(__('Nie udało się. Sprawdź poprawność podanych danych.'), 'flash-error');
    }
    if (! $this->request->data) {
      $this->request->data = (isset($page)) ? $page : array();
    }
    
    $categories = array('0' => 'Wybierz') +$this->BlogCategory->find('list', array('conditions' => array('BlogCategory.id !=' => $pageId)));
    $this->set('categories', $categories);
    
    $this->set('title', __('Kategoria Bloga'));
    $this->set('content_title', __('Kategoria Bloga'));
    $this->set('content_subtitle', __('Dodawanie'));
    $this->set('buttons_template', 'Buttons/KeyPages');
  }
  
  
  public function admin_category_delete($id) {
    $page = $this->BlogCategory->findById($id);
  
    if ($this->BlogCategory->delete($id)) {
    $this->Session->setFlash(__('Kategoria "%s" został usunięty.', h($page['BlogCategory']['name'])), 'flash-success');
    } else {
    $this->Session->setFlash(__('Nie udało się usunąć kategorii "%s".', h($page['BlogCategory']['name'])), 'flash-error');
    }
  
    return $this->redirect($this->referer());
  }
}
