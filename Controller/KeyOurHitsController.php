<?php

App::uses('AdminController', 'KeyAdmin.Controller');

class KeyOurHitsController extends AdminController {

  var $uses = array('KeyAdmin.Product');
  public $components = array('Paginator', 'RequestHandler');
  public $paginate = array(
    'limit' => 20,
  );

  public function beforeFilter() {
    parent::beforeFilter();
    $this->Paginator->settings = $this->paginate;
  }
  
  protected function setFormValidations() {
    $this->Product->validator()->add('id', array(
      'rule1' => array(
        'rule' => 'naturalNumber',
        'allowEmpty' => false,
        'message' => 'Wybierz produkt'
      ),
    ));
  }

  /**
   * 
   */
  public function admin_index() {
    if ($this->request->is('post') && isset($this->request->data['bulk_action']) && !empty($this->request->data['bulk_action'])) {
      $this->execBulkAction();
    }
    
    $this->Product->contain(array('ProductsImage', 'Category', 'Manufacturer'));
    $this->Paginator->settings['conditions'] = array(
      'OR' => array(
        'Product.our_hit_status' => 1,
        'Product.our_hit_date >' => '0000-00-00 00:00:00',
        'Product.our_hit_date_end >' => '0000-00-00 00:00:00'
      )
    );
    
    $this->set('products', $this->Paginator->paginate());

    $this->set('title', __('Nasz hit'));
    $this->set('content_title', __('Nasz hit'));
    $this->set('content_subtitle', __('Lista hitów'));
    $this->set('buttons_template', 'Buttons/KeyOurHits');
  }

  public function admin_edit($id) {
    if (!$id) {
      throw new NotFoundException(__('Wybrany nasz hit nie istnieje'));
    }

    $product = $this->Product->findById($id);
    if (!$product || (empty($product['Product']['our_hit_status']) && $product['Product']['our_hit_date'] == '0000-00-00 00:00:00' && $product['Product']['our_hit_date_end'] == '0000-00-00 00:00:00')) {
      throw new NotFoundException(__('Nie odnaleziono wybranego naszego hitu'));
    }

    if ($this->request->is(array('post', 'put'))) {
      $this->setFormValidations();
      
      $this->Product->id = $id;
      if ($this->Product->save($this->request->data)) {
        $this->Session->setFlash(__('Nasz hit został zapisany.'), 'flash-success');
        return $this->redirect(array('action' => 'index'));
      }
      $this->Session->setFlash(__('Nie udało się zapisać naszego hitu. Sprawdź poprawność podanych danych.'), 'flash-error');
    }

    if (!$this->request->data) {
      $this->request->data = $product;
    }

    $this->set('title', __('Nasz hit'));
    $this->set('content_title', __('Nasz hit'));
    $this->set('content_subtitle', __('Edycja hitu'));
    $this->set('buttons_template', 'Buttons/KeyOurHits');

    $this->render('KeyOurHits/admin_form');
  }

  public function admin_create() {
    if ($this->request->is('post')) {
      $this->setFormValidations();
      
      $id = (int) $this->request->data['Product']['id'];
      $product = $this->Product->findById($id);
      $new = empty($product['Product']['our_hit_status']) && $product['Product']['our_hit_date'] == '0000-00-00 00:00:00' && $product['Product']['our_hit_date_end'] == '0000-00-00 00:00:00';
      
      $this->Product->id = $id;
      
      if ($this->Product->save($this->request->data)) {
        $this->Session->setFlash($new ? __('Nasz hit został dodany.') : __('Nasz hit został zapisany.'), 'flash-success');
        return $this->redirect(array('action' => 'index'));
      }
      $this->Session->setFlash($new ? __('Nie udało się dodać naszego hitu. Sprawdź poprawność podanych danych.') : __('Nie udało się zapisać naszego hitu. Sprawdź poprawność podanych danych.'), 'flash-error');
    }

    $this->set('title', __('Nasz hit'));
    $this->set('content_title', __('Nasz hit'));
    $this->set('content_subtitle', __('Dodawanie hitu'));
    $this->set('buttons_template', 'Buttons/KeyOurHits');
    
    $this->set('create', 'true');

    $this->render('KeyOurHits/admin_form');
  }

  public function admin_delete($id) {
    $product = $this->Product->findById($id);

    if ($this->Product->deleteOurHit($id)) {
      $this->Session->setFlash(__('Nasz hit dla produktu "%s" został usunięty.', h($product['Product']['name'])), 'flash-success');
    } else {
      $this->Session->setFlash(__('Nie udało się usunąć naszego hitu dla produktu "%s".', h($product['Product']['name'])), 'flash-error');
    }
    
    return $this->redirect(array('action' => 'index'));
  }

  public function admin_get_products_table() {
    $category_id = $this->request->query['category_id'];
    $query = $this->request->query['query'];
    $products = $conditions = array();

    if ($category_id > 0) {
      $db = $this->Product->getDataSource();
      $subQuery = $db->buildStatement(array(
        'fields' => array('`PC`.`product_id`'),
        'table' => $db->fullTableName('products_categories'),
        'alias' => 'PC',
        'limit' => null,
        'offset' => null,
        'joins' => array(),
        'conditions' => array(
          '`PC`.`category_id`' => $category_id
        ),
        'order' => null,
        'group' => null
          ), $this->Product
      );
      $subQuery = ' `Product`.`id` IN ('.$subQuery.') ';
      $subQueryExpression = $db->expression($subQuery);
      
      $conditions[] = $subQueryExpression;
    }
    if (!empty($query)) {
      $conditions['Product.name LIKE'] = "%{$query}%";
    }

    if (!empty($conditions)) {
      $products = $this->Product->find('all', array(
        'conditions' => $conditions
      ));
    }

    $view = new View($this, false);
    $view->viewPath = 'KeyOurHits';
    $view->layout = false;
    $view->set('products', $products);
    $html = $view->render('category_product_table');
    
    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set('result', array('html' => $html));
    $this->set('_serialize', array('result'));
  }
  
  public function admin_get_product_data() {
    $product_id = $this->request->query['product_id'];
    $product = array();
    
    if ($product_id > 0) {
      $product = $this->Product->findById($product_id);
    }

    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set('product', $product);
    $this->set('_serialize', array('product'));
  }

  public function admin_save_our_hit_status() {
    $product_id = (int) $this->request->query['product_id'];
    $value = (int) $this->request->query['value'];

    $this->Product->id = $product_id;
    $data = array(
      'Product' => array(
        'our_hit_status' => $value
      )
    );

    if ($this->Product->save($data)) {
      $result = array('success' => 1);
    } else {
      $result = array('success' => 0);
    }

    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set('result', $result);
    $this->set('_serialize', array('result'));
  }
  
  public function admin_save_product_status() {
    $product_id = (int) $this->request->query['product_id'];
    $value = (int) $this->request->query['value'];

    $this->Product->id = $product_id;
    $data = array(
      'Product' => array(
        'status' => $value
      )
    );

    if ($this->Product->save($data)) {
      $result = array('success' => 1);
    } else {
      $result = array('success' => 0);
    }

    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set('result', $result);
    $this->set('_serialize', array('result'));
  }
  
  protected function execBulkAction() {
    $action = $this->request->data['bulk_action'];
    $product_ids = isset($this->request->data['check_product_id']) ? $this->request->data['check_product_id'] : array();
    
    if (empty($product_ids)) {
      $this->Session->setFlash(__('Zaznacz hity, dla których wykonać wybraną akcję.'), 'flash-error');
      return;
    }
    
    switch ($action) {
      case 'delete_our_hits':
        if ($this->Product->deleteOurHit($product_ids, false)) {
          $this->Session->setFlash(__('Wybrane hity zostały usunięte.'), 'flash-success');
        } else {
          $this->Session->setFlash(__('Nie udało się usunąć wybranych hitów, spróbuj ponownie.'), 'flash-error');
        }
        break;
      case 'deactivate_our_hits':
        if ($this->Product->updateAll(array('Product.our_hit_status' => 0), array('Product.id' => $product_ids))) {
          $this->Session->setFlash(__('Status wybranych hitów został ustawiony na nieaktywny.'), 'flash-success');
        } else {
          $this->Session->setFlash(__('Nie udało się zmienić statusu wybranych hitów, spróbuj ponownie.'), 'flash-error');
        }
        break;
      case 'activate_our_hits':
        if ($this->Product->updateAll(array('Product.our_hit_status' => 1), array('Product.id' => $product_ids))) {
          $this->Session->setFlash(__('Status wybranych hitów został ustawiony na aktywny.'), 'flash-success');
        } else {
          $this->Session->setFlash(__('Nie udało się zmienić statusu wybranych hitów, spróbuj ponownie.'), 'flash-error');
        }
        break;
      case 'delete_products':
        if ($this->Product->deleteAll(array('Product.id' => $product_ids), false)) {
          $this->Session->setFlash(__('Wybrane produkty zostały usunięte.'), 'flash-success');
        } else {
          $this->Session->setFlash(__('Nie udało się usunąć wybranych wybranych produktów, spróbuj ponownie.'), 'flash-error');
        }
        break;
      case 'deactivate_products':
        if ($this->Product->updateAll(array('Product.status' => 0), array('Product.id' => $product_ids))) {
          $this->Session->setFlash(__('Status wybranych produktów został ustawiony na nieaktywne.'), 'flash-success');
        } else {
          $this->Session->setFlash(__('Nie udało się zmienić statusu wybranych produktów, spróbuj ponownie.'), 'flash-error');
        }
        break;
      case 'activate_products':
        if ($this->Product->updateAll(array('Product.status' => 1), array('Product.id' => $product_ids))) {
          $this->Session->setFlash(__('Status wybranych produktów został ustawiony na nieaktywne.'), 'flash-success');
        } else {
          $this->Session->setFlash(__('Nie udało się zmienić statusu wybranych produktów, spróbuj ponownie.'), 'flash-error');
        }
        break;
      case 'clear_our_hits_date':
        if ($this->Product->updateAll(array('Product.our_hit_date' => null), array('Product.id' => $product_ids))) {
          $this->Session->setFlash(__('Data rozpoczęcia wybranych hitów została wyzerowana.'), 'flash-success');
        } else {
          $this->Session->setFlash(__('Nie udało się wyzerować daty rozpoczęcia wybranych hitów, spróbuj ponownie.'), 'flash-error');
        }
        break;
      case 'clear_our_hits_date_end':
        if ($this->Product->updateAll(array('Product.our_hit_date_end' => null), array('Product.id' => $product_ids))) {
          $this->Session->setFlash(__('Data zakończenia wybranych hitów została wyzerowana.'), 'flash-success');
        } else {
          $this->Session->setFlash(__('Nie udało się wyzerować daty zakończenia wybranych hitów, spróbuj ponownie.'), 'flash-error');
        }
        break;
      case 'manipulate_our_hits_date':
        $manipulate_date_value = (int) $this->request->data['manipulate_date_value'];
        if ($this->Product->manipulateOurHitDate($product_ids, $manipulate_date_value)) {
          $this->Session->setFlash(__('Data rozpoczęcia wybranych hitów została zmieniona.'), 'flash-success');
        } else {
          $this->Session->setFlash(__('Nie udało się zmienić daty rozpoczęcia wybranych hitów, spróbuj ponownie.'), 'flash-error');
        }
        break;
      case 'manipulate_our_hits_date_end':
        $manipulate_date_value = (int) $this->request->data['manipulate_date_value'];
        if ($this->Product->manipulateOurHitDateEnd($product_ids, $manipulate_date_value)) {
          $this->Session->setFlash(__('Data zakończenia wybranych hitów została zmieniona.'), 'flash-success');
        } else {
          $this->Session->setFlash(__('Nie udało się zmienić daty zakończenia wybranych hitów, spróbuj ponownie.'), 'flash-error');
        }
        break;
    }
  }
}
