<?php

App::uses('AdminController', 'KeyAdmin.Controller');

class KeyManufacturersController extends AdminController {

  public $uses = array('KeyAdmin.Manufacturer');
  public $components = array('Paginator', 'RequestHandler');
  public $paginate = array(
    'limit' => 20,
  );

  public function beforeFilter() {
    parent::beforeFilter();
    $this->Paginator->settings = $this->paginate;
  }

  public function admin_index() {
    $this->set('manufacturers', $this->Paginator->paginate());
    
    $this->set('title', __('Producenci'));
    $this->set('content_title', __('Producenci'));
    $this->set('content_subtitle', __('Lista producentów'));
    $this->set('buttons_template', 'Buttons/KeyManufacturers');
  }

  public function admin_edit($id) {
    if (!$id) {
      throw new NotFoundException(__('Wybrany producent nie istnieje'));
    }

    $manufacturer = $this->Manufacturer->findById($id);
    if (!$manufacturer) {
      throw new NotFoundException(__('Nie odnaleziono wybranego producenta'));
    }

    if ($this->request->is(array('post', 'put'))) {
      $this->Manufacturer->id = $id;
      if ($this->Manufacturer->save($this->request->data)) {
        $this->Session->setFlash(__('Producent został zapisany.'), 'flash-success');
        return $this->redirect(array('action' => 'index'));
      }
      $this->Session->setFlash(__('Nie udało się zapisać producenta. Sprawdź poprawność podanych danych.'), 'flash-error');
    }

    if (!$this->request->data) {
      $this->request->data = $manufacturer;
    }

    $this->set('title', __('Producenci'));
    $this->set('content_title', __('Producenci'));
    $this->set('content_subtitle', __('Edycja producenta'));
    $this->set('buttons_template', 'Buttons/KeyManufacturers');
    
    $this->render('KeyManufacturers/admin_form');
  }

  public function admin_create() {
    if ($this->request->is('post')) {
      $this->Manufacturer->create();
      
      if ($this->Manufacturer->save($this->request->data)) {
        $this->Session->setFlash(__('Producent został dodany.'), 'flash-success');
        return $this->redirect(array('action' => 'index'));
      }
      $this->Session->setFlash(__('Nie udało się dodać producenta. Sprawdź poprawność podanych danych.'), 'flash-error');
    }

    $this->set('title', __('Producenci'));
    $this->set('content_title', __('Producenci'));
    $this->set('content_subtitle', __('Dodawanie producenta'));
    $this->set('buttons_template', 'Buttons/KeyManufacturers');
    
    $this->render('KeyManufacturers/admin_form');
  }

  public function admin_delete($id) {
    $category = $this->Manufacturer->findById($id);
    
    if ($this->Manufacturer->delete($id)) {
      $this->Session->setFlash(__('Product "%s" został usunięty.', h($category['Manufacturer']['name'])), 'flash-success');
    } else {
      $this->Session->setFlash(__('Nie udało się usunąć producenta "%s".', h($category['Manufacturer']['name'])), 'flash-error');
    }

    return $this->redirect(array('action' => 'index'));
  }

}
