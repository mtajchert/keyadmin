<?php

App::uses('AdminController', 'KeyAdmin.Controller');

class KeyPromotionsController extends AdminController {

  var $uses = array('KeyAdmin.Product', 'KeyAdmin.Manufacturer', 'KeyAdmin.PromotionManage');
  public $components = array('Paginator', 'RequestHandler');
  public $paginate = array(
    'limit' => 20,
  );

  public function beforeFilter() {
    parent::beforeFilter();
    $this->Paginator->settings = $this->paginate;
  }
  
  protected function setFormValidations() {
    $this->Product->validator()->add('id', array(
      'rule1' => array(
        'rule' => 'naturalNumber',
        'allowEmpty' => false,
        'message' => 'Wybierz produkt'
      ),
    ));
    $this->Product->validator()->add('old_price', array(
      'rule1' => array(
        'rule' => 'numeric',
        'require' => true,
        'message' => 'Wpisz poprzednią cenę'
      ),
      'rule2' => array(
        'rule' => array('range', 0, 1999999999),
        'require' => true,
        'message' => 'Wpisz wartość większą od 0'
      )
    ));
    $this->Product->validator()->add('price_tax', array(
      'rule1' => array(
        'rule' => 'numeric',
        'require' => true,
        'message' => 'Wpisz nową cenę brutto'
      ),
      'rule2' => array(
        'rule' => array('range', 0, 1999999999),
        'require' => true,
        'message' => 'Wpisz wartość większą od 0'
      )
    ));
  }

  /**
   * 
   */
  public function admin_index() {
    if ($this->request->is('post') && isset($this->request->data['bulk_action']) && !empty($this->request->data['bulk_action'])) {
      $this->execBulkAction();
    }
    
    $this->Product->contain(array('ProductsImage', 'Category', 'Manufacturer'));
    $this->Paginator->settings['conditions'] = array(
      'OR' => array(
        'Product.old_price >' => 0,
        'Product.promotion_status' => 1
      )
    );
    $this->set('products', $this->Paginator->paginate());

    $this->set('title', __('Promocje'));
    $this->set('content_title', __('Promocje'));
    $this->set('content_subtitle', __('Lista promocji'));
    $this->set('buttons_template', 'Buttons/KeyPromotions');
  }

  public function admin_edit($id) {
    if (!$id) {
      throw new NotFoundException(__('Wybrana promocja nie istnieje'));
    }

    $product = $this->Product->findById($id);
    if (!$product || (empty($product['Product']['promotion_status']) && empty($product['Product']['old_price']))) {
      throw new NotFoundException(__('Nie odnaleziono wybranej promocji'));
    }

    if ($this->request->is(array('post', 'put'))) {
      $this->setFormValidations();
      
      $this->Product->id = $id;
      if ($this->Product->save($this->request->data)) {
        $this->Session->setFlash(__('Promocja została zapisana.'), 'flash-success');
        return $this->redirect(array('action' => 'index'));
      }
      $this->Session->setFlash(__('Nie udało się zapisać promocji. Sprawdź poprawność podanych danych.'), 'flash-error');
    }

    if (!$this->request->data) {
      $this->request->data = $product;
    }

    $this->set('title', __('Promocje'));
    $this->set('content_title', __('Promocje'));
    $this->set('content_subtitle', __('Edycja promocji'));
    $this->set('buttons_template', 'Buttons/KeyPromotions');

    $this->render('KeyPromotions/admin_form');
  }

  public function admin_create() {
    if ($this->request->is('post')) {
      $this->setFormValidations();
      
      $id = (int) $this->request->data['Product']['id'];
      $product = $this->Product->findById($id);
      $new = empty($product['Product']['promotion_status']) && empty($product['Product']['old_price']);
      
      $this->Product->id = $id;
      
      if ($this->Product->save($this->request->data)) {
        $this->Session->setFlash($new ? __('Promocja została dodana.') : __('Promocja została zapisana.'), 'flash-success');
        return $this->redirect(array('action' => 'index'));
      }
      $this->Session->setFlash($new ? __('Nie udało się dodać promocji. Sprawdź poprawność podanych danych.') : __('Nie udało się zapisać promocji. Sprawdź poprawność podanych danych.'), 'flash-error');
    }

    $this->set('title', __('Promocje'));
    $this->set('content_title', __('Promocje'));
    $this->set('content_subtitle', __('Dodawanie promocji'));
    $this->set('buttons_template', 'Buttons/KeyPromotions');
    
    $this->set('create', 'true');

    $this->render('KeyPromotions/admin_form');
  }

  public function admin_delete($id) {
    $restore_price = (bool) (int) $this->request->query['restore_price'];
    $product = $this->Product->findById($id);

    if ($this->Product->deletePromotion($id, $restore_price)) {
      $this->Session->setFlash(__('Promocja dla produktu "%s" została usunięta.', h($product['Product']['name'])), 'flash-success');
    } else {
      $this->Session->setFlash(__('Nie udało się usunąć promocji dla produktu "%s".', h($product['Product']['name'])), 'flash-error');
    }
    
    return $this->redirect(array('action' => 'index'));
  }
  
  public function admin_manage() {
    if ($this->request->is('post')) {
      $result = $this->PromotionManage->save($this->request->data);
      if ($result !== false && $result !== null) {
        if ($this->data['PromotionManage']['action'] == 'create') {
          $this->Session->setFlash(__('Promocje zostały utworzone/zmodyfikowane (%d promocji/e).', $result), 'flash-success');
        } else {
          $this->Session->setFlash(__('Promocje zostały usunięte (%d promocji/e).', $result), 'flash-success');
        }
        return $this->redirect(array('action' => 'index'));
      } else {
        $this->Session->setFlash(__('nie jest ok'), 'flash-error');
        if ($this->data['PromotionManage']['action'] == 'create') {
          $this->Session->setFlash(__('Nie udało się utworzyć/zmodyfikować promocji. Sprawdź poprawność podanych danych.'), 'flash-error');
        } else {
          $this->Session->setFlash(__('Nie udało się usunąć promocji. Sprawdź poprawność podanych danych.'), 'flash-error');
        }
      }
    }
    
    $manufactureres = $this->Manufacturer->find('all');
    $this->set('manufactureres', $manufactureres);
    $this->set('data', $this->data);
    
    $this->set('title', __('Promocje'));
    $this->set('content_title', __('Promocje'));
    $this->set('content_subtitle', __('Zarządzanie promocjami'));
    $this->set('buttons_template', 'Buttons/KeyPromotions');

    $this->render('KeyPromotions/admin_manage_form');
  }

  public function admin_save_promotion_status() {
    $product_id = (int) $this->request->query['product_id'];
    $value = (int) $this->request->query['value'];

    if ($this->Product->updateAll(array('Product.promotion_status' => $value), array('Product.id' => $product_id))) {
      $result = array('success' => 1);
    } else {
      $result = array('success' => 0);
    }
    
    /*$this->Product->id = $product_id;
    $data = array(
      'Product' => array(
        'promotion_status' => $value
      )
    );

    if ($this->Product->save($data, false)) {
      $result = array('success' => 1);
    } else {
      $result = array('success' => 0);
    }*/

    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set('result', $result);
    $this->set('_serialize', array('result'));
  }
  
  public function admin_save_product_status() {
    $product_id = (int) $this->request->query['product_id'];
    $value = (int) $this->request->query['value'];

    $this->Product->id = $product_id;
    $data = array(
      'Product' => array(
        'status' => $value
      )
    );

    if ($this->Product->save($data)) {
      $result = array('success' => 1);
    } else {
      $result = array('success' => 0);
    }

    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set('result', $result);
    $this->set('_serialize', array('result'));
  }
  
  protected function execBulkAction() {
    $action = $this->request->data['bulk_action'];
    $product_ids = isset($this->request->data['check_product_id']) ? $this->request->data['check_product_id'] : array();
    
    if (empty($product_ids)) {
      $this->Session->setFlash(__('Zaznacz promocje, dla których wykonać wybraną akcję.'), 'flash-error');
      return;
    }
    
    switch ($action) {
      case 'delete_promotions_not_restore_price':
        if ($this->Product->deletePromotion($product_ids, false)) {
          $this->Session->setFlash(__('Wybrane promocje zostały usunięte (ceny nie zostały przywrócone).'), 'flash-success');
        } else {
          $this->Session->setFlash(__('Nie udało się usunąć wybranych promocji, spróbuj ponownie.'), 'flash-error');
        }
        break;
      case 'delete_promotions_and_restore_price':
        if ($this->Product->deletePromotion($product_ids, true)) {
          $this->Session->setFlash(__('Wybrane promocje zostały usunięte, a ceny przywrócone.'), 'flash-success');
        } else {
          $this->Session->setFlash(__('Nie udało się usunąć wybranych promocji, spróbuj ponownie.'), 'flash-error');
        }
        break;
      case 'deactivate_promotions':
        if ($this->Product->updateAll(array('Product.promotion_status' => 0), array('Product.id' => $product_ids))) {
          $this->Session->setFlash(__('Status wybranych promocji został ustawiony na nieaktywne.'), 'flash-success');
        } else {
          $this->Session->setFlash(__('Nie udało się zmienić statusu wybranych promocji, spróbuj ponownie.'), 'flash-error');
        }
        break;
      case 'activate_promotions':
        if ($this->Product->updateAll(array('Product.promotion_status' => 1), array('Product.id' => $product_ids))) {
          $this->Session->setFlash(__('Status wybranych promocji został ustawiony na aktywne.'), 'flash-success');
        } else {
          $this->Session->setFlash(__('Nie udało się zmienić statusu wybranych promocji, spróbuj ponownie.'), 'flash-error');
        }
        break;
      case 'deactivate_products':
        if ($this->Product->updateAll(array('Product.status' => 0), array('Product.id' => $product_ids))) {
          $this->Session->setFlash(__('Status wybranych produktów został ustawiony na nieaktywne.'), 'flash-success');
        } else {
          $this->Session->setFlash(__('Nie udało się zmienić statusu wybranych produktów, spróbuj ponownie.'), 'flash-error');
        }
        break;
      case 'activate_products':
        if ($this->Product->updateAll(array('Product.status' => 1), array('Product.id' => $product_ids))) {
          $this->Session->setFlash(__('Status wybranych produktów został ustawiony na nieaktywne.'), 'flash-success');
        } else {
          $this->Session->setFlash(__('Nie udało się zmienić statusu wybranych produktów, spróbuj ponownie.'), 'flash-error');
        }
        break;
      case 'delete_products':
        if ($this->Product->deleteAll(array('Product.id' => $product_ids), false)) {
          $this->Session->setFlash(__('Wybrane produkty zostały usunięte.'), 'flash-success');
        } else {
          $this->Session->setFlash(__('Nie udało się usunąć wybranych wybranych produktów, spróbuj ponownie.'), 'flash-error');
        }
        break;
      case 'clear_promotions_date':
        if ($this->Product->updateAll(array('Product.promotion_date' => null), array('Product.id' => $product_ids))) {
          $this->Session->setFlash(__('Data rozpoczęcia wybranych promocji została wyzerowana.'), 'flash-success');
        } else {
          $this->Session->setFlash(__('Nie udało się wyzerować daty rozpoczęcia wybranych produktów, spróbuj ponownie.'), 'flash-error');
        }
        break;
      case 'clear_promotions_date_end':
        if ($this->Product->updateAll(array('Product.promotion_date_end' => null), array('Product.id' => $product_ids))) {
          $this->Session->setFlash(__('Data zakończenia wybranych promocji została wyzerowana.'), 'flash-success');
        } else {
          $this->Session->setFlash(__('Nie udało się wyzerować daty zakończenia wybranych produktów, spróbuj ponownie.'), 'flash-error');
        }
        break;
      case 'manipulate_promotions_date':
        $manipulate_date_value = (int) $this->request->data['manipulate_date_value'];
        if ($this->Product->manipulatePromotionDate($product_ids, $manipulate_date_value)) {
          $this->Session->setFlash(__('Data rozpoczęcia wybranych promocji została zmieniona.'), 'flash-success');
        } else {
          $this->Session->setFlash(__('Nie udało się zmienić daty rozpoczęcia wybranych promocji, spróbuj ponownie.'), 'flash-error');
        }
        break;
      case 'manipulate_promotions_date_end':
        $manipulate_date_value = (int) $this->request->data['manipulate_date_value'];
        if ($this->Product->manipulatePromotionDateEnd($product_ids, $manipulate_date_value)) {
          $this->Session->setFlash(__('Data zakończenia wybranych promocji została zmieniona.'), 'flash-success');
        } else {
          $this->Session->setFlash(__('Nie udało się zmienić daty zakończenia wybranych promocji, spróbuj ponownie.'), 'flash-error');
        }
        break;
      case 'manipulate_promotions_old_price_amount':
      case 'manipulate_promotions_old_price_percent':
        $manipulate_old_price_value = (float) $this->request->data['manipulate_old_price_value'];
        if ($this->Product->manipulateOldPrice($product_ids, $manipulate_old_price_value, $action == 'manipulate_promotions_old_price_amount' ? 'amount' : 'percent')) {
          $this->Session->setFlash(__('Poprzednia cena wybranych promocji została zmieniona.'), 'flash-success');
        } else {
          $this->Session->setFlash(__('Nie udało się zmienić poprzedniej ceny wybranych promocji, spróbuj ponownie.'), 'flash-error');
        }
        break;
    }
  }
}
