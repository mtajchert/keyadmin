<?php

App::uses('AdminController', 'KeyAdmin.Controller');

class KeyCustomersController extends AdminController {

  public $uses = array('KeyAdmin.Customer');
  public $components = array('Paginator', 'RequestHandler');
  public $paginate = array(
    'limit' => 20,
  );

  public function beforeFilter() {
    parent::beforeFilter();
    $this->Paginator->settings = $this->paginate;
  }

  /**
   * 
   */
  public function admin_index() {
    $this->Customer->recursive = 1;
    
    $this->Paginator->settings['conditions'] = array(
      'UserGroup.admin' => 0
    );
    $conditions = [];
    if ($this->request->is('post')) {
        $phraze = $this->request->data['Customer']['phraze'];
        if (!empty($phraze)) {
            $this->Session->write('KeyAdmin.customer_phraze', $phraze);
        } else {
            $this->Session->delete('KeyAdmin.customer_phraze');            
        }
        $conditions['OR'] = [
            'Customer.first_name LIKE' => "%$phraze%", 
            'Customer.last_name LIKE' => "%$phraze%", 
            'Customer.email LIKE' => "%$phraze%", 
            'Customer.contact_phone LIKE' => "%$phraze%" 
        ];
        $this->Paginator->settings['conditions'] = $conditions;
    } else {
        if ($this->Session->check('KeyAdmin.customer_phraze')) {
            $this->request->data['Customer']['phraze'] = $phraze = $this->Session->read('KeyAdmin.customer_phraze');
            $conditions['OR'] = [
                'Customer.first_name LIKE' => "%$phraze%",
                'Customer.last_name LIKE' => "%$phraze%",
                'Customer.email LIKE' => "%$phraze%",
                'Customer.contact_phone LIKE' => "%$phraze%"
            ];
        }
    }
    $this->Paginator->settings['conditions'] = $conditions;
    
    $this->set('customers', $this->Paginator->paginate());
    

    $this->set('title', __('Klienci'));
    $this->set('content_title', __('Klienci'));
    $this->set('content_subtitle', __('Lista klientów'));
    $this->set('buttons_template', 'Buttons/KeyCustomers');
  }

  public function admin_edit($id) {
    if (!$id) {
      throw new NotFoundException(__('Wybrany klient nie istnieje'));
    }
    
    $action = isset($this->request->query['action']) ? $this->request->query['action'] : null;

    $this->Customer->recursive = 1;
    $customer = $this->Customer->findById($id);
    if (!$customer) {
      throw new NotFoundException(__('Nie odnaleziono wybranego klienta'));
    }
    
    if ($this->request->is(array('post', 'put'))) {
      $this->request->data['Customer']['id'] = $id;
      $this->request->data['UserData']['id'] = $customer['UserData']['id'];
      
      $addressesIds = [-1];
      if (isset($this->request->data['UserAddress'])) {
        foreach ($this->request->data['UserAddress'] as $key => $item) {
          $addressesIds[] = $this->request->data['UserAddress'][$key]['id'];
        }
      }
      
      $existsAddresses = $this->Customer->UserAddress->find('all', array(
        'conditions' => array('UserAddress.user_id' => $id),
        'fields' => array('UserAddress.id')
      ));
      $existsAddressesIds = [-1];
      foreach ($existsAddresses as $item) {
        $existsAddressesIds[] = $item['UserAddress']['id'];
      }
      
      if ($this->Customer->saveAll($this->request->data)) {
        $this->Customer->UserAddress->deleteAll(array('UserAddress.id' => $existsAddressesIds, 'UserAddress.id !=' => $addressesIds));
        $this->Session->setFlash(__('Klient został zapisany.'), 'flash-success');
        if (empty($action)) {
          return $this->redirect(array('action' => 'index'));
        }
        $this->set('after', true);
      } else {
        $this->Session->setFlash(__('Nie udało się zapisać klienta. Sprawdź poprawność podanych danych na wszystkich zakładkach.'), 'flash-error');
      }
    }

    if (!$this->request->data) {
      $this->request->data = $customer;
    }
    
    $this->request->data['Customer']['password'] = '';

    $this->set('title', __('Klienci'));
    $this->set('content_title', __('Klienci'));
    $this->set('content_subtitle', __('Edycja klienta'));
    $this->set('buttons_template', 'Buttons/KeyCustomers');
    
    $countries = $this->Customer->UserAddress->Country->find('list', array(
      'fields' => array('Country.id', 'Country.name')
    ));
    $zones = $this->Customer->UserAddress->Zone->find('list', array(
      'conditions' => array(
        'Zone.country_id' => isset($this->request->data['UserAddress'][0]['country_id']) ? $this->request->data['UserAddress'][0]['country_id'] : 0
      ),
      'fields' => array('Zone.id', 'Zone.name')
    ));
    $this->set(compact('countries', 'zones'));
    
    $defaultCountry = $this->Customer->UserAddress->Country->find('first', array(
      'conditions' => array(
        'Country.default' => 1
      )
    ));
    $this->set('defaultCountryId', $defaultCountry['Country']['id']);
    
    $this->set('action', $action);
    $this->set('from', isset($this->request->query['from']) ? $this->request->query['from'] : null);
    $this->set('param', isset($this->request->query['param']) ? $this->request->query['param'] : null);

    $this->render('KeyCustomers/admin_form');
  }

  public function admin_create() {
    
    if ($this->request->is('post')) {
      $this->Customer->create();
      
      $group = $this->Customer->UserGroup->find('first', array(
        'conditions' => array(
          'admin' => 0
        )
      ));
      $this->request->data['Customer']['user_group_id'] = $group['UserGroup']['id'];
      
      if ($this->Customer->saveAll($this->request->data)) {
        $this->Session->setFlash(__('Klient został dodany.'), 'flash-success');
        return $this->redirect(array('action' => 'index'));
      }
      $this->Session->setFlash(__('Nie udało się dodać klienta. Sprawdź poprawność podanych danych na wszystkich zakładkach.'), 'flash-error');
    } else {
      $this->request->data = array('Customer' => array('guest_account' => 0), 'UserData' => array('status' => 1));
    }
    
    $this->request->data['Customer']['password'] = '';

    $this->set('title', __('Klienci'));
    $this->set('content_title', __('Klienci'));
    $this->set('content_subtitle', __('Dodawanie klienta'));
    $this->set('buttons_template', 'Buttons/KeyCustomers');
    
    $this->set('create', 'true');
    
    $countries = $this->Customer->UserAddress->Country->find('list', array(
      'fields' => array('Country.id', 'Country.name')
    ));
    $zones = $this->Customer->UserAddress->Zone->find('list', array(
      'conditions' => array(
        'Zone.country_id' => isset($this->request->data['UserAddress'][0]['country_id']) ? $this->request->data['UserAddress'][0]['country_id'] : 0
      ),
      'fields' => array('Zone.id', 'Zone.name')
    ));
    $this->set(compact('countries', 'zones'));
    
    $defaultCountry = $this->Customer->UserAddress->Country->find('first', array(
      'conditions' => array(
        'Country.default' => 1
      )
    ));
    $this->set('defaultCountryId', $defaultCountry['Country']['id']);
    $this->set('action', null);

    $this->render('KeyCustomers/admin_form');
  }

  public function admin_delete($id) {
    if (!$id) {
      throw new NotFoundException(__('Wybrany klient nie istnieje'));
    }
    $customer = $this->Customer->findById($id);
    if (!$customer) {
      throw new NotFoundException(__('Nie odnaleziono wybranego klienta'));
    }

    if ($this->Customer->delete($id, true)) {
      $this->Session->setFlash(__('Klient "%s" został usunięty.', h($customer['Customer']['customer'])), 'flash-success');
    } else {
      $this->Session->setFlash(__('Nie udało się usunąć klienta "%s".', h($customer['Customer']['customer'])), 'flash-error');
    }
    
    return $this->redirect(array('action' => 'index'));
  }

  public function admin_save_customer_status() {
    $customer_id = (int) $this->request->query['customer_id'];
    $value = (int) $this->request->query['value'];
    
    $this->Customer->recursive = 2;
    $customer = $this->Customer->findById($customer_id);
    
    $data = array(
      'UserData' => array(
        'status' => $value
      )
    );
    
    $this->Customer->UserData->id = $customer['UserData']['id'];
    if ($this->Customer->UserData->save($data)) {
      $result = array('success' => 1);
    } else {
      $result = array('success' => 0);
    }

    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set('result', $result);
    $this->set('_serialize', array('result'));
  }
  
  public function admin_get_zones() {
    $country_id = (int) $this->request->query['country_id'];

    $zonesArr = array();
    
    $zones = $this->Customer->UserAddress->Zone->find('all', array(
      'conditions' => array(
        'Zone.country_id' => $country_id
      ),
      'fields' => array('Zone.id', 'Zone.id', 'Zone.name')
    ));
    foreach ($zones as $zone) {
      $zonesArr[] = array('id' => $zone['Zone']['id'], 'name' => $zone['Zone']['name']);
    }
    
    $result = array(
      'success' => 1,
      'zones' => $zonesArr
    );

    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set('result', $result);
    $this->set('_serialize', array('result'));
  }
  
  public function admin_get_user_address_row() {
    $key = (int) $this->request->query['key'];
    
    $userAddress = array(
      'alias' => '',
      'default_billing' => $key == 0 ? 1 : 0,
      'default_shipping' => $key == 0 ? 1 : 0,
      'is_company' => 0
    );
    
    $this->request->data = array(
      'UserAddress' => array(
        $key => $userAddress
      )
    );
    
    $countries = $this->Customer->UserAddress->Country->find('list', array(
      'fields' => array('Country.id', 'Country.name')
    ));
    $defaultCountry = $this->Customer->UserAddress->Country->find('first', array(
      'conditions' => array(
        'Country.default' => 1
      )
    ));
    $zones = $this->Customer->UserAddress->Zone->find('list', array(
      'conditions' => array(
        'Zone.country_id' => $defaultCountry['Country']['id']
      ),
      'fields' => array('Zone.id', 'Zone.name')
    ));
    
    $defaultCountryId = $defaultCountry['Country']['id'];
    
    $view = new View($this, false);
    $view->viewPath = 'KeyCustomers';
    $view->layout = false;
    $view->set(compact('key', 'userAddress', 'countries', 'zones', 'defaultCountryId'));
    $html = $view->render('address_item');
     
    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set(array('html' => $html));
    $this->set('_serialize', array('html'));
  }
  
}
