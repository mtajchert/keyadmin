<?php

App::uses('Component', 'Controller');

class InvoicesComponent extends Component {
  
  protected $controller;
  
  public function __construct(\ComponentCollection $collection, $settings = array()) {
    parent::__construct($collection, $settings);
    $this->Order = ClassRegistry::init('KeyAdmin.Order');
    $this->Invoice = ClassRegistry::init('KeyAdmin.Invoice');
  }
  
  public function setController($controller) {
    $this->controller = $controller;
  }
  
  public function generateQuote($orderId) {
    if (is_numeric($orderId)) {
      $quote = array('Invoice' => array(
        'type' => 'QUOTE',
        'order_id' => $orderId
      ));
      $quote = $this->Invoice->save($quote);

      $this->Order->recursive = 2;
      $order = $this->Order->findById($orderId);
    } elseif (is_array($orderId)) {
      $quote = array('Invoice' => array(
        'type' => 'QUOTE',
        'order_id' => 0
      ));
      $quote = $this->Invoice->save($quote);

      $order = $orderId;
    }
    
    $plugin = $this->controller->plugin;
    $this->controller->plugin = CLIENT;
    $view = new View($this->controller, false);
    $view->viewPath = 'Invoice';
    $view->layout = false;
    $view->set(compact('order', 'quote'));
    $html = $view->render('quote');
    $this->controller->plugin = $plugin;
    
    $fpath = $this->Invoice->getPdfPath($quote);
    
    $mpdf = new mPDF('BLANK', 'A4', '9', 'Arial', 10, 10, 5, 5, 5, 5);
    
    $mpdf->SetHTMLHeader('<p style="width:100%; text-align:right; margin:0; padding:0; font-family:Arial; font-size:8pt; font-weight:bold; font-style:italic;">'.$quote['Invoice']['number_all'].' - Strona {PAGENO}/{nbpg}</span><hr style="margin:0;"/>');
    $mpdf->SetHTMLFooter('<hr style="margin:0;"/><p style="width:100%; text-align:right; margin:0; padding:0; font-family:Arial; font-size:8pt; font-weight:bold; font-style:italic;">'.$quote['Invoice']['number_all'].' - Strona {PAGENO}/{nbpg}</span>');
    
    @$mpdf->WriteHTML($html);
    @$mpdf->Output($fpath, 'F');
    
    return array('model' => $quote, 'path' => $fpath);
  }
  
  public function saveOrderInvoice($fixOrder, $data = array()) {
    if (is_numeric($fixOrder)) {
      $invoice = array('Invoice' => array(
        'type' => 'INVOICE',
        'order_id' => $fixOrder
      ));
      $invoice = array_merge_recursive($invoice, $data);
      $invoice = $this->Invoice->save($invoice);

      $this->Order->recursive = 2;      
      $order = $this->Order->findById($fixOrder);
    } elseif (is_array($fixOrder)) {
      $invoice = array('Invoice' => array(
        'type' => 'INVOICE',
        'order_id' => 0,
        'data' => serialize($fixOrder)
      ));
      $invoice = $this->Invoice->save($invoice);
      $order = $fixOrder;
    }
    $invoiceId = $this->Invoice->getLastInsertID();
    
    $this->Invoice->contain(array('Payment'));
    $invoice = $this->Invoice->read(null, $invoiceId);
    return $this->generateInvoice($invoice, $order);
  }
  
  
  
  public function updateOrderInvoice($invoiceId, $toUpdate = array()) {      
    $toUpdate['Invoice']['id'] = $invoiceId;
    $invoice = $this->Invoice->save($toUpdate);

  
    $this->Invoice->contain(array('Payment'));
    $invoice = $this->Invoice->read(null, $invoiceId);
    $this->Order->recursive = 2;
    $order = $this->Order->findById($invoice['Invoice']['order_id']);
    
    return $this->generateInvoice($invoice, $order);
  }
  
  
  public function generateInvoice($invoice, $order) {
    $plugin = $this->controller->plugin;
    $this->controller->plugin = CLIENT;
    $view = new View($this->controller, false);
    $view->viewPath = 'Invoice';
    $view->layout = false;
    $view->set(compact('order', 'invoice'));
    $html = $view->render('invoice');
    $this->controller->plugin = $plugin;
    
    $fpath = $this->Invoice->getPdfPath($invoice);
    $mpdf = new mPDF('BLANK', 'A4', '9', 'Arial', 10, 10, 5, 5, 5, 5);
    
    $mpdf->SetHTMLHeader('<p style="width:100%; text-align:right; margin:0; padding:0; font-family:Arial; font-size:8pt; font-weight:bold; font-style:italic;">'.$invoice['Invoice']['number_all'].' - Strona {PAGENO}/{nbpg}</span><hr style="margin:0;"/>');
    $mpdf->SetHTMLFooter('<hr style="margin:0;"/><p style="width:100%; text-align:right; margin:0; padding:0; font-family:Arial; font-size:8pt; font-weight:bold; font-style:italic;">'.$invoice['Invoice']['number_all'].' - Strona {PAGENO}/{nbpg}</span>');
    
    @$mpdf->WriteHTML($html);
    @$mpdf->Output($fpath, 'F');
    
    return array('model' => $invoice, 'path' => $fpath);
  }
  
}
