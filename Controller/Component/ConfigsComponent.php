<?php

App::uses('Component', 'Controller');

class ConfigsComponent extends Component {
  
  protected $configs = null;
  
  protected $configsValueText = array('email_signature');
  
  public function __construct(\ComponentCollection $collection, $settings = array()) {
    parent::__construct($collection, $settings);
    $this->Config = ClassRegistry::init('KeyAdmin.Config');
  }
  
  protected function load() {
    if (!is_null($this->configs)) {
      return;
    }
    $configs = $this->Config->find('all');
    $this->configs = array();
    foreach ($configs as $config) {
      $this->configs[$config['Config']['code']] = in_array($config['Config']['code'], $this->configsValueText) ? $config['Config']['valueText'] : $config['Config']['value'];
    }
  }

  public function get($code) {
    $this->load();
    return isset($this->configs[$code]) ? $this->configs[$code] : null;
  }
  
}
