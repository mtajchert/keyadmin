<?php

App::uses('Component', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class NotificationCenterComponent extends Component {
  
  public $components = array('KeyAdmin.Configs');
  
  protected $fields = array(
    array(
      'binary' => 0,
      'name' => 'Znaczniki ogólne',
      'fields' => array(
        array('{BIEZACA_DATA}', 'Bieżąca data np. 31.12.2015'),
        array('{BIEZACA_GODZINA}', 'Bieżąca godzina np. 21:59'),
        array('{PODPIS}', 'Podpis do wiadomości email (z konfiguracji)'),
        array('{NAZWA_SKLEPU}', 'Nazwa sklepu (z konfiguracji)'),
        array('{ADRES_WWW_SKLEPU}', 'Adres strony internetowej sklepu'),
        array('{ADRES_WWW_SKLEPU_HTTP}', 'Adres strony internetowej sklepu z prefixem http/https'),
      )
    ),
    array(
      'binary' => 1,
      'name' => 'Znaczniki dotyczące konta klienta',
      'fields' => array(
        array('{NAZWA_KLIENTA}', 'Imię i nazwisko klienta'),
        array('{EMAIL_KLIENTA}', 'Adres e-mail klienta'),
        array('{ADRES_KLIENTA}', 'Pełny adres domowy klienta'),
        array('{LINK_RESETOWANIA_HASLA}', 'Link do strony resetowania hasła dla danego klienta')
      )
    ),
    array(
      'binary' => 2,
      'name' => 'Znaczniki dotyczące zamówienia',
      'fields' => array(
        array('{NUMER_ZAMOWIENIA}', 'Numer zamówienia'),
        array('{LINK_PODGLAD_ZAMOWIENIA}', 'Link do podglądu zamówienia'),
        array('{DATA_ZAMOWIENIA}', 'Data złożenia zamówienia np. 31.12.2015'),
        array('{GODZINA_ZAMOWIENIA}', 'Godzina złożenia zamówienia np. 21:59'),
        array('{STATUS_ZAMOWIENIA}', 'Bieżący status zamówienia'),
        array('{LISTA_PRODUKTOW}', 'Lista produktów z zamówienia'),
        array('{TABELA_PRODUKTOW}', 'Tabela produktów z zamówienia'),
        array('{TABELA_PRODUKTOW_PROSTA}', 'Tabela produktów z zamówienia'),
        array('{FORMA_PLATNOSCI_NAZWA}', 'Nazwa wybranej formy płatności'),
        array('{FORMA_PLATNOSCI_INFO}', 'Dodatkowe informacje wybranej formy płatności'),
        array('{FORMA_PLATNOSCI_DANE}', 'Dane do rozliczenia'),
        array('{FORMA_DOSTAWY_CENA}', 'Cena wybranej formy dostawy dla tego zamówienia'),
        array('{FORMA_DOSTAWY_NAZWA}', 'Nazwa wybranej formy dostawy'),
        array('{FORMA_DOSTAWY_INFO}', 'Dodatkowe informacje wybranej formy dostawy'),
        array('{FORMA_DOSTAWY_ADDRESS}', 'Adres dostawy'),
        array('{CENA_PRODUKTOW}', 'Łączna cena produktów np. 1 234,56 zł'),
        array('{CENA_ZAMOWIENIA}', 'Łączna cena zamówienia np. 1 234,56 zł'),
        array('{NUMER_LISTU}', 'Numer listu przewozowego np. 123456789')
      )
    )
  );
  
  protected $isFieldsTranslated = false;
  
  public function __construct(\ComponentCollection $collection, $settings = array()) {
    parent::__construct($collection, $settings);
    $this->Notification = ClassRegistry::init('KeyAdmin.Notification');
  }

  public function getBody($code, $order = array(), $user = array()) {
    $notification = $this->Notification->find('first', array(
        'conditions' => array(
            'code' => $code
        )
    ));
    if (!$notification) {
      throw new InternalErrorException('Notification not found');
    }
    $data = $this->getDataArray($notification['Notification']['fields'], $user, $order);
    
    $subject = $this->prepareText($notification['Notification']['subject'], $data);
    $message = $this->prepareText($notification['Notification']['message'], $data);
  
    return array('subject' => $subject, 'message' => $message);
  }
  
  public function send($code, $recipient, $user, $order = array(), $attachment = array()) {
    $notification = $this->Notification->find('first', array(
      'conditions' => array(
        'code' => $code
      )
    ));
    if (!$notification) {
      throw new InternalErrorException('Notification not found');
    }
    
    $data = $this->getDataArray($notification['Notification']['fields'], $user, $order);
    $subject = $this->prepareText($notification['Notification']['subject'], $data);
    $message = $this->prepareText($notification['Notification']['message'], $data);
    
    $email = new CakeEmail();
    $email->config('notifications');
    
    $emailConfig = new EmailConfig();
    if (is_array($emailConfig->notifications['from'])) {
      $from = $emailConfig->notifications['from'];
    } else {
      $from = array($emailConfig->notifications['from'] => $this->Configs->get('shop_name'));
    }
    
    $email->from($from);
    $email->to(array($recipient[0] => (isset($recipient[1]) ? $recipient[1] : $recipient[0])));
    $contactEmail = Configure::read('Email.contact_email');
    $email->replyTo($contactEmail);
    $email->subject($subject);
    
    if (!empty($attachment)){
      $email->attachments($attachment);
    }
    
    
    return $email->send($message);
  }
  
  protected function translateFields() {
    if ($this->isFieldsTranslated) {
      return;
    }
    
    foreach ($this->fields as $fieldsGroup) {
      $fieldsGroup['name'] = __($fieldsGroup['name']);
      foreach ($fieldsGroup['fields'] as $field) {
        $field[0] = __($field[0]);
        $field[1] = __($field[1]);
      }
    }
  }
  
  public function getFieldsList($binary, $nested = true) {
    $this->translateFields();
    $fields = array();
    foreach ($this->fields as $fieldsGroup) {
      if (empty($fieldsGroup['binary']) || $binary & $fieldsGroup['binary']) {
        if ($nested) {
          $fields[] = $fieldsGroup;
        } else {
          $fields = array_merge($fields, $fieldsGroup['fields']);
        }
      }
    }
    
    return $fields;
  }
  
  public function prepareText($text, $data) {
    return str_replace(array_keys($data), array_values($data), $text);
  }
  
  protected function getDataArray($binary, $user, $order) {
    $data = array();
    $userKey = null;
    if (!empty($user)) {
      $keys = array_keys($user);
      $userKey = array_shift($keys);
    }
    
    $fields = $this->getFieldsList($binary, false);
    foreach ($fields as $field) {
      switch ($field[0]) {
        case '{BIEZACA_DATA}':
          $data[$field[0]] = date('d.m.Y');
          break;
        case '{BIEZACA_GODZINA}':
          $data[$field[0]] = date('H:i');
          break;
        case '{PODPIS}':
          $data[$field[0]] = $this->Configs->get('email_signature');
          break;
        case '{NAZWA_SKLEPU}':
          $data[$field[0]] = $this->Configs->get('shop_name');
          break;
        case '{ADRES_WWW_SKLEPU}':
          $data[$field[0]] = trim(str_replace(['http://', 'https://'], ['', ''], Router::url('/', true)), '/');
          break;
        case '{ADRES_WWW_SKLEPU_HTTP}':
          $data[$field[0]] = Router::url('/', true);
          break;        
        case '{NAZWA_KLIENTA}':
          $data[$field[0]] = empty($userKey) ? null : trim($user[$userKey]['first_name'].' '.$user[$userKey]['last_name']);
          break;
        case '{EMAIL_KLIENTA}':
          $data[$field[0]] = empty($userKey) ? null : $user[$userKey]['email'];
          break;
        case '{ADRES_KLIENTA}':
          if (empty($userKey) || !isset($order['ShippingOrderUserAddress'])) {
            $data[$field[0]] = null;
          } else {
            $this->UserAddress = ClassRegistry::init('KeyAdmin.UserAddress');
            $this->UserAddress->set(array('UserAddress' => $order['ShippingOrderUserAddress']));
            $data[$field[0]] = $this->UserAddress->getAddressString('<br/>');
          }
          break;
        case '{LINK_RESETOWANIA_HASLA}':
          $data[$field[0]] = @Router::url(array('plugin' => CLIENT, 'controller' => 'Auth', 'action' => 'reset_password', 'admin' => false, 'e' => $user[$userKey]['email'], 'h' => $user[$userKey]['hash']), true);
          break;
        
        case '{NUMER_ZAMOWIENIA}':
          $data[$field[0]] = $order['Order']['number'];
          break;
        case '{LINK_PODGLAD_ZAMOWIENIA}':
          $previewUrl = Router::url(array('plugin' => CLIENT, 'controller' => 'Order', 'action' => 'preview', 'admin' => false, $order['Order']['hash']), true);
          $data[$field[0]] = $previewUrl;
          break;
        case '{DATA_ZAMOWIENIA}':
          $data[$field[0]] = date('d.m.Y', strtotime($order['Order']['created']));
          break;
        case '{GODZINA_ZAMOWIENIA}':
          $data[$field[0]] = date('H:i', strtotime($order['Order']['created']));
          break;
        case '{STATUS_ZAMOWIENIA}':
          $data[$field[0]] = $order['OrderStatus']['name'];
          break;
        case '{LISTA_PRODUKTOW}':
          $string = '';
          if (isset($order['OrderProduct']) && !empty($order['OrderProduct'])) {
            $string = '<ul>';
            foreach ($order['OrderProduct'] as $orderProduct) {
              $string .= '<li>'.$orderProduct['name'];
              if (!empty($orderProduct['add_info'])) {
                $string .= ', '.$orderProduct['add_info'];
              }
              $string .= ', '.$orderProduct['amount'].' '.$orderProduct['Unit']['name'];
              $string .= ', '.$orderProduct['total_price_tax'].' '.__('zł').'</li>';
            }
            $string .= '</ul>';
          }
          $data[$field[0]] = $string;
          break;
        case '{TABELA_PRODUKTOW_PROSTA}':
          $string = '';
          if (isset($order['OrderProduct']) && !empty($order['OrderProduct'])) {
            $string = '<table style="width: 100%;max-width: 100%;border-spacing: 0;border-collapse: collapse;">';
            $thStyle = 'vertical-align: bottom;border-bottom: 2px solid #ddd;padding: 8px;line-height: 1.42857143;text-align: left;';
            $string .= '<thead><tr>
              <th style="'.$thStyle.'">'.__('Produkt').'</th>
              <th style="'.$thStyle.'">'.__('Cena').'</th>
              <th style="'.$thStyle.'">'.__('Ilość').'</th>
              <th style="'.$thStyle.'">'.__('Wartość').'</th>
            </tr></thead>';
          
            $tdStyle = 'padding: 8px;line-height: 1.42857143;vertical-align: top;border-top: 1px solid #ddd;';
            foreach ($order['OrderProduct'] as $orderProduct) {
              $string .= '<tr>';
              if (!empty($orderProduct['add_info'])) {
                  $string .= '<td style="'.$tdStyle.'">'.$orderProduct['name']." - {$orderProduct['add_info']}</td>";
              } else {
                  $string .= '<td style="'.$tdStyle.'">'.$orderProduct['name']."</td>";
              }              
              $string .= '<td style="'.$tdStyle.'">'.$orderProduct['price_tax'].' '.__('zł').'</td>';
              $string .= '<td style="'.$tdStyle.'">'.$orderProduct['amount'].' '.$orderProduct['Unit']['name'].'</td>';
              $string .= '<td style="'.$tdStyle.'">'.$orderProduct['total_price_tax'].' '.__('zł').'</td>';
              $string .= '</tr>';
            }
            $string .= '</table>';
          }
          $data[$field[0]] = $string;
          break;
        case '{TABELA_PRODUKTOW}':
          $string = '';
          if (isset($order['OrderProduct']) && !empty($order['OrderProduct'])) {
            $string = '<table style="width: 100%;max-width: 100%;border-spacing: 0;border-collapse: collapse;">';
            $thStyle = 'vertical-align: bottom;border-bottom: 2px solid #ddd;padding: 8px;line-height: 1.42857143;text-align: left;';
            $string .= '<thead><tr>
              <th style="'.$thStyle.'">'.__('Produkt').'</th>
              <th style="'.$thStyle.'">'.__('Nr katalogowy').'</th>
              <th style="'.$thStyle.'">'.__('Kod producenta').'</th>
              <th style="'.$thStyle.'">'.__('Cena').'</th>
              <th style="'.$thStyle.'">'.__('Ilość').'</th>
              <th style="'.$thStyle.'">'.__('Wartość').'</th>
            </tr></thead>';
            
            $tdStyle = 'padding: 8px;line-height: 1.42857143;vertical-align: top;border-top: 1px solid #ddd;';
            foreach ($order['OrderProduct'] as $orderProduct) {
              $string .= '<tr>';
              $string .= '<td style="'.$tdStyle.'">'.$orderProduct['name'].'</td>';
              $string .= '<td style="'.$tdStyle.'">'.$orderProduct['catalog_no'].'</td>';
              $string .= '<td style="'.$tdStyle.'">'.$orderProduct['ext_code'].'</td>';
              $string .= '<td style="'.$tdStyle.'">'.$orderProduct['price_tax'].' '.__('zł').'</td>';
              $string .= '<td style="'.$tdStyle.'">'.$orderProduct['amount'].' '.$orderProduct['Unit']['name'].'</td>';
              $string .= '<td style="'.$tdStyle.'">'.$orderProduct['total_price_tax'].' '.__('zł').'</td>';
              $string .= '</tr>';
            }
            $string .= '</table>';
          }
          $data[$field[0]] = $string;
          break;
        case '{FORMA_PLATNOSCI_NAZWA}':
          $data[$field[0]] = $order['Payment']['name'];
          break;
        case '{FORMA_PLATNOSCI_INFO}':
          $data[$field[0]] = $order['Payment']['info'];
          break;
        case '{FORMA_PLATNOSCI_DANE}':
          $this->OrderAddress = ClassRegistry::init('KeyAdmin.UserAddress');
          if (!empty($order['InvoiceOrderUserAddress']['id'])) {
              $data[$field[0]] = $this->OrderAddress->getFormattedAddress($order['InvoiceOrderUserAddress'], '<br/>');
          } else {
              $data[$field[0]] = $this->OrderAddress->getFormattedAddress($order['BillingOrderUserAddress'], '<br/>');
          }
          break;
        case '{FORMA_DOSTAWY_CENA}':
          $data[$field[0]] = $order['Order']['shipping_price_tax'].__('zł');
          break;
        case '{FORMA_DOSTAWY_NAZWA}':
          $data[$field[0]] = $order['Shipping']['name'];
          break;
        case '{FORMA_DOSTAWY_INFO}':
          $data[$field[0]] = $order['Shipping']['info'];
          break;
        case '{FORMA_DOSTAWY_ADDRESS}':
          $this->OrderAddress = ClassRegistry::init('KeyAdmin.UserAddress');
          $data[$field[0]] = $this->OrderAddress->getFormattedAddress($order['ShippingOrderUserAddress'], '<br/>');
          break;
        case '{CENA_PRODUKTOW}':
          $data[$field[0]] = $order['Order']['products_price_tax'].__('zł');
          break;
        case '{CENA_ZAMOWIENIA}':
          $data[$field[0]] = $order['Order']['order_price_tax'].__('zł');
          break;
        case '{NUMER_LISTU}':
          $data[$field[0]] = $order['Order']['shipment_no'];
          break;
      }
    }
    
    return $data;
  }
  
}
