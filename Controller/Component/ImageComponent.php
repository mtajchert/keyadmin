<?php 
App::uses('Component', 'Controller');

class ImageComponent extends Component {
	
	protected static $arr_formats_ext = array('jpg','png','gif','bmp','tiff','svg','psd');
	
	protected static $arr_formats_ext_image = array('jpg','png','gif','bmp','tiff','svg');
	
	protected static $arr_formats_ext_cache = null;
	
	public static function isImageMagick() {
		return class_exists('Imagick', false);
	}
	
	public static function getSupportedThumbFormats($_bool_get_all = false) {
		if ($_bool_get_all) {
			return self::$arr_formats_ext;
		}
		if (is_null(self::$arr_formats_ext_cache)) {
			self::$arr_formats_ext_cache = array();
			if (self::isImageMagick()) {
				$obj_im = new Imagick();
				$arr_platform_supp_formats = $obj_im->queryFormats();
				unset($obj_im);
			} else {
				$arr_platform_supp_formats = array('jpg');
			}
			
			foreach (self::$arr_formats_ext as $str_ext) {
				if (in_array(strtoupper($str_ext), $arr_platform_supp_formats) || in_array(strtolower($str_ext), $arr_platform_supp_formats)) {
					self::$arr_formats_ext_cache[] = strtolower($str_ext);
				}
			}
		}
		return self::$arr_formats_ext_cache;
	}
	
	public static function getSupportedImageFormats() {
		return array_intersect(self::$arr_formats_ext_image, self::getSupportedThumbFormats());
	}
	
	/**
	 * Jakas metoda z neta ktora jest do dupy i lepiej jej nie uzywac chyba ze do rozjebywania obrazow
	 */
	public static function CroppedThumbnail($imgSrc,$thumbnail_width,$thumbnail_height) { //$imgSrc is a FILE - Returns an image resource.
	    //getting the image dimensions 
	    list($width_orig, $height_orig) = getimagesize($imgSrc);  
	    $myImage = imagecreatefromjpeg($imgSrc);
	    $ratio_orig = $width_orig/$height_orig;
	   
	    if ($thumbnail_width/$thumbnail_height > $ratio_orig) {
	       $new_height = $thumbnail_width/$ratio_orig;
	       $new_width = $thumbnail_width;
	    } else {
	       $new_width = $thumbnail_height*$ratio_orig;
	       $new_height = $thumbnail_height;
	    }
	   
	    $x_mid = $new_width/2;  //horizontal middle
	    $y_mid = $new_height/2; //vertical middle
	   
	    $process = imagecreatetruecolor(round($new_width), round($new_height));
	   
	    imagecopyresampled($process, $myImage, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
	    $thumb = imagecreatetruecolor($thumbnail_width, $thumbnail_height);
	    imagecopyresampled($thumb, $process, 0, 0, ($x_mid-($thumbnail_width/2)), ($y_mid-($thumbnail_height/2)), $thumbnail_width, $thumbnail_height, $thumbnail_width, $thumbnail_height);
	
	    imagedestroy($process);
	    imagedestroy($myImage);
	    return $thumb;
	}
	
	/**
	 * Metoda skalujaca obraz do podanych wymiarow lub w odpowiedniej skali jesli podamy tylko jeden wymiar
	 */
	public static function resizeImage($_str_src_path, $_str_dst_path, $_int_dst_width, $_int_dst_height, $_int_quality = 100) {
		
		if ($_int_dst_width > 0 && $_int_dst_height > 0) {
			$obj_new_img = self::CroppedThumbnail($_str_src_path,$_int_dst_width,$_int_dst_height);
			imagejpeg($obj_new_img, $_str_dst_path, $_int_quality);
			imagedestroy($obj_new_img);
			return;
		}
		
		list($int_orig_width, $int_orig_height) = getimagesize($_str_src_path);
		
		$int_dst_width = 0;
		$int_dst_height = 0;
		
		$int_src_x = 0;
		$int_src_y = 0;
		
		if ($_int_dst_width == 0 || $_int_dst_height == 0) {
			if ($_int_dst_width > 0) {
				$int_dst_width = $_int_dst_width;
				$int_dst_height = round($_int_dst_width / $int_orig_width * $int_orig_height);
			} else {
				$int_dst_width = round($_int_dst_height / $int_orig_height * $int_orig_width);
				$int_dst_height = $_int_dst_height;
			}
		} else {
			//skalowanie z wysrodkowaniem nie dziala - wykorzystuje wiec wyzej funkcje z php.net
			$int_dst_width = $_int_dst_width;
			$int_dst_height = $_int_dst_height;
			
			$int_scale_w = $int_dst_width / $int_orig_width;
			$int_scale_h = $int_dst_height / $int_orig_height;
			
			//wysrodkowujemy w poziomie
			if ($int_scale_w <= $int_scale_h) {
				$int_src_x = round((($int_orig_width * $int_scale_h) - $int_dst_width) / 2);
				$int_src_y = 0;
				$int_orig_width -= 2 * $int_src_x;
			} else {
				$int_src_x = 0;
				$int_src_y = round((($int_orig_height * $int_scale_w) - $int_dst_height) / 2);
				$int_orig_height -= 2 * $int_src_y;
			}
		}
		
		$obj_new_img = imagecreatetruecolor($int_dst_width, $int_dst_height);
		$obj_tmp_img = imagecreatefromjpeg($_str_src_path);
		imagecopyresampled($obj_new_img, $obj_tmp_img, 0, 0, $int_src_x, $int_src_y, $int_dst_width, $int_dst_height, $int_orig_width, $int_orig_height);
		
		imagejpeg($obj_new_img, $_str_dst_path, $_int_quality);
		
		imagedestroy($obj_new_img);
		imagedestroy($obj_tmp_img);
	}
	
	/**
	 * Metoda do ktorej przekazujemy maksymalne wymiary obrazu a ona sobie ustala jak przeskalowac obraz zeby sie zmiescil w tych wymiarach z zachowaniem proporcji
	 */
	public static function resizeImageMaxDimensions($_str_src_path, $_str_dst_path, $_int_dst_width, $_int_dst_height, $_int_quality = 100) {
		$int_orig_width = 0;
		$int_orig_height = 0;
		list($int_orig_width, $int_orig_height) = getimagesize($_str_src_path);
		if (($int_orig_width - $_int_dst_width) > ($int_orig_height - $_int_dst_height)) {
			$_int_dst_width = $int_orig_width > $_int_dst_width ? $_int_dst_width : $int_orig_width;
			$_int_dst_height = 0;
		} else {
			$_int_dst_width = 0;
			$_int_dst_height = $int_orig_height > $_int_dst_height ? $_int_dst_height : $int_orig_height;
		}
		
		return self::resizeImage($_str_src_path, $_str_dst_path, $_int_dst_width, $_int_dst_height, $_int_quality);
	}
	
	public static function blurImage($_str_src_path, $_str_dst_path, $_int_blur = 50) {
		//copy($_str_src_path, $_str_dst_path);
		//return;
		$image = imagecreatefromjpeg($_str_src_path);
		for ($i=0;$i<=$_int_blur;$i++){
			imagefilter($image, IMG_FILTER_GAUSSIAN_BLUR);
		}
		imagejpeg($image, $_str_dst_path);
	}
	
	/**
	 * Konwersja obrazow do jpg
	 * @param string $_str_src_path
	 * @param string $_str_dst_path
	 * @return boolean
	 */
	public static function convertToJpg($_str_src_path, $_str_dst_path) {
		//sprawdzenie czy obslugiwany format
		$arr_src = explode('.', $_str_src_path);
		$str_src_ext = strtolower((string)array_pop($arr_src));
		$arr_supp = self::getSupportedThumbFormats();
		if (!in_array($str_src_ext, $arr_supp)) {
			return false;
		}
		
		//sprawdzenie jesli plik jpg to kopiujemy
		if ($str_src_ext == 'jpg') {
			copy($_str_src_path, $_str_dst_path);
			return true;
		}
		//jesli mamy ImageMagick to konwertujemy
		elseif (self::isImageMagick()) {
			$obj_im = new Imagick($_str_src_path);
			$obj_im -> setImageFormat('jpeg');
			$obj_im -> setImageCompression(Imagick::COMPRESSION_JPEG); 
			$obj_im -> setImageCompressionQuality(100);
			$obj_im -> flattenimages();
			$obj_im -> writeimage($_str_dst_path);
			$obj_im -> clear();
			$obj_im -> destroy();
			unset($obj_im);
		}
		//jesli nie jpg a nie mamy ImageMagick to zwracamy blad
		else {
			return false;
		}
		
		return true;
	}
	
	/**
	 * 
	 * @param string $_str_src_path - Sciezka pliku zrodlowego
	 * @param string $_str_dst_path - Sciezka pliku docelowego
	 * @param int $_int_dst_w - Szerokosc docelowa
	 *		-1 - oryginalny rozmiar
	 *		0 - rozmiar zostanie wyliczony
	 *		>0 - konkretny rozmiar
	 * @param int $_int_dst_h - Wysokosc docelowa
	 *		-1 - oryginalny rozmiar
	 *		0 - rozmiar zostanie wyliczony
	 *		>0 - konkretny rozmiar
	 * @param int $_int_options - Opcje resizowania bitowo:
	 *		1 - skalowanie obrazu (set: tak, noset: nie, default: set)
	 *		2 - przycinanie/rozciaganie (tylko jesli wlaczono skalowanie, set: przycinanie, noset: rozciaganie, default: set)
	 *		4 - wysrodkowanie (set: tak, noset: nie, default: set)
	 *		8 - maksymalne wymiary (set: podano max wymiary, nalezy wyliczyc wlasciwe, noset: podano normalnie, default: noset)
	 * @return boolean
	 */
	public static function convertToJpgAndResize($_str_src_path, $_str_dst_path, $_int_dst_w, $_int_dst_h, $_int_options = 7) {
		//sprawdzenie czy obslugiwany format
		$arr_src = explode('.', $_str_src_path);
		$str_src_ext = strtolower((string)array_pop($arr_src));
		$arr_supp = self::getSupportedThumbFormats();
		if (!in_array($str_src_ext, $arr_supp)) {
			return false;
		}
		
		//obiekt ImageMagick o ile dostepne
		$obj_im = self::isImageMagick() ? new Imagick($_str_src_path) : null;
		
		//pobranie oryginalnych wymiarow obrazu
		if ($obj_im) {
			$int_orig_w = $obj_im -> getimagewidth();
			$int_orig_h = $obj_im -> getimageheight();
		} else {
			list($int_orig_w, $int_orig_h) = getimagesize($_str_src_path);
		}
		$int_src_w = $int_orig_w;
		$int_src_h = $int_orig_h;
		
		//ustalenie/wyliczenie wymiarow nowego obrazu
		$_int_dst_w == -1 ? $_int_dst_w = $int_orig_w : null;
		$_int_dst_h == -1 ? $_int_dst_h = $int_orig_h : null;
		if (($_int_options & 8) > 0 && $_int_dst_w > 0 && $_int_dst_h > 0) {
			if ($_int_dst_w / $int_orig_w < $_int_dst_h / $int_orig_h) {
				$_int_dst_h = 0;
			} else {
				$_int_dst_w = 0;
			}
		} elseif ($_int_dst_w <= 0 && $_int_dst_h <= 0) {
			$_int_dst_w = $int_orig_w;
			$_int_dst_h = $int_orig_h;
		}
		if ($_int_dst_w <= 0 || $_int_dst_h <= 0) {
			if ($_int_dst_w > 0) {
				$_int_dst_h = round($_int_dst_w / $int_orig_w * $int_orig_h);
			} else {
				$_int_dst_w = round($_int_dst_h / $int_orig_h * $int_orig_w);
			}
		}
		
		//wyliczenie rozmiarow skalowanego obrazu do skopiowania
		$int_sc_w = $_int_dst_w / $int_orig_w;
		$int_sc_h = $_int_dst_h / $int_orig_h;
		if (($_int_options & 1) > 0) {
			//przycinanie lub rozciaganie
			if (($_int_options & 2) > 0) {
				if ($int_sc_w-1 > $int_sc_h-1) {
					$int_src_w = $int_orig_w;
					$int_src_h = $_int_dst_h * $int_src_w / $_int_dst_w;
				} else {
					$int_src_h = $int_orig_h;
					$int_src_w = $_int_dst_w * $int_src_h / $_int_dst_h;
				}
			} else {
				$int_src_w = $int_orig_w;
				$int_src_h = $int_orig_h;
			}
		} else {
			$int_src_w = $_int_dst_w;
			$int_src_h = $_int_dst_h;
		}
		
		//ustalenie pozycji obszaru do wyciecia jesli wysrodkowujemy
		$int_src_x = $int_src_y = 0;
		if (($_int_options & 4) > 0) {
			$int_src_x = ($int_orig_w - $int_src_w) / 2;
			$int_src_y = ($int_orig_h - $int_src_h) / 2;
		}
		
		//wyciecie obrazu i zapis
		if ($obj_im) {
			$obj_im = new Imagick($_str_src_path);
			$obj_im -> setImageFormat('jpeg');
			$obj_im -> setImageCompression(Imagick::COMPRESSION_JPEG); 
			$obj_im -> setImageCompressionQuality(100);
			$obj_im -> flattenimages();
			$obj_im -> cropimage($int_src_w, $int_src_h, $int_src_x, $int_src_y);
			$obj_im -> resizeimage($_int_dst_w, $_int_dst_h, Imagick::FILTER_LANCZOS, 1);
			$obj_im -> writeimage($_str_dst_path);
			$obj_im -> clear();
			$obj_im -> destroy();
			unset($obj_im);
		} else {
			$obj_new_img = imagecreatetruecolor($_int_dst_w, $_int_dst_h);
			$obj_tmp_img = imagecreatefromjpeg($_str_src_path);
			//echo "0, 0, $int_x, $int_y, $_int_dst_w, $_int_dst_h, $int_orig_w, $int_orig_h";die;
			imagecopyresampled($obj_new_img, $obj_tmp_img, 0, 0, $int_src_x, $int_src_y, $_int_dst_w, $_int_dst_h, $int_src_w, $int_src_h);

			imagejpeg($obj_new_img, $_str_dst_path, 100);

			imagedestroy($obj_new_img);
			imagedestroy($obj_tmp_img);
		}
		
		return true;
	}
	
	public static function getEscapeFileName($_str_src_name) {
		return preg_replace('/![a-zA-Z0-9_-]/', '_', $_str_src_name);
	}
	
	public static function getEscapeUrlName($_str_src_name) {
		$_str_src_name = str_replace(
			preg_split('~~u', 'ąćęłńóśźżüĄĆĘŁŃÓŚŹŻÜ|', null, PREG_SPLIT_NO_EMPTY),
			preg_split('~~u', 'acelnoszzuACELNOSZZU ', null, PREG_SPLIT_NO_EMPTY),
			$_str_src_name
		);
		$_str_src_name = preg_replace('/![a-zA-Z0-9_-]/', '	', $_str_src_name);
		
		$_str_src_name = trim($_str_src_name);
		$_str_src_name = preg_replace('/\s+/', '-', $_str_src_name);
		return strtolower($_str_src_name);
	}
	
	public static function getJpgCommonColor($_str_img_path) {
		$obj_img = ImageCreateFromJpeg($_str_img_path);
		$int_w = imagesx($obj_img);
		$int_h = imagesy($obj_img);
		
		$arr_map = array();
		for ($int_i = 0; $int_i < $int_w; $int_i++) {
			for ($int_j = 0; $int_j < $int_h; $int_j++) {
				$int_rgb = ImageColorAt($obj_img, $int_i, $int_j);
				$str_rgb = (($int_rgb >> 16) & 0xFF).','.(($int_rgb >> 8) & 0xFF).','.($int_rgb & 0xFF);
				if (!array_key_exists($str_rgb, $arr_map)) {
					$arr_map[$str_rgb] = 0;
				}
				$arr_map[$str_rgb]++;
			}
		}
		
		$str_max_rgb = '';
		$int_max_val = 0;
		foreach ($arr_map as $str_rgb_token => $int_max_token) {
			if ($int_max_token > $int_max_val) {
				$int_max_val = $int_max_token;
				$str_max_rgb = $str_rgb_token;
			}
		}
		
		$arr_hex = explode(',', $str_max_rgb);
		$arr_hex[0] = dechex($arr_hex[0]);
		$arr_hex[1] = dechex($arr_hex[1]);
		$arr_hex[2] = dechex($arr_hex[2]);
		$str_hex = '#'
			.(strlen($arr_hex[0]) < 2 ? '0' : '').$arr_hex[0]
			.(strlen($arr_hex[1]) < 2 ? '0' : '').$arr_hex[1]
			.(strlen($arr_hex[2]) < 2 ? '0' : '').$arr_hex[2];
		
		return $str_hex;
		
		echo '<div style="display:block;float:left;width:300px;height:100px;background-color:rgb('.$str_max_rgb.');"></div>';
die;


for ($i=0; $i<$imgw; $i++)
{
	for ($j=0; $j<$imgh; $j++)
	{
		// get the rgb value for current pixel
		$rgb = ImageColorAt($im, $i, $j); 
		
		// extract each value for r, g, b
		
		$r = ($rgb >> 16) & 0xFF;
		$g = ($rgb >> 8) & 0xFF;
		$b = $rgb & 0xFF;
		
		if (!array_key_exists("$r,$g,$b", $map)) {
			$map["$r,$g,$b"] = 0;
		}
		$map["$r,$g,$b"]++;
	
	}
}

$moo = 0;
$rgb = '';
foreach ($map as $k => $v) {
	if ($v > $moo) {
		$moo = $v;
		$rgb = $k;
	}
}

echo '<img src="'.$source_file.'" />';
$hex=explode(',',$rgb);
$hex[0] = dechex($hex[0]);
strlen($hex[0]) < 2 ? $hex[0] = '0'.$hex[0] : null;
$hex[1] = dechex($hex[1]);
strlen($hex[1]) < 2 ? $hex[1] = '0'.$hex[1] : null;
$hex[2] = dechex($hex[2]);
strlen($hex[2]) < 2 ? $hex[2] = '0'.$hex[2] : null;
echo '<div style="display:block;float:left;width:300px;height:100px;background-color:#'.$hex[0].$hex[1].$hex[2].';"></div>';
echo '<div style="display:block;float:left;width:300px;height:100px;background-color:rgb('.$rgb.');"></div>';

	}
	
}
