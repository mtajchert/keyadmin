<?php
App::uses('Component', 'Controller');

class ReportsComponent extends Component {
  protected $controller;



  public function __construct(\ComponentCollection $collection, $settings = array()) {
    parent::__construct($collection, $settings);
    $this->Order = ClassRegistry::init('KeyAdmin.Order');
  }



  public function setController($controller) {
    $this->controller = $controller;
  }



  public function generateOrdersReport($date_from, $date_to, $type) {
    $orders = $this->Order->find('all', array(
      'contain' => array(
        'OrderProduct',
        'OrderProduct.TaxRate',
        'OrderProduct.Product',
        'Shipping',
        'Shipping.TaxRate',
        'Payment'
      ),
      'conditions' => array(
        array(
          'AND' => array(
            'Order.created >=' => $date_from,
            'Order.created <=' => $date_to
          )
        )
      )
    ));
    
    if ($type == 'PDF') {
      $view = new View($this->controller, false);
      $view->viewPath = 'KeyOrders';
      $view->layout = false;
      $view->set('orders', $orders);
      $view->set('date_from', $date_from);
      $view->set('date_to', $date_to);
      $html = $view->render('orders_report');
      
      $mpdf = new mPDF();
      $mpdf->SetHTMLHeader('<table width="100%" style="vertical-align:bottom; font-size:10px; color:#888; font-style:italic;"><tr><td style="text-align:left;">Raport sprzedaży, zamówienia od ' . $date_from . ' do ' . $date_to . '</td></tr></table>');
      $mpdf->SetHTMLFooter('<table width="100%" style="vertical-align:bottom; font-size:10px; color:#888; font-style:italic;"><tr><td style="text-align:left;">Wygenerowano: ' . date('Y-m-d H:i:s') . '</td><td style="text-align:right;">{PAGENO}/{nbpg}</td></tr></table>');
      
      $mpdf->WriteHTML($html);
      $mpdf->Output('Raport sprzedaży - zamówienia od ' . substr($date_from, 0, 10) . ' do ' . substr($date_to, 0, 10) . '.pdf', 'D');
      exit();
    } else if ($type == 'CSV') {
      $file = fopen(TMP . "order-report.csv","w");
      
      fputcsv($file, array('Nr zam.', 'Nazwa prod.', 'Cena netto', 'Cena brutto', 'Cena zakupu netto', 'Ilość', 'Wartość netto', 'Wartość brutto', 'Wartość kosztów netto'));
      $totalTax = $totalNet = 0;
      foreach ($orders as $order) {
        foreach ($order['OrderProduct'] as $product) {
            $purchasePrice = ($product['purchase_price'] > 0)? $product['purchase_price']: $product['Product']['purchase_price'];
          $toSave = array();
          $toSave[] = '#'.$order['Order']['id'];
          $toSave[] = $product['name'].' '.$product['add_info'];
          $toSave[] = str_replace('.', ',', $product['price']);
          $toSave[] = str_replace('.', ',', $product['price_tax']);
          $toSave[] = str_replace('.', ',', $purchasePrice);
          $toSave[] = $product['amount'];
          $totalTax += $product['total_price_tax'];
          $netPrice = round($product['total_price_tax']/1.23, 2);
          $totalNet += $netPrice;
          $toSave[] = str_replace('.', ',', $netPrice);
          $toSave[] = str_replace('.', ',', $product['total_price_tax']);
          $toSave[] = str_replace('.', ',', $product['amount']*$purchasePrice);
          fputcsv($file, $toSave);
        }
      }
      fputcsv($file, array(' ', ' ', ' ', ' ', ' ', str_replace('.', ',', $totalNet), str_replace('.', ',', $totalTax)));
      
      fclose($file);
      
      header("Cache-Control: public, must-revalidate");
      header("Pragma: hack"); // WTF? oh well, it works...
      header("Content-Type: text/csv");
      header('Content-Disposition: attachment; filename="raport-zam-'.time().'.csv"');
      header("Content-Transfer-Encoding: binary\n");
      header("Content-Length: ".filesize(TMP . "order-report.csv"));
      echo file_get_contents(TMP . "order-report.csv");
      die;
    }
    
  }
}
