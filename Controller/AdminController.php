<?php

App::uses('AppController', 'Controller');

class AdminController extends AppController {
  
  public function beforeFilter() {
    
    parent::beforeFilter();    
    AuthComponent::$sessionKey = 'Admin';
    $this->Auth->loginAction = array('plugin' => 'KeyAdmin', 'controller' => 'KeyAuth', 'action' => 'login', 'admin' => false);
    $this->Auth->loginRedirect = array('plugin' => 'KeyAdmin', 'controller' => 'KeyDashboard', 'action' => 'index', 'admin' => true);
    $this->Auth->logoutRedirect = array('plugin' => 'KeyAdmin', 'controller' => 'KeyAuth', 'action' => 'login', 'admin' => false);
    $this->Auth->authError = __('Zaloguj się, aby wyświetlić podaną stronę');
    $this->Auth->authenticate = array(
      'Form' => array(
        'passwordHasher' => array(
          'className' => 'Simple',
          'hashType' => 'sha1'
        ),
        'fields' => array(
          'username' => 'email',
          'password' => 'password'
        )
      )
    );
    
    $this->set('authUser', $this->Auth->user());
  }
}
