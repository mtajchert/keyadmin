<?php

App::uses('AdminController', 'KeyAdmin.Controller');

class KeyConfigsController extends AdminController {

  public $uses = array('KeyAdmin.Config');
  public $components = array('RequestHandler');

  public function admin_index() {
    $configs = $this->Config->find('all');
    $keys = array();
    foreach ($configs as $key => $config) {
      $keys[$config['Config']['code']] = $key;
    }
    
    //prepare data to display
    $configsNew = array('Config' => array());
    foreach ($configs as $key => $config) {
      $configsNew['Config'][$key] = $config['Config'];
    }
    $configs = $configsNew;

    if ($this->request->is(array('post', 'put'))) {
      //prepare data to save
      $data = array();
      foreach ($this->request->data['Config'] as $key => $config) {
        $data[$key] = array('Config' => $config);
      }
      $this->request->data = $data;
      
      if ($this->Config->saveAll($this->request->data)) {
        $this->Session->setFlash(__('Ustawienia zostały zapisane.'), 'flash-success');
        return $this->redirect(array('action' => 'index'));
      }
      $this->Session->setFlash(__('Nie udało się zapisać ustawień. Sprawdź poprawność podanych danych.'), 'flash-error');
      $this->log(print_r($this->Config->validationErrors, true));
    }
    
    if (!$this->request->data) {
      $this->data = $this->request->data = $configs;
    }
    
    $this->set('title', __('Ustawienia'));
    $this->set('content_title', __('Ustawienia'));
    $this->set('content_subtitle', __('Edycja ustawień'));
    
    $this->set('configs', $configs);
    $this->set('keys', $keys);
  }
  
}
