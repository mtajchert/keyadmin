<?php
App::uses('AdminController', 'KeyAdmin.Controller');

class KeyCustomizationsController extends AdminController
{

    /**
     */
    public function admin_index($model, $modelId)
    {
        $this->loadModel('KeyAdmin.Customization');
        $customization = $this->Customization->find('first', [
            'conditions' => [
                'Customization.model' => $model,
                'Customization.model_id' => $modelId
            ],
            'contain' => [
                'CustomizationItem' => [
                    'order' => [
                        'CustomizationItem.sort_order' => 'ASC'
                    ]
                ]
            ]
        ]);
        
        if (empty($customization)) {
            $toSave = [
                'Customization' => [
                    'model' => $model,
                    'model_id' => $modelId                    
                ]
            ];
            $customization = $this->Customization->save($toSave);
            $customization['CustomizationItem'] = [];            
        }
        $this->set('content_title', __("Personalizacja: {$model}-{$modelId}"));
        $this->set(compact('customization', 'model', 'modelId'));
    }
    
    
    public function admin_add_item($type, $customizationId)
    {
        $this->layout = 'ajax';
        $this->loadModel('KeyAdmin.CustomizationItem');
    
        $toSave = [
            'CustomizationItem' => [
                'customization_id' => $customizationId,
                'type' => $type,
                'text1' => '',
                'text2' => '',
                'image1' => '',
                'image2' => '',
                'youtube_url' => '',
                'sort_order' => 0
            ]
        ];
        $customization = $this->request->data = $this->CustomizationItem->save($toSave);
    
        $this->set(compact('customization'));
    }
    
    public function admin_save($customizationId, $active)
    {
        
        $toSave = Hash::insert($this->request->data, 'CustomizationItem.{n}.customization_id', $customizationId);
        $toSave = array_values($toSave['CustomizationItem']);
        foreach ($toSave as $key => &$value) {
            $value['sort_order'] = $key;
        }
        $this->loadModel('KeyAdmin.CustomizationItem');
        $this->CustomizationItem->saveAll($toSave); 
        
        
        $this->loadModel('KeyAdmin.Customization');
        $this->Customization->updateAll([
            'Customization.is_active' => (!empty($active))? 1: 0
        ], [
            'Customization.id' => $customizationId
        ]);
        die();
    }
    
    
    public function admin_image_upload($paramName)
    {
        error_reporting(E_ALL | E_STRICT);
        $options = array(
            'upload_dir' => CLIENT_WWW . 'img' . DS . 'description' . DS,
            'image_versions' => array(),
            'param_name' => $paramName
        );
        App::uses('UploadHandler', 'Lib');
        new UploadHandler($options);
    
        exit();
    }
    
    public function admin_save_image($templateId, $column)
    {
        $this->loadModel('KeyAdmin.CustomizationItem');
        $this->CustomizationItem->updateAll(array(
            $column => "'{$this->request->data['file_name']}'"
            ), array(
                'CustomizationItem.id' => $templateId
            ));
    
        \Tinify\setKey("ZJt0gbI3pjULzGINGIkkGwZjGGrOJajL");
        $dirFile = CLIENT_WWW . 'img/description/' . $this->request->data['file_name'];
        $source = \Tinify\fromFile($dirFile);
        $resized = $source->resize(array(
            "method" => "scale",
            "width" => 1200
        ));
        $resized->toFile($dirFile);
    
        echo '/img/description/' . $this->request->data['file_name'];
        die();
    }
    
    public function admin_delete_image($column, $templateId)
    {
        $this->loadModel('KeyAdmin.CustomizationItem');
        $this->CustomizationItem->updateAll(array(
            $column => null
        ), array(
            'CustomizationItem.id' => $templateId
        ));
        die();
    }
    
    
    public function admin_delete_customization($customizationId) {
        $this->loadModel('KeyAdmin.CustomizationItem');
        $this->CustomizationItem->delete($customizationId);
        exit;
    }
}
