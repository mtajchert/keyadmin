<?php

App::uses('AdminController', 'KeyAdmin.Controller');

class KeyCategoriesController extends AdminController {

  var $uses = array('KeyAdmin.Category', 'KeyAdmin.Product');
  public $components = array('Paginator', 'RequestHandler', 'KeyAdmin.Image');
  public $paginate = array(
    'limit' => 20,
  );

  public function beforeFilter() {
    parent::beforeFilter();
    $this->Paginator->settings = $this->paginate;
  }

  /**
   * 
   */
  public function admin_index() {
    $parent_id = (int) isset($this->params['named']['parent_id']) ? $this->params['named']['parent_id'] : 0;
    $tmp_arr = explode('_', $parent_id);
    $parent_id_i = array_pop($tmp_arr);
    $this->Category->recursive = -1;
    $this->Paginator->settings['conditions'] = array('Category.parent_id' => $parent_id_i);
    $this->set('categories', $this->Paginator->paginate());
    $this->set('parent_id', $parent_id);
    
    $this->set('title', __('Kategorie'));
    $this->set('content_title', __('Kategorie'));
    $this->set('content_subtitle', __('Lista kategorii'));
    $this->set('buttons_template', 'Buttons/KeyCategories');
  }

  public function admin_edit($id) {
    if (!$id) {
      throw new NotFoundException(__('Wybrana kategoria nie istnieje'));
    }

    $category = $this->Category->findById($id);
    if (!$category) {
      throw new NotFoundException(__('Nie odnaleziono wybranej kategorii'));
    }

    if ($this->request->is(array('post', 'put'))) {
      $this->Category->id = $id;
      if ($this->Category->save($this->request->data)) {
        $this->Session->setFlash(__('Kategoria została zapisana.'), 'flash-success');
        
        if ($this->request->data['Category']['parent_id'] > 0) {
          $nestes_parent_id = $this->Category->getNestedId($this->request->data['Category']['parent_id']);
          return $this->redirect(array('action' => 'index', 'parent_id' => $nestes_parent_id));
        } else {
          return $this->redirect(array('action' => 'index'));
        }
      }
      
      $this->request->data['Category']['image'] = $category['Category']['image'];
      $this->Session->setFlash(__('Nie udało się zapisać kategorii. Sprawdź poprawność podanych danych.'), 'flash-error');
    }

    if (!$this->request->data) {
      $this->request->data = $category;
    }
    
    $banner_product = null;
    if ($this->request->data['Category']['banner_product_id'] > 0) {
      $banner_product = $this->Product->findById($this->request->data['Category']['banner_product_id']);
    }
    $this->set('banner_product', $banner_product);
    
    $this->set('title', __('Kategorie'));
    $this->set('content_title', __('Kategorie'));
    $this->set('content_subtitle', __('Edycja kategorii'));
    $this->set('buttons_template', 'Buttons/KeyCategories');

    $this->render('KeyCategories/admin_form');
  }

  public function admin_create() {
    if ($this->request->is('post')) {
      $this->Category->create();

      $this->request->data['Category']['parent_id'] = (int) $this->request->data['Category']['parent_id'];
      $this->request->data['Category']['sort_order'] = (int) $this->request->data['Category']['sort_order'];
      
      
      if ($this->Category->save($this->request->data)) {
        $this->Session->setFlash(__('Kategoria została dodana.'), 'flash-success');
        
        if ($this->request->data['Category']['parent_id'] > 0) {
          $nestes_parent_id = $this->Category->getNestedId($this->request->data['Category']['parent_id']);
          return $this->redirect(array('action' => 'index', 'parent_id' => $nestes_parent_id));
        } else {
          return $this->redirect(array('action' => 'index'));
        }
      }
      
      $this->request->data['Category']['image'] = '';
      $this->Session->setFlash(__('Nie udało się dodać kategorii. Sprawdź poprawność podanych danych.'), 'flash-error');
    }
    
    $this->set('title', __('Kategorie'));
    $this->set('content_title', __('Kategorie'));
    $this->set('content_subtitle', __('Dodawanie kategorii'));
    $this->set('buttons_template', 'Buttons/KeyCategories');

    $this->render('KeyCategories/admin_form');
  }

  public function admin_delete($id) {
    $category = $this->Category->findById($id);
    
    if ($this->Category->delete($id)) {
      $this->Session->setFlash(__('Kategoria "%s" została usunięta.', h($category['Category']['name'])), 'flash-success');
    } else {
      $this->Session->setFlash(__('Nie udało się usunąć kategorii "%s".', h($category['Category']['name'])), 'flash-error');
    }

    return $this->redirect(array('action' => 'index'));
  }

  public function admin_getCategoriesTreeData() {
    $parent_ids = $this->request->query['parent_id'];
    $with_root = isset($this->request->query['with_root']) ? (bool) $this->request->query['with_root'] : false;
    $categories = array();

    foreach ($parent_ids as $parent_id) {
      $tmp_arr = explode('_', $parent_id);
      $parent_id_i = array_pop($tmp_arr);
      $categories[$parent_id] = $this->Category->find('all', array(
        'joins' => array(
          array(
            'table' => 'categories',
            'alias' => 'Childs',
            'type' => 'LEFT',
            'conditions' => array(
              'Childs.parent_id = Category.id'
            ),
            'fields' => array('Childs.id')
          )
        ),
        'conditions' => array(
          'Category.parent_id' => $parent_id_i
        ),
        'fields' => array(
          'Category.id', 'Category.name', 'count(Childs.id) as child_count'
        ),
        'group' => 'Category.id'
      ));
    }
    
    if (in_array(0, $parent_ids) && $with_root) {
      $categories[0] = array_merge(array(array(
        'Category' => array(
          'id' => 0,
          'name' => __('Wszystkie kategorie')
        ),
        0 => array(
          'child_count' => 0
        )
      )), $categories[0]);
    }
    
    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set('categories', $categories);
    $this->set('_serialize', array('categories'));
  }

  public function admin_saveCategorySortOrder() {
    $categoryId = (int) $this->request->query['category_id'];
    $value = (int) $this->request->query['value'];
    
    if ($this->Category->updateAll(array('sort_order' => $value), array('Category.id' => $categoryId))) {
      $result = array('success' => 1);
    } else {
      $result = array('success' => 0);
    }

    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set('result', $result);
    $this->set('_serialize', array('result'));
  }

  public function admin_saveCategoryStatus() {
    $categoryId = (int) $this->request->query['category_id'];
    $value = (int) $this->request->query['value'];
    
    if ($this->Category->updateAll(array('status' => $value), array('Category.id' => $categoryId))) {
      $result = array('success' => 1);
    } else {
      $result = array('success' => 0);
    }

    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set('result', $result);
    $this->set('_serialize', array('result'));
  }

  public function admin_getCategoryBreadcrumbPath() {
    $category_id = (int) $this->request->query['category_id'];

    $path = array();

    do {
      $category = $this->Category->findById($category_id);
      $path[] = array(
        'id' => $category['Category']['id'],
        'name' => $category['Category']['name']
      );

      $category_id = $category['Category']['parent_id'];
    } while ($category_id > 0);

    $path = array_reverse($path);

    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set('path', $path);
    $this->set('_serialize', array('path'));
  }
  
  
  /**
   * 
   */
  public function admin_upload_image() {
    if ($this->request->is('post')){
      
      $this->Category->validator()->remove('name');
      if ($this->Category->save($this->request->data)){
        $this->Session->setFlash(__('Zdjęcie zostało dodane.'), 'flash-success');
        return $this->redirect($this->referer());
        
      }else{
        
        throw new Exception('Unable to uplaod file');
      }
    }
    
  }

}
