<?php

App::uses('AdminController', 'KeyAdmin.Controller');
App::uses('OrderProduct', 'KeyAdmin.Model');

class KeyInvoicesController extends AdminController {

  public $uses = array('KeyAdmin.Invoice', 'KeyAdmin.Order', 'KeyAdmin.Customer', 'KeyAdmin.Product', 'KeyAdmin.OrderProduct', 'KeyAdmin.OrderUserAddress', 'KeyAdmin.UserAddress');
  public $components = array('Paginator', 'RequestHandler', 'KeyAdmin.Invoices');
  public $helpers = array('KeyAdmin.User');
  public $paginate = array(
    'limit' => 20,
    'order' => array('Invoice.created' => 'DESC')
  );

  public function beforeFilter() {
    parent::beforeFilter();
    $this->Paginator->settings = $this->paginate;
  }
  
  public function admin_index() {
    return $this->redirect(array('action' => 'index_invoices'));
  }

  public function admin_index_invoices() {
    if (isset($this->request->params['named']['download'])) {
      $this->set('redirect_url', Router::url(array('plugin' => 'KeyAdmin', 'controller' => 'KeyInvoices', 'admin' => true, 'action' => 'admin_download_pdf', $this->request->params['named']['download'])));
    }
    
    $this->Invoice->contain(array('Order'));
    $this->set('invoices', $this->Paginator->paginate(null, array(
      'Invoice.type' => 'INVOICE'
    )));
    
    $this->set('title', __('Faktury'));
    $this->set('content_title', __('Faktury'));
    $this->set('content_subtitle', __('Lista faktur'));
    $this->set('buttons_template', 'Buttons/KeyInvoices');
  }
  
  public function admin_index_quotes() {
    if (isset($this->request->params['named']['download'])) {
      $this->set('redirect_url', Router::url(array('plugin' => 'KeyAdmin', 'controller' => 'KeyInvoices', 'admin' => true, 'action' => 'admin_download_pdf', $this->request->params['named']['download'])));
    }
    
    $this->Invoice->contain(array('Order'));
    $this->set('invoices', $this->Paginator->paginate(null, array(
      'Invoice.type' => 'QUOTE'
    )));
    
    $this->set('title', __('Faktury Proforma'));
    $this->set('content_title', __('Faktury Proforma'));
    $this->set('content_subtitle', __('Lista faktur Proforma'));
    $this->set('buttons_template', 'Buttons/KeyInvoices');
  }

  public function admin_create_invoice() {
    if ($this->request->is('post')) {
      $this->Order->create();
      $this->request->data['Order']['number'] = time();
      
      $data = $this->request->data;
      $data['Order']['order_status_id'] = 1;
      $this->Order->set($data);
      $data['Order']['billing_order_address_id'] = (int) str_replace('0-', '', $data['Order']['billing_order_address_id']);
      
      if ($this->Order->validates()) {
        $data['BillingOrderUserAddress'] = $this->UserAddress->findById($data['Order']['billing_order_address_id'])['UserAddress'];
        $data['Customer'] = $this->Customer->findById($data['Order']['user_id'])['Customer'];
        $data['Shipping'] = $this->Order->Shipping->findById($data['Order']['shipping_id'])['Shipping'];
        $data['Payment'] = $this->Order->Payment->findById($data['Order']['payment_id'])['Payment'];
        
        foreach ($data['OrderProduct'] as $index => $orderProduct) {
          $data['OrderProduct'][$index]['Unit'] = $this->Order->OrderProduct->Unit->findById($orderProduct['unit_id'])['Unit'];
        }
        
        
        $this->Session->setFlash(__('Faktura VAT została dodana.'), 'flash-success');
        $this->Invoices->setController($this);        
        $invoice = $this->Invoices->generateInvoice($data)['model'];
        return $this->redirect(array('action' => 'index_invoices', 'download' => $invoice['Invoice']['id']));
      }
      $this->Session->setFlash(__('Nie udało się dodać faktury VAT. Sprawdź poprawność podanych danych.'), 'flash-error');
      $this->request->data = $this->fillRequestData($this->request->data);
    } else {
      $this->request->data = ['Order' => ['shipping_id' => 0, 'payment_id' => 0, 'shipping_price_tax' => 0, 'products_price_tax' => 0, 'order_price_tax' => 0]];
    }

    $this->set('title', __('Faktury VAT'));
    $this->set('content_title', __('Faktury VAT'));
    $this->set('content_subtitle', __('Dodawanie faktury VAT'));
    $this->set('buttons_template', 'Buttons/KeyInvoices');

    $this->set('order', $this->request->data);
    $this->set('create', true);

    $taxRatesFull = $this->Product->TaxRate->find('all', array(
      'fields' => array('TaxRate.id', 'TaxRate.description', 'TaxRate.rate')
    ));
    list($userAddresses, $defaultBillingUserAddressId, $defaultShippingUserAddressId) = $this->Order->getAddressesListAndData(
        isset($this->request->data['Order']['user_id']) ? $this->request->data['Order']['user_id'] : 0,
        isset($this->request->data['Order']['id']) ? $this->request->data['Order']['id'] : 0
    );

    $shippingsFull = array();
    $payments = array();
    if ($this->request->is('post')) {
      $params = array(
        'weight' => 0,
        'products_price_tax' => $this->request->data['Order']['products_price_tax'],
        'products_amount' => 0,
        'country_id' => isset($this->request->data['ShippingOrderUserAddress']['country_id']) ? $this->request->data['ShippingOrderUserAddress']['country_id'] : 0
      );
      
      if (isset($this->request->data['OrderProduct'])) {
        foreach ($this->request->data['OrderProduct'] as $orderProduct) {
          $params['weight'] += floatVal($orderProduct['weight']);
          $params['products_amount'] += floatVal($orderProduct['amount']);
        }
      }
      
      $shippingsFull = $this->Order->getShippingForOrder(0, $params);
      $payments = $this->Order->getPaymentsForOrder(0, array(
        'shipping_id' => $this->request->data['Order']['shipping_id']
      ));
    }

    $orderStatusesFull = $this->Order->OrderStatus->find('all', array(
      'order' => 'OrderStatus.sort_order'
    ));
    
    $payments = $this->Order->Payment->find('list');
    $this->set(compact('payments', 'taxRatesFull', 'userAddresses', 'defaultBillingUserAddressId', 'defaultShippingUserAddressId', 'shippingsFull', 'payments', 'orderStatusesFull'));
    
    $this->render('KeyInvoices/admin_form');
  }
  
  public function admin_create_quote() {
    if ($this->request->is('post')) {
      $this->Order->create();
      $this->request->data['Order']['number'] = time();
      
      $this->request->data['Order']['shipping_id'] = 6;
      $this->request->data['Order']['payment_id'] = 14;
      $this->request->data['Order']['shipping_order_address_id'] = $this->request->data['Order']['billing_order_address_id'];
      $this->request->data['Order']['products_price_tax'] = number_format($this->request->data['Order']['products_price_tax'], 2);
      $this->request->data['Order']['order_price_tax'] = $this->request->data['Order']['products_price_tax'];
      $data = $this->request->data;
      $data['Order']['order_status_id'] = 1;
      $this->Order->set($data);
      $data['Order']['billing_order_address_id'] = (int) str_replace('0-', '', $data['Order']['billing_order_address_id']);
      
      if ($this->Order->validates()) {
        $data['BillingOrderUserAddress'] = $this->UserAddress->findById($data['Order']['billing_order_address_id'])['UserAddress'];
        $data['Customer'] = $this->Customer->findById($data['Order']['user_id'])['Customer'];
        $data['Shipping'] = $this->Order->Shipping->findById($data['Order']['shipping_id'])['Shipping'];
        
        foreach ($data['OrderProduct'] as $index => $orderProduct) {
          $data['OrderProduct'][$index]['Unit'] = $this->Order->OrderProduct->Unit->findById($orderProduct['unit_id'])['Unit'];
        }
        
        $this->Session->setFlash(__('Faktura Proforma została dodana.'), 'flash-success');
        $this->Invoices->setController($this);
        $invoice = $this->Invoices->generateQuote($data)['model'];
        
        return $this->redirect(array('action' => 'index_quotes', 'download' => $invoice['Invoice']['id']));
      }
      pr($this->Order->validationErrors);die();
      $this->Session->setFlash(__('Nie udało się dodać faktury Proforma. Sprawdź poprawność podanych danych.'), 'flash-error');
      $this->request->data = $this->fillRequestData($this->request->data);
    } else {
      $this->request->data = ['Order' => ['shipping_id' => 0, 'payment_id' => 0, 'shipping_price_tax' => 0, 'products_price_tax' => 0, 'order_price_tax' => 0]];
    }

    $this->set('title', __('Faktury Proforma'));
    $this->set('content_title', __('Faktury Proforma'));
    $this->set('content_subtitle', __('Dodawanie faktury Proforma'));
    $this->set('buttons_template', 'Buttons/KeyInvoices');

    $this->set('order', $this->request->data);
    $this->set('create', true);

    $taxRatesFull = $this->Product->TaxRate->find('all', array(
      'fields' => array('TaxRate.id', 'TaxRate.description', 'TaxRate.rate')
    ));
    list($userAddresses, $defaultBillingUserAddressId, $defaultShippingUserAddressId) = $this->Order->getAddressesListAndData(
        isset($this->request->data['Order']['user_id']) ? $this->request->data['Order']['user_id'] : 0,
        isset($this->request->data['Order']['id']) ? $this->request->data['Order']['id'] : 0
    );

    $shippingsFull = array();
    $payments = array();
    if ($this->request->is('post')) {
      $params = array(
        'weight' => 0,
        'products_price_tax' => $this->request->data['Order']['products_price_tax'],
        'products_amount' => 0,
        'country_id' => isset($this->request->data['ShippingOrderUserAddress']['country_id']) ? $this->request->data['ShippingOrderUserAddress']['country_id'] : 0
      );
      
      if (isset($this->request->data['OrderProduct'])) {
        foreach ($this->request->data['OrderProduct'] as $orderProduct) {
          $params['weight'] += floatVal($orderProduct['weight']);
          $params['products_amount'] += floatVal($orderProduct['amount']);
        }
      }
      
      $shippingsFull = $this->Order->getShippingForOrder(0, $params);
      $payments = $this->Order->getPaymentsForOrder(0, array(
        'shipping_id' => $this->request->data['Order']['shipping_id']
      ));
    }

    $orderStatusesFull = $this->Order->OrderStatus->find('all', array(
      'order' => 'OrderStatus.sort_order'
    ));
    
    $this->set(compact('taxRatesFull', 'userAddresses', 'defaultBillingUserAddressId', 'defaultShippingUserAddressId', 'shippingsFull', 'payments', 'orderStatusesFull'));
    
    $this->render('KeyInvoices/admin_form');
  }
  
  protected function fillRequestData($data) {
    if (!isset($data['Customer']) && isset($data['Order']['user_id']) && $data['Order']['user_id'] > 0) {
      $this->Customer->contain(array('UserData', 'UserAddress'));
      $customer = $this->Order->Customer->findById($data['Order']['user_id']);
      
      $data['Customer'] = $customer['Customer'];
      $data['Customer']['UserData'] = $customer['UserData'];
      $data['Customer']['UserAddress'] = $customer['UserAddress'];
    }
    
    $address_update = explode(',', $data['Order']['address_update']);
    
    if (!isset($data['BillingOrderUserAddress']) && isset($data['Order']['billing_order_address_id']) && !empty($data['Order']['billing_order_address_id'])) {
      $update = false;
      if (substr($data['Order']['billing_order_address_id'], 0, 2) == '0-') {
        $order_address_id = substr($data['Order']['billing_order_address_id'], 2);
        if (in_array($order_address_id, $address_update)) {
          $update = true;
        }
      }
      $billingAddress = $this->Order->BillingOrderUserAddress->getOrderUserAddressData($data['Order']['billing_order_address_id'], $update);
      
      $data['BillingOrderUserAddress'] = $billingAddress;
    }
    
    if (!isset($data['ShippingOrderUserAddress']) && isset($data['Order']['shipping_order_address_id']) && !empty($data['Order']['shipping_order_address_id'])) {
      $update = false;
      if (substr($data['Order']['shipping_order_address_id'], 0, 2) == '0-') {
        $order_address_id = substr($data['Order']['shipping_order_address_id'], 2);
        if (in_array($order_address_id, $address_update)) {
          $update = true;
        }
      }
      $shippingAddress = $this->Order->ShippingOrderUserAddress->getOrderUserAddressData($data['Order']['shipping_order_address_id'], $update);
      
      $data['ShippingOrderUserAddress'] = $shippingAddress;
    }
    
    if (!isset($data['Shipping']) && isset($data['Order']['shipping_id']) && $data['Order']['shipping_id'] > 0) {
      $shipping = $this->Order->Shipping->findById($data['Order']['shipping_id']);
      $data['Shipping'] = $shipping['Shipping'];
    }
    
    if (!isset($data['Payment']) && isset($data['Order']['payment_id']) && $data['Order']['payment_id'] > 0) {
      $payment = $this->Order->Payment->findById($data['Order']['payment_id']);
      $data['Payment'] = $payment['Payment'];
    }
    
    if (!isset($data['OrderStatus']) && isset($data['Order']['order_status_id']) && $data['Order']['order_status_id'] > 0) {
      $order_status = $this->Order->OrderStatus->findById($data['Order']['order_status_id']);
      $data['OrderStatus'] = $order_status['OrderStatus'];
    }
    
    if (isset($data['OrderProduct'])) {
      foreach ($data['OrderProduct'] as $key => $orderProduct) {
        if (!isset($orderProduct['Product']) && $orderProduct['product_id'] > 0) {
          $product = $this->Order->OrderProduct->Product->findById($orderProduct['product_id']);
          $data['OrderProduct'][$key]['Product'] = $product['Product'];
        }
        if (!isset($orderProduct['Unit']) && $orderProduct['unit_id'] > 0) {
          $unit = $this->Order->OrderProduct->Unit->findById($orderProduct['unit_id']);
          $data['OrderProduct'][$key]['Unit'] = $unit['Unit'];
        }
      }
    }
    
    if (!isset($data['OrderStatusHistory']) && isset($data['Order']['id']) && $data['Order']['id'] > 0) {
      $this->Order->OrderStatusHistory->contain(array('User', 'OrderStatus'));
      $order_status_history = $this->Order->OrderStatusHistory->find('all', array(
        'conditions' => array(
          'order_id' => $data['Order']['id']
        )
      ));
      $data['OrderStatusHistory'] = array();
      foreach ($order_status_history as $item) {
        $itemN = $item['OrderStatusHistory'];
        $itemN['User'] = $item['User'];
        $itemN['OrderStatus'] = $item['OrderStatus'];
        $data['OrderStatusHistory'][] = $itemN;
      }
    }
    
    return $data;
  }

  public function admin_delete_invoice($id) {
    $order = $this->Invoice->findById($id);
    
    if ($this->Invoice->delete($id, false)) {
      $this->Session->setFlash(__('Faktura VAT "%s" została usunięta.', h($order['Invoice']['number_all'])), 'flash-success');
    } else {
      $this->Session->setFlash(__('Nie udało się usunąć faktury VAT "%s".', h($order['Invoice']['number_all'])), 'flash-error');
    }

    return $this->redirect(array('action' => 'index_invoices'));
  }
  
  public function admin_delete_quote($id) {
    $order = $this->Invoice->findById($id);
    
    if ($this->Invoice->delete($id, false)) {
      $this->Session->setFlash(__('Faktura Proforma "%s" została usunięta.', h($order['Invoice']['number_all'])), 'flash-success');
    } else {
      $this->Session->setFlash(__('Nie udało się usunąć faktury Proforma "%s".', h($order['Invoice']['number_all'])), 'flash-error');
    }

    return $this->redirect(array('action' => 'index_quotes'));
  }
  
  public function admin_get_customers_table() {
    $query = $this->request->query['query'];
    $customers = $conditions = array();

    if (!empty($query)) {
      $conditions['OR'] = array(
        'Customer.first_name LIKE' => "%{$query}%",
        'Customer.last_name LIKE' => "%{$query}%",
        'Customer.company LIKE' => "%{$query}%",
        'Customer.contact_phone LIKE' => "%{$query}%",
        'Customer.customer LIKE' => "%{$query}%"
      );
    }

    if (!empty($conditions)) {
      $this->Customer->recursive = 1;
      $customers = $this->Customer->find('all', array(
        'conditions' => $conditions
      ));
    }
    
    $view = new View($this, false);
    $view->viewPath = 'KeyInvoices';
    $view->layout = false;
    $view->set('customers', $customers);
    $view->set('empty_query', empty($query));
    $html = $view->render('customers_table');
     
    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set('result', array('html' => $html));
    $this->set('_serialize', array('result'));
  }
  
  public function admin_get_customer_info() {
    $customer_id = $this->request->query['customer_id'];
    $this->Customer->recursive = 1;
    $customer = $this->Customer->findById($customer_id);
    
    $view = new View($this, false);
    $view->viewPath = 'KeyInvoices';
    $view->layout = false;
    $view->set('user', $customer['Customer']);
    $view->set('userData', $customer['UserData']);
    $view->set('userAddress', isset($customer['UserAddress'][0]) ? $customer['UserAddress'][0] : null);
    $html = $view->render('customer_info');
     
    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set(array('html' => $html, 'user' => $customer));
    $this->set('_serialize', array('html', 'user'));
  }
  
  public function admin_get_product_row() {
    $product_id = (int) $this->request->query['product_id'];
    $products_products_option_id = (int) $this->request->query['products_products_option_id'];
    $key = (int) $this->request->query['key'];
    
    $product = $this->OrderProduct->Product->find('first', array(
      'conditions' => array(
        'Product.id' => $product_id
      )
    ));
    
    if ($products_products_option_id > 0) {
      $this->OrderProduct->Product->ProductsProductsOption->contain(array('ProductOption', 'ProductOptionValue'));
      $product_option = $this->OrderProduct->Product->ProductsProductsOption->find('first', array(
        'conditions' => array(
          'ProductsProductsOption.id' => $products_products_option_id
        )
      ));
    }
    
    $unit = $this->OrderProduct->Product->Unit->find('first', array(
      'conditions' => array(
        'Unit.id' => $product['Product']['unit_id']
      )
    ));
    
    $catalog_no = $product['Product']['catalog_no'];
    if (isset($product_option) && !empty($product_option['ProductsProductsOption']['catalog_no'])) {
      $catalog_no = $product_option['ProductsProductsOption']['catalog_no'];
    }
    
    $orderProduct = array(
      'product_id' => $product_id,
      'name' => $product['Product']['name'],
      'add_info' => empty($catalog_no) ? '' : __('Nr kat.:').' '.$catalog_no,
      'catalog_no' => $product['Product']['catalog_no'],
      'ext_code' => $product['Product']['ext_code'],
      'ean' => $product['Product']['ean'],
      'pkwiu' => $product['Product']['pkwiu'],
      'weight' => $product['Product']['weight'],
      
      'price' => $product['Product']['price'],
      'tax_rate_id' => $product['Product']['tax_rate_id'],
      'tax' => $product['Product']['tax'],
      'price_tax' => $product['Product']['price_tax'],
      
      'manufacturer_id' => $product['Product']['manufacturer_id'],
      'shipping_time_id' => $product['Product']['shipping_time_id'],
      'product_warranty_id' => $product['Product']['product_warranty_id'],
      'product_condition_id' => $product['Product']['product_condition_id'],
      'unit_id' => $product['Product']['unit_id'],
      'products_products_option_id' => $products_products_option_id,
      
      'Product' => $product['Product'],
      'Unit' => $unit['Unit']
    );
    
    if (isset($product_option)) {
      $orderProduct['add_info'] .= (empty($orderProduct['add_info']) ? '' : __(', ')).$product_option['ProductOption']['name'].': '.$product_option['ProductOptionValue']['name'];
      $orderProduct['price'] += $product_option['ProductsProductsOption']['change_price'];
      $orderProduct['tax'] += $product_option['ProductsProductsOption']['change_price_tax'] - $product_option['ProductsProductsOption']['change_price'];
      $orderProduct['price_tax'] += $product_option['ProductsProductsOption']['change_price_tax'];
    }
    
    if ((float)$product['Product']['order_min_quantity'] > 0) {
      $orderProduct['amount'] = $product['Product']['order_min_quantity'];
    } else {
      $orderProduct['amount'] = 1;
    }
    $orderProduct['total_price_tax'] = round($orderProduct['amount'] * $orderProduct['price_tax'], 2);
    
    
    $taxRatesFull = $this->OrderProduct->Product->TaxRate->find('all', array(
      'fields' => array('TaxRate.id', 'TaxRate.description', 'TaxRate.rate')
    ));
    
    $this->request->data = array(
      'OrderProduct' => array(
        $key => $orderProduct
      )
    );
    
    $view = new View($this, false);
    $view->viewPath = 'KeyInvoices';
    $view->layout = false;
    $view->set(compact('key', 'orderProduct', 'taxRatesFull'));
    $html = $view->render('product_row');
     
    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set(array('html' => $html));
    $this->set('_serialize', array('html'));
  }
  
  public function admin_get_order_product_preview() {
    $order_product_id = (int) $this->request->query['order_product_id'];
    
    $this->OrderProduct->contain(array('Product', 'Manufacturer', 'ShippingTime', 'ProductWarranty', 'ProductCondition', 'Unit'));
    $orderProduct = $this->OrderProduct->findById($order_product_id);
    
    $view = new View($this, false);
    $view->viewPath = 'KeyInvoices';
    $view->layout = false;
    $view->set(compact('orderProduct'));
    $html = $view->render('product_preview');
     
    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set(array(
      'html' => $html,
      'lang' => array(
        'title' => __('Szczegóły pozycji zamówienia'),
        'btn_ok_title' => __('OK'),
        'btn_product_title' => __('Pokaż aktualny produkt')
      ),
      'order_product' => $orderProduct
    ));
    $this->set('_serialize', array('html', 'lang', 'order_product'));
  }
  
  public function admin_get_order_user_address_data() {
    $order_user_address_id = $this->request->query['order_user_address_id'];
    $update = $this->request->query['update'];
    
    $orderUserAddress = $this->Order->BillingOrderUserAddress->getOrderUserAddressData($order_user_address_id, in_array($order_user_address_id, $update));
    $html = '';
    if (!empty($order_user_address_id)) {
      $html = $this->Customer->UserAddress->getFormattedAddress($orderUserAddress, '<br>');
    }
     
    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set(array(
      'html' => $html,
      'orderUserAddress' => $orderUserAddress
    ));
    $this->set('_serialize', array('html', 'orderUserAddress'));
  }
  
  public function admin_get_order_user_addresses_list() {
    $user_id = (int) $this->request->query['user_id'];
    $order_id = (int) $this->request->query['order_id'];
    $update = $this->request->query['update'];
    
    list($userAddresses, $defaultBillingUserAddressId, $defaultShippingUserAddressId) = $this->Order->getAddressesListAndData($user_id, $order_id, $update);
     
    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set(compact('userAddresses', 'defaultBillingUserAddressId', 'defaultShippingUserAddressId'));
    $this->set('_serialize', array('userAddresses', 'defaultBillingUserAddressId', 'defaultShippingUserAddressId'));
  }
  
  public function admin_get_shippings_list() {
    $params = array(
      'weight' => (float) $this->request->query['weight'],
      'products_price_tax' => (float) $this->request->query['products_price_tax'],
      'products_amount' => (float) $this->request->query['products_amount'],
      'country_id' => (int) $this->request->query['country_id']
    );
    
    $shippingsFull = $this->Order->getShippingForOrder(0, $params);
     
    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set(compact('shippingsFull'));
    $this->set('_serialize', array('shippingsFull'));
  }
  
  public function admin_get_payments_list() {
    $params = array(
      'shipping_id' => (int) $this->request->query['shipping_id']
    );
    
    $payments = $this->Order->getPaymentsForOrder(0, $params);
     
    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set(compact('payments'));
    $this->set('_serialize', array('payments'));
  }
  
  public function admin_get_products_products_option_form() {
    $product_id = (int) $this->request->query['product_id'];
    
    $this->OrderProduct->Product->contain(array('ProductsProductsOption', 'ProductsProductsOption.ProductOption', 'ProductsProductsOption.ProductOptionValue'));
    $product = $this->OrderProduct->Product->findById($product_id);
    
    $view = new View($this, false);
    $view->viewPath = 'KeyInvoices';
    $view->layout = false;
    $view->set(compact('product'));
    $html = $view->render('products_products_option_choose');
     
    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set(array(
      'result' => array(
        'optionChoose' => (isset($product['ProductsProductsOption']) && !empty($product['ProductsProductsOption'])),
        'html' => $html,
        'product' => $product
      )
    ));
    $this->set('_serialize', array('result'));
  }
  
  public function admin_download_pdf($id) {
    $invoice = $this->Invoice->findById($id);
    
    $fpath = $this->Invoice->getPdfPath($invoice);
    
    header("Cache-Control: public, must-revalidate");
    header("Pragma: hack"); // WTF? oh well, it works...
    header("Content-Type: text/csv");
    header('Content-Disposition: attachment; filename="F-'.str_replace('/', '-', $invoice['Invoice']['number_all']).'.pdf"');
    header("Content-Transfer-Encoding: binary\n");
    header("Content-Length: ".filesize($fpath));
    echo file_get_contents($fpath);
    die;
  }
  
  
  
  /**
   * 
   */
  public function admin_generate_pdf(){
    $mpdf = new mPDF('BLANK', 'A4-L', '9', 'Arial', 10, 10, 5, 5, 5, 5);
    
    $mpdf->SetHTMLHeader('<p style="width:100%; text-align:right; margin:0; padding:0; font-family:Arial; font-size:8pt; font-weight:bold; font-style:italic;">Zestawienie - Strona {PAGENO}/{nbpg}</span><hr style="margin:0;"/>');
    $mpdf->SetHTMLFooter('<hr style="margin:0;"/><p style="width:100%; text-align:right; margin:0; padding:0; font-family:Arial; font-size:8pt; font-weight:bold; font-style:italic;">Zestawienie - Strona {PAGENO}/{nbpg}</span>');
    
    $this->Invoice->contain(array('Order.BillingOrderUserAddress', 'Order.OrderProduct', 'Order.Payment'));
    $invoices = $this->Invoice->find('all', array(
      'conditions' => array(
        'AND' => array(
          array('Invoice.set_date >=' => $this->request->data['Invoice']['report_date_from']),
          array('Invoice.set_date <=' => $this->request->data['Invoice']['report_date_to']),
        )
      ),      
      'order' => array('Invoice.set_date' => 'ASC'),      
    ));
    
    
    $this->plugin = CLIENT;
    $view = new View($this, false);
    $view->viewPath = 'Invoice';
    $view->layout = false;
    $view->set(compact('invoices'));
    $html = $view->render('invoice_report');
    
    @$mpdf->WriteHTML($html);
    @$mpdf->Output('zestawienie.pdf', 'D');
  }

}
