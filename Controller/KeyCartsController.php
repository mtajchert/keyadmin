<?php
App::uses('AdminController', 'KeyAdmin.Controller');

class KeyCartsController extends AdminController {
  public $uses = array(
    'KeyAdmin.Cart'
  );
  public $components = array(
    'Paginator',
    'RequestHandler'
  );
  public $paginate = array(
    'limit' => 20,
    'order' => array(
      'Cart.created' => 'DESC'
    ),
    'conditions' => array(
      'Cart.order_id' => NULL
    )
  );



  public function beforeFilter() {
    parent::beforeFilter();
    $this->Paginator->settings = $this->paginate;
  }



  public function admin_index() {
    $this->Cart->contain(array(
      'Customer',
      'Customer.UserData',
      'Customer.UserAddress',
      'Payment',
      'Shipping'
    ));
    $this->set('carts', $this->Paginator->paginate());
    
    $this->set('title', __('Porzucone koszyki'));
    $this->set('content_title', __('Porzucone koszyki'));
    $this->set('content_subtitle', __('Lista porzuconych koszyków'));
    $this->set('buttons_template', 'Buttons/KeyCarts');
  }



  public function admin_preview($id) {
    if (! $id) {
      throw new NotFoundException(__('Wybrany koszyk nie istnieje'));
    }
    
    $this->Cart->contain(array(
      'Customer',
      'Customer.UserData',
      'Customer.UserAddress',
      'CartProduct',
      'CartProduct.TaxRate',
      'CartProduct.Unit',
      'CartProduct.Product',
      'Payment',
      'Shipping',
      'BillingOrderUserAddress',
      'BillingOrderUserAddress.Country',
      'BillingOrderUserAddress.Zone',
      'ShippingOrderUserAddress',
      'ShippingOrderUserAddress.Country',
      'ShippingOrderUserAddress.Zone'
    ));
    $cart = $this->Cart->findById($id);
    if (! $cart) {
      throw new NotFoundException(__('Nie odnaleziono wybranego koszyka'));
    }
    
    $this->request->data = $cart;
    
    $this->set('title', __('Porzucone koszyki'));
    $this->set('content_title', __('Porzucone koszyki'));
    $this->set('content_subtitle', __('Podgląd koszyka'));
    $this->set('buttons_template', 'Buttons/KeyCarts');
    
    $this->set('cart', $cart);
    $this->set('create', false);
    
    $this->set(compact('taxRatesFull', 'userAddresses', 'defaultBillingUserAddressId', 'defaultShippingUserAddressId', 'shippingsFull', 'payments'));
    
    $this->render('KeyCarts/admin_preview');
  }



  public function admin_delete($id) {
    $cart = $this->Cart->findById($id);
    
    if ($this->Cart->delete($id, false)) {
      $this->Cart->CartProduct->deleteAll(array(
        'CartProduct.cart_id' => $id
      ));
      $this->Session->setFlash(__('Koszyk "%s" zostało usunięte.', h($cart['Cart']['id'])), 'flash-success');
    } else {
      $this->Session->setFlash(__('Nie udało się usunąć koszyka "%s".', h($cart['Cart']['id'])), 'flash-error');
    }
    
    return $this->redirect(array(
      'action' => 'index'
    ));
  }



  public function admin_get_cart_product_preview() {
    $cart_product_id = ( int ) $this->request->query['cart_product_id'];
    
    $this->Cart->CartProduct->contain(array(
      'Product.ProductsImage' => array('limit' => 1, 'order' => array('ProductsImage.sort_order' => 'ASC')),
      'Manufacturer',
      'ShippingTime',
      'ProductWarranty',
      'ProductCondition',
      'Unit'
    ));
    $cartProduct = $this->Cart->CartProduct->findById($cart_product_id);
    
    $view = new View($this, false);
    $view->viewPath = 'KeyCarts';
    $view->layout = false;
    $view->set(compact('cartProduct'));
    $html = $view->render('product_preview');
    
    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set(array(
      'html' => $html,
      'lang' => array(
        'title' => __('Szczegóły pozycji koszyka'),
        'btn_ok_title' => __('OK'),
        'btn_product_title' => __('Pokaż aktualny produkt')
      ),
      'cart_product' => $cartProduct
    ));
    $this->set('_serialize', array(
      'html',
      'lang',
      'cart_product'
    ));
  }
}
