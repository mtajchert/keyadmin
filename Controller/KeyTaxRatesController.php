<?php

App::uses('AdminController', 'KeyAdmin.Controller');

class KeyTaxRatesController extends AdminController {

  public $uses = array('KeyAdmin.TaxRate');
  public $components = array('Paginator', 'RequestHandler');
  public $paginate = array(
    'limit' => 20,
  );

  public function beforeFilter() {
    parent::beforeFilter();
    $this->Paginator->settings = $this->paginate;
  }

  public function admin_index() {
    $this->set('tax_rates', $this->Paginator->paginate());
    
    $this->set('title', __('Stawki VAT'));
    $this->set('content_title', __('Stawki VAT'));
    $this->set('content_subtitle', __('Lista stawek VAT'));
    $this->set('buttons_template', 'Buttons/KeyTaxRates');
  }

  public function admin_edit($id) {
    if (!$id) {
      throw new NotFoundException(__('Wybrana stawka VAT nie istnieje'));
    }

    $tax_rate = $this->TaxRate->findById($id);
    if (!$tax_rate) {
      throw new NotFoundException(__('Nie odnaleziono wybranej stawki VAT'));
    }

    if ($this->request->is(array('post', 'put'))) {
      $this->TaxRate->id = $id;
      if ($this->TaxRate->save($this->request->data)) {
        if ($this->request->data['TaxRate']['default']) {
          $default = $this->TaxRate->find('all', array(
            'conditions' => array(
              'TaxRate.default' => 1,
              'TaxRate.id !=' => $id
            )
          ));
          foreach ($default as $default_i) {
            $default_i['TaxRate']['default'] = 0;
            $this->TaxRate->save($default_i);
          }
        }
        
        $this->Session->setFlash(__('Stawka VAT została zapisana.'), 'flash-success');
        return $this->redirect(array('action' => 'index'));
      }
      $this->Session->setFlash(__('Nie udało się zapisać stawki VAT. Sprawdź poprawność podanych danych.'), 'flash-error');
    }

    if (!$this->request->data) {
      $this->request->data = $tax_rate;
    }

    $this->set('title', __('Stawki VAT'));
    $this->set('content_title', __('Stawki VAT'));
    $this->set('content_subtitle', __('Edycja stawki VAT'));
    $this->set('buttons_template', 'Buttons/KeyTaxRates');
    
    $this->set('tax_rate', $tax_rate);
    
    $this->render('KeyTaxRates/admin_form');
  }

  public function admin_create() {
    if ($this->request->is('post')) {
      $this->TaxRate->create();
      
      if ($this->TaxRate->save($this->request->data)) {
        if ($this->request->data['TaxRate']['default']) {
          $default = $this->TaxRate->find('all', array(
            'conditions' => array(
              'TaxRate.default' => 1,
              'TaxRate.id !=' => $this->TaxRate->getLastInsertID()
            )
          ));
          foreach ($default as $default_i) {
            $default_i['TaxRate']['default'] = 0;
            $this->TaxRate->save($default_i);
          }
        }
        
        $this->Session->setFlash(__('Stawka VAT została dodana.'), 'flash-success');
        return $this->redirect(array('action' => 'index'));
      }
      $this->Session->setFlash(__('Nie udało się dodać stawki VAT. Sprawdź poprawność podanych danych.'), 'flash-error');
    } else {
      $this->request->data = ['TaxRate' => ['default' => 0]];
    }

    $this->set('title', __('Stawki VAT'));
    $this->set('content_title', __('Stawki VAT'));
    $this->set('content_subtitle', __('Dodawanie stawkę VAT'));
    $this->set('buttons_template', 'Buttons/KeyTaxRates');
    
    $this->set('tax_rate', $this->request->data);
    
    $this->render('KeyTaxRates/admin_form');
  }

  public function admin_delete($id) {
    $tax_rate = $this->TaxRate->findById($id);
    
    if ($tax_rate['TaxRate']['default']) {
      $this->Session->setFlash(__('Nie można usunąć domyślnej stawki VAT.'), 'flash-error');
    } else {
      if ($this->TaxRate->delete($id)) {
        $this->Session->setFlash(__('Stawka VAT "%s" została usunięta.', h($tax_rate['TaxRate']['description'])), 'flash-success');
      } else {
        $this->Session->setFlash(__('Nie udało się usunąć stawki VAT "%s".', h($tax_rate['TaxRate']['description'])), 'flash-error');
      }
    }

    return $this->redirect(array('action' => 'index'));
  }
  
  public function admin_saveTaxRateSortOrder() {
    $tax_rate_id = (int) $this->request->query['tax_rate_id'];
    $value = (int) $this->request->query['value'];

    $tax_rate = $this->TaxRate->findById($tax_rate_id);
    $tax_rate['TaxRate']['sort_order'] = $value;
    if ($this->TaxRate->save($tax_rate)) {
      $result = array('success' => 1);
    } else {
      $result = array('success' => 0);
    }

    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set('result', $result);
    $this->set('_serialize', array('result'));
  }

}
