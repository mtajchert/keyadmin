<?php

App::uses('AdminController', 'KeyAdmin.Controller');

class KeyNotificationsController extends AdminController {

  public $uses = array('KeyAdmin.Notification');
  public $components = array('Paginator', 'RequestHandler', 'KeyAdmin.NotificationCenter');
  public $paginate = array(
    'limit' => 20,
  );

  public function beforeFilter() {
    parent::beforeFilter();
    $this->Paginator->settings = $this->paginate;
  }

  public function admin_index() {
    $this->set('notifications', $this->Paginator->paginate());
    
    $this->set('title', __('Powiadomienia'));
    $this->set('content_title', __('Powiadomienia'));
    $this->set('content_subtitle', __('Lista powiadomień'));
  }

  public function admin_send_test() {
    $this->Customer = ClassRegistry::init('KeyAdmin.Customer');
    $this->Customer->recursive = 1;
    $customer = $this->Customer->findById(191);
    if ($this->NotificationCenter->send('account_registration', [$customer['Customer']['email'], trim($customer['Customer']['first_name'].' '.$customer['Customer']['last_name'])], $customer)) {
      die('successful');
    }
    die('error');
  }
  
  public function admin_edit($id) {
    if (!$id) {
      throw new NotFoundException(__('Wybrane powiadomienie nie istnieje'));
    }

    $notification = $this->Notification->findById($id);
    if (!$notification) {
      throw new NotFoundException(__('Nie odnaleziono wybranego powiadomienia'));
    }

    if ($this->request->is(array('post', 'put'))) {
      $this->Notification->id = $id;
      if ($this->Notification->save($this->request->data)) {
        $this->Session->setFlash(__('Powiadomienie zostało zapisane.'), 'flash-success');
        return $this->redirect(array('action' => 'index'));
      }
      $this->Session->setFlash(__('Nie udało się zapisać powiadomienia. Sprawdź poprawność podanych danych.'), 'flash-error');
    }

    if (!$this->request->data) {
      $this->request->data = $notification;
    }

    $this->set('title', __('Powiadomienia'));
    $this->set('content_title', __('Powiadomienia'));
    $this->set('content_subtitle', __('Edycja powiadomienia'));
    
    $this->set('notification', $notification);
    $this->set('fields', $this->NotificationCenter->getFieldsList($notification['Notification']['fields']));
    
    $this->render('KeyNotifications/admin_form');
  }

}
