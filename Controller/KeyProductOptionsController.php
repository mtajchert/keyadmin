<?php
App::uses('AdminController', 'KeyAdmin.Controller');

class KeyProductOptionsController extends AdminController
{

    public $uses = array(
        'KeyAdmin.ProductOption',
        'KeyAdmin.ProductOptionValue',
        'KeyAdmin.CarManufacturer',
        'KeyAdmin.CarModel',
    );

    public $components = array(
        'Paginator',
        'RequestHandler'
    );

    public $paginate = array(
        'limit' => 20
    );

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Paginator->settings = $this->paginate;
    }

    
    
    public function admin_index_cars()
    {
        $this->CarManufacturer->contain(array('CarModel'));
        $cars = $this->CarManufacturer->find('all');
        $this->set(compact('cars'));
        $this->set('title', __('Marki/modele aut'));
        $this->set('content_title', __('Marki/modele aut'));
        $this->set('content_subtitle', __('Lista mark i modeli'));
        
    }
    
    
    
    public function admin_index()
    {
        $this->set('product_options', $this->Paginator->paginate());
        
        $this->set('title', __('Cechy produktów'));
        $this->set('content_title', __('Cechy produktów'));
        $this->set('content_subtitle', __('Lista cechy produktów'));
        $this->set('buttons_template', 'Buttons/KeyProductOptions');
    }

    public function admin_edit($id)
    {
        if (! $id) {
            throw new NotFoundException(__('Wybrana cecha produktów nie istnieje'));
        }
        
        $product_option = $this->ProductOption->findById($id);
        if (! $product_option) {
            throw new NotFoundException(__('Nie odnaleziono wybranej cechy produktów'));
        }
        
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $this->ProductOption->id = $id;
            if ($this->ProductOption->save($this->request->data)) {
                $this->Session->setFlash(__('Cecha produktów została zapisana.'), 'flash-success');
                return $this->redirect(array(
                    'action' => 'index'
                ));
            }
            $this->Session->setFlash(__('Nie udało się zapisać cechy produktów. Sprawdź poprawność podanych danych.'), 'flash-error');
        }
        
        if (! $this->request->data) {
            $this->request->data = $product_option;
        }
        
        $this->set('title', __('Cechy produktów'));
        $this->set('content_title', __('Cechy produktów'));
        $this->set('content_subtitle', __('Edycja cechy produktów'));
        $this->set('buttons_template', 'Buttons/KeyProductOptions');
        
        $this->set('product_option', $product_option);
        
        $this->render('KeyProductOptions/admin_form');
    }

    public function admin_create()
    {
        if ($this->request->is('post')) {
            $this->ProductOption->create();
            if ($this->ProductOption->save($this->request->data)) {
                $this->Session->setFlash(__('Cecha produktów została dodana.'), 'flash-success');
                return $this->redirect(array(
                    'action' => 'index'
                ));
            }
            $this->Session->setFlash(__('Nie udało się dodać cechy produktów. Sprawdź poprawność podanych danych.'), 'flash-error');
        } else {
            $this->request->data = array(
                'ProductOption' => array(
                    'type' => 'list'
                )
            );
        }
        
        $this->set('title', __('Cechy produktów'));
        $this->set('content_title', __('Cechy produktów'));
        $this->set('content_subtitle', __('Dodawanie cechy produktów'));
        $this->set('buttons_template', 'Buttons/KeyProductOptions');
        
        $this->set('product_option', $this->request->data);
        
        $this->render('KeyProductOptions/admin_form');
    }

    public function admin_delete($id)
    {
        $product_option = $this->ProductOption->findById($id);
        
        if ($this->ProductOption->delete($id)) {
            $this->ProductOptionValue->deleteAll(array(
                'ProductOptionValue.product_option_id' => $id
            ));
            $this->Session->setFlash(__('Cecha produktów "%s" została usunięta wraz z wartościami.', h($product_option['ProductOption']['name'])), 'flash-success');
        } else {
            $this->Session->setFlash(__('Nie udało się usunąć cechy produktów "%s".', h($product_option['ProductOption']['name'])), 'flash-error');
        }
        
        return $this->redirect(array(
            'action' => 'index'
        ));
    }

    public function admin_edit_value($id)
    {
        if (! $id) {
            throw new NotFoundException(__('Wybrana wartość cechy nie istnieje'));
        }
        
        $product_option_value = $this->ProductOptionValue->findById($id);
        if (! $product_option_value) {
            throw new NotFoundException(__('Nie odnaleziono wybranej wartości cechy'));
        }
        
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $this->ProductOptionValue->id = $id;
            if ($this->ProductOptionValue->save($this->request->data)) {
                $this->Session->setFlash(__('Wartość cechy została zapisana.'), 'flash-success');
                return $this->redirect(array(
                    'action' => 'index'
                ));
            }
            $this->Session->setFlash(__('Nie udało się zapisać wartości cechy. Sprawdź poprawność podanych danych.'), 'flash-error');
        }
        
        if (! $this->request->data) {
            $this->request->data = $product_option_value;
        }
        
        $this->set('title', __('Cechy produktów'));
        $this->set('content_title', __('Cechy produktów'));
        $this->set('content_subtitle', __('Edycja wartości cechy'));
        $this->set('buttons_template', 'Buttons/KeyProductOptions');
        
        $this->set('productOptions', $this->ProductOption->find('list'));
        
        $this->render('KeyProductOptions/admin_value_form');
    }

    public function admin_create_value($product_option_id = 0)
    {
        if ($this->request->is('post')) {
            $this->ProductOptionValue->create();
            if ($this->ProductOptionValue->save($this->request->data)) {
                $this->Session->setFlash(__('Wartość cechy została dodana.'), 'flash-success');
                return $this->redirect(array(
                    'action' => 'index'
                ));
            }
            $this->Session->setFlash(__('Nie udało się dodać wartości cechy. Sprawdź poprawność podanych danych.'), 'flash-error');
        } else {
            $this->request->data = array(
                'ProductOptionValue' => array(
                    'product_option_id' => (int) $product_option_id
                )
            );
        }
        
        $this->set('title', __('Cechy produktów'));
        $this->set('content_title', __('Cechy produktów'));
        $this->set('content_subtitle', __('Dodawanie wartości cechy'));
        $this->set('buttons_template', 'Buttons/KeyProductOptions');
        
        $this->set('productOptions', $this->ProductOption->find('list'));
        
        $this->render('KeyProductOptions/admin_value_form');
    }

    public function admin_delete_value($id)
    {
        $product_option_value = $this->ProductOptionValue->findById($id);
        
        if ($this->ProductOptionValue->delete($id)) {
            $this->Session->setFlash(__('Wartość cechy "%s" została usunięta.', h($product_option_value['ProductOptionValue']['name'])), 'flash-success');
        } else {
            $this->Session->setFlash(__('Nie udało się usunąć wartości cechy "%s".', h($product_option_value['ProductOptionValue']['name'])), 'flash-error');
        }
        
        return $this->redirect(array(
            'action' => 'index'
        ));
    }

    public function admin_saveProductOptionSortOrder()
    {
        $product_option_id = (int) $this->request->query['product_option_id'];
        $value = (int) $this->request->query['value'];
        
        $product_option = $this->ProductOption->findById($product_option_id);
        $product_option['ProductOption']['sort_order'] = $value;
        if ($this->ProductOption->save($product_option)) {
            $result = array(
                'success' => 1
            );
        } else {
            $result = array(
                'success' => 0
            );
        }
        
        $this->RequestHandler->renderAs($this, 'json');
        $this->set('jsonp', true);
        $this->set('result', $result);
        $this->set('_serialize', array(
            'result'
        ));
    }

    public function admin_getProductOptionValuesList()
    {
        $product_option_id = (int) $this->request->query['product_option_id'];
        
        $values = $this->ProductOptionValue->find('all', array(
            'conditions' => array(
                'ProductOptionValue.product_option_id' => $product_option_id
            )
        ));
        
        $view = new View($this, false);
        $view->viewPath = 'KeyProductOptions';
        $view->layout = false;
        $view->set('product_option_id', $product_option_id);
        $view->set('productOptionValues', $values);
        $html = $view->render('product_option_values_list');
        
        $result = array(
            'success' => 1,
            'html' => $html
        );
        
        $this->RequestHandler->renderAs($this, 'json');
        $this->set('jsonp', true);
        $this->set('result', $result);
        $this->set('_serialize', array(
            'result'
        ));
    }
}
