<?php
App::uses('AdminController', 'KeyAdmin.Controller');

class KeyPagesController extends AdminController
{

    public $uses = array(
        'KeyAdmin.Page',
        'KeyAdmin.BlogCategory',
        'KeyAdmin.Blog'
    );

    public $components = array(
        'Paginator',
        'RequestHandler'
    );

    public $paginate = array(
        'limit' => 20
    );

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Paginator->settings = $this->paginate;
    }

    public function admin_index()
    {
        $this->set('pages', $this->Paginator->paginate());
        
        $this->set('title', __('Strony tekstowe'));
        $this->set('content_title', __('Strony tekstowe'));
        $this->set('content_subtitle', __('Lista stron tekstowych'));
    }

    public function admin_edit($id)
    {
        if (! $id) {
            throw new NotFoundException(__('Wybrana strona nie istnieje'));
        }
        
        $page = $this->Page->findById($id);
        if (! $page) {
            throw new NotFoundException(__('Nie odnaleziono wybranej strony'));
        }
        
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $this->Page->id = $id;
            if ($this->Page->save($this->request->data)) {
                $this->Session->setFlash(__('Strona została zapisana.'), 'flash-success');
                return $this->redirect(array(
                    'action' => 'index'
                ));
            }
            $this->Session->setFlash(__('Nie udało się zapisać strony. Sprawdź poprawność podanych danych.'), 'flash-error');
        }
        
        if (! $this->request->data) {
            $this->request->data = $page;
        }
        
        $this->set('title', __('Strony tekstowe'));
        $this->set('content_title', __('Strony tekstowe'));
        $this->set('content_subtitle', __('Edycja strony tekstowej'));
        // $this->set('buttons_template', 'Buttons/KeyManufacturers');
        
        $this->render('KeyPages/admin_form');
    }

    public function admin_create()
    {
        if ($this->request->is('post')) {
            $this->Manufacturer->create();
            
            if ($this->Manufacturer->save($this->request->data)) {
                $this->Session->setFlash(__('Producent został dodany.'), 'flash-success');
                return $this->redirect(array(
                    'action' => 'index'
                ));
            }
            $this->Session->setFlash(__('Nie udało się dodać producenta. Sprawdź poprawność podanych danych.'), 'flash-error');
        }
        
        $this->set('title', __('Producenci'));
        $this->set('content_title', __('Producenci'));
        $this->set('content_subtitle', __('Dodawanie producenta'));
        $this->set('buttons_template', 'Buttons/KeyManufacturers');
        
        $this->render('KeyManufacturers/admin_form');
    }

    public function admin_delete($id)
    {
        /*
         * $page = $this->Page->findById($id);
         *
         * if ($this->Page->delete($id)) {
         * $this->Session->setFlash(__('Strona "%s" został usunięty.', h($page['Manufacturer']['name'])), 'flash-success');
         * } else {
         * $this->Session->setFlash(__('Nie udało się usunąć producenta "%s".', h($page['Manufacturer']['name'])), 'flash-error');
         * }
         */
        return $this->redirect(array(
            'action' => 'index'
        ));
    }

    public function admin_blog_index()
    {
        $this->Paginator->settings['conditions'] = array(
            'Page.group' => 'blog'
        );
        $this->Paginator->settings['contain'] = array(
            'BlogCategory'
        );
        $this->Paginator->settings['order'] = array(
            'Page.created' => 'DESC'
        );
        $this->set('pages', $this->Paginator->paginate());
        
        $this->set('title', __('Blog'));
        $this->set('content_title', __('Blog'));
        $this->set('content_subtitle', __('Lista wpisów'));
        $this->set('buttons_template', 'Buttons/KeyPages');
    }

    /**
     *
     * @param unknown $pageId            
     * @throws NotFoundException
     */
    public function admin_blog_edit($pageId)
    {
        if (! empty($pageId)) {
            $this->Blog->contain(array(
                'PromoteBlog'
            ));
            $page = $this->Blog->findById($pageId);
            if (! $page) {
                throw new NotFoundException(__('Nie odnaleziono wybranej strony'));
            }
        }
        
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            if (! empty($pageId)) {
                $this->request->data['Blog']['id'] = $pageId;
                $this->Blog->PromoteBlog->deleteAll(array(
                    'PromoteBlog.blog_id' => $pageId
                ));
            }
            $this->Blog->create();
            if ($this->Blog->saveAll($this->request->data)) {
                $this->Session->setFlash(__('Wpis został zapisany.'), 'flash-success');
                return $this->redirect(array(
                    'action' => 'blog_index'
                ));
            }
            
            $this->Session->setFlash(__('Nie udało się dodać wpisu. Sprawdź poprawność podanych danych.'), 'flash-error');
        }
        if (! $this->request->data) {
            if (! empty($page['Blog']['tags'])) {
                $page['Blog']['tags'] = unserialize($page['Blog']['tags']);
                $page['Blog']['tags'] = implode(';', $page['Blog']['tags']);
            }
            $this->request->data = (isset($page)) ? $page : array();
            // $this->request->data['Blog']['promote_blog'] = Hash::extract($page, 'PromoteBlog.{n}.blog_category_id');
        }
        
        $this->BlogCategory->contain(array(
            'ChildCategory'
        ));
        $categories = $this->BlogCategory->find('all', array(
            'conditions' => array(
                'OR' => [
                    [
                        'BlogCategory.parent_id' => 0
                    ],
                    [
                        'BlogCategory.parent_id' => null
                    ]
                ]
            )
        ));
        $categoryList = array();
        foreach ($categories as $item) {
            $categoryList[$item['BlogCategory']['id']] = $item['BlogCategory']['title'];
            if (! empty($item['ChildCategory'])) {
                foreach ($item['ChildCategory'] as $child) {
                    $categoryList[$child['id']] = $item['BlogCategory']['title'] . ' > ' . $child['title'];
                }
            }
        }
        
        $this->set('categories', $categoryList);
        $this->set('title', __('Blog'));
        $this->set('content_title', __('Blog'));
        $this->set('content_subtitle', __('Dodawanie wpisu'));
        $this->set('buttons_template', 'Buttons/KeyPages');
    }

    public function admin_blog_delete($id)
    {
        $page = $this->Page->findById($id);
        
        if ($this->Page->delete($id)) {
            $this->Session->setFlash(__('Wpis "%s" został usunięty.', h($page['Page']['title'])), 'flash-success');
        } else {
            $this->Session->setFlash(__('Nie udało się usunąć wpisu "%s".', h($page['Page']['title'])), 'flash-error');
        }
        
        return $this->redirect(array(
            'action' => 'blog_index'
        ));
    }
}
