<?php

App::uses('AdminController', 'KeyAdmin.Controller');

class KeyUnitsController extends AdminController {

  public $uses = array('KeyAdmin.Unit');
  public $components = array('Paginator', 'RequestHandler');
  public $paginate = array(
    'limit' => 20,
  );

  public function beforeFilter() {
    parent::beforeFilter();
    $this->Paginator->settings = $this->paginate;
  }

  public function admin_index() {
    $this->set('units', $this->Paginator->paginate());
    
    $this->set('title', __('Jednostki miary'));
    $this->set('content_title', __('Jednostki miary'));
    $this->set('content_subtitle', __('Lista jednostek miary'));
    $this->set('buttons_template', 'Buttons/KeyUnits');
  }

  public function admin_edit($id) {
    if (!$id) {
      throw new NotFoundException(__('Wybrana jednostka miary nie istnieje'));
    }

    $unit = $this->Unit->findById($id);
    if (!$unit) {
      throw new NotFoundException(__('Nie odnaleziono wybranej jednostki miary'));
    }

    if ($this->request->is(array('post', 'put'))) {
      $this->Unit->id = $id;
      
      $this->request->data['Unit']['sort_order'] = (int) $this->request->data['Unit']['sort_order'];
      
      if ($this->Unit->save($this->request->data)) {
        if ($this->request->data['Unit']['default']) {
          $default = $this->Unit->find('all', array(
            'conditions' => array(
              'Unit.default' => 1,
              'Unit.id !=' => $id
            )
          ));
          foreach ($default as $default_i) {
            $default_i['Unit']['default'] = 0;
            $this->Unit->save($default_i);
          }
        }
        
        $this->Session->setFlash(__('Jednostka miary została zapisana.'), 'flash-success');
        return $this->redirect(array('action' => 'index'));
      }
      $this->Session->setFlash(__('Nie udało się zapisać jednostki miary. Sprawdź poprawność podanych danych.'), 'flash-error');
    }
    
    if (!$this->request->data) {
      $this->request->data = $unit;
    }

    $this->set('title', __('Jednostki miary'));
    $this->set('content_title', __('Jednostki miary'));
    $this->set('content_subtitle', __('Edycja jednostki miary'));
    $this->set('buttons_template', 'Buttons/KeyUnits');
    
    $this->set('unit', $unit);
    
    $this->render('KeyUnits/admin_form');
  }

  public function admin_create() {
    if ($this->request->is('post')) {
      $this->Unit->create();
      
      $this->request->data['Unit']['sort_order'] = (int) $this->request->data['Unit']['sort_order'];
      
      if ($this->Unit->save($this->request->data)) {
        if ($this->request->data['Unit']['default']) {
          $default = $this->Unit->find('all', array(
            'conditions' => array(
              'Unit.default' => 1,
              'Unit.id !=' => $this->Unit->getLastInsertID()
            )
          ));
          foreach ($default as $default_i) {
            $default_i['Unit']['default'] = 0;
            $this->Unit->save($default_i);
          }
        }
        
        $this->Session->setFlash(__('Jednostka miary została dodana.'), 'flash-success');
        return $this->redirect(array('action' => 'index'));
      }
      $this->Session->setFlash(__('Nie udało się dodać jednostki miary. Sprawdź poprawność podanych danych.'), 'flash-error');
    } else {
      $this->request->data = ['Unit' => ['quantity_type' => 'int', 'default' => 0]];
    }

    $this->set('title', __('Jednostki miary'));
    $this->set('content_title', __('Jednostki miary'));
    $this->set('content_subtitle', __('Dodawanie jednostki miary'));
    $this->set('buttons_template', 'Buttons/KeyUnits');
    
    $this->set('unit', $this->request->data);
    
    $this->render('KeyUnits/admin_form');
  }

  public function admin_delete($id) {
    $unit = $this->Unit->findById($id);
    
    if ($unit['Unit']['default']) {
      $this->Session->setFlash(__('Nie można usunąć domyślnej jednostki miary.'), 'flash-error');
    } else {
      if ($this->Unit->delete($id)) {
        $this->Session->setFlash(__('Jednostka miary "%s" została usunięta.', h($unit['Unit']['name'])), 'flash-success');
      } else {
        $this->Session->setFlash(__('Nie udało się usunąć jednostki miary "%s".', h($unit['Unit']['name'])), 'flash-error');
      }
    }

    return $this->redirect(array('action' => 'index'));
  }
  
  public function admin_saveUnitSortOrder() {
    $unit_id = (int) $this->request->query['unit_id'];
    $value = (int) $this->request->query['value'];

    $unit = $this->Unit->findById($unit_id);
    $unit['Unit']['sort_order'] = $value;
    if ($this->Unit->save($unit)) {
      $result = array('success' => 1);
    } else {
      $result = array('success' => 0);
    }

    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set('result', $result);
    $this->set('_serialize', array('result'));
  }

}
