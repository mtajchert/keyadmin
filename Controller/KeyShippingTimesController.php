<?php

App::uses('AdminController', 'KeyAdmin.Controller');

class KeyShippingTimesController extends AdminController {

  public $uses = array('KeyAdmin.ShippingTime');
  public $components = array('Paginator', 'RequestHandler');
  public $paginate = array(
    'limit' => 20,
  );

  public function beforeFilter() {
    parent::beforeFilter();
    $this->Paginator->settings = $this->paginate;
  }

  public function admin_index() {
    $this->set('shipping_times', $this->Paginator->paginate());
    
    $this->set('title', __('Terminy wysyłki'));
    $this->set('content_title', __('Terminy wysyłki'));
    $this->set('content_subtitle', __('Lista terminów wysyłki'));
    $this->set('buttons_template', 'Buttons/KeyShippingTimes');
  }

  public function admin_edit($id) {
    if (!$id) {
      throw new NotFoundException(__('Wybrany termin wysyłki nie istnieje'));
    }

    $shipping_time = $this->ShippingTime->findById($id);
    if (!$shipping_time) {
      throw new NotFoundException(__('Nie odnaleziono wybranego terminu wysyłki'));
    }

    if ($this->request->is(array('post', 'put'))) {
      $this->ShippingTime->id = $id;
      if ($this->ShippingTime->save($this->request->data)) {
        $this->Session->setFlash(__('Termin wysyłki został zapisany.'), 'flash-success');
        return $this->redirect(array('action' => 'index'));
      }
      $this->Session->setFlash(__('Nie udało się zapisać terminu wysyłki. Sprawdź poprawność podanych danych.'), 'flash-error');
    }

    if (!$this->request->data) {
      $this->request->data = $shipping_time;
    }

    $this->set('title', __('Terminy wysyłki'));
    $this->set('content_title', __('Terminy wysyłki'));
    $this->set('content_subtitle', __('Edycja terminu wysyłki'));
    $this->set('buttons_template', 'Buttons/KeyShippingTimes');
    
    $this->set('shipping_time', $shipping_time);
    
    $this->render('KeyShippingTimes/admin_form');
  }

  public function admin_create() {
    if ($this->request->is('post')) {
      $this->ShippingTime->create();
      if ($this->ShippingTime->save($this->request->data)) {
        $this->Session->setFlash(__('Termin wysyłki został dodany.'), 'flash-success');
        return $this->redirect(array('action' => 'index'));
      }
      $this->Session->setFlash(__('Nie udało się dodać terminu wysyłki. Sprawdź poprawność podanych danych.'), 'flash-error');
    }

    $this->set('title', __('Terminy wysyłki'));
    $this->set('content_title', __('Terminy wysyłki'));
    $this->set('content_subtitle', __('Dodawanie terminu wysyłki'));
    $this->set('buttons_template', 'Buttons/KeyShippingTimes');
    
    $this->set('shipping_time', $this->request->data);
    
    $this->render('KeyShippingTimes/admin_form');
  }

  public function admin_delete($id) {
    $shipping_time = $this->ShippingTime->findById($id);
    
    if ($this->ShippingTime->delete($id)) {
      $this->Session->setFlash(__('Termin wysyłki "%s" został usunięty.', h($shipping_time['ShippingTime']['name'])), 'flash-success');
    } else {
      $this->Session->setFlash(__('Nie udało się usunąć terminu wysyłki "%s".', h($shipping_time['ShippingTime']['name'])), 'flash-error');
    }

    return $this->redirect(array('action' => 'index'));
  }

}
