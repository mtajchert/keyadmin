<?php

App::uses('AdminController', 'KeyAdmin.Controller');

class KeyShellsController extends AdminController {
  
  public function admin_index(){
    App::import('Console/Command', 'AppShell');
    App::import('Plugin/KeyAdmin/Console/Command', 'ImageRescaleShell');
    
    $job = new ImageRescaleShell();
    $job->startup();
    $job->dispatchMethod('products');
    die();
  }
}