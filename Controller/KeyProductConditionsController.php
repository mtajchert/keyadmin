<?php

App::uses('AdminController', 'KeyAdmin.Controller');

class KeyProductConditionsController extends AdminController {

  public $uses = array('KeyAdmin.ProductCondition');
  public $components = array('Paginator', 'RequestHandler');
  public $paginate = array(
    'limit' => 20,
  );

  public function beforeFilter() {
    parent::beforeFilter();
    $this->Paginator->settings = $this->paginate;
  }

  public function admin_index() {
    $this->set('product_conditions', $this->Paginator->paginate());
    
    $this->set('title', __('Stany produktów'));
    $this->set('content_title', __('Stany produktów'));
    $this->set('content_subtitle', __('Lista stanów produktów'));
    $this->set('buttons_template', 'Buttons/KeyProductConditions');
  }

  public function admin_edit($id) {
    if (!$id) {
      throw new NotFoundException(__('Wybrany stan produktów nie istnieje'));
    }

    $product_condition = $this->ProductCondition->findById($id);
    if (!$product_condition) {
      throw new NotFoundException(__('Nie odnaleziono wybranego stanu produktów'));
    }

    if ($this->request->is(array('post', 'put'))) {
      $this->ProductCondition->id = $id;
      if ($this->ProductCondition->save($this->request->data)) {
        if ($this->request->data['ProductCondition']['default']) {
          $default = $this->ProductCondition->find('all', array(
            'conditions' => array(
              'ProductCondition.default' => 1,
              'ProductCondition.id !=' => $id
            )
          ));
          foreach ($default as $default_i) {
            $default_i['ProductCondition']['default'] = 0;
            $this->ProductCondition->save($default_i);
          }
        }
        
        $this->Session->setFlash(__('Stan produktów został zapisany.'), 'flash-success');
        return $this->redirect(array('action' => 'index'));
      }
      $this->Session->setFlash(__('Nie udało się zapisać stanu produktów. Sprawdź poprawność podanych danych.'), 'flash-error');
    }

    if (!$this->request->data) {
      $this->request->data = $product_condition;
    }

    $this->set('title', __('Stany produktów'));
    $this->set('content_title', __('Stany produktów'));
    $this->set('content_subtitle', __('Edycja stanu produktów'));
    $this->set('buttons_template', 'Buttons/KeyProductConditions');
    
    $this->set('product_condition', $product_condition);
    
    $this->render('KeyProductConditions/admin_form');
  }

  public function admin_create() {
    if ($this->request->is('post')) {
      $this->ProductCondition->create();
      
      if ($this->ProductCondition->save($this->request->data)) {
        if ($this->request->data['ProductCondition']['default']) {
          $default = $this->ProductCondition->find('all', array(
            'conditions' => array(
              'ProductCondition.default' => 1,
              'ProductCondition.id !=' => $this->ProductCondition->getLastInsertID()
            )
          ));
          foreach ($default as $default_i) {
            $default_i['ProductCondition']['default'] = 0;
            $this->ProductCondition->save($default_i);
          }
        }
        
        $this->Session->setFlash(__('Stan produktów został dodany.'), 'flash-success');
        return $this->redirect(array('action' => 'index'));
      }
      $this->Session->setFlash(__('Nie udało się dodać stanu produktów. Sprawdź poprawność podanych danych.'), 'flash-error');
    } else {
      $this->request->data = ['ProductCondition' => ['default' => 0]];
    }

    $this->set('title', __('Stany produktów'));
    $this->set('content_title', __('Stany produktów'));
    $this->set('content_subtitle', __('Dodawanie stanu produktów'));
    $this->set('buttons_template', 'Buttons/KeyProductConditions');
    
    $this->set('product_condition', $this->request->data);
    
    $this->render('KeyProductConditions/admin_form');
  }

  public function admin_delete($id) {
    $product_condition = $this->ProductCondition->findById($id);
    
    if ($product_condition['ProductCondition']['default']) {
      $this->Session->setFlash(__('Nie można usunąć domyślnego stanu produktów.'), 'flash-error');
    } else {
      if ($this->ProductCondition->delete($id)) {
        $this->Session->setFlash(__('Stan produktów "%s" został usunięty.', h($product_condition['ProductCondition']['name'])), 'flash-success');
      } else {
        $this->Session->setFlash(__('Nie udało się usunąć stanu produktów "%s".', h($product_condition['ProductCondition']['name'])), 'flash-error');
      }
    }

    return $this->redirect(array('action' => 'index'));
  }

}
