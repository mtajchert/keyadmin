<?php
App::uses('AdminController', 'KeyAdmin.Controller');

class KeyDiscountsController extends AdminController
{

    var $uses = array(
        'KeyAdmin.Discount'
    );

    public $components = array(
        'Paginator'
    );

    public $paginate = array(
        'limit' => 20
    );

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Paginator->settings = $this->paginate;
    }

    /**
     */
    public function admin_index()
    {
        $this->set('title', __('Kupony rabatowe'));
        $this->set('content_title', __('Kupony rabatowe'));
        $this->set('content_subtitle', __('Lista kuponów rabatowych'));
        $this->set('buttons_template', 'Buttons/KeyDiscounts');
        
        $this->Paginator->settings['contain'] = [
            'Category' => [
                'fields' => [
                    'id',
                    'name'
                ]
            ],
            'Manufacturer' => [
                'fields' => [
                    'id',
                    'name'
                ]
            ]
        ];
        $this->set('discounts', $this->Paginator->paginate());
    }

    /**
     */
    public function admin_edit($discountId)
    {
        if (! empty($discountId)) {
            $this->Discount->contain([
                'Category',
                'Manufacturer'                
            ]);
            $discount = $this->Discount->findById($discountId);
            if (! $discount) {
                throw new NotFoundException(__('Nie odnaleziono rabatu'));
            }
        }
        
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            if (! empty($discountId)) {
                $this->request->data['Discount']['id'] = $discountId;
            }
            $this->Discount->create();
            if ($this->Discount->saveAll($this->request->data)) {
                $this->Session->setFlash(__('Kupon rabatowy została zapisany.'), 'flash-success');
                return $this->redirect(array(
                    'action' => 'index'
                ));
            }
            
            $this->Session->setFlash(__('Nie udało się. Sprawdź poprawność podanych danych.'), 'flash-error');
        }
        
        if (! $this->request->data) {
            $this->request->data = (isset($discount)) ? $discount : array();
            
            if (! isset($this->request->data['Discount']['code']) || empty($this->request->data['Discount']['code'])) {
                $prefix = Configure::read('Discount.prefix');
                $length = Configure::read('Discount.code_length');
                $this->request->data['Discount']['code'] = $this->Discount->generateNewCode($prefix, $length);
            }
        }
        
        $this->set('title', __('Kupony rabatowe'));
        $this->set('content_title', __('Kupony rabatowe'));
        if (! empty($discountId)) {
            $this->set('content_subtitle', __('Edycja'));
        } else {
            $this->set('content_subtitle', __('Dodawanie'));
        }
        
        $this->set('categories', $this->Discount->Category->getList());
        
        $this->loadModel('KeyAdmin.Manufacturer');
        $list = $this->Manufacturer->find('list', [
            'order' => [
                'Manufacturer.name' => 'ASC'
            ]
        ]);
        $this->set('manufacturers', $list);
        
        $this->set('buttons_template', 'Buttons/KeyDiscounts');
    }
}