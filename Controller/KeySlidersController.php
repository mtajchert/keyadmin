<?php
App::uses('AdminController', 'KeyAdmin.Controller');

class KeySlidersController extends AdminController
{

    public $uses = array(
        'KeyAdmin.Slider'
    );

    public $components = array(
        'Paginator',
        'RequestHandler'
    );

    public $paginate = array(
        'limit' => 20
    );

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Paginator->settings = $this->paginate;
        
        $this->set('title', __('Slidery'));
        $this->set('content_title', __('Slidery'));
    }

    public function admin_index()
    {
        $this->set('sliders', $this->Paginator->paginate());
        
        $this->set('content_subtitle', __('Lista sliderów'));
        $this->set('buttons_template', 'Buttons/KeySliders');
    }

    public function admin_edit($id)
    {
        if (! $this->Slider->exists($id)) {
            throw new NotFoundException(__('Wybrany slider nie istnieje'));
        }
        
        $slider = $this->Slider->findById($id);
        
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $this->Slider->id = $id;
            if ($this->Slider->save($this->request->data)) {
                $this->Session->setFlash(__('Slider został zapisany.'), 'flash-success');
                return $this->redirect(array(
                    'action' => 'index'
                ));
            }
            $this->Session->setFlash(__('Nie udało się zapisać slidera. Sprawdź poprawność podanych danych.'), 'flash-error');
        }
        
        if (! $this->request->data) {
            $this->request->data = $slider;
        }
        
        $this->set('content_subtitle', __('Edycja slidera'));
        $this->set('buttons_template', 'Buttons/KeySliders');
        
        $this->render('KeySliders/admin_form');
    }

    public function admin_create()
    {
        if ($this->request->is('post')) {
            $this->Slider->create();
            
            if ($this->Slider->save($this->request->data)) {
                $this->Session->setFlash(__('Slider został dodany.'), 'flash-success');
                return $this->redirect(array(
                    'action' => 'index'
                ));
            }
            $this->Session->setFlash(__('Nie udało się dodać slidera. Sprawdź poprawność podanych danych.'), 'flash-error');
        }
        
        $this->set('content_subtitle', __('Dodawanie slidera'));
        $this->set('buttons_template', 'Buttons/KeySliders');
        
        $this->render('KeySliders/admin_form');
    }

    public function admin_delete($id)
    {
        $slider = $this->Slider->findById($id);
        
        if ($this->Slider->delete($id)) {
            $this->Session->setFlash(__('Slider został usunięty.'), 'flash-success');
        } else {
            $this->Session->setFlash(__('Nie udało się usunąć slidera'), 'flash-error');
        }
        
        return $this->redirect(array(
            'action' => 'index'
        ));
    }
}
