<?php

App::uses('AdminController', 'KeyAdmin.Controller');

class KeyPaymentsController extends AdminController {

  public $uses = array('KeyAdmin.Payment');
  public $components = array('Paginator', 'RequestHandler');
  public $paginate = array(
    'limit' => 20,
  );

  public function beforeFilter() {
    parent::beforeFilter();
    $this->Paginator->settings = $this->paginate;
  }

  public function admin_index() {
    $this->Payment->recursive = 2;
    $list = $this->Paginator->paginate();
    
    $this->set('payments', $this->Paginator->paginate());

    $this->set('title', __('Formy płatności'));
    $this->set('content_title', __('Formy płatności'));
    $this->set('content_subtitle', __('Lista form płatności'));
    $this->set('buttons_template', 'Buttons/KeyPayments');
  }

  public function admin_edit($id) {
    if (!$id) {
      throw new NotFoundException(__('Wybrana forma płatności nie istnieje'));
    }

    $this->Payment->recursive = 2;
    $payment = $this->Payment->findById($id);
    if (!$payment) {
      throw new NotFoundException(__('Nie odnaleziono wybranej formy płatności'));
    }
    
    if ($this->request->is(array('post', 'put'))) {
      $this->request->data['Payment']['id'] = $id;
      $this->request->data['Payment']['sort_order'] = (int) $this->request->data['Payment']['sort_order'];
      
      if ($this->Payment->save($this->request->data)) {
        $this->Session->setFlash(__('Forma płatności została zapisana.'), 'flash-success');
        return $this->redirect(array('action' => 'index'));
      }
      
      $this->request->data['Payment']['image'] = $payment['Payment']['image'];
      $this->Session->setFlash(__('Nie udało się zapisać formy płatności. Sprawdź poprawność podanych danych.'), 'flash-error');
    }

    if (!$this->request->data) {
      $this->request->data = $payment;
    }
    
    $this->set('title', __('Formy płatności'));
    $this->set('content_title', __('Formy płatności'));
    $this->set('content_subtitle', __('Edycja formy płatności'));
    $this->set('buttons_template', 'Buttons/KeyPayments');

    $this->render('KeyPayments/admin_form');
  }

  public function admin_savePaymentSortOrder() {
    $payment_id = (int) $this->request->query['payment_id'];
    $value = (int) $this->request->query['value'];

    $payment = $this->Payment->findById($payment_id);
    $payment['Payment']['sort_order'] = $value;
    if ($this->Payment->save($payment, false)) {
      $result = array('success' => 1);
    } else {
      $result = array('success' => 0);
    }

    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set('result', $result);
    $this->set('_serialize', array('result'));
  }

  public function admin_savePaymentStatus() {
    $payment_id = (int) $this->request->query['payment_id'];
    $value = (int) $this->request->query['value'];

    $payment = $this->Payment->findById($payment_id);
    $payment['Payment']['status'] = $value;
    if ($this->Payment->save($payment, false)) {
      $result = array('success' => 1);
    } else {
      $result = array('success' => 0);
    }

    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set('result', $result);
    $this->set('_serialize', array('result'));
  }

}
