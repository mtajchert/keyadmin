<?php

App::uses('AdminController', 'KeyAdmin.Controller');

class KeyShippingsController extends AdminController {

  public $uses = array('KeyAdmin.Shipping');
  public $components = array('Paginator', 'RequestHandler');
  public $paginate = array(
    'limit' => 20,
  );

  public function beforeFilter() {
    parent::beforeFilter();
    $this->Paginator->settings = $this->paginate;
  }

  public function admin_index() {
    $this->Shipping->recursive = 2;
    $list = $this->Paginator->paginate();
    
    $this->set('shippings', $this->Paginator->paginate());

    $this->set('title', __('Formy dostawy'));
    $this->set('content_title', __('Formy dostawy'));
    $this->set('content_subtitle', __('Lista form dostawy'));
    $this->set('buttons_template', 'Buttons/KeyShippings');
  }

  public function admin_edit($id) {
    if (!$id) {
      throw new NotFoundException(__('Wybrana forma dostawy nie istnieje'));
    }

    $this->Shipping->recursive = 2;
    $shipping = $this->Shipping->findById($id);
    if (!$shipping) {
      throw new NotFoundException(__('Nie odnaleziono wybranej formy dostawy'));
    }
    
    if ($this->request->is(array('post', 'put'))) {
      $this->request->data['Shipping']['id'] = $id;
      $this->request->data['Shipping']['sort_order'] = (int) $this->request->data['Shipping']['sort_order'];
      
      $shippingPricesIds = [-1];
      if (isset($this->request->data['ShippingPrice'])) {
        foreach ($this->request->data['ShippingPrice'] as $key => $item) {
          $shippingPricesIds[] = $this->request->data['ShippingPrice'][$key]['id'];
          $this->request->data['ShippingPrice'][$key]['shipping_id'] = $id;
          if (!isset($this->request->data['ShippingPrice'][$key]['to_weight'])) {
            $this->request->data['ShippingPrice'][$key]['to_weight'] = 0;
          }
          if (!isset($this->request->data['ShippingPrice'][$key]['to_price_tax'])) {
            $this->request->data['ShippingPrice'][$key]['to_price_tax'] = 0;
          }
          if (!isset($this->request->data['ShippingPrice'][$key]['to_product_amount'])) {
            $this->request->data['ShippingPrice'][$key]['to_product_amount'] = 0;
          }
        }
      }
      
      $existsShippingPrices = $this->Shipping->ShippingPrice->find('all', array(
        'conditions' => array('ShippingPrice.shipping_id' => $id),
        'fields' => array('ShippingPrice.id')
      ));
      $existsShippingPricesIds = [-1];
      foreach ($existsShippingPrices as $item) {
        $existsShippingPricesIds[] = $item['ShippingPrice']['id'];
      }
      
      if ($this->Shipping->saveAll($this->request->data, array('price_type' => $this->request->data['Shipping']['price_type']))) {
        $this->Shipping->ShippingPrice->deleteAll(array('ShippingPrice.id' => $existsShippingPricesIds, 'ShippingPrice.id !=' => $shippingPricesIds));
        
        $this->Session->setFlash(__('Forma dostawy została zapisana.'), 'flash-success');
        return $this->redirect(array('action' => 'index'));
      }
      
      $this->request->data['Shipping']['image'] = $shipping['Shipping']['image'];
      $this->Session->setFlash(__('Nie udało się zapisać formy dostawy. Sprawdź poprawność podanych danych.'), 'flash-error');
    }

    if (!$this->request->data) {
      $this->request->data = $shipping;
    }
    
    $this->set('title', __('Formy dostawy'));
    $this->set('content_title', __('Formy dostawy'));
    $this->set('content_subtitle', __('Edycja formy dostawy'));
    $this->set('buttons_template', 'Buttons/KeyShippings');

    $this->set('shipping', $this->request->data);

    $taxRates = $this->Shipping->TaxRate->find('list', array(
      'fields' => array('TaxRate.id', 'TaxRate.description')
    ));
    $payments = $this->Shipping->Payment->find('list', array(
      'fields' => array('Payment.id', 'Payment.name')
    ));
    $countries = $this->Shipping->Country->find('list', array(
      'fields' => array('Country.id', 'Country.name')
    ));
    $this->set(compact('taxRates', 'payments', 'countries'));

    $this->render('KeyShippings/admin_form');
  }

  public function admin_create() {
    if ($this->request->is('post')) {
      $this->Shipping->create();

      $this->request->data['Shipping']['sort_order'] = (int) $this->request->data['Shipping']['sort_order'];
      
      if (isset($this->request->data['ShippingPrice'])) {
        foreach ($this->request->data['ShippingPrice'] as $key => $item) {
          if (!isset($this->request->data['ShippingPrice'][$key]['to_weight'])) {
            $this->request->data['ShippingPrice'][$key]['to_weight'] = 0;
          }
          if (!isset($this->request->data['ShippingPrice'][$key]['to_price_tax'])) {
            $this->request->data['ShippingPrice'][$key]['to_price_tax'] = 0;
          }
          if (!isset($this->request->data['ShippingPrice'][$key]['to_product_amount'])) {
            $this->request->data['ShippingPrice'][$key]['to_product_amount'] = 0;
          }
        }
      }
      
      if ($this->Shipping->saveAll($this->request->data, array('price_type' => $this->request->data['Shipping']['price_type']))) {
        $this->Session->setFlash(__('Forma dostawy została dodana.'), 'flash-success');
        return $this->redirect(array('action' => 'index'));
      }
      
      $this->request->data['Shipping']['image'] = '';
      $this->Session->setFlash(__('Nie udało się dodać formy dostawy. Sprawdź poprawność podanych danych.'), 'flash-error');
    } else {
      $tax_rate = $this->Shipping->TaxRate->find('first', array('conditions' => array('TaxRate.default' => 1)));
      $this->request->data = [
        'Shipping' => ['quantity_type' => 'int', 'default' => 0, 'tax_rate_id' => $tax_rate['TaxRate']['id'], 'large_scale' => 0, 'price_type' => '']
      ];
    }

    $this->set('title', __('Formy dostawy'));
    $this->set('content_title', __('Formy dostawy'));
    $this->set('content_subtitle', __('Dodawanie formy dostawy'));
    $this->set('buttons_template', 'Buttons/KeyShippings');

    $this->set('shipping', $this->request->data);

    $taxRates = $this->Shipping->TaxRate->find('list', array(
      'fields' => array('TaxRate.id', 'TaxRate.description')
    ));
    $payments = $this->Shipping->Payment->find('list', array(
      'fields' => array('Payment.id', 'Payment.name')
    ));
    $countries = $this->Shipping->Country->find('list', array(
      'fields' => array('Country.id', 'Country.name')
    ));
    $this->set(compact('taxRates', 'payments', 'countries'));

    $this->render('KeyShippings/admin_form');
  }

  public function admin_delete($id) {
    $shipping = $this->Shipping->findById($id);
    
    if ($this->Shipping->delete($id, true)) {
      $this->Shipping->ShippingPrice->deleteAll(array('shipping_id' => $id));
      $this->Session->setFlash(__('Forma dostawy "%s" została usunięta.', h($shipping['Shipping']['name'])), 'flash-success');
    } else {
      $this->Session->setFlash(__('Nie udało się usunąć formy dostawy "%s".', h($shipping['Shipping']['name'])), 'flash-error');
    }

    return $this->redirect(array('action' => 'index'));
  }

  public function admin_saveShippingSortOrder() {
    $shipping_id = (int) $this->request->query['shipping_id'];
    $value = (int) $this->request->query['value'];

    $shipping = $this->Shipping->findById($shipping_id);
    $shipping['Shipping']['sort_order'] = $value;
    if ($this->Shipping->save($shipping, false)) {
      $result = array('success' => 1);
    } else {
      $result = array('success' => 0);
    }

    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set('result', $result);
    $this->set('_serialize', array('result'));
  }

  public function admin_saveShippingStatus() {
    $shipping_id = (int) $this->request->query['shipping_id'];
    $value = (int) $this->request->query['value'];

    $shipping = $this->Shipping->findById($shipping_id);
    $shipping['Shipping']['status'] = $value;
    if ($this->Shipping->save($shipping, false)) {
      $result = array('success' => 1);
    } else {
      $result = array('success' => 0);
    }

    $this->RequestHandler->renderAs($this, 'json');
    $this->set('jsonp', true);
    $this->set('result', $result);
    $this->set('_serialize', array('result'));
  }

}
