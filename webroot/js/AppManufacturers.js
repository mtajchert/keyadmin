var App = App || {};

(function (App, $) {
  /**
   * 
   */
  App.Manufacturers = {
    lang: {
      confirmDeleteMessage: 'Czy na pewno chcesz usunąć producenta "<%- name %>"?'
    },
    
    /**
     * 
     */
    onDocReady: function () {
      // --
    },

    deleteManufacturer: function(name, deleteUrl) {
      var messageTemplate = _.template(this.lang.confirmDeleteMessage);
      App.Modal.confirmDelete(messageTemplate({name:name}), 'window.location.href=\'' + deleteUrl + '\'');
    }
    
  };
})(App, jQuery);

$(document).ready(function () {
  // --
});