var App = App || {};

(function (App, $) {
  /**
   * 
   */
  App.OurHits = {
    lang: {
      confirmDeleteMessage: 'Czy na pewno chcesz usunąć nasz hit "<%- name %>"?',
      noChoosedProduct: 'Wybierz produkt do naszego hitu',
      saveOurHitStatusErrorMsg: 'Wystąpił błąd podczas zapisu statusu',
      saveProductStatusErrorMsg: 'Wystąpił błąd podczas zapisu statusu',
      confirmDeleteOurHits: 'Czy na pewno chcesz usunąć nasz hit z zaznaczonych produktów?',
      confirmDeleteProducts: 'Czy na pewno chcesz usunąć wybrane produkty?',
      confirmActionTitle: 'Potwierdź wykonanie akcji',
      confirmDeactivateOurHits: 'Czy na pewno chcesz zmienić status wybranych hitów na nieaktywny?',
      confirmActivateOurHits: 'Czy na pewno chcesz zmienić status wybranych hitów na aktywny?',
      confirmDeactivateProducts: 'Czy na pewno chcesz zmienić status wybranych produktów na nieaktywny?',
      confirmActivateProducts: 'Czy na pewno chcesz zmienić status wybranych produktów na aktywny?',
      confirmClearOurHitsDate: 'Czy na pewno wyzerować datę rozpoczęcia wybranych hitów?',
      confirmClearOurHitsDateEnd: 'Czy na pewno wyzerować datę zakończenia wybranych hitów?',
      confirmManipulateOurHitsDate: 'Wprowadź ilość dni o jaką zmniejszyć/zwiększyć datę rozpoczęcia:',
      confirmManipulateOurHitsDateEnd: 'Wprowadź ilość dni o jaką zmniejszyć/zwiększyć datę zakończenia:',
      manipulateDateInputPlaceholder: 'Wpisz ilość dni'
    },
    
    getProductsTableUrl: null,
    getProductDataUrl: null,
    saveOurHitStatusUrl: null,
    saveProductStatusUrl: null,
    
    /**
     * 
     */
    onDocReady: function () {
      // --
    },
    
    openChooseProductBox: function() {
      if ($('#ChooseCategoryProductBox').length === 0) {
        var fn = _.template($('#modal-choose-category-product-template').html());
        var modalTemplate = fn({saveScript: 'App.OurHits.saveProductBox()', searchScript: 'App.OurHits.chooseProductBoxSearch()'});
        $('body').append(modalTemplate);
        
        $('#ChooseCategoryProductBoxQuery').keypress(function (e) {
          if (e.which == 13) {
            App.OurHits.chooseProductBoxSearch();
          }
        });
        
        App.CategoriesTree.onSelect = {context: this, func: function(selectedId, selectedData){
          this.chooseProductBoxSearch();
        }};
        App.CategoriesTree.loadMainTree();
      } else {
        App.CategoriesTree.loadExpanded();
      }
      
      $('#ChooseCategoryProductBox .modal-body').css({maxHeight: $(window).height() - 200, overflow:'auto'});
      $('#ChooseCategoryProductBox').modal('show');
    },
    
    chooseProductBoxSearch: function() {
      var selected = App.CategoriesTree.getSelectedData();
      this.chooseProductBoxLoadProducts(selected && selected.length > 0 ? selected[0].id : 0, $('#ChooseCategoryProductBoxQuery').val());
    },
    
    chooseProductBoxLoadProducts: function(category_id, query) {
      $.ajax({
        dataType: "json",
        evalScripts: true,
        url: this.getProductsTableUrl,
        data: ({category_id: category_id, query: query}),
        context: this,
        success: function (data, textStatus) {
          $('#ChooseCategoryProductBox .products-content').html(data.result.html);
        },
        error: function (data, textStatus) {
          alert('Error occured during getting products list. Please try again.');
        }
      });
    },
    
    saveProductBox: function() {
      var product_id = $('#ChooseCategoryProductBox input[name=ChooseCategoryProductBoxRadio]:checked').val();
      if (!$.isNumeric(product_id) || product_id <= 0) {
        alert(this.lang.noChoosedProduct);
        return;
      }
      
      $.ajax({
        dataType: "json",
        evalScripts: true,
        url: this.getProductDataUrl,
        data: ({product_id: product_id}),
        context: this,
        success: function (data, textStatus) {
          $('#ProductId').val(data.product.Product.id);
          $('#ProductName').val(data.product.Product.name);
          $('#ProductOurHitStatus').prop('checked', parseInt(data.product.Product.our_hit_status) ? true : false);
          $('#ProductOurHitDate').val(data.product.Product.our_hit_date === '0000-00-00 00:00:00' ? '' : moment(data.product.Product.our_hit_date).format('YYYY-MM-DD HH:mm'));
          $('#ProductOurHitDateEnd').val(data.product.Product.our_hit_date_end === '0000-00-00 00:00:00' ? '' : moment(data.product.Product.our_hit_date_end).format('YYYY-MM-DD HH:mm'));
          
          $('#ChooseCategoryProductBox').modal('hide');
        },
        error: function (data, textStatus) {
          alert('Error occured during getting choosed product data. Please try again.');
        }
      });
    },

    deleteOurHit: function (name, deleteUrl) {
      var messageTemplate = _.template(this.lang.confirmDeleteMessage);
      App.Modal.confirmDelete(messageTemplate({name:name}), 'window.location.href=\'' + deleteUrl + '\'');
    },
    
    saveOurHitStatus: function(product_id, value) {
      $.ajax({
        dataType: "json",
        evalScripts: true,
        url: this.saveOurHitStatusUrl,
        data: ({product_id: product_id, value: parseInt(value)}),
        context: this,
        success: function (data, textStatus) {
          if (!data.result.success) {
            alert(this.lang.saveOurHitStatusErrorMsg);
          }
        },
        error: function (data, textStatus) {
          alert('Error occured during saving our hit status. Please try again.');
        }
      });
    },
    
    saveProductStatus: function(product_id, value) {
      $.ajax({
        dataType: "json",
        evalScripts: true,
        url: this.saveProductStatusUrl,
        data: ({product_id: product_id, value: parseInt(value)}),
        context: this,
        success: function (data, textStatus) {
          if (!data.result.success) {
            alert(this.lang.saveProductStatusErrorMsg);
          }
        },
        error: function (data, textStatus) {
          alert(this.lang.saveProductStatusErrorMsg);
        }
      });
    },
    
    runBulkAction: function() {
      var action = $('#bulk_action').val();
      var contx = App.Modal;
      var func = null;
      var after = null;
      var args = {};
      var submit_script = "$('#OurHitAdminIndexForm').submit();";
      
      switch (action) {
        case 'delete_our_hits':
          func = App.Modal.confirmDelete;
          args = [this.lang.confirmDeleteOurHits, submit_script];
          break;
        case 'deactivate_our_hits':
          func = App.Modal.confirmYesCancel;
          args = [this.lang.confirmActionTitle, this.lang.confirmDeactivateOurHits, submit_script];
          break;
        case 'activate_our_hits':
          func = App.Modal.confirmYesCancel;
          args = [this.lang.confirmActionTitle, this.lang.confirmActivateOurHits, submit_script];
          break;
        case 'delete_products':
          func = App.Modal.confirmDelete;
          args = [this.lang.confirmDeleteProducts, submit_script];
          break;
        case 'deactivate_products':
          func = App.Modal.confirmYesCancel;
          args = [this.lang.confirmActionTitle, this.lang.confirmDeactivateProducts, submit_script];
          break;
        case 'activate_products':
          func = App.Modal.confirmYesCancel;
          args = [this.lang.confirmActionTitle, this.lang.confirmActivateProducts, submit_script];
          break;
        case 'clear_our_hits_date':
          func = App.Modal.confirmYesCancel;
          args = [this.lang.confirmActionTitle, this.lang.confirmClearOurHitsDate, submit_script];
          break;
        case 'clear_our_hits_date_end':
          func = App.Modal.confirmYesCancel;
          args = [this.lang.confirmActionTitle, this.lang.confirmClearOurHitsDateEnd, submit_script];
          break;
        case 'manipulate_our_hits_date':
          func = App.Modal.confirmOkCancel;
          var msg = '<form class="form-inline"><p>'
            + this.lang.confirmManipulateOurHitsDate
            + '<div class="form-group"><div class="input"><input id="manipulate_our_hits_date_input" placeholder="'
            + this.lang.manipulateDateInputPlaceholder
            + '" class="form-control" step="1" type="number"></div></div></form>';
          args = [this.lang.confirmActionTitle, msg, "App.OurHits.bulkActionCheckManipulateDate($('#manipulate_our_hits_date_input'))"];
          after = function(){
            $('#manipulate_our_hits_date_input').keydown(function(event){
              if (event.which == 13) {
                event.preventDefault();
                event.stopPropagation();
                App.OurHits.bulkActionCheckManipulateDate(this);
                return false;
              }
            }).focus();
          };
          break;
        case 'manipulate_our_hits_date_end':
          func = App.Modal.confirmOkCancel;
          var msg = '<form class="form-inline"><p>'
            + this.lang.confirmManipulateOurHitsDateEnd
            + '<div class="form-group"><div class="input"><input id="manipulate_our_hits_date_end_input" placeholder="'
            + this.lang.manipulateDateInputPlaceholder
            + '" class="form-control" step="1" type="number"></div></div></form>';
          args = [this.lang.confirmActionTitle, msg, "App.OurHits.bulkActionCheckManipulateDate($('#manipulate_our_hits_date_end_input'))"];
          after = function(){
            $('#manipulate_our_hits_date_end_input').keydown(function(event){
              if (event.which == 13) {
                event.preventDefault();
                event.stopPropagation();
                App.OurHits.bulkActionCheckManipulateDate(this);
                return false;
              }
            }).focus();
          };
          break;
      }
      
      if (typeof func === 'function') {
        var modal = func.apply(contx, args);
        if (typeof after === 'function') {
          modal.on('shown.bs.modal', function (e) {
            after.call();
          });
        }
      }
    },
    
    bulkActionCheckManipulateDate: function(input) {
      var value = $(input).val();
      var pattern = /^([-+]){0,1}([0-9]+)$/
      if (pattern.test(value) && (parseInt(value) > 0 || parseInt(value) < 0)) {
        $(input).closest('.form-group').removeClass('has-error');
        $('#OurHitAdminIndexForm').append($('<input>').attr({name:'manipulate_date_value',value:value}));
        $('#OurHitAdminIndexForm').submit();
      } else {
        $(input).closest('.form-group').addClass('has-error');
      }
    }
  };
})(App, jQuery);

$(document).ready(function () {
  // --
});