var App = App || {};

(function (App, $) {
  /**
   * 
   */
  App.Customers = {
    lang: {
      confirmDeleteMessage: 'Czy na pewno chcesz usunąć klienta "<%- name %>"?',
      saveStatusErrorMsg: 'Wystąpił błąd podczas zapisu statusu.',
      loadZonesErrorMsg: 'Wystąpił błąd podczas pobierania województw.',
      confirmDeleteTitle: 'Potwierdź usunięcie',
      confirmDeleteAddressMsg: 'Czy na pewno chcesz usunąć adres "<%- alias %>"?',
      delete: 'Usuń',
      cancel: 'Anuluj',
      defaultBillingAddressLabel: 'Domyślny adres rozliczeniowy',
      defaultShippingAddressLabel: 'Domyślny adres dostawy',
    },
    
    saveStatusUrl: null,
    getZonesUrl: null,
    getUserAddressRowUrl: null,
    
    /**
     * 
     */
    onDocReady: function () {
      // --
    },

    deleteCustomer: function(name, deleteUrl) {
      var messageTemplate = _.template(this.lang.confirmDeleteMessage);
      App.Modal.confirmDelete(messageTemplate({name:name}), 'window.location.href=\'' + deleteUrl + '\'');
    },
    
    saveStatus: function(customer_id, value) {
      $.ajax({
        dataType: 'json',
        evalScripts: true,
        url: this.saveStatusUrl,
        data: ({customer_id: customer_id, value: parseInt(value)}),
        context: this,
        success: function (data, textStatus) {
          if (!data.result.success) {
            alert(this.lang.saveStatusErrorMsg);
          }
        },
        error: function (data, textStatus) {
          alert(this.lang.saveStatusErrorMsg);
        }
      });
    },
    
    changeCountry: function(country_id) {
      $.ajax({
        dataType: 'json',
        evalScripts: true,
        url: this.getZonesUrl,
        data: ({country_id: country_id}),
        context: this,
        success: function (data, textStatus) {
          if (!data.result.success) {
            alert(this.lang.loadZonesErrorMsg);
          } else {
            var options = [$('#UserAddress0ZoneId option').first()];
            $(data.result.zones).each(function(){
              //console.log(this);
              options.push($('<option>').attr({value:this.id}).text(this.name));
            });
            $('#UserAddress0ZoneId option').remove();
            $('#UserAddress0ZoneId').append(options);
          }
        },
        error: function (data, textStatus) {
          alert(this.lang.loadZonesErrorMsg);
        }
      });
    },
    
    editAddress: function(node) {
      if ($(node).find('.item-content').is(':visible')) {
        this.saveAddress(node);
      } else {
        $(node).find('.item-content').slideDown();
      }
    },
    
    saveAddress: function(node) {
      var defaultBilling = $(node).find('input[type=checkbox][name^="data[UserAddress]"][name$="[default_billing]"]').first().prop('checked');
      var defaultShipping = $(node).find('input[type=checkbox][name^="data[UserAddress]"][name$="[default_shipping]"]').first().prop('checked');
      
      var toUpdates = [];
      if (defaultBilling) {
        $('#tab-addresses .address-item').each(function(){
          $(this).find('input[type=checkbox][name^="data[UserAddress]"][name$="[default_billing]"]').first().prop('checked', false);
          toUpdates.push(this);
        });
        $(node).find('input[type=checkbox][name^="data[UserAddress]"][name$="[default_billing]"]').first().prop('checked', true);
      }
      if (defaultShipping) {
        $('#tab-addresses .address-item').each(function(){
          $(this).find('input[type=checkbox][name^="data[UserAddress]"][name$="[default_shipping]"]').first().prop('checked', false);
          toUpdates.push(this);
        });
        $(node).find('input[type=checkbox][name^="data[UserAddress]"][name$="[default_shipping]"]').first().prop('checked', true);
      }
      $(toUpdates).each(function(){
        App.Customers.updateAddressTable(this);
      });
      
      App.Customers.updateAddressTable(node);
      $(node).find('.item-content').slideUp();
      $('html, body').animate({scrollTop: 0}, 'slow');
    },
    
    updateAddressTable: function(node) {
      var alias = $(node).find('input[name^="data[UserAddress]"][name$="[alias]"]').first().val();
      var defaultBilling = $(node).find('input[type=checkbox][name^="data[UserAddress]"][name$="[default_billing]"]').first().prop('checked');
      var defaultShipping = $(node).find('input[type=checkbox][name^="data[UserAddress]"][name$="[default_shipping]"]').first().prop('checked');
      
      var defaults = [];
      if (defaultBilling) {
        defaults.push(this.lang.defaultBillingAddressLabel);
      }
      if (defaultShipping) {
        defaults.push(this.lang.defaultShippingAddressLabel);
      }
      
      $(node).find('.address-alias').text(alias);
      $(node).find('.address-defaults').html(defaults.join('<br>'));
    },
    
    removeAddress: function(node) {
      var messageTemplate = _.template(this.lang.confirmDeleteAddressMsg);
      var alias = $(node).find('input[name^="data[UserAddress]"][name$="[alias]"]').first().val();
      
      bootbox.confirm({
        title: this.lang.confirmDeleteTitle,
        message: messageTemplate({alias:alias}),
        buttons: {
          cancel: {
            label: this.lang.cancel,
            className: 'btn-default'
          },
          confirm: {
            label: this.lang.delete,
            className: 'btn-primary'
          }
        },
        callback: function(result){
          if (result) {
            $(node).remove();
            if ($('#tab-addresses .address-item').length == 0) {
              $('.no-addresses').show();
            }
          }
        }
      });
    },
    
    addAddress: function() {
      var key = 0;
      $('input[name^="data[UserAddress]"][name$="[id]"]').each(function(){
        var keyItem = parseInt(String($(this).attr('name')).match(/([0-9]+)/g)[0]);
        if (keyItem >= key) {
          key = keyItem + 1;
        }
      });
      
      $.ajax({
        dataType: 'json',
        evalScripts: true,
        url: this.getUserAddressRowUrl,
        data: ({key: key}),
        context: this,
        success: function (data, textStatus) {
          $('#tab-addresses .no-addresses, #tab-addresses .address-item').last().after(data.html);
          $('.no-addresses').hide();
          $('#tab-addresses .address-item').last().find('.item-content').slideDown(function(){
            $(this).find('input[name^="data[UserAddress]"][name$="[alias]"]').focus();
          });
        },
        error: function (data, textStatus) {
          alert('Error occured during getting new address form. Please try again.');
        }
      });
    }
    
  };
})(App, jQuery);

$(document).ready(function () {
  // --
});