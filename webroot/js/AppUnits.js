var App = App || {};

(function (App, $) {
  /**
   * 
   */
  App.Units = {
    lang: {
      confirmDeleteMessage: 'Czy na pewno chcesz usunąć jednostkę miary "<%- name %>"?',
      saveSortOrderErrorMsg: 'Wystąpił błąd podczas zapisu kolejności sortowania.',
      sortOrderInvalidValue: 'Wpisana wartość jest nieprawidłowa'
    },
    
    saveSortOrderUrl: null,
    
    /**
     * 
     */
    onDocReady: function () {
      // --
    },

    deleteUnit: function(name, deleteUrl) {
      var messageTemplate = _.template(this.lang.confirmDeleteMessage);
      App.Modal.confirmDelete(messageTemplate({name:name}), 'window.location.href=\'' + deleteUrl + '\'');
    },
    
    saveSortOrder: function(unit_id, value) {
      if (value.match(/^([0-9]{1,}$)/) && parseInt(value) >= 0) {
        $.ajax({
          dataType: "json",
          evalScripts: true,
          url: this.saveSortOrderUrl,
          data: ({unit_id: unit_id, value: parseInt(value)}),
          context: this,
          success: function (data, textStatus) {
            if (!data.result.success) {
              alert(this.lang.saveSortOrderErrorMsg);
            }
          },
          error: function (data, textStatus) {
            alert(this.lang.saveSortOrderErrorMsg);
          }
        });
      } else {
        alert(this.lang.sortOrderInvalidValue);
      }
    },
    
  };
})(App, jQuery);

$(document).ready(function () {
  // --
});