var App = App || {};

(function (App, $) {
  /**
   * 
   */
  App.Invoices = {
    lang: {
      confirmDeleteInvoiceMessage: 'Czy na pewno chcesz usunąć fakturę VAT "<%- number %>"?',
      confirmDeleteQuoteMessage: 'Czy na pewno chcesz usunąć fakturę Proforma "<%- number %>"?',
      noChoosedCustomer: 'Wybierz klienta',
      noChoosedProduct: 'Wybierz produkt do promocji',
      chooseAddress: 'Wybierz adres',
      chooseShipping: 'Wybierz formę dostawy',
      choosePayment: 'Wybierz formę płatności',
      currency: 'zł',
      noChoosedProductsProductsOption: 'Wybierz cechy produktu'
    },
    getCustomersTableUrl: null,
    getCustomerInfoUrl: null,
    getProductsTableUrl: null,
    getProductRowUrl: null,
    getProductPreviewUrl: null,
    productEditUrl: null,
    getOrderUserAddressDataUrl: null,
    getOrderUserAddressesListUrl: null,
    addUserAddressesUrl: null,
    getShippingsListUrl: null,
    getPaymentsListUrl: null,
    getProductsProductsOptionFormUrl: null,

    addressCache: [],

    /**
     * 
     */
    onDocReady: function () {
      // --
    },
    
    deleteInvoice: function(number, deleteUrl) {
      var messageTemplate = _.template(this.lang.confirmDeleteInvoiceMessage);
      App.Modal.confirmDelete(messageTemplate({number:number}), 'window.location.href=\'' + deleteUrl + '\'');
    },
    
    openChooseCustomerBox: function () {
      $('#ChooseCustomerBox').remove();

      var fn = _.template($('#modal-choose-customer-template').html());
      var modalTemplate = fn({saveScript: 'App.Invoices.saveChooseCustomerBox()', searchScript: 'App.Invoices.chooseCustomerBoxSearch()'});
      $('body').append(modalTemplate);

      $('#ChooseCustomerBoxQuery').keypress(function (e) {
        if (e.which == 13) {
          App.Invoices.chooseCustomerBoxSearch();
        }
      });

      $('#ChooseCustomerBox .modal-body').css({maxHeight: $(window).height() - 200, overflow: 'auto'});
      $('#ChooseCustomerBox').modal('show');
    },
    chooseCustomerBoxSearch: function () {
      this.chooseCustomerBoxLoadCustomers($('#ChooseCustomerBoxQuery').val());
    },
    chooseCustomerBoxLoadCustomers: function (query) {
      $.ajax({
        dataType: "json",
        evalScripts: true,
        url: this.getCustomersTableUrl,
        data: ({query: query}),
        context: this,
        success: function (data, textStatus) {
          $('#ChooseCustomerBox .customers-content').html(data.result.html);
        },
        error: function (data, textStatus) {
          alert('Error occured during getting customers list. Please try again.');
        }
      });
    },
    saveChooseCustomerBox: function () {
      var customer_id = $('#ChooseCustomerBox input[name=ChooseCustomerBoxRadio]:checked').val();
      if (!$.isNumeric(customer_id) || customer_id <= 0) {
        alert(this.lang.noChoosedCustomer);
        return;
      }

      $.ajax({
        dataType: "json",
        evalScripts: true,
        url: this.getCustomerInfoUrl,
        data: ({customer_id: customer_id}),
        context: this,
        success: function (data, textStatus) {
          $('#OrderUserId').val(data.user.Customer.id);
          $('#OrderChooseCustomer').val(data.user.Customer.first_name + ' ' + data.user.Customer.last_name);
          $('#customer_info').html(data.html);
          $('#ChooseCustomerBox').modal('hide');
          
          this.reloadAddressesList();
          $('#billing_address_no_user, #shipping_address_no_user').slideUp();
          $('.address-options').slideDown();
        },
        error: function (data, textStatus) {
          alert('Error occured during getting choosed product data. Please try again.');
        }
      });
    },
    changeProductPrice: function(row, change) {
      var inputPrice = row.find('input[name^="data[OrderProduct]"][name$="[price]"]');
      var inputTax = row.find('input[name^="data[OrderProduct]"][name$="[tax]"]');
      var inputPriceTax = row.find('input[name^="data[OrderProduct]"][name$="[price_tax]"]');
      var inputTotalPriceTax = row.find('input[name^="data[OrderProduct]"][name$="[total_price_tax]"]');
      var inputAmount = row.find('input[name^="data[OrderProduct]"][name$="[amount]"]');
      var inputDiscount = row.find('input[name^="data[OrderProduct]"][name$="[discount]"]');
      
      var price = parseFloat(inputPrice.val().replace(',', '.'));
      var taxRate = parseFloat(String(row.find('select[name^="data[OrderProduct]"][name$="[tax_rate_id]"] option:selected').data('rate')).replace(',', '.'));
      var tax = parseFloat(inputTax.val().replace(',', '.'));
      var priceTax = parseFloat(inputPriceTax.val().replace(',', '.'));
      var totalPriceTax = parseFloat(inputTotalPriceTax.val().replace(',', '.'));
      var amount = parseFloat(inputAmount.val().replace(',', '.'));
      var discount = parseFloat(inputDiscount.val().replace(',', '.'));
      
      
      if (change == 'price' || change == 'tax_rate') {
        priceTax = parseFloat(Number(price * ((100 + taxRate) / 100)).toFixed(2));
        tax = priceTax - price;
        totalPriceTax = priceTax * amount;
      } else if (change == 'price_tax') {
        price = parseFloat(Number(priceTax / ((100 + taxRate) / 100)).toFixed(2));
        tax = priceTax - price;
        totalPriceTax = priceTax * amount;
      }
      if (discount > 0){
        totalPriceTax -= totalPriceTax * (discount/100);
      }
      
      inputPrice.val(Number(price).toFixed(2));
      inputTax.val(Number(tax).toFixed(2));
      inputPriceTax.val(Number(priceTax).toFixed(2));
      inputTotalPriceTax.val(Number(totalPriceTax).toFixed(2));
      
      this.updateProductsTotalPrice();
      this.updateOrderPrice();
      this.reloadShippingsList();
    },
    changeProductAmount: function(row) {
      var inputPriceTax = row.find('input[name^="data[OrderProduct]"][name$="[price_tax]"]');
      var inputAmount = row.find('input[name^="data[OrderProduct]"][name$="[amount]"]');
      var inputTotalPriceTax = row.find('input[name^="data[OrderProduct]"][name$="[total_price_tax]"]');
      
      var priceTax = parseFloat(inputPriceTax.val().replace(',', '.'));
      var totalPriceTax = parseFloat(inputTotalPriceTax.val().replace(',', '.'));
      var amount = parseFloat(inputAmount.val().replace(',', '.'));
      
      totalPriceTax = priceTax * amount;
      
      inputTotalPriceTax.val(Number(totalPriceTax).toFixed(2));
      
      this.updateProductsTotalPrice();
      this.updateOrderPrice();
      this.reloadShippingsList();
    },
    deleteProduct: function(row) {
      $(row).remove();
      if ($('.products-table tbody tr:not(.no-products)').length == 0) {
        $('.products-table tbody tr.no-products').show();
      }
      this.updateProductsTotalPrice();
      this.updateOrderPrice();
      this.reloadShippingsList();
    },
    
    openChooseProductBox: function() {
      $('#ChooseCategoryProductBox').remove();
      var fn = _.template($('#modal-choose-category-product-template').html());
      var modalTemplate = fn({saveScript: 'App.Invoices.saveProductBox()', searchScript: 'App.Invoices.chooseProductBoxSearch()'});
      $('body').append(modalTemplate);

      $('#ChooseCategoryProductBoxQuery').keypress(function (e) {
        if (e.which == 13) {
          App.Invoices.chooseProductBoxSearch();
        }
      });

      App.CategoriesTree.onSelect = {context: this, func: function(selectedId, selectedData){
        this.chooseProductBoxSearch();
      }};
      App.CategoriesTree.loadMainTree();
      
      $('#ChooseCategoryProductBox .modal-body').css({maxHeight: $(window).height() - 200, overflow:'auto'});
      $('#ChooseCategoryProductBox').modal('show');
    },
    
    chooseProductBoxSearch: function() {
      var selected = App.CategoriesTree.getSelectedData();
      this.chooseProductBoxLoadProducts(selected && selected.length > 0 ? selected[0].id : 0, $('#ChooseCategoryProductBoxQuery').val());
    },
    
    chooseProductBoxLoadProducts: function(category_id, query) {
      $.ajax({
        dataType: "json",
        evalScripts: true,
        url: this.getProductsTableUrl,
        data: ({category_id: category_id, query: query}),
        context: this,
        success: function (data, textStatus) {
          $('#ChooseCategoryProductBox .products-content').html(data.result.html);
        },
        error: function (data, textStatus) {
          alert('Error occured during getting products list. Please try again.');
        }
      });
    },
    
    saveProductBox: function() {
      var product_id = 7158;//$('#ChooseCategoryProductBox input[name=ChooseCategoryProductBoxRadio]:checked').val();
      if (!$.isNumeric(product_id) || product_id <= 0) {
        alert(this.lang.noChoosedProduct);
        return;
      }
      
      $.ajax({
        dataType: "json",
        evalScripts: true,
        url: this.getProductsProductsOptionFormUrl,
        data: ({product_id: product_id}),
        context: this,
        success: function (data, textStatus) {
          if (data.result.optionChoose) {
            $('#ChooseProductsProductsOptionBox').remove();
            var fn = _.template($('#modal-choose-products-products-option-template').html());
            var modalTemplate = fn({productId: product_id, saveScript: 'App.Invoices.saveProductsProductsOptionBox()', optionsContent: data.result.html});
            $('body').append(modalTemplate);
            $('#ChooseCategoryProductBox').modal('hide');
            $('#ChooseProductsProductsOptionBox').modal('show');
          } else {
            this.addProductRow(product_id, 0);
          }
        },
        error: function (data, textStatus) {
          alert('Error occured during getting choosed product options data. Please try again.');
        }
      });
    },
    
    saveProductsProductsOptionBox: function() {
      var product_id = $('#ChooseProductsProductsOptionBoxProductId').val();
      var products_products_option_id = $('#ChooseProductsProductsOptionBox input[name=ChooseProductsProductsOptionBoxRadio]:checked').val();
      if (!$.isNumeric(products_products_option_id) || products_products_option_id <= 0) {
        alert(this.lang.noChoosedProductsProductsOption);
        return;
      }
      
      this.addProductRow(product_id, products_products_option_id);
    },
    
    addProductRow: function(product_id, products_products_option_id) {
      var key = 0;
      $('input[name^="data[OrderProduct]"][name$="[product_id]"]').each(function(){
        var keyItem = parseInt(String($(this).attr('name')).match(/([0-9]+)/g)[0]);
        if (keyItem >= key) {
          key = keyItem + 1;
        }
      });
      
      $.ajax({
        dataType: "json",
        evalScripts: true,
        url: this.getProductRowUrl,
        data: ({key: key, product_id: product_id, products_products_option_id: products_products_option_id}),
        context: this,
        success: function (data, textStatus) {
          $('.products-table tbody').append(data.html);
          $('.products-table tbody tr.no-products').hide();
          $('#ChooseCategoryProductBox, #ChooseProductsProductsOptionBox').modal('hide');
          
          this.updateProductsTotalPrice();
          this.updateOrderPrice();
          this.reloadShippingsList();
        },
        error: function (data, textStatus) {
          alert('Error occured during getting choosed product data. Please try again.');
        }
      });
    },
    
    previewOrderProduct: function (order_product_id) {
      $.ajax({
        dataType: "json",
        evalScripts: true,
        url: this.getProductPreviewUrl,
        data: ({order_product_id: order_product_id}),
        context: this,
        success: function (data, textStatus) {
          bootbox.dialog({
            message: data.html,
            title: data.title,
            buttons: {
              product: {
                label: data.lang.btn_product_title,
                className: 'btn-default',
                callback: function() {
                  window.open(App.Invoices.productEditUrl + data.order_product.Product.id, '_blank');
                }
              },
              success: {
                label: data.lang.btn_ok_title,
                className: "btn-primary",
                callback: function () {
                  bootbox.hideAll();
                }
              }
            }
          });
        },
        error: function (data, textStatus) {
          alert('Error occured during getting product preview. Please try again.');
        }
      });
    },
    
    updateProductsTotalPrice: function() {
      $('#OrderProductsPriceTax').val(this.getProductsPriceTax());
      var price = Number(parseFloat($('#OrderProductsPriceTax').val())).toFixed(2);
      $('.products_price_tax_display').html(price + ' ' + this.lang.currency);
    },
    
    updateOrderPrice: function() {
      $('#OrderOrderPriceTax').val(this.getOrderPriceTax());
      var price = Number(parseFloat($('#OrderOrderPriceTax').val())).toFixed(2);
      $('.order_price_tax_display').html(price + ' ' + this.lang.currency);
    },
    
    changeAddress: function(select) {
      if (typeof this.addressCache[$(select).val()] === 'undefined') {
        this.loadAddress($(select).val(), function(){
          App.Invoices.changeAddress(select);
        });
      } else {
        $(select).closest('.address').find('.address-content').html(this.addressCache[$(select).val()].html);
        $(select).find('option[value="' + $(select).val() + '"]').html(this.addressCache[$(select).val()].orderUserAddress.alias);
        
        if ($(select).attr('id') == 'OrderShippingOrderAddressId') {
          $('#shipping_country_id').val(this.addressCache[$(select).val()].orderUserAddress.country_id);
          this.reloadShippingsList();
        }
        
      }
    },
    
    loadAddress: function(addressId, callback) {
      $.ajax({
        dataType: "json",
        evalScripts: true,
        url: this.getOrderUserAddressDataUrl,
        data: ({order_user_address_id: addressId, update: $('#OrderAddressUpdate').val().split(',')}),
        context: this,
        success: function (data, textStatus) {
          this.addressCache[addressId] = data;
          $('.address select option[value="' + addressId + '"]').text(this.addressCache[addressId].orderUserAddress.alias);
          callback.call(this);
        },
        error: function (data, textStatus) {
          alert('Error occured during getting address data. Please try again.');
        }
      });
    },
    
    updateAddress: function(elem) {
      var select = $(elem).closest('.address').find('select');
      delete this.addressCache[$(select).val()];
      
      if (select.val().substr(0, 2) !== '0-') {
        var orderAddressUpdateInput = $('#OrderAddressUpdate');
        orderAddressUpdate = orderAddressUpdateInput.val().split(',');
        if (orderAddressUpdate.indexOf($(select).val()) == -1) {
          orderAddressUpdate.push($(select).val());
          orderAddressUpdateInput.val(orderAddressUpdate.join(','));
        }
      }
      
      this.loadAddress($(select).val(), function(){
        $('.address select').each(function(){
          App.Invoices.changeAddress(this);
        });
      });
    },
    
    reloadAddressesList: function (callback) {
      $.ajax({
        dataType: "json",
        evalScripts: true,
        url: this.getOrderUserAddressesListUrl,
        data: ({user_id: $('#OrderUserId').val(), order_id: $('#OrderId').val(), update: $('#OrderAddressUpdate').val().split(',')}),
        context: this,
        success: function (data, textStatus) {
          var orderBillingOrderAddressId = $('#OrderBillingOrderAddressId').val();
          var orderShippingOrderAddressId = $('#OrderShippingOrderAddressId').val();

          var options = [$('<option>').attr({value: ''}).text(this.lang.chooseAddress)];
          for (var key in data.userAddresses) {
            if (data.userAddresses.hasOwnProperty(key)) {
              options.push($('<option>').attr({value: key}).text(data.userAddresses[key]));
            }
          }
          
          $('#OrderBillingOrderAddressId').html('').append(options);
          $('#OrderBillingOrderAddressId option[value="' + orderBillingOrderAddressId + '"]').prop('selected', true);
          

          options = [$('<option>').attr({value: ''}).text(this.lang.chooseAddress)];
          for (var key in data.userAddresses) {
            if (data.userAddresses.hasOwnProperty(key)) {
              options.push($('<option>').attr({value: key}).text(data.userAddresses[key]));
            }
          }
          
          $('#OrderShippingOrderAddressId').html('').append(options);
          $('#OrderShippingOrderAddressId option[value="' + orderShippingOrderAddressId + '"]').prop('selected', true);
          
          $('.address select').each(function(){
            App.Invoices.changeAddress(this);
          });
          
          if (typeof callback == 'function') {
            callback.call(this);
          }
        },
        error: function (data, textStatus) {
          alert('Error occured during getting addresses list. Please try again.');
        }
      });
    },
    
    addAddress: function(elem) {
      var select = $(elem).closest('.address').find('select');
      var url = this.addUserAddressesUrl + '/' + $('#OrderUserId').val() + '?action=add_address&from=order&param=' + $(select).attr('id');
      window.open(url, '_blank');
    },
    
    afterAddAddress: function(window, params) {
      window.close();
      var select = $('#' + params.param);
      
      var currentAddresses = $.map($(select).find('option'), function(e) { return e.value; });
      
      this.reloadAddressesList(function(){
        var newAddresses = $.map($(select).find('option'), function(e) { return e.value; });
        var diff = $(newAddresses).not(currentAddresses).get();
        console.log(diff);
        if (diff && diff.length > 0) {
          $(select).find('option:selected').prop('selected', false);
          console.log('option[value="' + diff[diff.length-1] + '"]');
          $(select).find('option[value="' + diff[diff.length-1] + '"]').prop('selected', true);
          this.changeAddress(select);
        }
      });
    },
    
    getProductsWeight: function() {
      var weight = 0;
      $('.products-table input[name^="data[OrderProduct]"][name$="[weight]"]').each(function(){
        weight += parseFloat($(this).val());
      });
      return weight;
    },
    
    getProductsPriceTax: function() {
      var price_tax = 0;
      $('.products-table input[name^="data[OrderProduct]"][name$="[total_price_tax]"]').each(function(){
        price_tax += parseFloat($(this).val());
      });
      return price_tax;
    },
    
    getProductsAmount: function() {
      var amount = 0;
      $('.products-table input[name^="data[OrderProduct]"][name$="[amount]"]').each(function(){
        amount += parseFloat($(this).val());
      });
      return amount;
    },
    
    getShippingPriceTax: function() {
      return parseFloat($('#OrderShippingId option:selected').data('price_tax'));
    },
    
    getOrderPriceTax: function() {
      var price_tax = this.getProductsPriceTax() + this.getShippingPriceTax();
      return price_tax;
    },
    
    reloadShippingsList: function () {
      return false;
      $.ajax({
        dataType: "json",
        evalScripts: true,
        url: this.getShippingsListUrl,
        data: ({
          weight: this.getProductsWeight(),
          products_price_tax: this.getProductsPriceTax(),
          products_amount: this.getProductsAmount(),
          country_id: $('#shipping_country_id').val()
        }),
        context: this,
        success: function (data, textStatus) {
          var shippingId = $('#OrderShippingId').val();

          var options = [$('<option>').attr({value: '', 'data-price_tax': 0}).text(this.lang.chooseShipping)];
          for (var i = 0; i < data.shippingsFull.length; i++) {
            options.push($('<option>').attr({value: data.shippingsFull[i].Shipping.id, 'data-price_tax': data.shippingsFull[i].Shipping.for_order_price_tax}).text(data.shippingsFull[i].Shipping.name + ' - ' + data.shippingsFull[i].Shipping.for_order_price_tax + ' ' + this.lang.currency));
          }
          
          $('#OrderShippingId').html('').append(options);
          $('#OrderShippingId option[value="' + shippingId + '"]').prop('selected', true);
          
          if ($('#shipping_country_id').val() == '' || $('#shipping_country_id').val() == 0) {
            $('#shipping_no_country').slideDown();
          } else {
            $('#shipping_no_country').slideUp();
          }
          
          this.changeShipping();
        },
        error: function (data, textStatus) {
          alert('Error occured during getting shippings list. Please try again.');
        }
      });
    },
    
    changeShipping: function() {
      this.updateShippingPrice();
      this.updateOrderPrice();
      this.reloadPaymentsList();
    },
    
    updateShippingPrice: function() {
      $('#OrderShippingPriceTax').val(this.getShippingPriceTax());
      var price = Number(parseFloat($('#OrderShippingPriceTax').val())).toFixed(2);
      $('.shipping_price_tax_display').html(price + ' ' + this.lang.currency);
    },
    
    reloadPaymentsList: function () {
      $.ajax({
        dataType: "json",
        evalScripts: true,
        url: this.getPaymentsListUrl,
        data: ({
          shipping_id: $('#OrderShippingId').val()
        }),
        context: this,
        success: function (data, textStatus) {
          var paymentId = $('#OrderPaymentId').val();

          var options = [$('<option>').attr({value: ''}).text(this.lang.choosePayment)];
          for (var key in data.payments) {
            if (data.payments.hasOwnProperty(key)) {
              options.push($('<option>').attr({value: key}).text(data.payments[key]));
            }
          }
          
          $('#OrderPaymentId').html('').append(options);
          $('#OrderPaymentId option[value="' + paymentId + '"]').prop('selected', true);
          
          if ($('#OrderShippingId').val() == '' || $('#OrderShippingId').val() == 0) {
            $('#payment_no_shipping').slideDown();
          } else {
            $('#payment_no_shipping').slideUp();
          }
          
          this.changePayment();
        },
        error: function (data, textStatus) {
          alert('Error occured during getting payments list. Please try again.');
        }
      });
    },
    
    changePayment: function() {
      //nothing
    },
    
    showOrderStatusHistory: function() {
      $('#orderStatusHistoryShow').slideUp();
      $('#orderStatusHistoryHide, #orderStatusHistoryContent').slideDown();
    },
    
    hideOrderStatusHistory: function() {
      $('#orderStatusHistoryHide, #orderStatusHistoryContent').slideUp();
      $('#orderStatusHistoryShow').slideDown();
    },
    
    openOrdersReportBox: function () {
      $('#ChooseCustomerBox').remove();

      if ($('#OrdersReport').length <= 0) {
        var fn = _.template($('#modal-orders-report-template').html());
        var modalTemplate = fn({saveScript: 'App.Invoices.saveOrdersReportBox()'});
        $('body').append(modalTemplate);
        
        $('#OrdersReportDateFrom, #OrdersReportDateTo').datetimepicker({
          icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
          },
          locale: 'pl',
          format: 'YYYY-MM-DD'
        });
      }

      $('#OrdersReport').modal('show');
    },
    
    saveOrdersReportBox: function() {
      window.location.href = this.generateOrdersReportUrl + '?from=' + $('#orders_report_date_from').val() + '&to=' + $('#orders_report_date_to').val();
    },
    
    
    
    openReportBox: function () {

      if ($('#InvoicesReport').length <= 0) {
        var fn = _.template($('#modal-invoices-report-template').html());
        var modalTemplate = fn();
        $('body').append(modalTemplate);
        
        $('#InvoiceReportDateFrom, #InvoiceReportDateTo').datepicker({format: 'yyyy-mm-dd'});
      }

      $('#InvoicesReport').modal('show');
    },
  };
})(App, jQuery);

$(document).ready(function () {
  // --
  
  //for closing bootbox windows after backdrop click
  /*$(document).on('click', '.bootbox', function (event) {
    var classname = event.target.className;
    if (classname && !$('.' + classname).parents('.modal-dialog').length)
      bootbox.hideAll();
  });*/
});

