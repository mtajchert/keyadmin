var App = App || {};

(function (App, $) {
  /**
   * 
   */
  App.ProductOptions = {
    lang: {
      confirmDeleteMessage: 'Czy na pewno chcesz usunąć cechę produktów "<%- name %>" wraz z wartościami?',
      confirmValueDeleteMessage: 'Czy na pewno chcesz usunąć wartość "<%- name %>"?',
      saveSortOrderErrorMsg: 'Wystąpił błąd podczas zapisu kolejności sortowania.',
      sortOrderInvalidValue: 'Wpisana wartość jest nieprawidłowa'
    },
    
    saveSortOrderUrl: null,
    getValuesUrl: null,
    valuesCache: [],
    
    /**
     * 
     */
    onDocReady: function () {
      // --
    },

    deleteProductOption: function(name, deleteUrl) {
      var messageTemplate = _.template(this.lang.confirmDeleteMessage);
      App.Modal.confirmDelete(messageTemplate({name:name}), 'window.location.href=\'' + deleteUrl + '\'');
    },
    
    deleteProductOptionValue: function(name, deleteUrl) {
      var messageTemplate = _.template(this.lang.confirmValueDeleteMessage);
      App.Modal.confirmDelete(messageTemplate({name:name}), 'window.location.href=\'' + deleteUrl + '\'');
    },
    
    saveSortOrder: function(product_option_id, value) {
      if (value.match(/^([0-9]{1,}$)/) && parseInt(value) >= 0) {
        $.ajax({
          dataType: "json",
          evalScripts: true,
          url: this.saveSortOrderUrl,
          data: ({product_option_id: product_option_id, value: parseInt(value)}),
          context: this,
          success: function (data, textStatus) {
            if (!data.result.success) {
              alert(this.lang.saveSortOrderErrorMsg);
            }
          },
          error: function (data, textStatus) {
            alert(this.lang.saveSortOrderErrorMsg);
          }
        });
      } else {
        alert(this.lang.sortOrderInvalidValue);
      }
    },
    
    toggleValuesList: function(node, product_option_id) {
      if (typeof this.valuesCache[product_option_id] === 'undefined') {
        this.loadValuesList(product_option_id, function(){
          App.ProductOptions.toggleValuesList(node, product_option_id);
        });
        return;
      }
      
      if ($('#product_option_values_list_' + product_option_id).length > 0) {
        $('#product_option_values_list_' + product_option_id + ' .anim').slideUp(function(){
          $('#product_option_values_list_' + product_option_id).remove();
        });
      } else {
        var tr = $(node).closest('tr').after(this.valuesCache[product_option_id]);
        $('#product_option_values_list_' + product_option_id + ' .anim').slideDown();
      }
    },
    
    loadValuesList: function(product_option_id, callback) {
      $.ajax({
          dataType: "json",
          evalScripts: true,
          url: this.getValuesUrl,
          data: ({product_option_id: product_option_id}),
          context: this,
          success: function (data, textStatus) {
            if (!data.result.success) {
              alert('Error occured during getting values list. Please try again.');
            } else {
              this.valuesCache[product_option_id] = data.result.html;
              if (typeof callback == 'function') {
                callback.call(this);
              }
            }
          },
          error: function (data, textStatus) {
            alert('Error occured during getting values list. Please try again.');
          }
        });
    }
    
  };
})(App, jQuery);

$(document).ready(function () {
  // --
});