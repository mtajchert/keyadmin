var App = App || {};

(function (App, $) {
  /**
   * 
   */
  App.Modal = {
    confirmDeleteTemplate: _.template($('#modal-confirm-delete-template').html()),
    confirmYesNoCancelTemplate: _.template($('#modal-confirm-yes-no-cancel-template').html()),
    confirmYesCancelTemplate: _.template($('#modal-confirm-yes-cancel-template').html()),
    confirmOkCancelTemplate: _.template($('#modal-confirm-ok-cancel-template').html()),
    
    confirmDelete: function(message, confirmScript) {
      var modalTemplate = this.confirmDeleteTemplate({confirmMessage:message, confirmScript:confirmScript});
      if ($('#ConfirmDeleteBox')) {
        $('#ConfirmDeleteBox').remove();
      }
      $('body').append(modalTemplate);
      
      return $('#ConfirmDeleteBox').modal('show');
    },
    
    confirmYesNoCancel: function(title, message, confirmYesScript, confirmNoScript) {
      var modalTemplate = this.confirmYesNoCancelTemplate({title:title, confirmMessage:message, confirmYesScript:confirmYesScript, confirmNoScript:confirmNoScript});
      if ($('#ConfirmYesNoCancelBox')) {
        $('#ConfirmYesNoCancelBox').remove();
      }
      $('body').append(modalTemplate);
      
      return $('#ConfirmYesNoCancelBox').modal('show');
    },
    
    confirmYesCancel: function(title, message, confirmYesScript) {
      var modalTemplate = this.confirmYesCancelTemplate({title:title, confirmMessage:message, confirmYesScript:confirmYesScript});
      if ($('#ConfirmYesCancelBox')) {
        $('#ConfirmYesCancelBox').remove();
      }
      $('body').append(modalTemplate);
      
      return $('#ConfirmYesCancelBox').modal('show');
    },
    
    confirmOkCancel: function(title, message, confirmOkScript) {
      var modalTemplate = this.confirmOkCancelTemplate({title:title, confirmMessage:message, confirmOkScript:confirmOkScript});
      if ($('#ConfirmOkCancelBox')) {
        $('#ConfirmOkCancelBox').remove();
      }
      $('body').append(modalTemplate);
      
      return $('#ConfirmOkCancelBox').modal('show');
    },
    
  };
})(App, jQuery);

$(document).ready(function () {
  // --
});