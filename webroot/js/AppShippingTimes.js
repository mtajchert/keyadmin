var App = App || {};

(function (App, $) {
  /**
   * 
   */
  App.ShippingTimes = {
    lang: {
      confirmDeleteMessage: 'Czy na pewno chcesz usunąć termin wysyłki "<%- name %>"?'
    },
    
    /**
     * 
     */
    onDocReady: function () {
      // --
    },

    deleteShippingTime: function(name, deleteUrl) {
      var messageTemplate = _.template(this.lang.confirmDeleteMessage);
      App.Modal.confirmDelete(messageTemplate({name:name}), 'window.location.href=\'' + deleteUrl + '\'');
    }
    
  };
})(App, jQuery);

$(document).ready(function () {
  // --
});