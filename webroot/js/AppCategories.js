var App = App || {};

(function (App, $) {
  /**
   * 
   */
  App.Categories = {
    lang: {
      confirmDeleteMsg: 'Czy na pewno chcesz usunąć kategorię "<%- name %>"?',
      saveSortOrderErrorMsg: 'Wystąpił błąd podczas zapisu kolejności sortowania.',
      sortOrderInvalidValue: 'Wpisana wartość jest nieprawidłowa',
      saveStatusErrorMsg: 'Wystąpił błąd podczas zapisu statusu.',
      getCategoryPathErrorMsg: 'Wystąpił błąd podczas pobierania ścieżki kategorii',
      loadingCategoriesMsg: 'Pobieranie kategorii...',
      noChoosedProduct: 'Wybierz produkt do przekierowania banera',
    },
    saveSortOrderUrl: null,
    saveStatusUrl: null,
    getCategoryBreadcrumbPathUrl: null,
    getProductsTableUrl: null,
    getProductDataUrl: null,
    
    /**
     * 
     */
    onDocReady: function () {
      this.loadCategoryBreadcrumbPath();
      
      $('.category-image-add').on('click', function(e){
        e.preventDefault();
        App.Categories.getImageChooseModal($(e.currentTarget).data('id'));
        return false;
      });
      
      
      $('body').on('change', '.save-fast-category', function(e){
          e.preventDefault();
          $.ajax({
              type: "POST",
              url: '/admin/products/save_category',
              data: $(this).serialize(),
              success: function (data, textStatus) {
                
              },
              error: function (data, textStatus) {
                alert();
              }
            });
          return false;
      });
      
    },
    
    
    getImageChooseModal: function(categoryId){
      var template = _.template($('#category-image-modal-body').html());
      bootbox.dialog({
        message: template({categoryId: categoryId}),
        title: "Dodawanie zdjęcia",
        buttons: {
          success: {
            label: "<strong>ZATWIERDZAM</strong>",
            className: "btn-success",
            callback: function() {
              $('#categories-image-form').submit();
            }
          },
          danger: {
            label: "Zamknij",
            className: "btn-danger",
            callback: function() {
            }
          }
        }
      });
      
      $('.bootbox.modal').on('shown.bs.modal', function(){
        $('.image-choosen').fileinput({
          allowedFileExtensions: ['jpg'],
          uploadAsync: true,
          maxFileCount: 1,
          minFileCount: 1,
          language: 'pl',
          previewFileType: 'image',
          browseClass: 'btn btn-success',
          browseLabel: ' Wybierz',
          browseIcon: '<i class="icon-picture"></i>',
          removeClass: 'btn btn-danger',
          removeIcon: '<i class="icon-trash"></i>',
          removeLabel: ' Usuń'
        });
      });
    },
    
    
    
    saveSortOrder: function(category_id, value) {
      if (value.match(/^([0-9]{1,}$)/) && parseInt(value) >= 0) {
        $.ajax({
          dataType: "json",
          evalScripts: true,
          url: this.saveSortOrderUrl,
          data: ({category_id: category_id, value: parseInt(value)}),
          context: this,
          success: function (data, textStatus) {
            if (!data.result.success) {
              alert(this.lang.saveSortOrderErrorMsg);
            }
          },
          error: function (data, textStatus) {
            alert(this.lang.saveSortOrderErrorMsg);
          }
        });
      } else {
        alert(this.lang.sortOrderInvalidValue);
      }
    },
    
    saveStatus: function(category_id, value) {
      $.ajax({
        dataType: "json",
        evalScripts: true,
        url: this.saveStatusUrl,
        data: ({category_id: category_id, value: parseInt(value)}),
        context: this,
        success: function (data, textStatus) {
          if (!data.result.success) {
            alert(this.lang.saveStatusErrorMsg);
          }
        },
        error: function (data, textStatus) {
          alert(this.lang.saveStatusErrorMsg);
        }
      });
    },
    
    loadCategoryBreadcrumbPath: function() {
      if ($('#CategoryParentId').val() > 0) {
        var placeholder = $('#CategoryParentIdPath').attr('placeholder');
        $('#CategoryParentIdPath').attr('placeholder', this.lang.loadingCategoriesMsg);
        $.ajax({
          dataType: "json",
          evalScripts: true,
          url: this.getCategoryBreadcrumbPathUrl,
          data: ({category_id: $('#CategoryParentId').val()}),
          context: this,
          success: function (data, textStatus) {
            var path = [];
            var selected_nested_id = [];
            
            for (var i = 0; i < data.path.length; i++) {
              path.push(data.path[i].name);
              selected_nested_id.push(data.path[i].id);
            }
            $('#CategoryParentIdNid').val(selected_nested_id.join('_'));
            
            $('#CategoryParentIdPath').attr('placeholder', placeholder);
            $('#CategoryParentIdPath').val(' / ' + path.join(' / '));
          },
          error: function (data, textStatus) {
            alert(this.lang.getCategoryPathErrorMsg);
          }
        });
      } else {
        $('#CategoryParentIdPath').val('');
      }
    },
    
    openParentCategoryBox: function() {
      if ($('#ParentCategoryBox').length === 0) {
        var fn = _.template($('#modal-choose-category-template').html());
        var modalTemplate = fn({saveScript: 'App.Categories.saveParentCategoryBox()'});
        $('body').append(modalTemplate);
        
        App.CategoriesTree.selected = [$('#CategoryParentIdNid').val()];
        App.CategoriesTree.loadMainTree();
      } else {
        App.CategoriesTree.selected = [$('#CategoryParentIdNid').val()];
        App.CategoriesTree.loadExpanded();
      }
      
      $('#ParentCategoryBox .modal-body').css({maxHeight: $(window).height() - 200, overflow:'auto'});
      $('#ParentCategoryBox').modal('show');
    },
    
    saveParentCategoryBox: function() {
      var selected = App.CategoriesTree.getSelectedData();
      $('#CategoryParentId').val(selected && selected.length > 0 ? selected[0].id : 0);
      $('#CategoryParentIdNid').val(selected && selected.length > 0 ? selected[0].nid : '0');
      
      this.loadCategoryBreadcrumbPath();
      
      $('#ParentCategoryBox').modal('hide');
    },
    
    deleteCategory: function(name, deleteUrl) {
      var messageTemplate = _.template(this.lang.confirmDeleteMsg);
      App.Modal.confirmDelete(messageTemplate({name:name}), 'window.location.href=\'' + deleteUrl + '\'');
    },
    
    changeBannerRedirectType: function() {
      switch ($('input[name="data[Category][banner_redirect_type]"]:checked').val()) {
        case 'no':
          $('.banner-redirect-product, .banner-redirect-url').slideUp();
          break;
        case 'product':
          $('.banner-redirect-url').slideUp();
          $('.banner-redirect-product').slideDown();
          $('.banner-redirect-product button').focus();
          break;
        case 'url':
          $('.banner-redirect-url').slideDown();
          $('.banner-redirect-product').slideUp();
          $('.banner-redirect-url input[type=text]').focus();
          break;
      }
    },
    
    // -----------------------------------------------
    
    openChooseProductBox: function() {
      if ($('#ChooseCategoryProductBox').length === 0) {
        var fn = _.template($('#modal-choose-category-product-template').html());
        var modalTemplate = fn({saveScript: 'App.Categories.saveProductBox()', searchScript: 'App.Categories.chooseProductBoxSearch()'});
        $('body').append(modalTemplate);
        
        $('#ChooseCategoryProductBoxQuery').keypress(function (e) {
          if (e.which == 13) {
            App.Categories.chooseProductBoxSearch();
          }
        });
        
        //App.CategoriesTree.selected = [$('#PromotionProductIdCategoryNid').val()];
        App.CategoriesTree.onSelect = {context: this, func: function(selectedId, selectedData){
          this.chooseProductBoxSearch();
        }};
        App.CategoriesTree.loadMainTree();
      } else {
        //App.CategoriesTree.selected = [$('#PromotionProductIdCategoryNid').val()];
        App.CategoriesTree.loadExpanded();
      }
      
      $('#ChooseCategoryProductBox .modal-body').css({maxHeight: $(window).height() - 200, overflow:'auto'});
      $('#ChooseCategoryProductBox').modal('show');
    },
    
    chooseProductBoxSearch: function() {
      var selected = App.CategoriesTree.getSelectedData();
      this.chooseProductBoxLoadProducts(selected && selected.length > 0 ? selected[0].id : 0, $('#ChooseCategoryProductBoxQuery').val());
    },
    
    chooseProductBoxLoadProducts: function(category_id, query) {
      $.ajax({
        dataType: "json",
        evalScripts: true,
        url: this.getProductsTableUrl,
        data: ({category_id: category_id, query: query}),
        context: this,
        success: function (data, textStatus) {
          $('#ChooseCategoryProductBox .products-content').html(data.result.html);
        },
        error: function (data, textStatus) {
          alert('Error occured during getting products list. Please try again.');
        }
      });
    },
    
    saveProductBox: function() {
      var product_id = $('#ChooseCategoryProductBox input[name=ChooseCategoryProductBoxRadio]:checked').val();
      if (!$.isNumeric(product_id) || product_id <= 0) {
        alert(this.lang.noChoosedProduct);
        return;
      }
      
      $.ajax({
        dataType: "json",
        evalScripts: true,
        url: this.getProductDataUrl,
        data: ({product_id: product_id}),
        context: this,
        success: function (data, textStatus) {
          $('#CategoryBannerProductId').val(data.product.Product.id);
          $('#CategoryBannerProductName').val(data.product.Product.name);
          /*window.aa = data.product.Product;
          if (parseFloat(data.product.Product.old_price) > 0 || parseInt(data.product.Product.promotion_status)) {
            $('#ProductOldPrice').val(parseFloat(data.product.Product.old_price) > 0 ? data.product.Product.old_price : '');
            $('#ProductPriceTax').val(parseFloat(data.product.Product.price_tax) > 0 ? data.product.Product.price_tax : '');
          } else {
            $('#ProductOldPrice').val(data.product.Product.price_tax);
            $('#ProductPriceTax').val('');
          }
          $('#ProductPromotionStatus').prop('checked', parseInt(data.product.Product.promotion_status) ? true : false);
          $('#ProductPromotionDate').val(data.product.Product.promotion_date === '0000-00-00 00:00:00' ? '' : moment(data.product.Product.promotion_date).format('YYYY-MM-DD HH:mm'));
          $('#ProductPromotionDateEnd').val(data.product.Product.promotion_date_end === '0000-00-00 00:00:00' ? '' : moment(data.product.Product.promotion_date_end).format('YYYY-MM-DD HH:mm'));
          */
          $('#ChooseCategoryProductBox').modal('hide');
        },
        error: function (data, textStatus) {
          alert('Error occured during getting choosed product data. Please try again.');
        }
      });
    },
    
  };
})(App, jQuery);

$(document).ready(function () {
  App.Categories.onDocReady();
});