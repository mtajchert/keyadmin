var App = App || {};

(function (App, $) {
  /**
   * 
   */
  App.Carts = {
    lang: {
      confirmDeleteMessage: 'Czy na pewno chcesz usunąć koszyk "<%- id %>"?',
    },
    getProductPreviewUrl: null,
    productEditUrl: null,

    /**
     * 
     */
    onDocReady: function () {
      // --
    },
    
    deleteCart: function(id, deleteUrl) {
      var messageTemplate = _.template(this.lang.confirmDeleteMessage);
      App.Modal.confirmDelete(messageTemplate({id:id}), 'window.location.href=\'' + deleteUrl + '\'');
    },
    
    previewCartProduct: function (cart_product_id) {
      $.ajax({
        dataType: "json",
        evalScripts: true,
        url: this.getProductPreviewUrl,
        data: ({cart_product_id: cart_product_id}),
        context: this,
        success: function (data, textStatus) {
          bootbox.dialog({
            message: data.html,
            title: data.title,
            buttons: {
              product: {
                label: data.lang.btn_product_title,
                className: 'btn-default',
                callback: function() {
                  window.open(App.Carts.productEditUrl + '/' + data.cart_product.Product.id, '_blank');
                }
              },
              success: {
                label: data.lang.btn_ok_title,
                className: "btn-primary",
                callback: function () {
                  bootbox.hideAll();
                }
              }
            }
          });
        },
        error: function (data, textStatus) {
          alert('Error occured during getting product preview. Please try again.');
        }
      });
    }

  };
})(App, jQuery);

$(document).ready(function () {
  // --
  
  //for closing bootbox windows after backdrop click
  /*$(document).on('click', '.bootbox', function (event) {
    var classname = event.target.className;
    if (classname && !$('.' + classname).parents('.modal-dialog').length)
      bootbox.hideAll();
  });*/
});

