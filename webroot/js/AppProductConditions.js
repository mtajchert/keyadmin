var App = App || {};

(function (App, $) {
  /**
   * 
   */
  App.ProductConditions = {
    lang: {
      confirmDeleteMessage: 'Czy na pewno chcesz usunąć stan produktów "<%- name %>"?'
    },
    
    /**
     * 
     */
    onDocReady: function () {
      // --
    },

    deleteProductCondition: function(name, deleteUrl) {
      var messageTemplate = _.template(this.lang.confirmDeleteMessage);
      App.Modal.confirmDelete(messageTemplate({name:name}), 'window.location.href=\'' + deleteUrl + '\'');
    }
    
  };
})(App, jQuery);

$(document).ready(function () {
  // --
});