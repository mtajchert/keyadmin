var App = App || {};
(function(App, $) {  
  
  /**
   * Object for Messages actions
   */
  App.ImageUpload = {
    
    submitTrigger: false,
    /**
     * 
     */
    onDocReady: function(){
      if ($('#baner-image-chosen').length > 0){
        $('#baner-image-chosen').fileinput({
          allowedFileExtensions: ['jpg'],
          uploadAsync: true,
          maxFileCount: 1,
          minFileCount: 1,
          language: 'pl',
          previewFileType: 'image',
          browseClass: 'btn btn-success',
          browseLabel: ' Wybierz',
          browseIcon: '<i class="glyphicon glyphicon-picture"></i>',
          removeClass: 'btn btn-danger',
          uploadClass: false
        }
        );
      }
      
      if ($('#offer-id').length > 0)
      $.ajax({
        url: '/orders/get_images/'+$('#offer-id').val(),
        type: 'GET',
        dataType: 'json',
        success: function(response) {
          
          $(".order-image-chosen").fileinput(response);
          
          /*$(".order-image-chosen").on('fileuploaded', function(event, data, previewId, index) {
            var response = data.response;
            console.log(response);
          });*/
          
          $(".order-image-chosen").on("filepredelete", function(jqXHR) {
            var abort = true;
            if (confirm("Na pewno chcesz usunąć to zdjęcie?")) {
                abort = false;
            }
            return abort;
          }); 
          $('.submit-order').on('click', function(e){
            e.preventDefault();
            if ($('.kv-file-upload').length == 0){
              $('form').submit();
            }else{
              $(".order-image-chosen").unbind('fileuploaded');
              $(".order-image-chosen").on('fileuploaded', function(event, data, previewId, index) {
                if (App.ImageUpload.submitTrigger){
                  $('form').submit();
                }
              });
              App.ImageUpload.submitTrigger = true;
              $(".order-image-chosen").fileinput("upload");
            }
          });          
        }
      });
    },
  } 
  
  
})(App, jQuery);

$(document).ready(function() {
  App.ImageUpload.onDocReady();
});