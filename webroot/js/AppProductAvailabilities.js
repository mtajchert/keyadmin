var App = App || {};

(function (App, $) {
  /**
   * 
   */
  App.ProductAvailabilities = {
    lang: {
      confirmDeleteMessage: 'Czy na pewno chcesz usunąć dostępność produktów "<%- name %>"?'
    },
    
    /**
     * 
     */
    onDocReady: function () {
      // --
    },

    deleteProductAvailability: function(name, deleteUrl) {
      var messageTemplate = _.template(this.lang.confirmDeleteMessage);
      App.Modal.confirmDelete(messageTemplate({name:name}), 'window.location.href=\'' + deleteUrl + '\'');
    }
    
  };
})(App, jQuery);

$(document).ready(function () {
  // --
});