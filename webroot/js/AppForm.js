var App = App || {};

(function (App, $) {
  /**
   * 
   */
  App.Form = {
      
    onDocReady: function(){
      var url = document.location.toString();
      if (url.match('#')) {
          $('.list-group-item a[href=#'+url.split('#')[1]+']').click() ;
      } 
      $('.list-group-item a').click(function(e){        
        window.location.hash = $(e.currentTarget).attr('href').split('#')[1];
      });
    
      $('.image-choosen').fileinput({
        allowedFileExtensions: ['jpg', 'png'],
        uploadAsync: true,
        minFileCount: 0,
        language: 'pl',
        previewFileType: 'image',
        browseClass: 'btn btn-success',
        browseLabel: ' Wybierz',
        browseIcon: '<i class="icon-picture"></i>',
        removeClass: 'btn btn-danger',
        removeIcon: '<i class="icon-trash"></i>',
        removeLabel: ' Usuń'
      });
    },
    
    
    clearInput: function(btn) {
      var counter = 0;
      do {
        btn = $(btn).parent();
      } while (counter < 10 && $(btn).find('input[type=text]').length === 0);
      $(btn).find('input[type=text]').val('').attr('value', '');
    },
    
    toggleShow: function(check, selector_show, selector_hide, inside) {
      inside = inside || 'html';
      if (check) {
        $(inside).find(selector_hide).slideUp();
        //$(inside).find(selector_hide + ':not(' + selector_show + ')').slideUp();
        $(inside).find(selector_show).slideDown();
      }
    }
  };
})(App, jQuery);

$(document).ready(function () {
  App.Form.onDocReady();
});