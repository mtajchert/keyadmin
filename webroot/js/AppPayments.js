var App = App || {};

(function (App, $) {
  /**
   * 
   */
  App.Payments = {
    lang: {
      saveSortOrderErrorMsg: 'Wystąpił błąd podczas zapisu kolejności sortowania.',
      sortOrderInvalidValue: 'Wpisana wartość jest nieprawidłowa',
      saveStatusErrorMsg: 'Wystąpił błąd podczas zapisu statusu.'
    },
    saveSortOrderUrl: null,
    saveStatusUrl: null,
    /**
     * 
     */
    onDocReady: function () {
      // --
    },
    saveSortOrder: function (payment_id, value) {
      if (value.match(/^([0-9]{1,}$)/) && parseInt(value) >= 0) {
        $.ajax({
          dataType: "json",
          evalScripts: true,
          url: this.saveSortOrderUrl,
          data: ({payment_id: payment_id, value: parseInt(value)}),
          context: this,
          success: function (data, textStatus) {
            if (!data.result.success) {
              alert(this.lang.saveSortOrderErrorMsg);
            }
          },
          error: function (data, textStatus) {
            alert(this.lang.saveSortOrderErrorMsg);
          }
        });
      } else {
        alert(this.lang.sortOrderInvalidValue);
      }
    },
    saveStatus: function (payment_id, value) {
      $.ajax({
        dataType: "json",
        evalScripts: true,
        url: this.saveStatusUrl,
        data: ({payment_id: payment_id, value: parseInt(value)}),
        context: this,
        success: function (data, textStatus) {
          if (!data.result.success) {
            alert(this.lang.saveStatusErrorMsg);
          }
        },
        error: function (data, textStatus) {
          alert(this.lang.saveStatusErrorMsg);
        }
      });
    }

  };
})(App, jQuery);

$(document).ready(function () {
  // --
});