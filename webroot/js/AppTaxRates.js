var App = App || {};

(function (App, $) {
  /**
   * 
   */
  App.TaxRates = {
    lang: {
      confirmDeleteMessage: 'Czy na pewno chcesz usunąć stawkę VAT "<%- name %>"?',
      saveSortOrderErrorMsg: 'Wystąpił błąd podczas zapisu kolejności sortowania.',
      sortOrderInvalidValue: 'Wpisana wartość jest nieprawidłowa'
    },
    
    saveSortOrderUrl: null,
    
    /**
     * 
     */
    onDocReady: function () {
      // --
    },

    deleteTaxRate: function(name, deleteUrl) {
      var messageTemplate = _.template(this.lang.confirmDeleteMessage);
      App.Modal.confirmDelete(messageTemplate({name:name}), 'window.location.href=\'' + deleteUrl + '\'');
    },
    
    saveSortOrder: function(tax_rate_id, value) {
      if (value.match(/^([0-9]{1,}$)/) && parseInt(value) >= 0) {
        $.ajax({
          dataType: "json",
          evalScripts: true,
          url: this.saveSortOrderUrl,
          data: ({tax_rate_id: tax_rate_id, value: parseInt(value)}),
          context: this,
          success: function (data, textStatus) {
            if (!data.result.success) {
              alert(this.lang.saveSortOrderErrorMsg);
            }
          },
          error: function (data, textStatus) {
            alert(this.lang.saveSortOrderErrorMsg);
          }
        });
      } else {
        alert(this.lang.sortOrderInvalidValue);
      }
    },
    
  };
})(App, jQuery);

$(document).ready(function () {
  // --
});