var App = App || {};

(function (App, $) {
  /**
   * 
   */
  App.CategoriesTree = {
    lang: {
      noCategoriesMsg: 'Brak kategorii',
      getCategoriesErrorMsg: 'Wystąpił błąd podczas pobierania kategorii',
      loadingCategoriesMsg: 'Pobieranie kategorii...'
    },
    dataLoadUrl: null,
    nodeUrlPattern: null,
    tree: null,
    treeview: null,
    selected: [],
    expanded: [],
    chooseMode: false,
    multiSelect: false,
    onSelect: null,
    onUnselect: null,
    withRoot: true,
    /**
     * 
     */
    onDocReady: function () {
      if (!this.chooseMode) {
        if (window.location.href.indexOf('parent_id:') >= 0) {
          this.nodeUrlPattern = window.location.href.replace(/parent_id:([0-9]+[0-9_]{0,})/, 'parent_id:xxx');

          var tmp_arr = window.location.href.match(/parent_id:([0-9]+[0-9_]{0,})/);
          this.selected = [tmp_arr[1]];
        } else {
          this.selected = [];
          this.nodeUrlPattern = window.location.href + (window.location.href.substr(-1) === '/' ? '' : '/') + 'parent_id:xxx';
        }
        this.nodeUrlPattern = this.nodeUrlPattern.replace(/\/page:([0-9]+)/, '');

        this.loadMainTree();
      }
    },
    loadMainTree: function () {
      this.loadTreeData([0], function (parent_id, tree) {
        for (var i = 0; i < tree[0].length; i++) {
          tree[0][i].tags[1] = tree[0][i].tags[0];
          if (this.selected.indexOf(tree[0][i].tags[1]) >= 0) {
            if (typeof tree[0][i].state == 'undefined') {
              tree[0][i].state = {
                checked: false,
                disabled: false,
                expanded: false,
                selected: true
              };
            } else {
              tree[0][i].state.selected = true;
            }
          }
          if (!this.chooseMode) {
            tree[0][i].href = this.nodeUrlPattern.replace('parent_id:xxx', 'parent_id:' + tree[0][i].tags[1]);
          }
        }
        this.tree = tree[0];
        this.reloadTree();
        this.loadExpanded();
      });
    },
    reloadTree: function () {
      if (App.CategoriesTree.tree.length == 0) {
        $('#categories_tree_loading').text(this.lang.noCategoriesMsg);
        return;
      }
      
      $('#categories_tree_loading').hide();
      $('#categories_tree').treeview({
        data: this.tree,
        enableLinks: !this.chooseMode,
        multiSelect: this.multiSelect,
        showBorder: false,
        showIcon: false,
        levels: 0
      });
      this.treeview = $('#categories_tree').treeview(true);
      this.tree = [this.treeview.getNode(0)].concat(this.treeview.getSiblings(0));
      $('#categories_tree').on('nodeExpanded', function (event, data) {
        if (data.nodes[0].tags[0] === 0) {
          App.CategoriesTree.loadSubtree(data);
        }
        if (!App.CategoriesTree.chooseMode) {
          App.CategoriesTree.saveExpanded();
        }
      });
      if (this.chooseMode) {
        if ($.isPlainObject(this.onSelect) && typeof this.onSelect.func === 'function') {
          $('#categories_tree').on('nodeSelected', function (event, data) {
            App.CategoriesTree.onSelect.func.call(App.CategoriesTree.onSelect.context, data.tags[0], data);
          });
        }
        if ($.isPlainObject(this.onUnselect) && typeof this.onUnselect.func === 'function') {
          $('#categories_tree').on('nodeUnselected', function (event, data) {
            App.CategoriesTree.onUnselect.func.call(App.CategoriesTree.onUnselect.context, data.tags[0], data);
          });
        }
      } else {
        $('#categories_tree').on('nodeCollapsed', function (event, data) {
          App.CategoriesTree.saveExpanded();
        });
        $('#categories_tree').on('nodeSelected', function (event, data) {
          window.location.href = data.href;
        });
      }
    },
    loadTreeData: function (parent_id, callback) {
      $.ajax({
        dataType: "json",
        evalScripts: true,
        url: this.dataLoadUrl,
        data: ({parent_id: parent_id, with_root: this.withRoot ? 1 : 0}),
        context: this,
        success: function (data, textStatus) {
          var tree = [];
          for (var i = 0; i < parent_id.length; i++) {
            tree[parent_id[i]] = [];
            for (var j = 0; j < data.categories[parent_id[i]].length; j++) {
              var item = {
                tags: [data.categories[parent_id[i]][j].Category.id],
                text: data.categories[parent_id[i]][j].Category.name
              };
              if (data.categories[parent_id[i]][j][0].child_count > 0) {
                item.nodes = [{text: this.lang.loadingCategoriesMsg, tags: [0]}];
              }
              tree[parent_id[i]].push(item);
            }
          }
          callback.call(this, parent_id, tree);
        },
        error: function (data, textStatus) {
          alert(parent_id + ' - ' + this.lang.getCategoriesErrorMsg);
        }
      });
    },
    loadSubtree: function (node) {
      this.loadTreeData([node.tags[1]], function (parent_id, subtree) {
        this.putSubtree(subtree[node.tags[1]], this.tree, node.tags[1]);
      });
    },
    putSubtree: function (subtree, branch, nested_id) {
      if (typeof nested_id === 'string') {
        nested_id = nested_id.split('_');
      }
      var parent_id = nested_id[0];
      var parentbranch = null;
      nested_id.splice(0, 1);
      for (var i = 0; i < branch.length; i++) {
        if (branch[i].tags[0] == parent_id) {
          parentbranch = branch[i];
          break;
        }
      }

      if (parentbranch == null) {
        return false;
      } else if (nested_id.length > 0) {
        this.putSubtree(subtree, parentbranch.nodes, nested_id);
      } else {
        for (var i = 0; i < subtree.length; i++) {
          subtree[i].tags[1] = parentbranch.tags[1] + '_' + subtree[i].tags[0];
          if (this.selected.indexOf(subtree[i].tags[1]) >= 0) {
            if (typeof subtree[i].state == 'undefined') {
              subtree[i].state = {
                checked: false,
                disabled: false,
                expanded: false,
                selected: true
              };
            } else {
              subtree[i].state.selected = true;
            }
          }
          if (!this.chooseMode) {
            subtree[i].href = this.nodeUrlPattern.replace('parent_id:xxx', 'parent_id:' + subtree[i].tags[1]);
          }
        }
        parentbranch.nodes = subtree;
        this.reloadTree();
      }
    },
    saveExpanded: function () {
      var expanded = this.treeview.getExpanded();
      var expanded_nids = [];
      for (var i = 0; i < expanded.length; i++) {
        expanded_nids.push(expanded[i].tags[1]);
      }
      Cookies.set('cat-tree-exp', JSON.stringify(expanded_nids), { expires: 7, path: '/' });
    },
    loadExpanded: function () {
      var expanded_nids = this.expanded;
      for (var i = 0; i < this.selected.length; i++) {
        var offset = 0;
        var pos = this.selected[i].indexOf('_', offset);
        while (pos >= 0) {
          expanded_nids.push(this.selected[i].substr(0, pos));
          offset = pos + 1;
          pos = this.selected[i].indexOf('_', offset);
        }
      }
      if (!this.chooseMode) {
        expanded_nids = expanded_nids.concat(Cookies.get('cat-tree-exp') && Cookies.get('cat-tree-exp').length > 0 ? JSON.parse(Cookies.get('cat-tree-exp')) : []);
      }

      var arrayUnique = function (a) {
        return a.reduce(function (p, c) {
          if (p.indexOf(c) < 0)
            p.push(c);
          return p;
        }, []);
      };

      expanded_nids = arrayUnique(expanded_nids);

      if (expanded_nids.length > 0) {
        this.loadTreeData(expanded_nids, function (parent_id, tree) {
          for (var i = 0; i < parent_id.length; i++) {
            this.putSubtree(tree[parent_id[i]], this.tree, parent_id[i]);
          }
          for (var i = 0; i < parent_id.length; i++) {
            this.treeview.expandNode(this.getNodeByNestedId(parent_id[i], {silent: true}));
          }

          if (this.chooseMode && $('#categories_tree .node-selected').length) {
            $('#categories_tree').parent().scrollTo($('#categories_tree .node-selected'), {duration:500, offset:-40});
          }
        });
      }
    },
    getNodeByNestedId: function (nested_id) {
      var nid = nested_id.split('_');
      var branch = this.tree;
      var id = nid.shift();
      do {
        var found = false;
        for (var i = 0; i < branch.length; i++) {
          if (branch[i].tags[0] == id) {
            if (nid.length === 0) {
              return branch[i];
            } else {
              branch = branch[i].nodes;
              id = nid.shift();
              found = true;
            }
          }
        }
      } while (found);
      return null;
    },
    
    getSelectedData: function() {
      var selected = [];
      var nodes = this.treeview.getSelected();
      for (var i = 0; i < nodes.length; i++) {
        selected.push({
          id: nodes[i].tags[0],
          nid: nodes[i].tags[1]
        });
      }
      return selected;
    }
  };
})(App, jQuery);

$(document).ready(function () {
  // -- 
});