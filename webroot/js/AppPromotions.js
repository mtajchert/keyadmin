var App = App || {};

(function (App, $) {
  /**
   * 
   */
  App.Promotions = {
    lang: {
      confirmDeleteTitle: 'Potwierdź usunięcie',
      confirmDeleteMessage: 'Czy po usunięciu promocji dla produktu "<%= name %>" przywrócić starą cenę produktu?',
      noChoosedProduct: 'Wybierz produkt do promocji',
      savePromotionStatusErrorMsg: 'Wystąpił błąd podczas zapisu statusu',
      saveProductStatusErrorMsg: 'Wystąpił błąd podczas zapisu statusu',
      confirmDeletePromotionsNotRestorePrice: 'Czy na pewno chcesz usunąć zaznaczone promocje pozostawiając obecne ceny produktów?',
      confirmDeletePromotionsAndRestorePrice: 'Czy na pewno chcesz usunąć zaznaczone promocje oraz przywrócić poprzednie ceny produktów?',
      confirmActionTitle: 'Potwierdź wykonanie akcji',
      confirmDeactivatePromotions: 'Czy na pewno chcesz zmienić status wybranych promocji na nieaktywne?',
      confirmActivatePromotions: 'Czy na pewno chcesz zmienić status wybranych promocji na aktywne?',
      confirmDeactivateProducts: 'Czy na pewno chcesz zmienić status wybranych produktów na nieaktywne?',
      confirmActivateProducts: 'Czy na pewno chcesz zmienić status wybranych produktów na aktywne?',
      confirmDeleteProducts: 'Czy na pewno chcesz usunąć wybrane produkty?',
      confirmClearPromotionsDate: 'Czy na pewno wyzerować datę rozpoczęcia wybranych promocji?',
      confirmClearPromotionsDateEnd: 'Czy na pewno wyzerować datę zakończenia wybranych promocji?',
      confirmManipulatePromotionsDate: 'Wprowadź ilość dni o jaką zmniejszyć/zwiększyć datę rozpoczęcia promocji:',
      confirmManipulatePromotionsDateEnd: 'Wprowadź ilość dni o jaką zmniejszyć/zwiększyć datę zakończenia promocji:',
      manipulateDateInputPlaceholder: 'Wpisz ilość dni',
      confirmManipulateOldPriceAmount: 'Wprowadź wartość o jaką zmniejszyć/zwiększyć poprzednią cenę produktu:',
      manipulatePriceAmountInputPlaceholder: 'Wpisz wartość',
      confirmManipulateOldPricePercent: 'Wprowadź procent o jaki zmniejszyć/zwiększyć poprzednią cenę produktu:',
      manipulatePricePercentInputPlaceholder: 'Wpisz procent',
    },
    
    getProductsTableUrl: null,
    getProductDataUrl: null,
    savePromotionStatusUrl: null,
    saveProductStatusUrl: null,
    
    /**
     * 
     */
    onDocReady: function () {
      // --
    },
    
    openChooseProductBox: function() {
      if ($('#ChooseCategoryProductBox').length === 0) {
        var fn = _.template($('#modal-choose-category-product-template').html());
        var modalTemplate = fn({saveScript: 'App.Promotions.saveProductBox()', searchScript: 'App.Promotions.chooseProductBoxSearch()'});
        $('body').append(modalTemplate);
        
        $('#ChooseCategoryProductBoxQuery').keypress(function (e) {
          if (e.which == 13) {
            App.Promotions.chooseProductBoxSearch();
          }
        });
        
        App.CategoriesTree.selected = [$('#PromotionProductIdCategoryNid').val()];
        App.CategoriesTree.onSelect = {context: this, func: function(selectedId, selectedData){
          this.chooseProductBoxSearch();
        }};
        App.CategoriesTree.loadMainTree();
      } else {
        App.CategoriesTree.selected = [$('#PromotionProductIdCategoryNid').val()];
        App.CategoriesTree.loadExpanded();
      }
      
      $('#ChooseCategoryProductBox .modal-body').css({maxHeight: $(window).height() - 200, overflow:'auto'});
      $('#ChooseCategoryProductBox').modal('show');
    },
    
    chooseProductBoxSearch: function() {
      var selected = App.CategoriesTree.getSelectedData();
      this.chooseProductBoxLoadProducts(selected && selected.length > 0 ? selected[0].id : 0, $('#ChooseCategoryProductBoxQuery').val());
    },
    
    chooseProductBoxLoadProducts: function(category_id, query) {
      $.ajax({
        dataType: "json",
        evalScripts: true,
        url: this.getProductsTableUrl,
        data: ({category_id: category_id, query: query}),
        context: this,
        success: function (data, textStatus) {
          $('#ChooseCategoryProductBox .products-content').html(data.result.html);
        },
        error: function (data, textStatus) {
          alert('Error occured during getting products list. Please try again.');
        }
      });
    },
    
    saveProductBox: function() {
      var product_id = $('#ChooseCategoryProductBox input[name=ChooseCategoryProductBoxRadio]:checked').val();
      if (!$.isNumeric(product_id) || product_id <= 0) {
        alert(this.lang.noChoosedProduct);
        return;
      }
      
      $.ajax({
        dataType: "json",
        evalScripts: true,
        url: this.getProductDataUrl,
        data: ({product_id: product_id}),
        context: this,
        success: function (data, textStatus) {
          $('#ProductId').val(data.product.Product.id);
          $('#ProductName').val(data.product.Product.name);
          window.aa = data.product.Product;
          if (parseFloat(data.product.Product.old_price) > 0 || parseInt(data.product.Product.promotion_status)) {
            $('#ProductOldPrice').val(parseFloat(data.product.Product.old_price) > 0 ? data.product.Product.old_price : '');
            $('#ProductPriceTax').val(parseFloat(data.product.Product.price_tax) > 0 ? data.product.Product.price_tax : '');
          } else {
            $('#ProductOldPrice').val(data.product.Product.price_tax);
            $('#ProductPriceTax').val('');
          }
          $('#ProductPromotionStatus').prop('checked', parseInt(data.product.Product.promotion_status) ? true : false);
          $('#ProductPromotionDate').val(data.product.Product.promotion_date === '0000-00-00 00:00:00' ? '' : moment(data.product.Product.promotion_date).format('YYYY-MM-DD HH:mm'));
          $('#ProductPromotionDateEnd').val(data.product.Product.promotion_date_end === '0000-00-00 00:00:00' ? '' : moment(data.product.Product.promotion_date_end).format('YYYY-MM-DD HH:mm'));
          
          $('#ChooseCategoryProductBox').modal('hide');
        },
        error: function (data, textStatus) {
          alert('Error occured during getting choosed product data. Please try again.');
        }
      });
    },

    deletePromotion: function (name, deleteUrl) {
      var messageTemplate = _.template(this.lang.confirmDeleteMessage);
      App.Modal.confirmYesNoCancel(
              this.lang.confirmDeleteTitle,
              messageTemplate({name: name}),
              'window.location.href=\'' + deleteUrl + (deleteUrl.indexOf('?') === -1 ? '?' : '') + 'restore_price=1\'',
              'window.location.href=\'' + deleteUrl + (deleteUrl.indexOf('?') === -1 ? '?' : '') + 'restore_price=0\''
              );
    },
    
    savePromotionStatus: function(product_id, value) {
      $.ajax({
        dataType: "json",
        evalScripts: true,
        url: this.savePromotionStatusUrl,
        data: ({product_id: product_id, value: parseInt(value)}),
        context: this,
        success: function (data, textStatus) {
          if (!data.result.success) {
            alert(this.lang.savePromotionStatusErrorMsg);
          }
        },
        error: function (data, textStatus) {
          alert(this.lang.savePromotionStatusErrorMsg);
        }
      });
    },
    
    saveProductStatus: function(product_id, value) {
      $.ajax({
        dataType: "json",
        evalScripts: true,
        url: this.saveProductStatusUrl,
        data: ({product_id: product_id, value: parseInt(value)}),
        context: this,
        success: function (data, textStatus) {
          if (!data.result.success) {
            alert(this.lang.saveProductStatusErrorMsg);
          }
        },
        error: function (data, textStatus) {
          alert(this.lang.saveProductStatusErrorMsg);
        }
      });
    },
    
    runBulkAction: function() {
      var action = $('#bulk_action').val();
      var contx = App.Modal;
      var func = null;
      var after = null;
      var args = {};
      var submit_script = "$('#PromotionsAdminIndexForm').submit();";
      
      switch (action) {
        case 'delete_promotions_not_restore_price':
          func = App.Modal.confirmDelete;
          args = [this.lang.confirmDeletePromotionsNotRestorePrice, submit_script];
          break;
        case 'delete_promotions_and_restore_price':
          func = App.Modal.confirmDelete;
          args = [this.lang.confirmDeletePromotionsAndRestorePrice, submit_script];
          break;
        case 'deactivate_promotions':
          func = App.Modal.confirmYesCancel;
          args = [this.lang.confirmActionTitle, this.lang.confirmDeactivatePromotions, submit_script];
          break;
        case 'activate_promotions':
          func = App.Modal.confirmYesCancel;
          args = [this.lang.confirmActionTitle, this.lang.confirmActivatePromotions, submit_script];
          break;
        case 'deactivate_products':
          func = App.Modal.confirmYesCancel;
          args = [this.lang.confirmActionTitle, this.lang.confirmDeactivateProducts, submit_script];
          break;
        case 'activate_products':
          func = App.Modal.confirmYesCancel;
          args = [this.lang.confirmActionTitle, this.lang.confirmActivateProducts, submit_script];
          break;
        case 'delete_products':
          func = App.Modal.confirmDelete;
          args = [this.lang.confirmDeleteProducts, submit_script];
          break;
        case 'clear_promotions_date':
          func = App.Modal.confirmYesCancel;
          args = [this.lang.confirmActionTitle, this.lang.confirmClearPromotionsDate, submit_script];
          break;
        case 'clear_promotions_date_end':
          func = App.Modal.confirmYesCancel;
          args = [this.lang.confirmActionTitle, this.lang.confirmClearPromotionsDateEnd, submit_script];
          break;
        case 'manipulate_promotions_date':
          func = App.Modal.confirmOkCancel;
          var msg = '<form class="form-inline"><p>'
            + this.lang.confirmManipulatePromotionsDate
            + '<div class="form-group"><div class="input"><input id="manipulate_promotions_date_input" placeholder="'
            + this.lang.manipulateDateInputPlaceholder
            + '" class="form-control" step="1" type="number"></div></div></form>';
          args = [this.lang.confirmActionTitle, msg, "App.Promotions.bulkActionCheckManipulateDate($('#manipulate_promotions_date_input'))"];
          after = function(){
            $('#manipulate_promotions_date_input').keydown(function(event){
              if (event.which == 13) {
                event.preventDefault();
                event.stopPropagation();
                App.Promotions.bulkActionCheckManipulateDate(this);
                return false;
              }
            }).focus();
          };
          break;
        case 'manipulate_promotions_date_end':
          func = App.Modal.confirmOkCancel;
          var msg = '<form class="form-inline"><p>'
            + this.lang.confirmManipulatePromotionsDateEnd
            + '<div class="form-group"><div class="input"><input id="manipulate_promotions_date_end_input" placeholder="'
            + this.lang.manipulateDateInputPlaceholder
            + '" class="form-control" step="1" type="number"></div></div></form>';
          args = [this.lang.confirmActionTitle, msg, "App.Promotions.bulkActionCheckManipulateDate($('#manipulate_promotions_date_end_input'))"];
          after = function(){
            $('#manipulate_promotions_date_end_input').keydown(function(event){
              if (event.which == 13) {
                event.preventDefault();
                event.stopPropagation();
                App.Promotions.bulkActionCheckManipulateDate(this);
                return false;
              }
            }).focus();
          };
          break;
        case 'manipulate_promotions_old_price_amount':
          func = App.Modal.confirmOkCancel;
          var msg = '<form class="form-inline"><p>'
            + this.lang.confirmManipulateOldPriceAmount
            + '<div class="form-group"><div class="input"><input id="manipulate_price_amount_input" placeholder="'
            + this.lang.manipulatePriceAmountInputPlaceholder
            + '" class="form-control" step="0.01" type="number"></div></div></form>';
          args = [this.lang.confirmActionTitle, msg, "App.Promotions.bulkActionCheckManipulateOldPrice($('#manipulate_price_amount_input'), 'amount')"];
          after = function(){
            $('#manipulate_price_amount_input').keydown(function(event){
              if (event.which == 13) {
                event.preventDefault();
                event.stopPropagation();
                App.Promotions.bulkActionCheckManipulateOldPrice(this, 'amount');
                return false;
              }
            }).focus();
          };
          break;
        case 'manipulate_promotions_old_price_percent':
          func = App.Modal.confirmOkCancel;
          var msg = '<form class="form-inline"><p>'
            + this.lang.confirmManipulateOldPricePercent
            + '<div class="form-group"><div class="input"><input id="manipulate_price_percent_input" placeholder="'
            + this.lang.manipulatePriceAmountInputPlaceholder
            + '" class="form-control" step="0.01" type="number"></div></div></form>';
          args = [this.lang.confirmActionTitle, msg, "App.Promotions.bulkActionCheckManipulateOldPrice($('#manipulate_price_percent_input'), 'percent')"];
          after = function(){
            $('#manipulate_price_percent_input').keydown(function(event){
              if (event.which == 13) {
                event.preventDefault();
                event.stopPropagation();
                App.Promotions.bulkActionCheckManipulateOldPrice(this, 'percent');
                return false;
              }
            }).focus();
          };
          break;
          break;
      }
      
      if (typeof func === 'function') {
        var modal = func.apply(contx, args);
        if (typeof after === 'function') {
          modal.on('shown.bs.modal', function (e) {
            after.call();
          });
        }
      }
    },
    
    bulkActionCheckManipulateDate: function(input) {
      var value = $(input).val();
      var pattern = /^([-+]){0,1}([0-9]+)$/
      if (pattern.test(value) && (parseInt(value) > 0 || parseInt(value) < 0)) {
        $(input).closest('.form-group').removeClass('has-error');
        $('#PromotionsAdminIndexForm').append($('<input>').attr({name:'manipulate_date_value',value:value}));
        $('#PromotionsAdminIndexForm').submit();
      } else {
        $(input).closest('.form-group').addClass('has-error');
      }
    },
    
    bulkActionCheckManipulateOldPrice: function(input, type) {
      var value = $(input).val();
      var pattern = type === 'value' ? /^([-+]){0,1}([0-9]+([.,][0-9]{1,2}){0,1})$/ : /^([-+]){0,1}([0-9]+([.,][0-9]{1,5}){0,1})$/;
      if (pattern.test(value) && (parseFloat(value) > 0 || parseFloat(value) < 0)) {
        $(input).closest('.form-group').removeClass('has-error');
        $('#PromotionsAdminIndexForm').append($('<input>').attr({name:'manipulate_old_price_value',value:parseFloat(value)}));
        $('#PromotionsAdminIndexForm').submit();
      } else {
        $(input).closest('.form-group').addClass('has-error');
      }
    },
    
    loadPromotionManageForm: function () {
      App.CategoriesTree.onSelect = {context: this, func: function (selectedId, selectedData) {
          this.savePromotionManageCategories();
        }};
      App.CategoriesTree.onUnselect = {context: this, func: function (unselectedId, selectedData) {
          this.savePromotionManageCategories();
        }};
      
      var selected_nids = [];
      $('.categories_list input[name="data[PromotionManage][categories_nid][]"]').each(function(){
        selected_nids.push($(this).val());
      });
      App.CategoriesTree.selected = selected_nids;
      
      App.CategoriesTree.loadMainTree();
    },
    
    savePromotionManageCategories: function() {
      var selected = App.CategoriesTree.getSelectedData();
      var inputs = [];
      for (var i = 0; i < selected.length; i++) {
        inputs.push($('<input>').attr({
          type: 'hidden',
          name: 'data[PromotionManage][categories][]',
          value: selected[i].id
        }));
        inputs.push($('<input>').attr({
          type: 'hidden',
          name: 'data[PromotionManage][categories_nid][]',
          value: selected[i].nid
        }));
      }
      $('.categories_list input[name="data[PromotionManage][categories][]"], .categories_list input[name="data[PromotionManage][categories_nid][]"]').remove();
      $('.categories_list').append(inputs);
    }
    
  };
})(App, jQuery);

$(document).ready(function () {
  // --
});