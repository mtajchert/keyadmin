var App = App || {};

(function (App, $) {
  /**
   * 
   */
  App.Shippings = {
    lang: {
      confirmDeleteMessage: 'Czy na pewno chcesz usunąć formę dostawy "<%- name %>"?',
      saveSortOrderErrorMsg: 'Wystąpił błąd podczas zapisu kolejności sortowania.',
      sortOrderInvalidValue: 'Wpisana wartość jest nieprawidłowa',
      saveStatusErrorMsg: 'Wystąpił błąd podczas zapisu statusu.'
    },
    templates: {
      shipping_price_to_weight: '<div class="item form-inline"><input name="data[ShippingPrice][<%= key %>][id]" class="hide" value="" id="ShippingPrice<%= key %>Id" type="hidden"><div class="form-group"><label for="ShippingPrice<%= key %>ToWeight" class="col-xs-12 control-label">waga do: *</label></div><div class="form-group"><div class="input-group m-b required"><input name="data[ShippingPrice][<%= key %>][to_weight]" placeholder="wpisz wagę" class="form-control" step="0.01" value="" id="ShippingPrice<%= key %>ToWeight" required="required" type="number"><span class="input-group-addon">kg</span></div></div><div class="form-group"><label for="ShippingPrice<%= key %>PriceTax" class="col-xs-12 control-label">cena: *</label></div><div class="form-group"><div class="input-group m-b required"><input name="data[ShippingPrice][<%= key %>][price_tax]" placeholder="wpisz cenę" class="form-control" step="0.01" value="" id="ShippingPrice<%= key %>PriceTax" required="required" type="number"><span class="input-group-addon">zł</span></div></div><div class="form-group rel-icons"><a title="Usuń" data-original-title="" href="javascript:void(0);" onclick="App.Shippings.deleteShippingPrice(this);"><em class="icon-close"></em></a></div></div>',
      shipping_price_to_price_tax: '<div class="item form-inline"><input name="data[ShippingPrice][<%= key %>][id]" class="hide" value="" id="ShippingPrice<%= key %>Id" type="hidden"><div class="form-group"><label for="ShippingPrice<%= key %>ToPriceTax" class="col-xs-12 control-label">wartość do: *</label></div><div class="form-group"><div class="input-group m-b required"><input name="data[ShippingPrice][<%= key %>][to_price_tax]" placeholder="wpisz wartość" class="form-control" step="0.01" value="" id="ShippingPrice<%= key %>ToPriceTax" required="required" type="number"><span class="input-group-addon">zł</span></div></div><div class="form-group"><label for="ShippingPrice<%= key %>PriceTax" class="col-xs-12 control-label">cena: *</label></div><div class="form-group"><div class="input-group m-b required"><input name="data[ShippingPrice][<%= key %>][price_tax]" placeholder="wpisz cenę" class="form-control" step="0.01" value="" id="ShippingPrice<%= key %>PriceTax" required="required" type="number"><span class="input-group-addon">zł</span></div></div><div class="form-group rel-icons"><a title="Usuń" data-original-title="" href="javascript:void(0);" onclick="App.Shippings.deleteShippingPrice(this);"><em class="icon-close"></em></a></div></div>',
      shipping_price_to_product_amount: '<div class="item form-inline"><input name="data[ShippingPrice][<%= key %>][id]" class="hide" value="" id="ShippingPrice<%= key %>Id" type="hidden"><div class="form-group"><label for="ShippingPrice<%= key %>ToProductAmount" class="col-xs-12 control-label">ilość do: *</label></div><div class="form-group"><div class="input-group m-b required"><input name="data[ShippingPrice][<%= key %>][to_product_amount]" placeholder="wpisz ilość" class="form-control" step="0.01" value="" id="ShippingPrice<%= key %>ToProductAmount" required="required" type="number"><span class="input-group-addon">j.m.</span></div></div><div class="form-group"><label for="ShippingPrice<%= key %>PriceTax" class="col-xs-12 control-label">cena: *</label></div><div class="form-group"><div class="input-group m-b required"><input name="data[ShippingPrice][<%= key %>][price_tax]" placeholder="wpisz cenę" class="form-control" step="0.01" value="" id="ShippingPrice<%= key %>PriceTax" required="required" type="number"><span class="input-group-addon">zł</span></div></div><div class="form-group rel-icons"><a title="Usuń" data-original-title="" href="javascript:void(0);" onclick="App.Shippings.deleteShippingPrice(this);"><em class="icon-close"></em></a></div></div>'
    },
    
    saveSortOrderUrl: null,
    saveStatusUrl: null,
    
    /**
     * 
     */
    onDocReady: function () {
      // --
    },

    deleteShipping: function(name, deleteUrl) {
      var messageTemplate = _.template(this.lang.confirmDeleteMessage);
      App.Modal.confirmDelete(messageTemplate({name:name}), 'window.location.href=\'' + deleteUrl + '\'');
    },
    
    saveSortOrder: function(shipping_id, value) {
      if (value.match(/^([0-9]{1,}$)/) && parseInt(value) >= 0) {
        $.ajax({
          dataType: "json",
          evalScripts: true,
          url: this.saveSortOrderUrl,
          data: ({shipping_id: shipping_id, value: parseInt(value)}),
          context: this,
          success: function (data, textStatus) {
            if (!data.result.success) {
              alert(this.lang.saveSortOrderErrorMsg);
            }
          },
          error: function (data, textStatus) {
            alert(this.lang.saveSortOrderErrorMsg);
          }
        });
      } else {
        alert(this.lang.sortOrderInvalidValue);
      }
    },
    
    saveStatus: function(shipping_id, value) {
      $.ajax({
        dataType: "json",
        evalScripts: true,
        url: this.saveStatusUrl,
        data: ({shipping_id: shipping_id, value: parseInt(value)}),
        context: this,
        success: function (data, textStatus) {
          if (!data.result.success) {
            alert(this.lang.saveStatusErrorMsg);
          }
        },
        error: function (data, textStatus) {
          alert(this.lang.saveStatusErrorMsg);
        }
      });
    },
    
    toggleAllCategories: function(state) {
      if (state) {
        $('.payments_list').slideUp();
        $('.payments_list input').prop('disabled', true);
      } else {
        $('.payments_list').slideDown();
        $('.payments_list input').prop('disabled', false);
      }
    },
    
    toggleAllCountries: function(state) {
      if (state) {
        $('.countries_list').slideUp();
        $('.countries_list input').prop('disabled', true);
        //$('.countries_list input.empty').prop('disabled', false);
      } else {
        $('.countries_list').slideDown();
        $('.countries_list input').prop('disabled', false);
        //$('.countries_list input.empty').prop('disabled', true);
      }
    },
    
    deleteShippingPrice: function(btnNode) {
      $(btnNode).closest('.item').remove();
    },
    
    addShippingPrice: function() {
      switch ($('#ShippingPriceType').val()) {
        case 'depend_weight':
          var itemTemplate = _.template(this.templates.shipping_price_to_weight);
          break;
        case 'depend_price':
          var itemTemplate = _.template(this.templates.shipping_price_to_price_tax);
          break;
        case 'depend_amount':
          var itemTemplate = _.template(this.templates.shipping_price_to_product_amount);
          break;
      }
      
      var key = -1;
      var exp = /data\[ShippingPrice\]\[(.*)\]\[price_tax\]/;
      $('input[name^="data[ShippingPrice]["][name$="][price_tax]').each(function(){
        var match = $(this).attr('name').match(exp);
        if (match.length == 2) {
          if (match[1] > key) {
            key = parseInt(match[1]);
          }
        }
      });
      key++;
      
      $('#ShippingPricesItems .items').append(itemTemplate({key: key}));
      if (key == 0) {
        $('#ShippingPricesItems .items .rel-icons a').remove();
      }
    },
    
    changePriceType: function(type) {
      if (type == 'const') {
        $('#ShippingPriceConst').slideDown();
        $('#ShippingPricesItems').slideUp(400, function(){
          $('#ShippingPricesItems .items').html('');
        });
      } else if (['depend_weight', 'depend_price', 'depend_amount'].indexOf(type) !== -1) {
        $('#ShippingPricesItems .items').html('');
        this.addShippingPrice();
        $('#ShippingPricesItems').slideDown();
        $('#ShippingPriceConst').slideUp(400, function(){
          $('#ShippingPriceTax').val('');
        });
      } else {
        $('#ShippingPricesItems').slideUp(400, function(){
          $('#ShippingPricesItems .items').html('');
        });
        $('#ShippingPriceConst').slideUp(400, function(){
          $('#ShippingPriceTax').val('');
        });
      }
    }
  };
})(App, jQuery);

$(document).ready(function () {
  // --
});