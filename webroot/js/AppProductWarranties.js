var App = App || {};

(function (App, $) {
  /**
   * 
   */
  App.ProductWarranties = {
    lang: {
      confirmDeleteMessage: 'Czy na pewno chcesz usunąć gwarancję produktów "<%- name %>"?'
    },
    
    /**
     * 
     */
    onDocReady: function () {
      // --
    },

    deleteProductWarranty: function(name, deleteUrl) {
      var messageTemplate = _.template(this.lang.confirmDeleteMessage);
      App.Modal.confirmDelete(messageTemplate({name:name}), 'window.location.href=\'' + deleteUrl + '\'');
    }
    
  };
})(App, jQuery);

$(document).ready(function () {
  // --
});