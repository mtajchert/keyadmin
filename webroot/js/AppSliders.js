var App = App || {};

(function (App, $) {
  /**
   * 
   */
  App.Sliders = {
    lang: {
      confirmDeleteMessage: 'Czy na pewno chcesz usunąć slider "<%- name %>"?'
    },
    
    /**
     * 
     */
    onDocReady: function () {
      // --
    },

    deleteSlider: function(name, deleteUrl) {
      var messageTemplate = _.template(this.lang.confirmDeleteMessage);
      App.Modal.confirmDelete(messageTemplate({name:name}), 'window.location.href=\'' + deleteUrl + '\'');
    }
    
  };
})(App, jQuery);

$(document).ready(function () {
  App.Sliders.onDocReady();
});