var App = App || {};

(function(App, $) {
    /**
     * 
     */
    App.Products = {
        lang : {
            confirmDeleteMessage : 'Czy na pewno chcesz usunąć produkt "<%- name %>"?',
            saveSortOrderErrorMsg : 'Wystąpił błąd podczas zapisu kolejności sortowania.',
            sortOrderInvalidValue : 'Wpisana wartość jest nieprawidłowa',
            saveStatusErrorMsg : 'Wystąpił błąd podczas zapisu statusu.',
            savePriceErrorMsg : 'Wystąpił błąd podczas zapisu ceny.',
            priceInvalidValue : 'Wpisana cena jest nieprawidłowa',
            saveFeatureErrorMsg : 'Wystąpił błąd podczas zapisu.'
        },
        saveSortOrderUrl : null,
        saveStatusUrl : null,
        savePriceUrl : null,
        saveFeatureStatusUrl : null,
        getCategoryNestedNameUrl : null,
        getProductOptionRow : null,
        getProductOptionValues : null,

        /**
         * 
         */
        onDocReady : function() {
            if ($('.rel-product-id').length > 0) {
                $('.rel-product-id').on('change', function(e) {
                    App.Products.loadRelProductOptions($(this));
                });
            }
            $('.btn-delate-template-image').on('click', function(e){
                e.preventDefault();
                bootbox.confirm("Na pewno chcesz usunąć to zdjęcie?", function(result){
                    if (result == true) {
                        $.ajax({
                            url : $('.btn-delate-template-image').attr('href'),
                            success : function(data, textStatus) {
                                $('.btn-delate-template-image').closest('.form-group').find('.form-image').removeClass('hide');
                                $('.btn-delate-template-image').closest('.form-group').find('.view-image').addClass('hide');
                            },
                            error : function(data, textStatus) {
                                alert('Bład, spróbuj ponownie później');
                            }
                        });
                    }
                });
                return false;
            })
        },

        deleteProduct : function(name, deleteUrl) {
            var messageTemplate = _.template(this.lang.confirmDeleteMessage);
            App.Modal.confirmDelete(messageTemplate({
                name : name
            }), 'window.location.href=\'' + deleteUrl + '\'');
        },

        saveSortOrder : function(product_id, value) {
            if (value.match(/^([0-9]{1,}$)/) && parseInt(value) >= 0) {
                $.ajax({
                    dataType : "json",
                    evalScripts : true,
                    url : this.saveSortOrderUrl,
                    data : ({
                        product_id : product_id,
                        value : parseInt(value)
                    }),
                    context : this,
                    success : function(data, textStatus) {
                        if (!data.result.success) {
                            alert(this.lang.saveSortOrderErrorMsg);
                        }
                    },
                    error : function(data, textStatus) {
                        alert(this.lang.saveSortOrderErrorMsg);
                    }
                });
            } else {
                alert(this.lang.sortOrderInvalidValue);
            }
        },

        saveStatus : function(product_id, value) {
            $.ajax({
                dataType : "json",
                evalScripts : true,
                url : this.saveStatusUrl,
                data : ({
                    product_id : product_id,
                    value : parseInt(value)
                }),
                context : this,
                success : function(data, textStatus) {
                    if (!data.result.success) {
                        alert(this.lang.saveStatusErrorMsg);
                    }
                },
                error : function(data, textStatus) {
                    alert(this.lang.saveStatusErrorMsg);
                }
            });
        },

        saveProductPriceTax : function(product_id, value) {
            if (value.match(/^([0-9]+([.,][0-9]{1,2}){0,1})/)
                    && parseInt(value) >= 0) {
                $.ajax({
                    dataType : "json",
                    evalScripts : true,
                    url : this.savePriceUrl,
                    data : ({
                        product_id : product_id,
                        price : value,
                        type : 'priceTax'
                    }),
                    context : this,
                    success : function(data, textStatus) {
                        if (!data.result.success) {
                            alert(this.lang.savePriceErrorMsg);
                        }
                    },
                    error : function(data, textStatus) {
                        alert(this.lang.savePriceErrorMsg);
                    }
                });
            } else {
                alert(this.lang.priceInvalidValue);
            }
        },

        saveProductOldPrice : function(product_id, value) {
            if (String(value).length === 0
                    || value.match(/^([0-9]+([.,]{1}[0-9]{1,2}){0,1})$/)) {
                $.ajax({
                    dataType : "json",
                    evalScripts : true,
                    url : this.savePriceUrl,
                    data : ({
                        product_id : product_id,
                        price : value,
                        type : 'oldPrice'
                    }),
                    context : this,
                    success : function(data, textStatus) {
                        if (!data.result.success) {
                            alert(this.lang.savePriceErrorMsg);
                        }
                    },
                    error : function(data, textStatus) {
                        alert(this.lang.savePriceErrorMsg);
                    }
                });
            } else {
                alert(this.lang.priceInvalidValue);
            }
        },

        saveProductFeatureStatus : function(product_id, feature, status) {
            $.ajax({
                dataType : "json",
                evalScripts : true,
                url : this.saveFeatureStatusUrl,
                data : ({
                    product_id : product_id,
                    status : status,
                    feature : feature
                }),
                context : this,
                success : function(data, textStatus) {
                    if (!data.result.success) {
                        alert(this.lang.saveFeatureErrorMsg);
                    }
                },
                error : function(data, textStatus) {
                    alert(this.lang.saveFeatureErrorMsg);
                }
            });
        },

        changePrice : function(type) {
            var tax_rate = parseInt($('#ProductTaxRateId option:selected')
                    .data('rate'));
            var price_net = $('#ProductPrice').val();
            var price_gross = $('#ProductPriceTax').val();
            var tax = $('#ProductTax').val();

            if (type === 'net' || type == 'tax') {
                price_gross = (price_net * ((100 + tax_rate) / 100)).toFixed(2);
                tax = (price_gross - price_net).toFixed(2);

                if (type == 'tax') {
                    $(
                            '.products-options-table tbody tr:not(.no-products-options)')
                            .each(
                                    function() {
                                        App.Products.changeProductOptionPrice(
                                                $(this), 'price');
                                    });
                }
            } else if (type === 'gross') {
                price_net = (price_gross / ((100 + tax_rate) / 100)).toFixed(2);
                tax = (price_gross - price_net).toFixed(2);
            }

            $('#ProductPrice').val(price_net);
            $('#ProductPriceTax').val(price_gross);
            $('#ProductTax').val(tax);
        },

        changePurchasePrice : function(type) {
            var tax_rate = parseInt($('#ProductTaxRateId option:selected')
                    .data('rate'));
            var price_net = $('#ProductPurchasePrice').val();
            var price_gross = $('#ProductPurchasePriceTax').val();

            if (type === 'net' || type == 'tax') {
                price_gross = (price_net * ((100 + tax_rate) / 100)).toFixed(2);

                if (type == 'tax') {
                    $(
                            '.products-options-table tbody tr:not(.no-products-options)')
                            .each(
                                    function() {
                                        App.Products.changeProductOptionPrice(
                                                $(this), 'price');
                                    });
                }
            } else if (type === 'gross') {
                price_net = (price_gross / ((100 + tax_rate) / 100)).toFixed(2);
            }

            $('#ProductPurchasePrice').val(price_net);
            $('#ProductPurchasePriceTax').val(price_gross);
        },

        loadProductForm : function() {
            App.CategoriesTree.onSelect = {
                context : this,
                func : function(selectedId, selectedData) {
                    console.log(selectedId, selectedData);
                    var option = $('<option>').attr({
                        value : selectedId
                    }).text(selectedData.text);
                    $('#ProductDefaultCategoryId').append(option);
                    $.ajax({
                        dataType : "json",
                        evalScripts : true,
                        url : this.getCategoryNestedNameUrl,
                        data : ({
                            category_id : selectedId
                        }),
                        context : option,
                        success : function(data, textStatus) {
                            if (!data.result.success) {
                                alert(data);
                            } else {
                                $(this).text(data.result.nname);
                            }
                        },
                        error : function(data, textStatus) {
                            alert(data);
                        }
                    });
                    this.saveProductCategories();
                }
            };
            App.CategoriesTree.onUnselect = {
                context : this,
                func : function(unselectedId, selectedData) {
                    $(
                            '#ProductDefaultCategoryId option[value="'
                                    + unselectedId + '"]').remove();
                    this.saveProductCategories();
                }
            };

            var selected_nids = [];
            $('.categories_list input[name="data[Category][CategoryNid][]"]')
                    .each(function() {
                        selected_nids.push($(this).val());
                    });
            App.CategoriesTree.selected = selected_nids;

            App.CategoriesTree.loadMainTree();
        },

        saveProductCategories : function() {
            var selected = App.CategoriesTree.getSelectedData();
            var inputs = [];
            for (var i = 0; i < selected.length; i++) {
                inputs.push($('<input>').attr({
                    type : 'hidden',
                    name : 'data[Category][Category][]',
                    value : selected[i].id
                }));
                inputs.push($('<input>').attr({
                    type : 'hidden',
                    name : 'data[Category][CategoryNid][]',
                    value : selected[i].nid
                }));
            }
            $(
                    '.categories_list input[name="data[Category][Category][]"], .categories_list input[name="data[Category][CategoryNid][]"]')
                    .remove();
            $('.categories_list').append(inputs);
        },

        toggleAllShippings : function(state) {
            if (state) {
                $('.shippings_list').slideUp();
                $('.shippings_list input').prop('disabled', true);
            } else {
                $('.shippings_list').slideDown();
                $('.shippings_list input').prop('disabled', false);
            }
        },

        disableProductOptionValues : function(ids) {
            for (var i = 0; i < ids.length; i++) {
                $('#ProductNewProductOption').prop('disabled', true);
                $(
                        '#ProductNewProductOptionValue option[value="' + ids[i]
                                + '"]').prop('disabled', true).hide();
                $('#ProductNewProductOptionValue').val('')
            }
        },

        deleteProductOption : function(node) {
            var tr = $(node).closest('tr');
            var option_value_id = tr
                    .find(
                            'input[name^="data[ProductsProductsOption]"][name$="[product_option_value_id]"]')
                    .val();
            tr.remove();
            $(
                    '#ProductNewProductOptionValue option[value="'
                            + option_value_id + '"]').prop('disabled', false)
                    .show();

            if ($('.products-options-table tbody tr:not(.no-products-options)').length == 0) {
                $('.products-options-table .no-products-options').show();
                $('#ProductNewProductOption').prop('disabled', false);

            }
        },

        addProductOption : function() {
            var option_id = $('#ProductNewProductOption').val();
            var option_value_id = $('#ProductNewProductOptionValue').val();

            if (option_id == '' || option_id < 0) {
                $('#ProductNewProductOption').parent().addClass('has-error');
            } else {
                $('#ProductNewProductOption').parent().removeClass('has-error');
            }

            if (option_value_id == '' || option_value_id < 0) {
                $('#ProductNewProductOptionValue').parent().addClass(
                        'has-error');
                return false;
            } else {
                $('#ProductNewProductOptionValue').parent().removeClass(
                        'has-error');
            }

            var key = 0;
            $(
                    'input[name^="data[ProductsProductsOption]"][name$="[product_id]"]')
                    .each(
                            function() {
                                var keyItem = parseInt(String(
                                        $(this).attr('name'))
                                        .match(/([0-9]+)/g)[0]);
                                if (keyItem >= key) {
                                    key = keyItem + 1;
                                }
                            });

            $
                    .ajax({
                        dataType : "json",
                        evalScripts : true,
                        url : this.getProductOptionRow,
                        data : ({
                            key : key,
                            product_id : $('#ProductId').val(),
                            product_option_id : option_id,
                            product_option_value_id : option_value_id
                        }),
                        context : this,
                        success : function(data, textStatus) {
                            if (!data.result.success) {
                                alert('Error occured during getting new product option row. Please try again.');
                            } else {
                                $('.products-options-table tbody').append(
                                        data.result.html);
                                $(
                                        '#ProductNewProductOptionValue option[value="'
                                                + option_value_id + '"]').prop(
                                        'disabled', true).hide();
                                $('#ProductNewProductOptionValue').val('')
                                $('#ProductNewProductOption').prop('disabled',
                                        true);
                                $(
                                        '.products-options-table .no-products-options')
                                        .hide();
                            }
                        },
                        error : function(data, textStatus) {
                            alert('Error occured during getting new product option row. Please try again.');
                        }
                    });
        },

        changeProductOption : function(select) {
            var option_id = $(select).val();

            $('#ProductNewProductOptionValue option[value!=""]').remove();

            if (option_id == '' || option_id < 0) {
                return;
            }

            $
                    .ajax({
                        dataType : "json",
                        evalScripts : true,
                        url : this.getProductOptionValues,
                        data : ({
                            product_option_id : option_id
                        }),
                        context : this,
                        success : function(data, textStatus) {
                            if (!data.result.success) {
                                alert('Error occured during getting product option values. Please try again.');
                            } else {
                                var options = [];
                                for ( var key in data.result.productOptionValues) {
                                    if (data.result.productOptionValues
                                            .hasOwnProperty(key)) {
                                        options
                                                .push($('<option>')
                                                        .attr({
                                                            value : key
                                                        })
                                                        .text(
                                                                data.result.productOptionValues[key]));
                                    }
                                }
                                $('#ProductNewProductOptionValue').append(
                                        options);
                            }
                        },
                        error : function(data, textStatus) {
                            alert('Error occured during getting product option values. Please try again.');
                        }
                    });
        },

        changeProductOptionPrice : function(row, type) {
            var inputPrice = row
                    .find('input[name^="data[ProductsProductsOption]"][name$="[change_price]"]');
            var inputPriceTax = row
                    .find('input[name^="data[ProductsProductsOption]"][name$="[change_price_tax]"]');

            var price = parseFloat(inputPrice.val().replace(',', '.'));
            var taxRate = parseFloat(String(
                    $('#ProductTaxRateId option:selected').data('rate'))
                    .replace(',', '.'));
            var priceTax = parseFloat(inputPriceTax.val().replace(',', '.'));

            if (type == 'price' || type == 'tax_rate') {
                priceTax = parseFloat(Number(price * ((100 + taxRate) / 100))
                        .toFixed(2));
            } else if (type == 'price_tax') {
                price = parseFloat(Number(priceTax / ((100 + taxRate) / 100))
                        .toFixed(2));
                tax = priceTax - price;
            }

            inputPrice.val(Number(price).toFixed(2));
            inputPriceTax.val(Number(priceTax).toFixed(2));
        },

        loadRelProductOptions : function($dropdown) {
            $
                    .ajax({
                        dataType : "html",
                        evalScripts : true,
                        url : this.getProductOptions,
                        data : ({
                            product_id : $dropdown.val()
                        }),
                        context : this,
                        success : function(data, textStatus) {
                            $dropdown.closest('.well').find('.rel-option-id')
                                    .html(data);
                            if ($dropdown.closest('.well').find(
                                    '.rel-option-id option').length > 0) {
                                $dropdown.closest('.well').find(
                                        '.rel-option-id').prop('disabled',
                                        false);
                            } else {
                                $dropdown.closest('.well').find(
                                        '.rel-option-id')
                                        .prop('disabled', true);
                            }
                        },
                        error : function(data, textStatus) {
                            alert('Error occured during getting product option values. Please try again.');
                        }
                    });
        },

        getAuctionTemplate : function(productId) {
            $
                    .ajax({
                        dataType : "html",
                        url : '/admin/products/get_auction_template/'
                                + productId,
                        type : 'GET',
                        context : this,
                        success : function(data, textStatus) {
                            bootbox.dialog({
                                message : data,
                                title : 'Szablon Allegro',
                                buttons : {
                                    success : {
                                        label : 'Zatwierdź',
                                        className : "btn-primary",
                                        callback : function() {
                                            $('.bootbox form').submit();
                                            return false;
                                        }
                                    },
                                    close : {
                                        label : 'Zamknij',
                                        className : 'btn-default',
                                        callback : function() {
                                            bootbox.hideAll();
                                            return false;
                                        }
                                    }
                                }
                            });
                            $('.bootbox .modal-dialog').addClass('modal-lg');
                            CKEDITOR
                                    .replace(
                                            'ProductDescription',
                                            {
                                                filebrowserBrowseUrl : '/admin/ckeditor/browse',
                                                filebrowserUploadUrl : '/admin/ckeditor/upload',
                                            });
                        },
                        error : function(data, textStatus) {
                            alert('Error occured during getting product preview. Please try again.');
                        }
                    });
        }
    };
})(App, jQuery);

$(document).ready(function() {
    App.Products.onDocReady();
});