var App = App || {};

(function (App, $) {
    App.Customization = {
        onDocReady: function () {
            $('.button-box .btn').on('click', function(e) {
                e.preventDefault();
                App.Customization.addLine($(this).data('type'));
                return false;          
            });
            
            $('body').on('click', '.btn-sort-down', function(e){
                e.preventDefault();
                var a = $(this).closest('.customization-container').next()[0];
                var b = $(this).closest('.customization-container')[0];
                if (a == undefined) {
                    return false;
                }
                //$(this).closest('.customization-container').next().after($(this).closest('.customization-container'));
                $(this).closest('.line-container').find('textarea').each(function(){
                    var $textarea = $(this);
                    $textarea.val(CKEDITOR.instances[$textarea.attr('id')].getData());
                    CKEDITOR.instances[this.id].destroy();
                });
                
                var aparent = a.parentNode;
                var asibling = a.nextSibling === b ? a : a.nextSibling;
                b.parentNode.insertBefore(a, b);
                aparent.insertBefore(b, asibling);
                
                $(this).closest('.line-container').find('textarea').each(function(){
                    CKEDITOR.replace(this.id, {
                        filebrowserBrowseUrl: '/admin/ckeditor/browse',
                        filebrowserUploadUrl: '/admin/ckeditor/upload',
                        height: 200
                    });
                });
                App.Customization.setSort(this);
                return false;
            });
            
            
            $('body').on('click', '.btn-sort-up', function(e){
                e.preventDefault();
                var a = $(this).closest('.customization-container').prev()[0];
                var b = $(this).closest('.customization-container')[0];
                if (a == undefined) {
                    return false;
                }
                //$(this).closest('.customization-container').next().after($(this).closest('.customization-container'));                
                $(this).closest('.line-container').find('textarea').each(function(){
                    var $textarea = $(this);
                    $textarea.val(CKEDITOR.instances[$textarea.attr('id')].getData());
                    CKEDITOR.instances[this.id].destroy();
                });
                
                var aparent = a.parentNode;
                var asibling = a.nextSibling === b ? a : a.nextSibling;
                b.parentNode.insertBefore(a, b);
                aparent.insertBefore(b, asibling);
                
                $(this).closest('.line-container').find('textarea').each(function(){
                    CKEDITOR.replace(this.id, {
                        filebrowserBrowseUrl: '/admin/ckeditor/browse',
                        filebrowserUploadUrl: '/admin/ckeditor/upload',
                        height: 200
                    });
                });
                
                App.Customization.setSort(this);
                return false;
            });
            
            
            $('body').on('click', '.btn-delete-line', function(e){
                e.preventDefault();
                
                if (confirm('Czy aby na pewno?')) {
                    $.get( $(this).attr('href'), function( data ) {
                        $(e.currentTarget).closest('.customization-container').remove();
                    });
                }
                return false;
            });
            
            $('body').on('click', '.btn-delate-template-image', function(e){
                e.preventDefault();
                bootbox.confirm("Na pewno chcesz usunąć to zdjęcie?", function(result){
                    if (result == true) {
                        $.ajax({
                            url : $('.btn-delate-template-image').attr('href'),
                            success : function(data, textStatus) {
                                $('.btn-delate-template-image').closest('.form-group').find('.form-image').removeClass('hide');
                                $('.btn-delate-template-image').closest('.form-group').find('.view-image').addClass('hide');
                            },
                            error : function(data, textStatus) {
                                alert('Bład, spróbuj ponownie później');
                            }
                        });
                    }
                });
                return false;
            });
            
            
            $('body').on('click', '.btn-save-tab', function(e){
                e.preventDefault();
                $(this).html('Zapisuję ...').attr('disabled', true);
                App.Customization.save($('.line-container'));
                $(this).html('<i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;&nbsp;Zapisz').attr('disabled', false);
                return false;
            });            
        },
        
        setSort: function(btn) {
            $(btn).closest('.tab-pane').find('.sort-order').each(function(key){
                $(this).val(key);
            });
            App.Customization.save($('.line-container'));
        },
        
        save: function(tab)
        {
            $(tab).find('textarea').each(function(){
                var $textarea = $(this);
                if (CKEDITOR.instances[$textarea.attr('id')] != undefined) {
                    $textarea.val(CKEDITOR.instances[$textarea.attr('id')].getData());                    
                }
            });
            
            $.ajax({
                url: '/admin/customizations/save/' + $('#customization-id').val() + '/' + $('input[name="data[is_active]"]:checked').length,
                method: 'POST',
                data: $(tab).find('input, textarea, select').serialize(),
                success: function (data, textStatus) {
                    alert('Zapisano');
                },
                error: function (data, textStatus) {
                    alert('Problem. Odświerz stronę.')
                }
              });
        },
        
        
        addLine: function(type) 
        {
            $.ajax({
                dataType: "html",
                url: '/admin/customizations/add_item/' + type + '/' + $('#customization-id').val(),
                context: this,
                success: function (data, textStatus) {
                    $('.line-container').append(data);
                },
                error: function (data, textStatus) {
                }
            });
        },
        
        
        setUpload: function (id) {
            var url = '/admin/customizations/image_upload/' + $(id).attr('name');
            var name = $(id).attr('name');
            var $container = $(id).closest('.form-group');
            var $progressBar = $(id).closest('.form-group').find('.progress .progress-bar');
            $(id).fileupload({
                maxChunkSize: 100000, // 10 MB
                url: url,
                add: function (e, data) {
                    var that = this;
                    var time = new Date().getTime();
                    fileName = time+data.files[0].name;      
                    
                    $progressBar.html('0%');
                    $progressBar.css('width', '0%');
                    
                    $.getJSON(url, {file: fileName}, function (result) {
                        var file = result.file;
                        data.uploadedBytes = file && file.size;
                        data.files[0]['new_name'] = fileName;
                        
                        $.blueimp.fileupload.prototype
                            .options.add.call(that, e, data);
                    });
                },
                progressall: function (e, data) {
                  var progress = parseInt( data.loaded / data.total * 100, 10);
                  $progressBar.html(progress+'%');
                  $progressBar.css('width', progress+'%');
                  
                  if (progress == 100){
                    setTimeout( function(){            
                        $progressBar.html('<strong>Przetwarzanie...</strong>').addClass('progress-bar-success');            
                    }, 100 ); 
                  }
                },
                done: function(e,data) {
                  var response = $.parseJSON(data._response.result);
                  $.each(response, function(key, value){
                      $.ajax({
                          evalScripts: true,
                          url: '/admin/customizations/save_image/' + $(id).data('id') + '/' + key,
                          type: 'POST',
                          data: {file_name: value[0].name},
                          success: function (data, textStatus) {                          
                              $container.find('.form-image').addClass('hide');
                              $container.find('.view-image').removeClass('hide');
                              $container.find('.view-image img').attr('src', data);
                          },
                          error: function (data, textStatus) {
                                alert('Skontaktuj się z administratiorem, bo jest syf');
                          }
                      });
                  });
                  
                }  
              });
        },
        
    };
})(App, jQuery);

$(document).ready(function () {
  App.Customization.onDocReady();
});